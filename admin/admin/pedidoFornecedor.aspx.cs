﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using SpreadsheetLight;
public partial class admin_pedidoFornecedor : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(txtDataFinal.Text))
        {
            string filtroEntregue = "-1";
            if (rdbNao.Checked) filtroEntregue = "0";

            sql.SelectParameters.Clear();
            sql.SelectParameters.Add("entregue", filtroEntregue);


            fillGrid(false);
        }
        else
        {
            string filtroEntregue = "-1";
            if (rdbNao.Checked) filtroEntregue = "0";

            sql2.SelectParameters.Clear();
            sql2.SelectParameters.Add("entregue", filtroEntregue);
            sql2.SelectParameters.Add("dataInicial", txtDataInicial.Text);
            sql2.SelectParameters.Add("dataFinal", txtDataFinal.Text);

            fillGrid2(false);
        }

        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
    }


    private void fillGrid(bool rebind)
    {
        grd.DataSource = sql;
        grd.DataBind();

        gridPendentes.DataSource = sqlPendentes;
        gridPendentes.DataBind();
    }

    private void fillGrid2(bool rebind)
    {
        grd.DataSource = sql2;
        grd.DataBind();

        gridPendentes.DataSource = sqlPendentes;
        gridPendentes.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        int tentativas = 0;
        int tentativasMaximas = 3;
        int tempo = 0;
        bool sucesso = false;
        string erro = "";
        while (tentativas <= tentativasMaximas && sucesso == false)
        {
            try
            {
                if (e.RowType != GridViewRowType.Data)
                {
                    return;
                }
                dynamic dataRow = grd.GetRow(e.VisibleIndex);


                int idPedidoFornecedor = 0;
                int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idPedidoFornecedor" }).ToString(), out idPedidoFornecedor);
                ASPxCheckBox chkSelecionar = (ASPxCheckBox)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["selecionar"] as GridViewDataColumn, "chkSelecionar");
                //Literal litEntregue = (Literal)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["entregue"] as GridViewDataColumn, "litEntregue");
                LinkButton btnExportar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["exportar"] as GridViewDataColumn, "btnExportar");
                LinkButton btnReenviar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["reenviar"] as GridViewDataColumn, "btnReenviar");
                //LinkButton btnEditar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["editar"] as GridViewDataColumn, "btnEditar");
                //LinkButton btnVer = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["ver"] as GridViewDataColumn, "btnVer");
                //LinkButton btnConfirmar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["confirmado"] as GridViewDataColumn, "btnConfirmar");
                Label lblConfirmado = (Label)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["confirmado"] as GridViewDataColumn, "lblConfirmado");

                if (idPedidoFornecedor == 0)
                {
                    int i = 0;
                    while ((i < e.Row.Cells.Count))
                    {
                        e.Row.Cells[i].Style.Add("background", "#FFFFFF");
                        i++;
                    }
                    //litEntregue.Text = "-";
                    if (btnExportar != null) btnExportar.Visible = false;
                    //btnEditar.Visible = false;
                    //btnVer.Visible = false;
                }
                else
                {
                    chkSelecionar.Enabled = false;
                    //var pedidoDc = new dbCommerceDataContext();
                    //var pedido = (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();


                    bool confirmado = Convert.ToBoolean(dataRow["confirmado"].ToString());

                    if (confirmado)
                    {
                        //btnConfirmar.Visible = false;
                        lblConfirmado.Text = "Visualizado pelo Fornecedor";
                        lblConfirmado.Style.Add("color", "#999999");
                    }
                    else
                    {
                        //btnConfirmar.Visible = true;
                        lblConfirmado.Text = "Aguardando visualização do Fornecedor";
                    }


                    //var itensDoPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c);
                    int totalItens = Convert.ToInt32(dataRow["qtdProdutos"].ToString());
                    int totalEntregues = Convert.ToInt32(dataRow["entregues"].ToString());
                    int faltaEntregar = Convert.ToInt32(dataRow["faltaEntregar"].ToString());
                    DateTime dataLimite = Convert.ToDateTime(dataRow["dataLimite"].ToString());
                    string dp = dataRow["dataPrevista"].ToString();
                    int fornecedorId = Convert.ToInt32(dataRow["fornecedorId"].ToString());

                    if (dp != "" && fornecedorId == 112)
                    {
                        DateTime dataPrevista = Convert.ToDateTime(dataRow["dataPrevista"].ToString());
                        dataLimite = dataPrevista;
                    }

                    if (totalItens > totalEntregues && totalEntregues == 0 && (dataLimite.Date == DateTime.Now.Date))
                    {
                        int i = 0;
                        while ((i < e.Row.Cells.Count))
                        {
                            e.Row.Cells[i].Style.Add("background", "#FFFF00");
                            i++;
                        }
                    }
                    if (totalItens > totalEntregues && totalEntregues == 0 && (dataLimite.Date < DateTime.Now.Date))
                    {
                        int i = 0;
                        while ((i < e.Row.Cells.Count))
                        {
                            e.Row.Cells[i].Style.Add("background", "#FF151C");
                            i++;
                        }
                    }
                    else if (totalItens == faltaEntregar)
                    {
                        int i = 0;
                        while ((i < e.Row.Cells.Count))
                        {
                            e.Row.Cells[i].Style.Add("background", "#FFFFBB");
                            i++;
                        }
                        //litEntregue.Text = "Não";
                    }
                    else if (totalItens > totalEntregues)
                    {
                        int i = 0;

                        if (dp != "" && fornecedorId == 112 && (Convert.ToDateTime(dp).Date < DateTime.Now.Date))
                        {
                            while ((i < e.Row.Cells.Count))
                            {
                                e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                                i++;
                            }
                        }
                        else
                        {
                            while ((i < e.Row.Cells.Count))
                            {
                                e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                                i++;
                            }
                        }
                        //litEntregue.Text = "Parcialmente";
                    }
                    else
                    {
                        int i = 0;
                        while ((i < e.Row.Cells.Count))
                        {
                            e.Row.Cells[i].Style.Add("background", "#D2E9FF");
                            i++;
                        }
                        //litEntregue.Text = "Sim";
                    }
                }

                //if (!String.IsNullOrEmpty(grd.GetRowValues(e.VisibleIndex, new string[] { "dataUltimaEntrega" }).ToString()))
                //    if (Convert.ToDateTime(grd.GetRowValues(e.VisibleIndex, new string[] { "dataUltimaEntrega" })).Month > Convert.ToDateTime(grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimite" })).Month)
                //    {
                //        int i = 0;
                //        while ((i < e.Row.Cells.Count))
                //        {
                //            e.Row.Cells[i].Style.Add("background", "#F393E1");
                //            i++;
                //        }

                //    }

                if (!String.IsNullOrEmpty(grd.GetRowValues(e.VisibleIndex, new string[] { "Fechamentos" }).ToString()))
                    if (Convert.ToInt32(grd.GetRowValues(e.VisibleIndex, new string[] { "Fechamentos" }).ToString()) > 1)
                    {
                        int i = 0;
                        while ((i < e.Row.Cells.Count))
                        {
                            e.Row.Cells[i].Style.Add("background", "#F393E1");
                            i++;
                        }
                    }



                sucesso = true;
            }
            catch (Exception ex)
            {
                tentativas++;
                Thread.Sleep(new TimeSpan(0, 0, tempo));
                tempo++;
            }
        }

    }

    protected void btnGerarFaturas_OnClick(object sender, EventArgs e)
    {
        int tentativas = 0;
        int tentativasMaximas = 3;
        int tempo = 0;
        bool sucesso = false;
        string erro = "";
        while (tentativas <= tentativasMaximas && sucesso == false)
        {
            try
            {
                var listaFornecedoresNaoPedir = new List<int>();
                listaFornecedoresNaoPedir.Add(49);
                listaFornecedoresNaoPedir.Add(53);
                listaFornecedoresNaoPedir.Add(72);
                listaFornecedoresNaoPedir.Add(66);

                int inicio = 0;
                int fim = 0;

                if (grd.PageIndex == 0)
                {
                    inicio = 0;
                    fim = inicio + grd.GetCurrentPageRowValues("selecionar").Count;
                }
                else
                {
                    inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                    fim += inicio + grd.GetCurrentPageRowValues("selecionar").Count;
                }

                var listaFornecedores = new List<int>();
                for (int i = inicio; i < fim; i++)
                {
                    ASPxCheckBox chkSelecionar = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["selecionar"] as GridViewDataColumn, "chkSelecionar");
                    int fornecedorId = (int)grd.GetRowValues(i, new string[] { "fornecedorId" });
                    //DateTime data = (DateTime)grd.GetRowValues(i, new string[] { "data" });
                    if (chkSelecionar.Checked)
                    {
                        listaFornecedores.Add(fornecedorId);
                    }
                }

                if (listaFornecedores.Count == 0)
                {
                    Response.Write("<script>alert('Para gerar faturas, selecione um fornecedor/data.');</script>");
                }
                else
                {
                    HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                    usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                    if (usuarioLogadoId == null)
                    {
                        return;
                    }

                    var pedidosDc = new dbCommerceDataContext();
                    var pedidos = (from c in pedidosDc.tbPedidos where c.dataConfirmacaoPagamento != null && c.statusDoPedido == 3 select c);
                    foreach (var listaFornecedor in listaFornecedores)
                    {
                        int fornecedorId = listaFornecedor;
                        var fornecedorDc = new dbCommerceDataContext();
                        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();

                        var pedidoFornecedorCheck = (from c in pedidosDc.tbPedidoFornecedors where c.idFornecedor == fornecedorId && c.pendente == true select c).FirstOrDefault();
                        var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
                        pedidoFornecedor.data = DateTime.Now;
                        pedidoFornecedor.idFornecedor = fornecedorId;
                        pedidoFornecedor.confirmado = false;
                        pedidoFornecedor.avulso = false;
                        pedidoFornecedor.pendente = true;
                        if (fornecedor.fornecedorPrazoPedidos > 0)
                        {
                            int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                            if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                            {
                                int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                                diasPrazo = diasPrazoFinal;
                            }
                            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
                        }
                        else
                        {
                            pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
                        }

                        pedidoFornecedor.usuario = "";
                        if (usuarioLogadoId != null)
                        {
                            pedidoFornecedor.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
                        }
                        if (!listaFornecedoresNaoPedir.Any(x => x == fornecedorId))
                        {
                            if (pedidoFornecedorCheck == null) pedidosDc.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
                            pedidosDc.SubmitChanges();
                        }

                        int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;
                        bool estoqueConferido = fornecedor.estoqueConferido == null ? true : (bool)fornecedor.estoqueConferido;

                        foreach (var pedido in pedidos)
                        {
                            int totalItens = 0;
                            var itensPedidoDc = new dbCommerceDataContext();
                            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
                            foreach (var itemPedido in itensPedido)
                            {
                                bool cancelado = itemPedido.cancelado == null ? false : (bool)itemPedido.cancelado;
                                if (!cancelado)
                                {
                                    for (int i = 1; i <= Convert.ToInt32(itemPedido.itemQuantidade); i++)
                                    {
                                        var produtosDc = new dbCommerceDataContext();
                                        var produtosFilho =
                                            (from c in produtosDc.tbProdutoRelacionados
                                             where c.idProdutoPai == itemPedido.produtoId
                                             select c);
                                        int totalProdutosFilho = produtosFilho.Count();
                                        if (totalProdutosFilho > 0)
                                        {
                                            bool possuiEstoqueTotal = true;
                                            foreach (var produtoRelacionado in produtosFilho)
                                            {
                                                if (produtoRelacionado.tbProduto.produtoFornecedor == fornecedorId)
                                                {
                                                    var estoqueDoItem =
                                                        produtosDc.admin_produtoEmEstoque(produtoRelacionado.idProdutoFilho)
                                                            .FirstOrDefault();
                                                    int estoqueTotalDoItem = estoqueDoItem == null
                                                        ? 0
                                                        : Convert.ToInt32(estoqueDoItem.estoqueLivre);
                                                    if (estoqueTotalDoItem < 1 |
                                                        fornecedor.estoqueConferido == false | fornecedor.estoqueConferido == null)
                                                    {
                                                        possuiEstoqueTotal = false;
                                                    }
                                                }
                                            }
                                            foreach (var produtoRelacionado in produtosFilho)
                                            {
                                                if (produtoRelacionado.tbProduto.produtoFornecedor == fornecedorId)
                                                {
                                                    var checagemProdutoFilho = (from c in pedidosDc.tbPedidoFornecedorItems
                                                                                where
                                                                                    c.idProduto == produtoRelacionado.idProdutoFilho &&
                                                                                    c.idItemPedido == itemPedido.itemPedidoId
                                                                                select c).Any();
                                                    var checagemProdutoFilhoEstoque = (from c in produtosDc.tbProdutoReservaEstoques
                                                                                       where
                                                                                           c.idProduto == produtoRelacionado.idProdutoFilho &&
                                                                                           c.idItemPedido == itemPedido.itemPedidoId
                                                                                       select c).Any();

                                                    if (!checagemProdutoFilho && !checagemProdutoFilhoEstoque)
                                                    {
                                                        var estoqueDoItem =
                                                            produtosDc.admin_produtoEmEstoque(produtoRelacionado.idProdutoFilho)
                                                                .FirstOrDefault();
                                                        int estoqueTotalDoItem = estoqueDoItem == null
                                                            ? 0
                                                            : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                                                        if (estoqueTotalDoItem >= 1 &&
                                                            estoqueConferido &&
                                                            (possuiEstoqueTotal |
                                                             (fornecedor.fornecedorId != 21 && fornecedorId != 41)))
                                                        {
                                                            var reserva = new tbProdutoReservaEstoque();
                                                            reserva.idPedido = pedido.pedidoId;
                                                            reserva.idProduto = produtoRelacionado.idProdutoFilho;
                                                            reserva.dataHora = DateTime.Now;
                                                            reserva.idItemPedido = itemPedido.itemPedidoId;
                                                            reserva.quantidade = 1;
                                                            produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                                                            produtosDc.SubmitChanges();
                                                        }
                                                        else
                                                        {
                                                            var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                                                            pedidoFornecedorItem.custo = produtoRelacionado.desconto == 100
                                                                ? 0
                                                                : (decimal)produtoRelacionado.tbProduto.produtoPrecoDeCusto;
                                                            pedidoFornecedorItem.brinde = produtoRelacionado.desconto == 100
                                                                ? true
                                                                : false;
                                                            pedidoFornecedorItem.diferenca = 0;
                                                            pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
                                                            pedidoFornecedorItem.idProduto = produtoRelacionado.idProdutoFilho;
                                                            pedidoFornecedorItem.entregue = false;
                                                            pedidoFornecedorItem.idPedido = pedido.pedidoId;
                                                            pedidoFornecedorItem.idItemPedido = itemPedido.itemPedidoId;
                                                            pedidoFornecedorItem.quantidade = 1;

                                                            if (!listaFornecedoresNaoPedir.Any(x => x == fornecedorId))
                                                            {
                                                                pedidosDc.tbPedidoFornecedorItems.InsertOnSubmit(
                                                                    pedidoFornecedorItem);
                                                                pedidosDc.SubmitChanges();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            totalItens++;
                                            var produto =
                                                (from c in produtosDc.tbProdutos where c.produtoId == itemPedido.produtoId select c)
                                                    .First();

                                            if (produto.produtoFornecedor == fornecedorId)
                                            {
                                                var checagemProduto = (from c in pedidosDc.tbPedidoFornecedorItems
                                                                       where
                                                                           c.idProduto == produto.produtoId &&
                                                                           c.idItemPedido == itemPedido.itemPedidoId
                                                                       select c).Count();
                                                var checagemProdutoFilhoEstoque = (from c in produtosDc.tbProdutoReservaEstoques
                                                                                   where
                                                                                       c.idProduto == produto.produtoId &&
                                                                                       c.idItemPedido == itemPedido.itemPedidoId
                                                                                   select c).Any();

                                                if (checagemProduto < Convert.ToInt32(itemPedido.itemQuantidade) && !checagemProdutoFilhoEstoque)
                                                {
                                                    var estoqueDoItem =
                                                        produtosDc.admin_produtoEmEstoque(produto.produtoId).FirstOrDefault();
                                                    int estoqueTotalDoItem = estoqueDoItem == null
                                                        ? 0
                                                        : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                                                    if (estoqueTotalDoItem >= 1 &&
                                                        estoqueConferido)
                                                    {
                                                        var reserva = new tbProdutoReservaEstoque();
                                                        reserva.idPedido = pedido.pedidoId;
                                                        reserva.idProduto = produto.produtoId;
                                                        reserva.dataHora = DateTime.Now;
                                                        reserva.idItemPedido = itemPedido.itemPedidoId;
                                                        reserva.quantidade = 1;
                                                        produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                                                        produtosDc.SubmitChanges();
                                                    }
                                                    else
                                                    {
                                                        var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                                                        pedidoFornecedorItem.custo = (decimal)produto.produtoPrecoDeCusto;
                                                        pedidoFornecedorItem.brinde = false;
                                                        pedidoFornecedorItem.diferenca = 0;
                                                        pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
                                                        pedidoFornecedorItem.idProduto = produto.produtoId;
                                                        pedidoFornecedorItem.entregue = false;
                                                        pedidoFornecedorItem.idPedido = pedido.pedidoId;
                                                        pedidoFornecedorItem.idItemPedido = itemPedido.itemPedidoId;
                                                        pedidoFornecedorItem.quantidade = 1;
                                                        if (!listaFornecedoresNaoPedir.Any(x => x == fornecedorId))
                                                        {
                                                            pedidosDc.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
                                                            pedidosDc.SubmitChanges();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        itemPedido.prazoDeFabricacao = pedidoFornecedor.dataLimite;
                                    }
                                }
                            }
                            itensPedidoDc.SubmitChanges();
                            /*var itensPedidosPorPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedido == pedido.pedidoId select c).Count();
                            if (totalItens >= itensPedidosPorPedido)
                            {

                                rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId); var clientesDc = new dbCommerceDataContext();
                                var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                                interacaoInclui("Separação de Estoque", "True", pedido.pedidoId);
                                rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
                               // rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", "andre@bark.com.br");
                            }*/
                        }
                        pedidoFornecedor.pendente = false;
                        pedidosDc.SubmitChanges();
                        try
                        {
                            if (!listaFornecedoresNaoPedir.Any(x => x == fornecedorId))
                            {
                                //enviaExcelEmailFornecedor(idPedidoFornecedor);
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    foreach (var pedido in pedidos)
                    {
                        int totalItens = 0;
                        var itensPedidoDc = new dbCommerceDataContext();
                        var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
                        foreach (var itemPedido in itensPedido)
                        {
                            bool cancelado = itemPedido.cancelado == null ? false : true;
                            if (!cancelado)
                            {
                                for (int i = 1; i <= Convert.ToInt32(itemPedido.itemQuantidade); i++)
                                {
                                    var produtosDc = new dbCommerceDataContext();
                                    var produtosFilho =
                                        (from c in produtosDc.tbProdutoRelacionados
                                         where c.idProdutoPai == itemPedido.produtoId
                                         select c);
                                    if (produtosFilho.Any())
                                    {
                                        foreach (var produtoRelacionado in produtosFilho)
                                        {
                                            totalItens++;
                                        }
                                    }
                                    else
                                    {
                                        totalItens++;
                                    }
                                }
                            }
                        }

                        var reserva = new dbCommerceDataContext();

                        var itensPedidosPorPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedido == pedido.pedidoId && (c.tbPedidoFornecedor.avulso == null | c.tbPedidoFornecedor.avulso == false) select c).Count();
                        var itensPedidosReservados = (from c in reserva.tbProdutoReservaEstoques where c.idPedido == pedido.pedidoId select c).Count();
                        if ((totalItens <= (itensPedidosPorPedido + itensPedidosReservados)) && totalItens > 0)
                        {
                            try
                            {
                                rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                                var clientesDc = new dbCommerceDataContext();
                                var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                                interacaoInclui("Separação de Estoque", "True", pedido.pedidoId);
                                rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
                                // rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", "andre@bark.com.br");
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }
                    //hiddenExportarId.Value = idPedidoFornecedor.ToString();
                    //timer1.Enabled = true;
                    fillGrid(true);
                    //Response.Write("<script>alert('Para gerar faturas, selecione um fornecedor por vez');</script>");
                }
                sucesso = true;
            }
            catch (Exception ex)
            {
                tentativas++;
                Thread.Sleep(new TimeSpan(0, 0, tempo));
                tempo++;
            }
        }
    }

    //private void exportaExcelPedidoFornecedor(int pedidoFornecedorId)
    //{
    //    var fornecedorDc = new dbCommerceDataContext();


    //    var pedidosDc = new dbCommerceDataContext();
    //    var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

    //    Workbook book = new Workbook();
    //    WorksheetStyle negrito = book.Styles.Add("negrito");
    //    WorksheetStyle normal = book.Styles.Add("normal");
    //    negrito.Font.Bold = true;
    //    normal.Font.Bold = false;
    //    Worksheet sheet = book.Worksheets.Add("Pedido");



    //    WorksheetRow cabecalho = sheet.Table.Rows.Add();
    //    cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
    //    cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
    //    cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

    //    decimal totalDoPedido = 0;
    //    var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
    //    var produtosIds = new List<fornecedorCusto>();

    //    WorksheetRow separacao = sheet.Table.Rows.Add();
    //    WorksheetRow titulos = sheet.Table.Rows.Add();

    //    titulos.Cells.Add("ID do Produto", DataType.String, "normal");
    //    titulos.Cells.Add("Complemento", DataType.String, "normal");
    //    titulos.Cells.Add("Nome", DataType.String, "normal");
    //    titulos.Cells.Add("Quantidade", DataType.String, "normal");
    //    titulos.Cells.Add("Valor", DataType.String, "normal");
    //    titulos.Cells.Add("Total", DataType.String, "normal");
    //    titulos.Cells.Add("Diferença", DataType.String, "normal");


    //    foreach (var fornecedorItem in pedidosFornecedor)
    //    {
    //        bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
    //        if (!jaAdicionado)
    //        {
    //            int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
    //            var produtoDc = new dbCommerceDataContext();
    //            var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

    //            /*WorksheetRow linha = sheet.Table.Rows.Add();

    //            linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
    //            linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
    //            linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
    //            linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
    //            linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
    //            linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");*/

    //            totalDoPedido += (fornecedorItem.custo * quantidade);
    //            var produtoId = new fornecedorCusto();
    //            produtoId.produtoId = fornecedorItem.idProduto;
    //            produtoId.custo = fornecedorItem.custo;
    //            produtoId.produtoNome = produto.produtoNome;
    //            produtoId.diferenca = fornecedorItem.diferenca;
    //            produtosIds.Add(produtoId);
    //        }
    //    }

    //    foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
    //    {
    //        int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
    //        var produtoDc = new dbCommerceDataContext();
    //        var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

    //        WorksheetRow linha = sheet.Table.Rows.Add();

    //        linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
    //        linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
    //        linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
    //        linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
    //        linha.Cells.Add(produtosId.custo.ToString("C"), DataType.String, "normal");
    //        linha.Cells.Add((produtosId.custo * quantidade).ToString("C"), DataType.String, "normal");
    //        linha.Cells.Add((produtosId.diferenca).ToString("C"), DataType.String, "normal");
    //    }
    //    WorksheetRow total = sheet.Table.Rows.Add();
    //    WorksheetRow titulos2 = sheet.Table.Rows.Add();

    //    titulos2.Cells.Add("", DataType.String, "normal");
    //    titulos2.Cells.Add("", DataType.String, "normal");
    //    titulos2.Cells.Add("", DataType.String, "normal");
    //    titulos2.Cells.Add("", DataType.String, "normal");
    //    titulos2.Cells.Add("", DataType.String, "normal");
    //    titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");

    //    using (MemoryStream memoryStream = new MemoryStream())
    //    {
    //        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();
    //        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";


    //        book.Save(memoryStream);
    //        Response.Clear();
    //        Response.ContentType = "application/ms-excel";
    //        Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
    //        Response.BinaryWrite(memoryStream.ToArray());
    //        Response.Flush();
    //        Response.Close();
    //        Response.End();
    //    }
    //}

    private void exportaExcelPedidoFornecedor(int pedidoFornecedorId)
    {
        var fornecedorDc = new dbCommerceDataContext();

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xlsx";

        SLDocument sl = new SLDocument();
        sl.AddWorksheet("Pedido");
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);


        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);

        sl.SetCellValue("A1", "Pedido: " + pedidoFornecedorId);
        sl.SetCellValue("B1", "Data de Solicitação: " + pedido.data.ToShortDateString());
        sl.SetCellValue("C1", "Data limite de entrega: " + pedido.dataLimite.ToShortDateString());

        sl.SetCellValue("A3", "ID do Produto");
        sl.SetCellValue("B3", "Complemento");
        sl.SetCellValue("C3", "Nome");
        sl.SetCellValue("D3", "Quantidade");
        sl.SetCellValue("E3", "Valor");
        sl.SetCellValue("F3", "Total");
        sl.SetCellValue("G3", "Diferença");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtoId.produtoNome = produto.produtoNome;
                produtoId.diferenca = fornecedorItem.diferenca;
                produtosIds.Add(produtoId);
            }
        }

        int linha = 4;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            sl.SetCellValue(linha, 1, produto.produtoIdDaEmpresa.ToString());
            sl.SetCellValue(linha, 2, produto.complementoIdDaEmpresa.ToString());
            sl.SetCellValue(linha, 3, produto.produtoNome);
            sl.SetCellValue(linha, 4, quantidade.ToString());
            sl.SetCellValue(linha, 5, produtosId.custo.ToString("C"));
            sl.SetCellValue(linha, 6, (produtosId.custo * quantidade).ToString("C"));
            sl.SetCellValue(linha, 7, (produtosId.diferenca).ToString("C"));
            linha++;

        }
        sl.SetRowStyle(linha + 1, style1);
        sl.SetCellValue(linha + 1, 6, "Total do Pedido: " + totalDoPedido.ToString("C"));

        sl.SaveAs(Response.OutputStream);
        Response.End();

    }
    private void enviaExcelEmailFornecedor(int pedidoFornecedorId)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");



        WorksheetRow cabecalho = sheet.Table.Rows.Add();
        cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
        cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
        cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");


        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtosIds.Add(produtoId);
            }
        }

        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");


        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();

        string fornecedorEmail = fornecedor.fornecedorEmail;
        if (!string.IsNullOrEmpty(fornecedorEmail)) fornecedorEmail += ",";
        fornecedorEmail += "compras@graodegente.com.br";

        string assunto = DateTime.Now.ToShortDateString() + " Pedido " + fornecedor.fornecedorNome + " " + pedido.idPedidoFornecedor + " entrega até " + pedido.dataLimite.ToShortDateString();
        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";
        string mensagem = "Segue em anexo o pedido, favor confirmar recebimento e enviar o romaneio no email.";

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress("compras@graodegente.com.br");
        var emailsPara = fornecedorEmail.Split(',');
        foreach (var emailPara in emailsPara)
        {
            mailMessage.To.Add(new MailAddress(emailPara));
        }
        mailMessage.ReplyTo = new MailAddress("compras@graodegente.com.br");
        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);

            // Create attachment
            ContentType contentType = new ContentType();
            contentType.MediaType = "application/ms-excel";
            contentType.Name = nomeArquivo;
            contentType.CharSet = "UTF-8";
            Attachment attachment = new Attachment(memoryStream, contentType);

            // Add the attachment
            mailMessage.Attachments.Add(attachment);

            // Send Mail via SmtpClient
            smtpClient.Send(mailMessage);
        }
    }


    private class fornecedorCusto
    {
        public decimal custo { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public decimal diferenca { get; set; }
    }

    //protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    //{
    //    fillGrid(true);
    //}

    protected void timer1_OnTick(object sender, EventArgs e)
    {
        if (Session[hiddenExportarId.Value] != "1")
        {
            Session[hiddenExportarId.Value] = "1";
            int id = Convert.ToInt32(hiddenExportarId.Value);
            exportaExcelPedidoFornecedor(id);
        }
        else
        {
            timer1.Enabled = false;
        }
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        exportaExcelPedidoFornecedor(Convert.ToInt32(e.CommandArgument));
    }
    protected void btnReenviar_OnCommand(object sender, CommandEventArgs e)
    {
        enviaExcelEmailFornecedor(Convert.ToInt32(e.CommandArgument));
        Response.Write("<script>alert('Pedido reenviado ao fornecedor com sucesso.');</script>");
    }

    protected void btnEditar_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("pedidoFornecedorEditar.aspx?idPedidoFornecedor=" + e.CommandArgument);
    }

    protected void btnVer_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("pedidoFornecedorVisualizar.aspx?idPedidoFornecedor=" + e.CommandArgument);
    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void btnConfirmar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido =
            (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
        pedido.confirmado = true;

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }
        var interacaoObj = new tbPedidoFornecedorInteracao();
        interacaoObj.data = DateTime.Now;
        interacaoObj.idPedidoFornecedor = idPedidoFornecedor;
        interacaoObj.interacao = "Pedido confirmado com o Fornecedor";
        interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
        data.SubmitChanges();

        fillGrid(true);
    }

    protected void btnGerarFaturas2_OnClick(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }

        var pedidosDc = new dbCommerceDataContext();
        var pedidos = (from c in pedidosDc.tbPedidos where c.dataConfirmacaoPagamento != null && c.statusDoPedido == 3 orderby c.dataConfirmacaoPagamento select c);

        foreach (var pedido in pedidos)
        {
            int totalItens = 0;
            var itensPedidoDc = new dbCommerceDataContext();
            var itensPedido = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
            foreach (var itemPedido in itensPedido)
            {
                var produtosDc = new dbCommerceDataContext();
                var produtosFilho = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == itemPedido.produtoId select c);
                if (produtosFilho.Any())
                {
                    foreach (var produtoRelacionado in produtosFilho)
                    {
                        totalItens++;
                    }
                }
                else
                {
                    totalItens++;
                }
            }

            var itensPedidosPorPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedido == pedido.pedidoId && (c.tbPedidoFornecedor.avulso == null | c.tbPedidoFornecedor.avulso == false) select c).Count();
            if (totalItens <= itensPedidosPorPedido)
            {
                rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                var clientesDc = new dbCommerceDataContext();
                var cliente = (from c in clientesDc.tbClientes where c.clienteId == pedido.clienteId select c).First();
                interacaoInclui("Separação de Estoque", "True", pedido.pedidoId);
                rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", cliente.clienteEmail);
                // rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Separação de Estoque", "andre@bark.com.br");
            }
        }
        //hiddenExportarId.Value = idPedidoFornecedor.ToString();
        //timer1.Enabled = true;
        fillGrid(true);
        //Response.Write("<script>alert('Para gerar faturas, selecione um fornecedor por vez');</script>");

    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        //if (e.Column.FieldName == "diferenca")
        //{
        //    decimal discount = Convert.ToDecimal(e.GetListSourceFieldValue("Discount"));

        //    var idPedido = e.GetListSourceFieldValue("idPedidoFornecedor");
        //    if (!string.IsNullOrEmpty(idPedido.ToString().Trim()))
        //    {
        //        int idPedidoFornecedor = Convert.ToInt32(e.GetListSourceFieldValue("idPedidoFornecedor"));
        //        var data = new dbCommerceDataContext();
        //        var totalItens = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor && c.entregue select new { diferenca = c.diferenca > 0 ? (c.diferenca - c.custo) * c.quantidade : 0 });
        //        e.Value = totalItens.Count() > 0 ? totalItens.Sum(x => x.diferenca) : 0;
        //    }
        //}
        if (e.Column.FieldName == "entregue")
        {
            var entregue = Convert.ToInt32(e.GetListSourceFieldValue("entregues"));
            var falta = Convert.ToInt32(e.GetListSourceFieldValue("faltaEntregar"));

            if (falta == 0)
            {
                e.Value = "Sim";
            }
            else if (falta > 0 && entregue > 0)
            {
                e.Value = "Parcialmente";
            }
            else
            {
                e.Value = "Não";
            }
        }
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataLimite")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataEntrega")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataVencimento")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataPagamento")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataUltimaEntrega")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'data'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataLimite")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataLimite'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataEntrega")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataEntrega'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataVencimento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataVencimento'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataPagamento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataPagamento'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataUltimaEntrega")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;

            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataUltimaEntrega'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "data")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["data"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("data") >= dateFrom) &
                             (new OperandProperty("data") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["data"] != null)
                    e.Value = Session["data"].ToString();
            }
        }
        if (e.Column.FieldName == "dataLimite")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataLimite"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataLimite") >= dateFrom) &
                             (new OperandProperty("dataLimite") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["dataLimite"] != null) e.Value = Session["dataLimite"].ToString();
            }
        }
        if (e.Column.FieldName == "dataEntrega")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataEntrega"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataEntrega") >= dateFrom) &
                             (new OperandProperty("dataEntrega") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["dataEntrega"] != null) e.Value = Session["dataEntrega"].ToString();
            }
        }
        if (e.Column.FieldName == "dataVencimento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataVencimento"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataVencimento") >= dateFrom) &
                             (new OperandProperty("dataVencimento") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["dataVencimento"] != null) e.Value = Session["dataVencimento"].ToString();
            }
        }
        if (e.Column.FieldName == "dataPagamento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataPagamento"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataPagamento") >= dateFrom) &
                             (new OperandProperty("dataPagamento") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["dataPagamento"] != null) e.Value = Session["dataPagamento"].ToString();
            }
        }
        if (e.Column.FieldName == "dataUltimaEntrega")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataUltimaEntrega"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataUltimaEntrega") >= dateFrom) &
                             (new OperandProperty("dataUltimaEntrega") <= dateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }
            else
            {
                if (Session["dataUltimaEntrega"] != null) e.Value = Session["dataUltimaEntrega"].ToString();
            }
        }
        if (e.Column.FieldName == "idPedidoFornecedor")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["idPedidoFornecedor"] = e.Value;
                var ids = e.Value.Split(',').Select(p => p.Trim()).ToList();
                CriteriaOperator teste = new InOperator("idPedidoFornecedor", ids);
                e.Criteria = teste;
            }
            else
            {
                if (Session["idPedidoFornecedor"] != null) e.Value = Session["idPedidoFornecedor"].ToString();
            }
        }
    }

    protected void grd_OnDataBound(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("idPedidoFornecedor").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("idPedidoFornecedor").Count;
        }

        var listaPedidosFornecedor = new List<int>();
        for (int i = inicio; i < fim; i++)
        {
            var idPedidoFornecedorCheck = grd.GetRowValues(i, new string[] { "idPedidoFornecedor" });
            if (!string.IsNullOrEmpty(idPedidoFornecedorCheck.ToString().Trim()))
            {
                int idPedidoFornecedor = Convert.ToInt32(idPedidoFornecedorCheck);
                listaPedidosFornecedor.Add(idPedidoFornecedor);
            }
        }


        /* var pedidosDc = new dbCommerceDataContext();
         var produtosDc = new dbCommerceDataContext();
         List<int> pedidos = new List<int>();

         var sendoEmbalados = pedidosDc.admin_pedidosCompletosSeparacaoEstoque();
         litTotalPedidosSendoEmbalados.Text = sendoEmbalados.Count().ToString();
         pedidos.AddRange((from c in pedidosDc.tbPedidoFornecedorItems
             where listaPedidosFornecedor.Contains(c.idPedidoFornecedor) && c.entregue != true
                            select (int)c.idPedido).Distinct().ToList());
         var pedidosLista =
             (from c in pedidosDc.tbPedidos
                 where pedidos.Contains(c.pedidoId) && c.statusDoPedido == 11
                 select c.pedidoId).ToList();

         int totalPedidoCompletos = 0;
         foreach (var pedido in pedidosLista)
         {
             int totalItensDoPedido = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedido == pedido select c).Count();
             int totalItensSeraoEntregues = (from c in pedidosDc.tbPedidoFornecedorItems
                 where (listaPedidosFornecedor.Contains(c.idPedidoFornecedor) | c.entregue == true) && c.idPedido == pedido
                 select c).Count();
             if (totalItensSeraoEntregues >= totalItensDoPedido) totalPedidoCompletos++;
         }
         litTotalPedidosEmbalados.Text = totalPedidoCompletos.ToString();*/
    }

    protected void rdb_OnCheckedChanged(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(txtDataFinal.Text))
        {
            fillGrid(true);
        }
        else
        {
            fillGrid2(true);
        }
    }

    protected void btnNovo_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("pedidoFornecedorAvulsoNovo.aspx");
    }


    protected void gridPendentes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        try
        {

            int idPedidoFornecedor = 0;
            int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idPedidoFornecedor" }).ToString(), out idPedidoFornecedor);
            LinkButton btnVer = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["ver"] as GridViewDataColumn, "btnVer");
        }
        catch (Exception)
        {

        }
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

    }



    protected void btnFechar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = 0;
        int.TryParse(e.CommandArgument.ToString(), out idPedidoFornecedor);
        var pedidosDc = new dbCommerceDataContext();
        var pedido =
            (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c)
                .FirstOrDefault();
        if (pedido != null)
        {
            pedido.pendente = false;
            pedidosDc.SubmitChanges();
            Response.Redirect("pedidoFornecedorAvulso.aspx");
        }
    }


    protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    {
        string filtroEntregue = "-1";
        if (rdbNao.Checked) filtroEntregue = "0";

        sql2.SelectParameters.Clear();
        sql2.SelectParameters.Add("entregue", filtroEntregue);
        sql2.SelectParameters.Add("dataInicial", txtDataInicial.Text);
        sql2.SelectParameters.Add("dataFinal", txtDataFinal.Text);

        fillGrid2(false);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}