﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ICSharpCode.SharpZipLib.Zip;

public partial class admin_gerarNotasPorData : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int mes = System.DateTime.Now.Month;
            ddlAno.Items.Add(new ListItem(DateTime.Now.AddYears(-1).Year.ToString(), DateTime.Now.AddYears(-1).Year.ToString()));
            ddlAno.Items.Add(new ListItem(DateTime.Now.Year.ToString(), DateTime.Now.Year.ToString()));
            ddlAno.SelectedValue = DateTime.Now.Year.ToString();
            ddlMes.SelectedValue = mes.ToString();
            fillValores();
        }
    }
    private void fillValores()
    {
        panelSelecionarData.Visible = true;
        pnGerarNota.Visible = false;

        int mes = Convert.ToInt32(ddlMes.SelectedValue);
        int ano = Convert.ToInt32(ddlAno.SelectedValue);

        var dcPedidos = new dbCommerceDataContext();

        var pedidos = (from c in dcPedidos.tbPedidos where c.dataConfirmacaoPagamento.Value.Month == mes && c.dataConfirmacaoPagamento.Value.Year == ano && c.statusDoPedido != 6 select c);
        var totalCartao = (from c in pedidos where c.tipoDePagamentoId == 2 && c.moipStatus != "Autorizado" && c.moipStatus != "Concluído" select c).ToList();
        var totalMarketplace = (from c in pedidos where c.tipoDePagamentoId == 10 select c).ToList();
        var totalNotasEmitidasDia = (from c in pedidos where c.nfeNumero != null && c.nfeNumero > 0 select c).ToList();

        decimal totalNotasValor = totalNotasEmitidasDia.Any() ? Convert.ToDecimal(totalNotasEmitidasDia.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);
        decimal totalCartaoValor = totalCartao.Any() ? Convert.ToDecimal(totalCartao.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);
        decimal totalMarketplaceValor = totalMarketplace.Any() ? Convert.ToDecimal(totalMarketplace.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);
        decimal totalNotasEmitidasValor = totalNotasEmitidasDia.Any() ? Convert.ToDecimal(totalNotasEmitidasDia.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);

        decimal totalDeNotasAGerar = totalMarketplaceValor > totalCartaoValor ? totalMarketplaceValor : totalCartaoValor;
        totalDeNotasAGerar = totalDeNotasAGerar - totalNotasEmitidasValor;
        if (totalDeNotasAGerar < 0) totalDeNotasAGerar = 0;

        litTotalDeNotasGeradas.Text = totalNotasValor.ToString("C");
        litTotalPedidosCartao.Text = totalCartaoValor.ToString("C");
        litTotalPedidosMarketplace.Text = totalMarketplaceValor.ToString("C");
        litTotalDeNotasAEmitir.Text = totalDeNotasAGerar.ToString("C");

        pnResumoDia.Visible = true;
        btnMostrarGerarNotas.Visible = true;

        if (totalDeNotasAGerar > 0)
        {
            litSituacao.Text = "Notas não emitidas";
            btnMostrarGerarNotas.Visible = true;
            btnMostrarGerarNotas.Visible = true;
            btnReemitirNotas.Visible = false;
        }
        else
        {
            litSituacao.Text = "Notas do dia emitidas corretamente";
            btnMostrarGerarNotas.Visible = false;
            btnReemitirNotas.Visible = true;
        }

    }

    protected void btnMostrarGerarNotas_OnClick(object sender, EventArgs e)
    {
        panelSelecionarData.Visible = false;
        pnGerarNota.Visible = true;
        selecionaPedidosParaNotas();

    }

    private void selecionaPedidosParaNotas()
    {
        int mes = Convert.ToInt32(ddlMes.SelectedValue);
        int ano = Convert.ToInt32(ddlAno.SelectedValue);

        var dcPedidos = new dbCommerceDataContext();
        var dcPedidoItens = new dbCommerceDataContext();
        var dcProdutos = new dbCommerceDataContext();

        var pedidos = (from c in dcPedidos.tbPedidos where c.dataConfirmacaoPagamento.Value.Month == mes && c.dataConfirmacaoPagamento.Value.Year == ano && c.statusDoPedido != 6 select c);
        var totalCartao = (from c in pedidos where c.tipoDePagamentoId == 2 && c.moipStatus != "Autorizado" && c.moipStatus != "Concluído" select c).ToList();
        var totalMarketplace = (from c in pedidos where c.tipoDePagamentoId == 10 select c).ToList();
        var totalNotasEmitidasDia = (from c in pedidos where c.nfeNumero != null && c.nfeNumero > 0 select c).ToList();

        decimal totalNotasValor = totalNotasEmitidasDia.Any() ? Convert.ToDecimal(totalNotasEmitidasDia.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);
        decimal totalCartaoValor = totalCartao.Any() ? Convert.ToDecimal(totalCartao.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);
        decimal totalMarketplaceValor = totalMarketplace.Any() ? Convert.ToDecimal(totalMarketplace.Sum(x => x.valorCobrado)) : Convert.ToDecimal(0);


        //Valor que falta gerar
        decimal totalDeNotasAGerarTotal = totalMarketplaceValor > totalCartaoValor ? totalMarketplaceValor : totalCartaoValor;
        decimal totalDeNotasAGerar = totalMarketplaceValor > totalCartaoValor ? totalMarketplaceValor : totalCartaoValor;
        totalDeNotasAGerar = totalDeNotasAGerar - totalNotasValor;


        #region pedidos que não foi emitido nota
        var pedidosFiltrar = (from c in pedidos
            select new
            {
                marketplace = c.tipoDePagamentoId == 10 ? 1 : 0,
                cartao = c.tipoDePagamentoId == 2 && c.moipStatus != "Autorizado" && c.moipStatus != "Concluído" ? 1 : 0,
                c.pedidoId,
                c.valorCobrado,
                estadoOrdenacao = c.endEstado == "ES" | c.endEstado == "MG" | c.endEstado == "RJ" | c.endEstado == "SP" | c.endEstado == "PR" | c.endEstado == "RS" | c.endEstado == "SC" ? 0 : 1,
                nfeNumero = c.nfeNumero == null ? 0 : c.nfeNumero
            });
        pedidosFiltrar = pedidosFiltrar.Where(x => x.nfeNumero == 0);

        decimal pedidosMarketplaceAGerarValor = pedidosFiltrar.Where(x => x.marketplace == 1).Any()
            ? Convert.ToDecimal(pedidosFiltrar.Where(x => x.marketplace == 1).Sum(x => x.valorCobrado))
            : 0;
        #endregion


        //Adiciono todos os pedidos do marketplace pois é obrigatória a emissão de nota fiscal
        var pedidoIds = new List<int>();
        pedidoIds.AddRange(pedidosFiltrar.Where(x => x.marketplace == 1).Select(x => x.pedidoId));
        decimal totalAdicionado = pedidosMarketplaceAGerarValor;



        //Se o valor vendidos no marketplace for menor que o valor total de notas que tenho que emitir
        if (pedidosMarketplaceAGerarValor < totalDeNotasAGerar)
        {
            //Primeiro eu adiciono os pedidos que são de outras regiões e não são mais marketplace
            var pedidosOutrasRegioes = (pedidosFiltrar.Where(x => x.marketplace == 0).Where(x => x.estadoOrdenacao == 1)).OrderBy(x => x.valorCobrado);
            foreach (var pedidoRegiao in pedidosOutrasRegioes)
            {
                if (totalAdicionado < totalDeNotasAGerar)
                {
                    pedidoIds.Add(pedidoRegiao.pedidoId);
                    totalAdicionado += (decimal)pedidoRegiao.valorCobrado;
                }
            }

            //Se passou do valor retiro alguns pedidos
            if (totalAdicionado > totalDeNotasAGerar)
            {
                bool possuiPedidoRetirar = true;

                while (possuiPedidoRetirar)
                {
                    var totalUltrapassado = totalAdicionado - totalNotasValor;
                    var pedidosNaFaixa = pedidosOutrasRegioes.Where(x => x.valorCobrado < totalUltrapassado).OrderByDescending(x => x.valorCobrado);
                    if (pedidosNaFaixa.Any())
                    {
                        var pedidoNaFaixa = pedidosNaFaixa.First();
                        totalAdicionado = totalAdicionado - (decimal) pedidoNaFaixa.valorCobrado;
                        pedidoIds.Remove(pedidoNaFaixa.pedidoId);
                    }
                    else
                    {
                        possuiPedidoRetirar = false;
                    }
                }
            }

            if (totalAdicionado < totalDeNotasAGerar)
            {
                var pedidosRegiaoSulSudeste = (pedidosFiltrar.Where(x => x.marketplace == 0).Where(x => x.estadoOrdenacao == 0)).OrderBy(x => x.valorCobrado);
                foreach (var pedidoRegiao in pedidosRegiaoSulSudeste)
                {
                    if (totalAdicionado < totalDeNotasAGerar)
                    {
                        pedidoIds.Add(pedidoRegiao.pedidoId);
                        totalAdicionado += (decimal)pedidoRegiao.valorCobrado;
                    }
                }

                if (totalAdicionado > totalDeNotasAGerar)
                {
                    bool possuiPedidoRetirar = true;

                    while (possuiPedidoRetirar)
                    {
                        var totalUltrapassado = totalAdicionado - totalDeNotasAGerar;
                        var pedidosNaFaixa = pedidosRegiaoSulSudeste.Where(x => x.valorCobrado < totalUltrapassado).OrderByDescending(x => x.valorCobrado);
                        if (pedidosNaFaixa.Any())
                        {
                            var pedidoNaFaixa = pedidosNaFaixa.First();
                            totalAdicionado = totalAdicionado - (decimal)pedidoNaFaixa.valorCobrado;
                            pedidoIds.Remove(pedidoNaFaixa.pedidoId);
                        }
                        else
                        {
                            possuiPedidoRetirar = false;
                        }
                    }
                }
            }
        }

        litTotalDeNotasSeraoGeradas.Text = totalAdicionado.ToString("C");
        litTotalDePedidosSeraoGerados.Text = pedidoIds.Count.ToString();

        var pedidosListaGerar = (from ped in dcPedidos.tbPedidos where pedidoIds.Contains(ped.pedidoId) select ped);
        lstPedidos.DataSource = pedidosListaGerar;
        lstPedidos.DataBind();


        var listaProdutoItens = (from c in dcPedidoItens.tbItensPedidos where pedidoIds.Contains(c.pedidoId) select c.produtoId).Distinct().ToList();
        var listProdutos = (from c in dcProdutos.tbProdutos where listaProdutoItens.Contains(c.produtoId) orderby c.produtoNome select c).ToList();
        lstProdutos.DataSource = listProdutos;
        lstProdutos.DataBind();
    }

    protected void ddlMes_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        fillValores();
    }

    protected void lstPedidos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hiddenCep = (HiddenField) e.Item.FindControl("hiddenCep");
        var txtCodigoIbge = (TextBox)e.Item.FindControl("txtCodigoIbge");

        var cepsDc = new dbCommerceDataContext();
        try
        {
            string cepOut = "";
            if (hiddenCep != null)
            {
                cepOut = hiddenCep.Value;
            }

            var endereco = (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault();

            if (endereco != null)
            {
                var cidade = (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c).FirstOrDefault();
                if (cidade != null)
                {
                    txtCodigoIbge.Text = cidade.cod_ibge;
                }
            }

        }
        catch (Exception)
        {

        }
    }

    protected void btnGerarNotas_OnClick(object sender, EventArgs e)
    {
        var listaPedidosIgbe = new List<listaPedidosIbgs>();
        foreach (var pedido in lstPedidos.Items)
        {
            var hiddenPedidoId = (HiddenField)pedido.FindControl("hiddenPedidoId");
            var txtCodigoIbge = (TextBox)pedido.FindControl("txtCodigoIbge");

            var pedidoItem = new listaPedidosIbgs();
            pedidoItem.pedidoId = Convert.ToInt32(hiddenPedidoId.Value);
            pedidoItem.codigoIbge = txtCodigoIbge.Text.Trim();

            listaPedidosIgbe.Add(pedidoItem);
        }

        var produtodc = new dbCommerceDataContext();
        foreach (var produto in lstProdutos.Items)
        {
            var hiddenProdutoId = (HiddenField)produto.FindControl("hiddenProdutoId");
            var txtNCM = (TextBox)produto.FindControl("txtNCM");
            int produtoId = Convert.ToInt32(hiddenProdutoId.Value);
            var produtoItem = (from c in produtodc.tbProdutos where c.produtoId == produtoId select c).First();
            produtoItem.ncm = txtNCM.Text;
        }
        produtodc.SubmitChanges();

        rnNotaFiscal.gerarNotaFiscalEmLote(listaPedidosIgbe);

        string lote = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "notas\\lote_" + Guid.NewGuid() + ".zip";

        fillValores();

        Response.Clear();
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", "notasFiscais.zip"));
        Response.BufferOutput = false;

        byte[] buffer = new byte[1024 * 8];

        using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipOutput = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream))
        {

            foreach (var pedido in listaPedidosIgbe)
            {
                ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(pedido.pedidoId + ".xml");
                zipOutput.PutNextEntry(zipEntry);
                using (var fread = System.IO.File.OpenRead(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedido.pedidoId + ".xml"))
                {
                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(fread, zipOutput, buffer);
                }
            }
            zipOutput.Finish();
        }

        Response.Flush();
        Response.End();
    }

    protected void btnReemitirNotas_OnClick(object sender, EventArgs e)
    {
        fillValores();

        int mes = Convert.ToInt32(ddlMes.SelectedValue);
        int ano = Convert.ToInt32(ddlAno.SelectedValue);

        var dcPedidos = new dbCommerceDataContext();
        var pedidos = (from c in dcPedidos.tbPedidos where c.dataConfirmacaoPagamento.Value.Month == mes && c.dataConfirmacaoPagamento.Value.Year == ano select c);
        var totalNotasEmitidasDia = (from c in pedidos where c.nfeNumero != null && c.nfeNumero > 0 select c).ToList();

        Response.Clear();
        Response.ContentType = "application/zip";
        Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", "notasFiscais.zip"));
        Response.BufferOutput = false;

        byte[] buffer = new byte[1024 * 8];

        using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipOutput = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream))
        {

            foreach (var pedido in totalNotasEmitidasDia)
            {
                ICSharpCode.SharpZipLib.Zip.ZipEntry zipEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(pedido.pedidoId + ".xml");
                zipOutput.PutNextEntry(zipEntry);
                using (var fread = System.IO.File.OpenRead(System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedido.pedidoId + ".xml"))
                {
                    ICSharpCode.SharpZipLib.Core.StreamUtils.Copy(fread, zipOutput, buffer);
                }
            }
            zipOutput.Finish();
        }

        Response.Flush();
        Response.End();


    }
}