﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_gerarLoteCorreios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        var data = new dbCommerceDataContext();
        var hoje = Convert.ToDateTime("03/02/2016");
        int numeroLote = 1;
        var ultimoLote =
            (from c in data.tbPedidoPacotes where c.numeroLote != null orderby c.numeroLote descending select c)
                .FirstOrDefault();
        if (ultimoLote != null)
        {
            numeroLote = (int)ultimoLote.numeroLote + 1;
        }
        string lote = "";
                   
        var pacotes = (from c in data.tbPedidoPacotes
                       where c.formaDeEnvio.ToLower() == "pac" && (c.idPedido == 297247 |
c.idPedido == 297263 |
c.idPedido == 293008 |
c.idPedido == 293122 |
c.idPedido == 301626 |
c.idPedido == 243468 |
c.idPedido == 298397 |
c.idPedido == 289986 |
c.idPedido == 299147 |
c.idPedido == 294230 |
c.idPedido == 301191 |
c.idPedido == 300631 |
c.idPedido == 293122 |
c.idPedido == 288784)
                       select c).ToList();
        foreach (var pacote in pacotes)
        {
            string nomeCliente = pacote.tbPedido.endNomeDoDestinatario;
            if (nomeCliente.Length > 50) nomeCliente = nomeCliente.Substring(0, 50);
            while (nomeCliente.Length < 50)
            {
                nomeCliente += " ";
            }
            
            string cep = pacote.tbPedido.endCep.Replace("-", "").Replace(" ", "").Replace(".", "");
            if (cep.Length < 8) cep = "0" + cep;
            if (cep.Length < 8) cep = "0" + cep;
            cep = cep.Substring(0, 5) + "-" + cep.Substring(5, 3);

            string registro = "           ";
            string peso = pacote.peso.ToString();
            while (peso.Length < 5)
            {
                peso = "0" + peso;
            }

            string valor = "";
            if (pacote.tbPedidoEnvio.nfeValor != null)
            {
                valor = ((decimal) pacote.tbPedidoEnvio.nfeValor).ToString("N2");
            }
            while (valor.Length < 20)
            {
                valor = " " + valor;
            }

            string adicionais = "";
            while (adicionais.Length < 18)
            {
                adicionais = " " + adicionais;
            }

            string anotacoes = rnFuncoes.retornaIdCliente(pacote.idPedido) + "-" + pacote.numeroVolume;
            while (anotacoes.Length < 90)
            {
                anotacoes = anotacoes + " ";
            }

            string nota = "";
            if (pacote.tbPedidoEnvio.nfeNumero != null)
            {
                nota = pacote.tbPedidoEnvio.nfeNumero.ToString();
                while (nota.Length < 7)
                {
                    nota = "0" + nota;
                }
            }
            else
            {
                while (nota.Length < 7)
                {
                    nota = " " + nota;
                }
            }

            lote += nomeCliente + cep + registro + peso + peso + valor + adicionais + anotacoes + nota;
            lote += Environment.NewLine;
            pacote.numeroLote = numeroLote;
        }
        System.IO.File.WriteAllText(@"C:\inetpub\wwwroot\admin\correios\" + numeroLote + ".txt", lote);
        data.SubmitChanges();


    }
}