﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" Theme="Glass" CodeFile="pedidosEmFabricacao.aspx.cs" Inherits="admin_pedidosEmFabricacao" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos em Fabricação</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0" style="2">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" OnClick="btPdf_Click" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" OnClick="btXsl_Click" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" OnClick="btRtf_Click" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" OnClick="btCsv_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlPedidos" KeyFieldName="pedidoId" Width="834px" 
                    Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True"  />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        ShowHeaderFilterButton="True" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id Cliente" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" 
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome" 
                            VisibleIndex="1" Width="150px" Settings-AutoFilterCondition="Contains">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedores" FieldName="fornecedores" 
                            VisibleIndex="1" Width="150px" Settings-AutoFilterCondition="Contains">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Valor" FieldName="valorCobrado" 
                            VisibleIndex="2" Width="100px" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Data" FieldName="dataHoraDoPedido" 
                            UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Prazo" FieldName="prazoDeEntrega" 
                            UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Em atraso" FieldName="fornecedorEmAtraso" 
                            VisibleIndex="1" Width="150px" Settings-AutoFilterCondition="Contains">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" 
                            VisibleIndex="7" Width="30px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" 
                                NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                            </PropertiesHyperLinkEdit>
                            <Settings AllowAutoFilter="False" />
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataHyperLinkColumn>
                        <dxwgv:GridViewCommandColumn ButtonType="Image" VisibleIndex="8">
                            <ClearFilterButton Visible="True">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </ClearFilterButton>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
              <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
                <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="pedidoSelecionaPrazoDeFabricacaoAdmin" 
                    TypeName="rnPedidos">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona" 
                    TypeName="rnSituacao">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlPagamentos" runat="server" SelectMethod="condicaoDePagamentoSeleciona" 
                    TypeName="rnCondicoesDePagamento">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" 
                    TypeName="rnTipoDeEntrega">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>