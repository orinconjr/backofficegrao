﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidosEmbaladosAguardeEnvio.aspx.cs" Inherits="admin_pedidosEmbaladosAguardeEnvio" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Pedidos Embalados Aguardando Envio</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <dx:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated"
                                OnCustomUnboundColumnData="grd_CustomUnboundColumnData" KeyFieldName="pedidoId" EnableViewState="false">
                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                <Styles>
                                    <Footer Font-Bold="True">
                                    </Footer>
                                </Styles>
                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                <SettingsPager Position="TopAndBottom" PageSize="100" ShowDisabledButtons="False" AlwaysShowPager="True">
                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                </SettingsPager>
                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                        <DataItemTemplate>
                                            <%# Container.VisibleIndex + 1 %>
                                        </DataItemTemplate>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Data Embalagem" FieldName="dataFimEmbalagem" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data Pesagem" FieldName="dataPesagem" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Data Emissão" FieldName="dataEmissao" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataTextColumn Caption="Transportadora" FieldName="transportadora" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Peso" FieldName="peso" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Largura" FieldName="largura" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Altura" FieldName="altura" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Profundidade" FieldName="profundidade" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Endereço" FieldName="endereco" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Volume" FieldName="numeroVolume" VisibleIndex="0" Width="50">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Data Atualizada" FieldName="prazoMaximoPostagemAtualizado" VisibleIndex="0">
                                        <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy hh:mm:ss">
                                        </PropertiesDateEdit>
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataDateColumn Caption="Tempo" FieldName="tempo" VisibleIndex="0">
                                    </dx:GridViewDataDateColumn>
                                    <dx:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId"
                                        VisibleIndex="7" Width="30px">
                                        <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg"
                                            NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                        </PropertiesHyperLinkEdit>
                                        <Settings AllowAutoFilter="False" />
                                        <HeaderTemplate>
                                            <img alt="" src="images/legendaEditar.jpg" />
                                        </HeaderTemplate>
                                    </dx:GridViewDataHyperLinkColumn>
                                </Columns>
                                <StylesEditors>
                                    <Label Font-Bold="True">
                                    </Label>
                                </StylesEditors>
                            </dx:ASPxGridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

