﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using Specifications.Extensions;

public partial class admin_sendoEnderecadoAtual : System.Web.UI.Page {
 protected void Page_Load(object sender, EventArgs e) {

  fillGrid();

 }

 private void fillGrid() {

  var data = new dbCommerceDataContext();

  var lstProdEnd = (from tbTransf in data.tbTransferenciaEnderecos
                    from tbTransfEndProd in data.tbTransferenciaEnderecoProdutos
                    join nomeTransf in data.tbEnderecamentoTransferencias on tbTransf.idEnderecamentoTransferencia equals nomeTransf.idEnderecamentoTransferencia
                    join nomeUserExp in data.tbUsuarioExpedicaos on tbTransf.idUsuarioExpedicao equals nomeUserExp.idUsuarioExpedicao
                    join transfEndProd in data.tbTransferenciaEnderecoProdutos on tbTransf.idTransferenciaEndereco equals transfEndProd.idTransferenciaEndereco into sumTotalItensFalt
                    where tbTransf.dataFim == null
                    orderby tbTransf.dataInicio descending
                    select new {
                     colEnderecamentoTransf = nomeTransf.enderecamentoTransferencia,
                     colTotalItensFaltantes = sumTotalItensFalt.Count(x => x.dataEnderecamento == null) + " / " + sumTotalItensFalt.Count(),
                     colNomeUserEntrada = tbTransf.usuarioEntrada,
                     colUserExpedicao = nomeUserExp.nome,
                     colDataInicio = tbTransf.dataInicio,
                     colDataUltItemEnd = sumTotalItensFalt.Max(x => x.dataEnderecamento)
                    }).Distinct();

  grd.DataSource = lstProdEnd;
  if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
 }

 public void AlertShow(string message) {
  base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
 }

 //public string NomeUsuarioLogado() {
 // var usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];

 // return rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
 //}
}
