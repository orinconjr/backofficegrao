﻿<%@ Page Title=""  Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="biProdutos.aspx.cs" Inherits="admin_biProdutos" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<%@ Register assembly="DevExpress.Web.ASPxPivotGrid.v11.1" namespace="DevExpress.Web.ASPxPivotGrid" tagprefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">BI - Itens do Pedido</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                            <tr>
                                <td colspan="3" class="rotulos" style="padding-bottom: 10px;" >
                                    <asp:CheckBox runat="server" id="chkHoraAtual" Text="Filtrar pela Hora Atual" AutoPostBack="True" OnCheckedChanged="chkHoraAtual_OnCheckedChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td class="rotulos" style="width: 100px">
                                    Data inicial<br />
                                                <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos" 
                                                                    Width="90px" MaxLength="10"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server" 
                                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                                    ErrorMessage="Preencha a data inicial." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server" 
                                                    ControlToValidate="txtDataInicial" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data inicial." 
                                                    SetFocusOnError="True" 
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                            
                                </td>
                                <td class="rotulos" style="width: 100px">
                                    Data final<br  />
                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos" 
                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server" 
                                                        ControlToValidate="txtDataFinal" Display="None" 
                                                        ErrorMessage="Preencha a data final." 
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <b>
                                    <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server" 
                                                    ControlToValidate="txtDataFinal" Display="None" 
                                                    ErrorMessage="Por favor, preencha corretamente a data final" 
                                                    SetFocusOnError="True" 
                                            
                                            
                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                    </b>
                                </td>
                                <td valign="bottom">
                                    <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPequisar_Click" />
                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                                <tr>
                                    <td align="right">
                                        &nbsp;</td>
                                    <td height="38" 
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                                        valign="bottom" width="231">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" OnCustomUnboundFieldData="grd_CustomUnboundFieldData" oncustomcellvalue="ASPxPivotGrid1_CustomCellValue" OnCustomCellStyle="grd_OnCustomCellStyle" ID="grd" runat="server" CustomizationFieldsLeft="600" CustomizationFieldsTop="400" ClientInstanceName="pivotGrid" Width="100%">
                                <Fields>
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemValor" Caption="Valor" ID="Valor"  />                                  
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemValor" Caption="Valor Medio" ID="ValorMedio" SummaryType="Average"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="fornecedorNome" Caption="Fornecedor" ID="Fornecedor" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="marcaNome" Caption="Marca" ID="Marca" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="itemQuantidade" Caption="Quantidade" ID="Quantidade" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="valorCusto" Caption="Custo Pedido" ID="CustoPedido" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="custoPago" Caption="Custo Pago" ID="CustoPago" />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorCobrado" Caption="Valor Cobrado" ID="ValorCobrado" />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorPago" Caption="Total Pagamentos" ID="ValorPago" />
                                    <dx:PivotGridField Area="DataArea" Caption="Quebra" ID="Quebra" UnboundExpression="Iif([ValorCobrado]>0, 100 - (([ValorPago] * 100) / [ValorCobrado]), 0)" FieldName="quebra" UnboundType="Decimal" UnboundFieldName="variacaoValor" CellFormat-FormatString="{0:0.##}%"  CellFormat-FormatType="Numeric" />
                                    <%--<dx:PivotGridField Area="DataArea" Caption="CMV" ID="CMV"  UnboundExpression="Iif(([ValorPago] - [FretePago]) > 0, (([CustoPago] / ([ValorPago] - [FretePago])) * 100), 0)" FieldName="cmv" UnboundType="Decimal" UnboundFieldName="variacaoCmv" CellFormat-FormatString="{0:0.##}%"  CellFormat-FormatType="Numeric" />--%>
                                    <%--<dx:PivotGridField Area="DataArea" Caption="CustoPagoNotNUll"  ID="CustoPagoNotNull" UnboundExpression="Iif([ValorPago] = '', '', [ValorPago])" FieldName="custoPagoNotNull" UnboundType="String" />
                                    <dx:PivotGridField Area="DataArea" Caption="CustoPagoNotNUll"  ID="CustoPagoNotNull1" UnboundExpression="Iif(IsNullOrEmpty([CustoPagoNotNull]), 'Nulo', [CustoPagoNotNull])" FieldName="custoPagoNotNull1" UnboundType="String" />--%>
                                    <%--<dx:PivotGridField Area="DataArea" FieldName="cmv" Caption="CMV" ID="Cmv" UnboundType="Decimal" CellFormat-FormatString="{0:0.##}%"  CellFormat-FormatType="Numeric" />--%>
                                    
                                    <dx:PivotGridField Area="FilterArea" FieldName="produtoNome" Caption="Nome do Produto" ID="NomeDoProduto" SortBySummaryInfo-FieldName="Valor" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="produtoId" Caption="Id do Produto" ID="IdDoProduto" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="pedidoId" Caption="Id do Pedido" ID="IdDoPedido" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="dataHoraDoPedido" Caption="Hora do Pedido" ID="HoraPedido" GroupInterval="Hour"  />
                                    <dx:PivotGridField Area="RowArea" FieldName="categoria" Caption="Categoria" ID="Categoria" />
                                    <dx:PivotGridField Area="DataArea" FieldName="fretePago" Caption="Frete Pago" ID="FretePago"  />
                                    <dx:PivotGridField Area="FilterArea" FieldName="colecao" Caption="Coleção" ID="Colecao" />
                                    <dx:PivotGridField Area="ColumnArea" FieldName="dataHoraDoPedido" Caption="Data do Pedido" ID="DataPedido" GroupInterval="Date"  />
                                </Fields>
                                <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False" />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>