﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_voltarEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var retorno = new StringBuilder();
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidoFornecedorItems
                       join d in data.tbPedidos on c.idPedido equals d.pedidoId
                       where c.idPedidoFornecedor == 8801 && c.idPedido != null && c.entregue == false
                       && (d.statusDoPedido == 3 | d.statusDoPedido == 4 | d.statusDoPedido == 11 | d.statusDoPedido == 5)
                       select new
                       {
                           c.idPedidoFornecedorItem,
                           c.tbProduto.produtoNome,
                           c.tbProduto.tbProdutoFornecedor.fornecedorNome
                       });

        foreach (var pedido in pedidos)
        {
            retorno.AppendFormat("Etiqueta: {0}<br>", pedido.idPedidoFornecedorItem);
            retorno.AppendFormat("Fornecedor: {0}<br>", pedido.fornecedorNome);
            retorno.AppendFormat("Produto: {0}<br><br>", pedido.produtoNome);
        }

        litListaPedidos.Text = retorno.ToString();
    }
}