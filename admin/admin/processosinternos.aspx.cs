﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_processosinternos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            using (dbCommerceDataContext data = new dbCommerceDataContext())
            {
                var processos =
                    (from c in data.tbSubProcessoFabricas
                     select new
                     {
                         procsubproc = c.idProcessoFabrica + "-" + c.idSubProcessoFabrica,
                         nome = c.tbProcessoFabrica.processo + " - " + c.nome
                     }).ToList
                        ();
                ddlProcessosFabrica.DataSource = processos.OrderBy(x => x.nome);
                ddlProcessosFabrica.DataBind();

                var processosExternos =
                (from c in data.tbProcessoExternoFabricas
                 select new
                 {
                     proc = c.idProcessoExternoFabrica,
                     nome = c.processoExterno
                 }).ToList();
                ddlProcessosExternosFabrica.DataSource = processosExternos.OrderBy(x => x.nome);
                ddlProcessosExternosFabrica.DataBind();

                int produtoId = 0;
                try
                {
                    produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
                }
                catch (Exception)
                {
                }
                var produto = (from c in data.tbProdutos
                               where c.produtoId == produtoId
                               select new
                               {
                                   c.produtoId,
                                   c.produtoNome,
                                   c.fotoDestaque,
                               }).FirstOrDefault();
             
                imgfotodestaque.Src = "http://www.graodegente.com.br/fotos/" + produto.produtoId + "/" +
                                      produto.fotoDestaque + ".jpg";
                lblprodutonome.Text = produto.produtoNome;
            }
            fillGrid();
        }




    }

    private void fillGrid()
    {
     
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            int produtoId = 0;
            try
            {
                produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
            }
            catch (Exception) { }


            var dados = (from spf in data.tbSubProcessoFabricas
                join pf in data.tbProcessoFabricas on
                    spf.idProcessoFabrica equals pf.idProcessoFabrica
                join jppf in data.tbJuncaoProdutoProcessoFabricas on
                    new
                    {
                        idProcessoFabrica = spf.idProcessoFabrica,
                        idSubProcessoFabrica = spf.idSubProcessoFabrica
                    } equals
                    new
                    {
                        idProcessoFabrica = jppf.idProcessoFabrica,
                        idSubProcessoFabrica = jppf.idSubProcessoFabrica
                    }
                where jppf.idProduto == produtoId
                select new
                {
                    processo = pf.processo,
                    idSubProcessoFabrica = spf.idSubProcessoFabrica,
                    idProcessoFabrica = spf.idProcessoFabrica,
                    nome = spf.nome,
                    minutos = jppf.minutos,
                    idProduto = jppf.idProduto,
                    idJuncaoProdutoProcessoFabrica = jppf.idJuncaoProdutoProcessoFabrica
                }).ToList().OrderBy(x => x.processo).ThenBy(x => x.nome);

 

            GridView1.DataSource = dados.Where(x => x.idProduto == produtoId);
            GridView1.DataBind();

            var dadosExternos = (from pef in data.tbProcessoExternoFabricas
                        
                         join jppef in data.tbJuncaoProdutoProcessoExternoFabricas on
                                  pef.idProcessoExternoFabrica equals jppef.idProcessoExternoFabrica
                           
                         where jppef.idProduto == produtoId
                         select new
                         {
                             processoExterno = pef.processoExterno,
                             idProcessoExternoFabrica = pef.idProcessoExternoFabrica,
                             minutos = jppef.minutos,
                             idProduto = jppef.idProduto,
                             idJuncaoProdutoProcessoExternoFabrica = jppef.idJuncaoProdutoProcessoExternoFabrica
                         }).ToList().OrderBy(x => x.processoExterno);



            GridView2.DataSource = dadosExternos.Where(x => x.idProduto == produtoId);
            GridView2.DataBind();

        }
    }

   
    protected void btnGravar_Click(object sender, EventArgs e)
    {

        string[] procsubproc = ddlProcessosFabrica.SelectedValue.ToString().Split('-');
        int proc = Convert.ToInt32(procsubproc[0]);
        int subproc = Convert.ToInt32(procsubproc[1]);
        int produtoId = 0;
        try
        {
            produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
        }
        catch (Exception)
        {
        }
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            if (!
                data.tbJuncaoProdutoProcessoFabricas.Where(
                    x => x.idProduto == produtoId && x.idProcessoFabrica == proc && x.idSubProcessoFabrica == subproc)
                    .Any())
            {
                tbJuncaoProdutoProcessoFabrica _tbJuncaoProdutoProcessoFabrica = new tbJuncaoProdutoProcessoFabrica();
                _tbJuncaoProdutoProcessoFabrica.idProcessoFabrica = proc;
                _tbJuncaoProdutoProcessoFabrica.idSubProcessoFabrica = subproc;
                _tbJuncaoProdutoProcessoFabrica.idProduto = produtoId;
                _tbJuncaoProdutoProcessoFabrica.minutos = Convert.ToInt32(txtMinutos.Text);
                data.tbJuncaoProdutoProcessoFabricas.InsertOnSubmit(_tbJuncaoProdutoProcessoFabrica);
            }
            else
            {
                var item = (from p in data.tbJuncaoProdutoProcessoFabricas
                    where p.idProduto == produtoId && p.idProcessoFabrica == p.idProcessoFabrica
                          && p.idSubProcessoFabrica == subproc
                    select p).First();
                item.minutos = Convert.ToInt32(txtMinutos.Text);
            }
            data.SubmitChanges();
            fillGrid();
      
           
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idJuncaoProdutoProcessoFabrica = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {
            tbJuncaoProdutoProcessoFabrica item= (from p in data.tbJuncaoProdutoProcessoFabricas
                where p.idJuncaoProdutoProcessoFabrica == idJuncaoProdutoProcessoFabrica
                select p).First();
             data.tbJuncaoProdutoProcessoFabricas.DeleteOnSubmit(item);
            data.SubmitChanges();
        }
        fillGrid();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;

        fillGrid();
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        fillGrid();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idJuncaoProdutoProcessoFabrica = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value.ToString());
        string minutos = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox1")).Text;
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {
            tbJuncaoProdutoProcessoFabrica item = (from p in data.tbJuncaoProdutoProcessoFabricas
                                                   where p.idJuncaoProdutoProcessoFabrica == idJuncaoProdutoProcessoFabrica
                                                   select p).First();
            item.minutos = Convert.ToInt32(minutos);
            data.SubmitChanges();
        }
        GridView1.EditIndex = -1;

        fillGrid();
     
    }

    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idJuncaoProdutoProcessoExternoFabrica = Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Value);
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {
            tbJuncaoProdutoProcessoExternoFabrica item = (from p in data.tbJuncaoProdutoProcessoExternoFabricas
                                                   where p.idJuncaoProdutoProcessoExternoFabrica == idJuncaoProdutoProcessoExternoFabrica
                                                   select p).First();
            data.tbJuncaoProdutoProcessoExternoFabricas.DeleteOnSubmit(item);
            data.SubmitChanges();
        }
        fillGrid();
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;

        fillGrid();
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        fillGrid();
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idJuncaoProdutoProcessoExternoFabrica = Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Value.ToString());
        string minutos = ((TextBox)GridView2.Rows[e.RowIndex].FindControl("TextBox1")).Text;
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {
            tbJuncaoProdutoProcessoExternoFabrica item = (from p in data.tbJuncaoProdutoProcessoExternoFabricas
                                                   where p.idJuncaoProdutoProcessoExternoFabrica == idJuncaoProdutoProcessoExternoFabrica
                                                   select p).First();
            item.minutos = Convert.ToInt32(minutos);
            data.SubmitChanges();
        }
        GridView2.EditIndex = -1;

        fillGrid();

    }
    protected void btnGravarExterno_Click(object sender, EventArgs e)
    {

        int proc = Convert.ToInt32(ddlProcessosExternosFabrica.SelectedValue);
      
        int produtoId = 0;
        try
        {
            produtoId = Convert.ToInt32(Request.QueryString["produtoId"]);
        }
        catch (Exception)
        {
        }
        using (dbCommerceDataContext data = new dbCommerceDataContext())
        {

            if (!
                data.tbJuncaoProdutoProcessoExternoFabricas.Where(
                    x => x.idProduto == produtoId && x.idProcessoExternoFabrica == proc)
                    .Any())
            {
                tbJuncaoProdutoProcessoExternoFabrica _tbJuncaoProdutoProcessoExternoFabrica = new tbJuncaoProdutoProcessoExternoFabrica();
                _tbJuncaoProdutoProcessoExternoFabrica.idProcessoExternoFabrica = proc;
                //_tbJuncaoProdutoProcessoExternoFabrica.idSubProcessoExternoFabrica = subproc;
                _tbJuncaoProdutoProcessoExternoFabrica.idProduto = produtoId;
                _tbJuncaoProdutoProcessoExternoFabrica.minutos = Convert.ToInt32(txtMinutosExternos.Text);
                data.tbJuncaoProdutoProcessoExternoFabricas.InsertOnSubmit(_tbJuncaoProdutoProcessoExternoFabrica);
            }
            else
            {
                tbJuncaoProdutoProcessoExternoFabrica item = (from p in data.tbJuncaoProdutoProcessoExternoFabricas
                            where p.idProduto == produtoId && p.idProcessoExternoFabrica == proc
                               select p).First();
                //tbJuncaoProdutoProcessoExternoFabrica item = (from p in data.tbJuncaoProdutoProcessoExternoFabricas
                //                                              where p.idJuncaoProdutoProcessoExternoFabrica == idJuncaoProdutoProcessoExternoFabrica
                //                                              select p).First();
               // item.minutos = Convert.ToInt32(minutos);
                item.minutos = Convert.ToInt32(txtMinutosExternos.Text);
            }
            data.SubmitChanges();
            fillGrid();


        }
    }
}
 