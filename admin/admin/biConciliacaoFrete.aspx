﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="biConciliacaoFrete.aspx.cs" Inherits="admin_biConciliacaoFrete" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxPivotGrid.v11.1" Namespace="DevExpress.Web.ASPxPivotGrid" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">BI - Conciliação de Frete</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                            <tr>
                                                <td class="rotulos" style="width: 100px">Data inicial<br />
                                                    <asp:TextBox ID="txtDataInicial" runat="server" CssClass="campos"
                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqvDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Preencha a data inicial."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rgeDataInicial" runat="server"
                                                        ControlToValidate="txtDataInicial" Display="None"
                                                        ErrorMessage="Por favor, preencha corretamente a data inicial."
                                                        SetFocusOnError="True"
                                                        ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>

                                                </td>
                                                <td class="rotulos" style="width: 100px">Data final<br />
                                                    <asp:TextBox ID="txtDataFinal" runat="server" CssClass="campos"
                                                        Width="90px" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqvDataFinal" runat="server"
                                                        ControlToValidate="txtDataFinal" Display="None"
                                                        ErrorMessage="Preencha a data final."
                                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                    <b>
                                                        <asp:RegularExpressionValidator ID="rgeDataInicial0" runat="server"
                                                            ControlToValidate="txtDataFinal" Display="None"
                                                            ErrorMessage="Por favor, preencha corretamente a data final"
                                                            SetFocusOnError="True"
                                                            ValidationExpression="^(([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/[1-2][0-9]\d{2})$"></asp:RegularExpressionValidator>
                                                    </b>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="btnPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="btnPequisar_Click" />
                                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List" ShowMessageBox="True" ShowSummary="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">&nbsp;</td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <dx:ASPxPivotGrid OptionsData-DataFieldUnboundExpressionMode="UseSummaryValues" OnCustomUnboundFieldData="grd_CustomUnboundFieldData"  ID="grd" runat="server" CustomizationFieldsLeft="600" CustomizationFieldsTop="400" ClientInstanceName="pivotGrid" Width="100%">
                                <Fields>
                                    <dx:PivotGridField Area="ColumnArea" FieldName="transportadora" Caption="Transportadora" ID="ColumnTransportadora" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="codigoFatura" Caption="Número da Fatura" ID="CodigoFatura" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="filterdataHoraDoPedido" Caption="Data do Pedido" ID="FilterDataPedido"  GroupInterval="Date" />
                                    <dx:PivotGridField Area="FilterArea" FieldName="empresa" Caption="Empresa" ID="Empresa"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorSelecionado" Caption="Valor Orçado" ID="ValorSelecionado"  />
                                    <dx:PivotGridField Area="DataArea" FieldName="valorFrete" Caption="Valor Cobrado" ID="ValorCobrado" />
                                    <dx:PivotGridField Area="DataArea" FieldName="diferenca" Caption="Diferença" ID="Diferenca" UnboundType="Decimal" />
                                    <dx:PivotGridField Area="DataArea" FieldName="numeroNota" Caption="Número Nota Fiscal" ID="NumeroNota" />               </Fields>
                                <OptionsView ShowFilterHeaders="True" ShowHorizontalScrollBar="True" ShowColumnGrandTotals="False" />
                                <OptionsCustomization AllowSortBySummary="True" AllowSort="True" AllowFilterInCustomizationForm="True"></OptionsCustomization>
                                <OptionsPager RowsPerPage="100"></OptionsPager>
                            </dx:ASPxPivotGrid>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

