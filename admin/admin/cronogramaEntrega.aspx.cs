﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_cronogramaEntrega : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid();
    }

    private void fillGrid()
    {
        var data = new dbCommerceDataContext();
        var entregas = (from c in data.tbPedidoFornecedors
            join e in data.tbProdutoFornecedors on c.idFornecedor equals e.fornecedorId
            join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into itens
            where itens.Count(x => x.entregue) < itens.Count() && c.dataPrevista != null && e.entrega
            select new {c.dataPrevista, e.fornecedorNome, c.idPedidoFornecedor}
            );
        grdEntrega.DataSource = entregas.OrderBy(x => x.dataPrevista);
        if (!Page.IsPostBack && !Page.IsCallback) grdEntrega.DataBind();

        var coletas = (from c in data.tbPedidoFornecedors
            join e in data.tbProdutoFornecedors on c.idFornecedor equals e.fornecedorId
            join d in data.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into itens
            where itens.Count(x => x.entregue) < itens.Count() && c.dataPrevista != null && e.entrega == false
            select new {c.dataPrevista, e.fornecedorNome, e.fornecedorCidade, c.idPedidoFornecedor}
            );
        grdColeta.DataSource = coletas.OrderBy(x => x.dataPrevista);
        if (!Page.IsPostBack && !Page.IsCallback) grdColeta.DataBind();
    }
}