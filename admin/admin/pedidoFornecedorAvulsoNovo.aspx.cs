﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using CarlosAg.ExcelXmlWriter;
using SpreadsheetLight;

public partial class admin_pedidoFornecedorAvulsoNovo : System.Web.UI.Page
{
    public class pedidoFornecedorTemporario
    {
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public int itemId { get; set; }
        public int fornecedorId { get; set; }
        public string produtoIdDaEmpresa { get; set; }
        public string complementoIdDaEmpresa { get; set; }
        public string produtoNome { get; set; }
        public int quantidade { get; set; }
        public string motivo { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //try
            //{
            //    if (Session["pedidoFornecedorTemporario"] != null)
            //    {
            //        var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
            //        foreach (var temporario in lista)
            //        {
            //            lista.Remove(temporario);
            //        }
            //    }
            //    Session["pedidoFornecedorTemporario"] = null;
            //}
            //catch (Exception)
            //{

            //}
            //Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        }
    }

    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        var pedidoDc = new dbCommerceDataContext();
        int pedidoId = 0;
        int itemId = 0;
        int quantidade = 1;
        int.TryParse(txtQuantidade.Text, out quantidade);
        int.TryParse(txtPedidoItem.Text, out itemId);

        if (quantidade == 0) quantidade = 1;

        int.TryParse(txtPedido.Text, out pedidoId);
        if (pedidoId.ToString().Length != 5 && pedidoId > 0)
        {
            pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
        }
        var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();
        if (pedido == null && pedidoId > 0)
        {
            Response.Write("<script>alert('Pedido não localizado.');</script>");
            return;
        }
        if (ddlComplemento.Visible == false)
        {
            var produtosDc = new dbCommerceDataContext();
            int produtoId = 0;
            int.TryParse(txtIdDaEmpresa.Text, out produtoId);

            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c);
            if (produto.Count() > 1)
            {
                ddlComplemento.DataSource = produto;
                ddlComplemento.DataBind();
                ddlComplemento.Visible = true;
                Response.Write(
                    "<script>alert('Produto com opções. Favor selecionar o complemento e clicar em adicionar novamente.');</script>");
            }
            else if (produto.Count() == 1)
            {
                var produtoItem = produto.First();
                var produtosFilho =
                            (from c in produtosDc.tbProdutoRelacionados
                             where c.idProdutoPai == produtoItem.produtoId
                             select c);
                if (produtosFilho.Any())
                {
                    foreach (var produtoRelacionado in produtosFilho)
                    {
                        var produtoRel =
                            (from c in produtosDc.tbProdutos
                             where c.produtoId == produtoRelacionado.idProdutoFilho
                             select c).First();
                        for (int i = 1; i <= quantidade; i++)
                        {
                            var item = new pedidoFornecedorTemporario();
                            item.produtoId = produtoRelacionado.idProdutoFilho;
                            item.fornecedorId = produtoRel.produtoFornecedor;
                            item.produtoIdDaEmpresa = produtoRel.produtoIdDaEmpresa;
                            item.complementoIdDaEmpresa = produtoRel.complementoIdDaEmpresa;
                            item.produtoNome = produtoRel.produtoNome;
                            item.quantidade = 1;
                            item.pedidoId = pedidoId;
                            item.motivo = txtMotivo.Text;
                            item.itemId = itemId;
                            ddlComplemento.Visible = false;
                            adicionaItemListaTemporaria(item);
                        }
                    }
                    txtIdDaEmpresa.Text = "";
                    txtMotivo.Text = "";
                }
                else
                {
                    for (int i = 1; i <= quantidade; i++)
                    {
                        var item = new pedidoFornecedorTemporario();
                        item.produtoId = produtoItem.produtoId;
                        item.fornecedorId = produtoItem.produtoFornecedor;
                        item.produtoIdDaEmpresa = produtoItem.produtoIdDaEmpresa;
                        item.complementoIdDaEmpresa = produtoItem.complementoIdDaEmpresa;
                        item.produtoNome = produtoItem.produtoNome;
                        item.quantidade = 1;
                        item.pedidoId = pedidoId;
                        item.motivo = txtMotivo.Text;
                        item.itemId = itemId;
                        ddlComplemento.Visible = false;
                        adicionaItemListaTemporaria(item);
                        txtIdDaEmpresa.Text = "";
                        txtMotivo.Text = "";
                    }
                }
            }
            else
            {
                Response.Write("<script>alert('Produto não encontrado.');</script>");
            }
        }
        else
        {
            int produtoId = (Convert.ToInt32(ddlComplemento.SelectedValue));
            var produtosDc = new dbCommerceDataContext();


            var produtoItem = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
            var produtosFilho =
                        (from c in produtosDc.tbProdutoRelacionados
                         where c.idProdutoPai == produtoItem.produtoId
                         select c);
            if (produtosFilho.Any())
            {
                foreach (var produtoRelacionado in produtosFilho)
                {
                    var produtoRel =
                        (from c in produtosDc.tbProdutos
                         where c.produtoId == produtoRelacionado.idProdutoFilho
                         select c).First();
                    for (int i = 1; i <= quantidade; i++)
                    {
                        var item = new pedidoFornecedorTemporario();
                        item.produtoId = produtoRelacionado.idProdutoFilho;
                        item.fornecedorId = produtoRel.produtoFornecedor;
                        item.produtoIdDaEmpresa = produtoRel.produtoIdDaEmpresa;
                        item.complementoIdDaEmpresa = produtoRel.complementoIdDaEmpresa;
                        item.produtoNome = produtoRel.produtoNome;
                        item.quantidade = 1;
                        item.pedidoId = pedidoId;
                        item.motivo = txtMotivo.Text;
                        item.itemId = itemId;
                        ddlComplemento.Visible = false;
                        adicionaItemListaTemporaria(item);
                    }
                }
                txtIdDaEmpresa.Text = "";
                txtMotivo.Text = "";
            }
            else
            {
                for (int i = 1; i <= quantidade; i++)
                {
                    var item = new pedidoFornecedorTemporario();
                    item.produtoId = produtoItem.produtoId;
                    item.fornecedorId = produtoItem.produtoFornecedor;
                    item.produtoIdDaEmpresa = produtoItem.produtoIdDaEmpresa;
                    item.complementoIdDaEmpresa = produtoItem.complementoIdDaEmpresa;
                    item.produtoNome = produtoItem.produtoNome;
                    item.quantidade = 1;
                    item.pedidoId = pedidoId;
                    item.motivo = txtMotivo.Text;
                    item.itemId = itemId;
                    ddlComplemento.Visible = false;
                    adicionaItemListaTemporaria(item);
                    txtIdDaEmpresa.Text = "";
                    txtMotivo.Text = "";
                }
            }
        }
    }

    private void fillGrid()
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        lstProdutos.DataSource = lista;
        lstProdutos.DataBind();
    }
    private void adicionaItemListaTemporaria(pedidoFornecedorTemporario item)
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        lista.Add(item);
        //Session["pedidoFornecedorTemporario"] = lista;
        hdfLista.Text = Serialize(lista);
        fillGrid();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {

    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var listaIds = new List<int>();
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }

        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        if (!lista.Any())
        {
            Response.Write("<script>alert('Favor adicionar algum produto à lista.');</script>");
            return;
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];

        var listaFornecedores = new List<int>();
        bool pedidoEstoque = (Request.QueryString["estoque"] != null);
        if (pedidoEstoque)
        {
            listaFornecedores.Add(71);
        }
        else
        {
            listaFornecedores = lista.Select(x => x.fornecedorId).Distinct().ToList();
        }
        foreach (var listaFornecedor in listaFornecedores)
        {
            int fornecedorId = listaFornecedor;
            var fornecedorDc = new dbCommerceDataContext();
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).First();
            var pedidosDc = new dbCommerceDataContext();
            var pedidoFornecedorCheck = (from c in pedidosDc.tbPedidoFornecedors where c.idFornecedor == fornecedorId && c.pendente == true select c).FirstOrDefault();
            var pedidoFornecedor = pedidoFornecedorCheck == null ? new tbPedidoFornecedor() : pedidoFornecedorCheck;
            if (pedidoEstoque) pedidoFornecedor = new tbPedidoFornecedor();
            pedidoFornecedor.data = DateTime.Now;
            pedidoFornecedor.idFornecedor = fornecedorId;
            pedidoFornecedor.confirmado = false;
            pedidoFornecedor.avulso = true;
            pedidoFornecedor.pendente = true;
            if (pedidoEstoque) pedidoFornecedor.pendente = false;
            if (fornecedor.fornecedorPrazoPedidos > 0)
            {
                int diasPrazo = Convert.ToInt32(fornecedor.fornecedorPrazoPedidos);
                if ((fornecedor.prazoFornecedorDiasUteis ?? false) == true)
                {
                    int diasPrazoFinal = rnFuncoes.retornaPrazoDiasUteis(diasPrazo, 0);
                    diasPrazo = diasPrazoFinal;
                }
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(diasPrazo);
            }
            else
            {
                pedidoFornecedor.dataLimite = DateTime.Now.AddDays(1);
            }
            pedidoFornecedor.usuario = "";
            if (usuarioLogadoId != null)
            {
                pedidoFornecedor.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
            }
            if (pedidoFornecedorCheck == null | pedidoEstoque) pedidosDc.tbPedidoFornecedors.InsertOnSubmit(pedidoFornecedor);
            pedidosDc.SubmitChanges();

            int idPedidoFornecedor = pedidoFornecedor.idPedidoFornecedor;

            foreach (var pedido in lista)
            {
                int totalItens = 0;
                var itensPedidoDc = new dbCommerceDataContext();
                totalItens++;
                var produtosDc = new dbCommerceDataContext();
                var produto = (from c in produtosDc.tbProdutos where c.produtoId == pedido.produtoId select c).First();
                if (produto.produtoFornecedor == fornecedorId | pedidoEstoque)
                {
                    var pedidoFornecedorItem = new tbPedidoFornecedorItem();
                    pedidoFornecedorItem.custo = ckbGerarCusto.Checked ? 0 : (decimal)produto.produtoPrecoDeCusto;
                    pedidoFornecedorItem.brinde = false;
                    pedidoFornecedorItem.diferenca = 0;
                    pedidoFornecedorItem.idPedidoFornecedor = idPedidoFornecedor;
                    pedidoFornecedorItem.idProduto = produto.produtoId;
                    pedidoFornecedorItem.entregue = false;
                    pedidoFornecedorItem.quantidade = Convert.ToInt32(pedido.quantidade);
                    pedidoFornecedorItem.motivo = txtMotivo.Text;
                    if (pedido.itemId > 0) pedidoFornecedorItem.idItemPedido = pedido.itemId;
                    if (pedido.pedidoId > 0) pedidoFornecedorItem.idPedido = pedido.pedidoId;
                    pedidosDc.tbPedidoFornecedorItems.InsertOnSubmit(pedidoFornecedorItem);
                    pedidosDc.SubmitChanges();
                }
            }

            listaIds.Add(pedidoFornecedor.idPedidoFornecedor);

            if (pedidoEstoque)
            {
                Session["pedidoFornecedorTemporario"] = null;
                Response.Redirect("pedidoFornecedorEditar.aspx?idPedidoFornecedor=" + idPedidoFornecedor);
            }
        }
        Session["pedidoFornecedorTemporario"] = null;
        Response.Redirect("pedidoFornecedor.aspx");
    }


    private class fornecedorCusto
    {
        public decimal custo { get; set; }
        public int produtoId { get; set; }
    }

    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        var lista = new List<pedidoFornecedorTemporario>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        //if (Session["pedidoFornecedorTemporario"] == null)
        //{
        //    Session["pedidoFornecedorTemporario"] = new List<pedidoFornecedorTemporario>();
        //}
        //var lista = (List<pedidoFornecedorTemporario>)Session["pedidoFornecedorTemporario"];
        try
        {
            lista.Remove(lista[Convert.ToInt32(e.CommandArgument)]);
            hdfLista.Text = Serialize(lista);
        }
        catch (Exception)
        {

        }
        fillGrid();
    }

    public static string Serialize(List<pedidoFornecedorTemporario> tData)
    {
        var serializer = new XmlSerializer(typeof(List<pedidoFornecedorTemporario>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<pedidoFornecedorTemporario> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<pedidoFornecedorTemporario>));

        TextReader reader = new StringReader(tData);

        return (List<pedidoFornecedorTemporario>)serializer.Deserialize(reader);
    }

    protected void btnImportar_Click(object sender, EventArgs e)
    {
        try
        {
            string caminho = MapPath("~/");

            if (File.Exists(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower()))
                File.Delete(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());
                fupPlanilha.SaveAs(caminho + "planilhaPrecosFornecedor" + Path.GetExtension(fupPlanilha.FileName).ToLower());


            string caminhoPlanilha = caminho + "planilhaPrecosFornecedor" +
                                     Path.GetExtension(fupPlanilha.FileName).ToLower();

            using (SLDocument sl = new SLDocument(caminhoPlanilha, "importar"))
            {
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    try
                    {
                        int produtoId = Convert.ToInt32(sl.GetCellValueAsDecimal(row, 1));
                        int quantidade = Convert.ToInt32(sl.GetCellValueAsDecimal(row, 2));
                        var produtosDc = new dbCommerceDataContext();
                        var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select new {
                            c.produtoId,
                            c.produtoFornecedor,
                            c.produtoIdDaEmpresa,
                            c.complementoIdDaEmpresa,
                            c.produtoNome
                        });
                        if(produto.Count() > 0) { 
                        var produtoItem = produto.First();
                        var produtosFilho =
                                    (from c in produtosDc.tbProdutoRelacionados
                                     where c.idProdutoPai == produtoItem.produtoId
                                     select c);
                            if (produtosFilho.Any())
                            {
                                foreach (var produtoRelacionado in produtosFilho)
                                {
                                    var produtoRel =
                                        (from c in produtosDc.tbProdutos
                                         where c.produtoId == produtoRelacionado.idProdutoFilho
                                         select c).First();
                                    for (int i = 1; i <= quantidade; i++)
                                    {
                                        var item = new pedidoFornecedorTemporario();
                                        item.produtoId = produtoRelacionado.idProdutoFilho;
                                        item.fornecedorId = produtoRel.produtoFornecedor;
                                        item.produtoIdDaEmpresa = produtoRel.produtoIdDaEmpresa;
                                        item.complementoIdDaEmpresa = produtoRel.complementoIdDaEmpresa;
                                        item.produtoNome = produtoRel.produtoNome;
                                        item.quantidade = 1;
                                        item.pedidoId = 0;
                                        item.motivo = txtMotivo.Text;
                                        item.itemId = 0;
                                        ddlComplemento.Visible = false;
                                        adicionaItemListaTemporaria(item);
                                    }
                                }
                                txtIdDaEmpresa.Text = "";
                                txtMotivo.Text = "";
                            }
                            else
                            {
                                for (int i = 1; i <= quantidade; i++)
                                {
                                    var item = new pedidoFornecedorTemporario();
                                    item.produtoId = produtoItem.produtoId;
                                    item.fornecedorId = produtoItem.produtoFornecedor;
                                    item.produtoIdDaEmpresa = produtoItem.produtoIdDaEmpresa;
                                    item.complementoIdDaEmpresa = produtoItem.complementoIdDaEmpresa;
                                    item.produtoNome = produtoItem.produtoNome;
                                    item.quantidade = 1;
                                    item.pedidoId = 0;
                                    item.motivo = txtMotivo.Text;
                                    item.itemId = 0;
                                    ddlComplemento.Visible = false;
                                    adicionaItemListaTemporaria(item);
                                    txtIdDaEmpresa.Text = "";
                                    txtMotivo.Text = "";
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }
            }
        }
        catch(Exception ex)
        {

        }
    }
}