﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImpressaoCarregamentoCaminhao.aspx.cs" Inherits="admin_ImpressaoCarregamentoCaminhao" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Pack List</title>
    <style>
        .texto {
            font-family: Arial;
            font-size: 10px;
            font-style: normal;
            font-variant: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Label ID="lblMensagem" runat="server"></asp:Label>
        <div class="texto">
            Via Grão de Gente
            <asp:Literal ID="litDados" runat="server"></asp:Literal>
        </div>
        <div class="texto">
            Via Transportadora
            <asp:Literal ID="litDadosTransportadora" runat="server"></asp:Literal>
        </div>
        
    </form>
</body>
</html>
