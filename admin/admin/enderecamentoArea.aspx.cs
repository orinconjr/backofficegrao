﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_enderecamentoArea : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
            BindData();
    }
    void BindData()
    {
        //CsModulo modulo = new CsModulo();
        //DataTable table = new DataTable();
        var data = new dbCommerceDataContext();
        var areas = (from c in data.tbEnderecamentoAreas
                     select c).ToList();

       
        if (areas.Count > 0)
        {
            GridView1.DataSource = areas;
            GridView1.DataBind();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //CsModulo modulo = new CsModulo();
        //int cd_modulo = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
        //modulo.excluir(cd_modulo);


        //string meuscript;
        //if (!modulo.Tem_erro)
        //{
        //    meuscript = @"alert('Módulo excluído com sucesso');window.location='modulolistar.aspx';";
        //}
        //else
        //{

        //    meuscript = @"alert('" + modulo.Error + "');";
        //}

        //Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton MyButton = (LinkButton)e.Row.FindControl("cmdDelete");
            MyButton.Attributes.Add("onclick", "javascript:return " +
            "confirm('Confirma a exclusão do item " +
            DataBinder.Eval(e.Row.DataItem, "area") + "?')");
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;

        BindData();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        //string cd_modulo = GridView1.DataKeys[e.RowIndex].Value.ToString();
        //string nm_modulo = ((TextBox)GridView1.Rows[e.RowIndex].FindControl("TextBox1")).Text;
        //GridView1.EditIndex = -1;

        //CsModulo modulo = new CsModulo();
        //modulo.atualizar(Convert.ToInt32(cd_modulo), nm_modulo.ToString());
        //string meuscript;
        //if (!modulo.Tem_erro)
        //{
        //    meuscript = @"alert('Módulo atualizado com sucesso');window.location='modulolistar.aspx';";
        //}
        //else
        //{

        //    meuscript = @"alert('" + modulo.Error + "');";
        //}

        //Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {

        GridView1.EditIndex = e.NewEditIndex;
        BindData();
    }
}