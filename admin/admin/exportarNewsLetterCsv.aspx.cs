﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_exportarNewsLetterCsv : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid();
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    private void fillGrid()
    {
        using (var data = new dbCommerceDataContext())
        {

            var lista1 = (from c in data.tbClientes
                          join n in data.tbNewsletters on c.clienteEmail equals n.email
                          select new
                          {
                              n.email,
                              n.dataCadastro,
                              c.clienteId
                          });

            var cadastrosNewsSemCadastroTbCliente = (from n in data.tbNewsletters
                                                     join c in data.tbClientes on n.email equals c.clienteEmail into nc
                                                     from c in nc.DefaultIfEmpty()
                                                     where !nc.Select(x => x.clienteId).Any()
                                                     select new
                                                     {
                                                         Email = n.email,
                                                         Data = n.dataCadastro
                                                     }).ToList();


            var cadastradosNewsFizeramCompraLista = (from c in lista1
                                                     join p in data.tbPedidos on c.clienteId equals p.clienteId
                                                     select new
                                                     {
                                                         Email = c.email,
                                                         Data = p.dataHoraDoPedido
                                                     });


            var cadastradosNewsFizeramCompra = (from c in cadastradosNewsFizeramCompraLista
                                                group c by new { c.Email }
                                                into listaPersonalizada
                                                select new
                                                {
                                                    listaPersonalizada.Key.Email,
                                                    Data = listaPersonalizada.Max(x => x.Data ?? DateTime.Now)
                                                }).ToList();

            var cadastradosNewsSemCompraLista = (from c in lista1
                                                 join p in data.tbPedidos on c.clienteId equals p.clienteId into nsc
                                                 from p in nsc.DefaultIfEmpty()
                                                 where !nsc.Select(x => x.clienteId).Any()
                                                 select new
                                                 {
                                                     Email = c.email,
                                                     Data = c.dataCadastro
                                                 });

            var listaFinal = cadastrosNewsSemCadastroTbCliente.Union(cadastradosNewsFizeramCompra).Union(cadastradosNewsSemCompraLista);

            grd.DataSource = listaFinal;
            grd.DataBind();

        }
    }
}