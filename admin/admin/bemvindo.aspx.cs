using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_bemvindo : System.Web.UI.Page
{

    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            lblUsuario.Text = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        }
    }

    protected void btnPesquisar_OnClick(object sender, EventArgs e)
    {
        var pedidoContext = new dbCommerceDataContext();
        int pedidoId = Convert.ToInt32("0" + txtBusca.Text);
        if (pedidoId.ToString().Length != 5)
        {
            pedidoId = rnFuncoes.retornaIdInterno(pedidoId);
        }
        var pedido = (from c in pedidoContext.tbPedidos where c.pedidoId == pedidoId select c).FirstOrDefault();

        if (pedido != null)
            Response.Redirect("~/admin/pedido.aspx?pedidoId=" + pedido.pedidoId);
        else
            Response.Write("<script>alert('C�digo de produto n�o encontrado.');</script>");
    }
}