﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using SpreadsheetLight;
using System.Web.UI.HtmlControls;
public partial class admin_relatorioTransportadora2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
        }

    }
    void carregaGrid()
    {
        DataTable table = getDados();
        if (table.Rows.Count == 0)
        {
            grdRelatorioTransportadora.DataSource = null;
        }
        else
        {
            grdRelatorioTransportadora.DataSource = table;
        }
        grdRelatorioTransportadora.DataBind();
        lblitensencontrados.Text = table.Rows.Count.ToString();
    }

    protected void grdRelatorioTransportadora_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grdRelatorioTransportadora.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
      //  if (validarcampos())
            carregaGrid();
    }

    DataTable getDados()
    {
        string connectionString = ConfigurationManager.ConnectionStrings["connectionString"].ToString();
        DataTable table = new DataTable();
        using (
                SqlConnection conn =
                    new SqlConnection(connectionString))
        {
            DateTime dataInicial = new DateTime();
            DateTime dataFinal = new DateTime();
            string subquery = " where 1=1 ";
            if (!String.IsNullOrEmpty(txtDataInicial.Text) && !String.IsNullOrEmpty(txtDataInicial.Text))
            {
                dataInicial = Convert.ToDateTime(txtDataInicial.Text);
                dataFinal = Convert.ToDateTime(txtDataFinal.Text);
                subquery += " and ( dataHoraDoPedido >= @dataInicial and  dataHoraDoPedido <= @dataFinal) ";
            }

            string formadeenvio = "";
            if(ddlFormadeenvio.SelectedItem.Value !="")
            {
                formadeenvio = ddlFormadeenvio.SelectedItem.Value;
                subquery += "and formadeenvio = @formadeenvio ";
            }
            SqlCommand command = conn.CreateCommand();
            command.Connection.Open();
            command.CommandType = CommandType.Text;
            command.CommandText = "select * from vw_Consulta_Trans_Geral "+subquery;

            //command.Parameters.AddWithValue("@dataInicial", "2014-04-22");
            command.Parameters.AddWithValue("@dataInicial", dataInicial.Year + "-" + dataInicial.Month + "-" + dataInicial.Day+" 00:00:00");
            //command.Parameters.AddWithValue("@dataFinal", "2014-04-22");
            command.Parameters.AddWithValue("@dataFinal", dataFinal.Year + "-" + dataFinal.Month + "-" + dataFinal.Day + " 23:59:59");
            command.Parameters.AddWithValue("formadeenvio", formadeenvio);

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            table.Load(reader);
        }
        return table;
    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        //if (!String.IsNullOrEmpty(txtQtdMaxima.Text))
        //{
        //    try { int teste = Convert.ToInt32(txtQtdMaxima.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Quantidade Máxima deve ser um número\n";
        //        valido = false;
        //    }
        //}

        //if (!String.IsNullOrEmpty(txtPrecoDe.Text))
        //{
        //    try { decimal teste = Convert.ToDecimal(txtPrecoDe.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Preço de deve ser um número\n";
        //        valido = false;
        //    }
        //}
        //if (!String.IsNullOrEmpty(txtPrecoAte.Text))
        //{
        //    try { decimal teste = Convert.ToDecimal(txtPrecoAte.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Preço até deve ser um número\n";
        //        valido = false;
        //    }
        //}

        //if (!String.IsNullOrEmpty(txtPrecoPromocionalDe.Text))
        //{
        //    try { decimal teste = Convert.ToDecimal(txtPrecoPromocionalDe.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Preço Promociona de deve ser um número\n";
        //        valido = false;
        //    }
        //}
        //if (!String.IsNullOrEmpty(txtPrecoPromocionalAte.Text))
        //{
        //    try { decimal teste = Convert.ToDecimal(txtPrecoPromocionalAte.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"Preço Promociona até deve ser um número\n";
        //        valido = false;
        //    }
        //}
        //if (!String.IsNullOrEmpty(txtProdutoId.Text))
        //{
        //    try { int teste = Convert.ToInt32(txtProdutoId.Text); }
        //    catch (Exception)
        //    {
        //        erro += @"O ID do Produto deve ser um número\n";
        //        valido = false;
        //    }
        //}
        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }
        else
        {
            erro += @"Digite a Data inicial\n";
            valido = false;
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }
        else
        {
            erro += @"Digite a Data final\n";
            valido = false;
        }


        //if (erro != "")
        //{
        //    string meuscript = @"alert('" + erro + "');";
        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        //}
        return valido;
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            DataTable itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=relatorio_transportadora.xlsx");

            sl.SetCellValue("A1", "idPedido");
            sl.SetCellValue("B1", "idpedidoenvio");
            sl.SetCellValue("C1", "Data Pedido");
            sl.SetCellValue("D1", "Fim Separacao");
            sl.SetCellValue("E1", "Total Geral");
            sl.SetCellValue("F1", "UF");
            sl.SetCellValue("G1", "Cidade");
            sl.SetCellValue("H1", "CEP");
            sl.SetCellValue("I1", "Prazo Entrega");
            sl.SetCellValue("J1", "Forma Envio");
            sl.SetCellValue("K1", "Valor Selec.");
            sl.SetCellValue("L1", "Valor");
            sl.SetCellValue("M1", "Prazo");


            int linha = 2;
            foreach (DataRow item in itens.Rows)
            {
                sl.SetCellValue(linha, 1, item["idpedido"].ToString());
                sl.SetCellValue(linha, 2, item["idpedidoenvio"].ToString());
                sl.SetCellValue(linha, 3, item["dataHoraDoPedido"].ToString());
                sl.SetCellValue(linha, 4, item["dataFimSeparacao"].ToString());

                Decimal valorTotalGeral = 0;
                try { valorTotalGeral = Convert.ToDecimal(item["valorTotalGeral"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                sl.SetCellValue(linha, 5, valorTotalGeral.ToString("C"));

                sl.SetCellValue(linha, 6, item["endEstado"].ToString());
                sl.SetCellValue(linha, 7, item["endCidade"].ToString());
                sl.SetCellValue(linha, 8, item["endCep"].ToString());
                sl.SetCellValue(linha, 9, item["prazoDeEntrega"].ToString());
                sl.SetCellValue(linha, 10, item["endCep"].ToString());
                sl.SetCellValue(linha, 11, item["formadeenvio"].ToString());
                Decimal valorSelecionado = 0;
                try { valorSelecionado = Convert.ToDecimal(item["valorSelecionado"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                sl.SetCellValue(linha, 11, valorSelecionado.ToString("C"));

                Decimal valor = 0;
                try { valor = Convert.ToDecimal(item["valor"].ToString().Replace(".", ",")); }
                catch (Exception) { }

                sl.SetCellValue(linha, 12, valor.ToString("C"));

                sl.SetCellValue(linha, 13, item["prazo"].ToString());

                 linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }

}