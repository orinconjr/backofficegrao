﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="cadastroProcessosFabrica.aspx.cs" Inherits="admin_cadastroProcessosFabrica" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-table.css" type="text/css" />
    <style type="text/css">
        .classehoje a {
            color: #FFFFFF;
        }

            .classehoje a:hover {
                color: #FFFFFF;
            }

            .classehoje a:active {
                color: #FFFFFF;
            }

            .classehoje a:visited {
                color: #FFFFFF;
            }

        .soimprimir {
            display: none;
        }

        .borderN {
            border: 0px;
        }

        .borderR {
            border-right: 2px solid black;
        }

        @media print {
            #itensDoRomaneio {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
        }

        #tbProcessoAtual tr:nth-child(odd) {
            background-color: #DEDEDE;
        }

        .tabela {
            border-spacing: 0;
        }

            .tabela td {
                vertical-align: top;
                padding: 2px;
            }

            .tabela tr:nth-child(odd) {
                background-color: #DEDEDE;
            }

            .tabela, .tabela th, .tabela td {
                border: 1px solid #BBB;
            }

        .tabela {
            border-bottom: 0;
            border-left: 0;
        }

            .tabela th {
                font-weight: bold;
                font-size: 14px;
            }

            .tabela td, .tabela th {
                border-top: 0;
                border-right: 0;
            }
    </style>
    <script type="text/javascript">
        function checaChecks(campo) {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf(campo) > -1) {
                    div.checked = true;
                }
            }
        }
        function checaEstoque() {
            var divs = document.querySelectorAll("[id]");
            for (var i = 0, len = divs.length; i < len; i++) {
                var div = divs[i];
                if (div.id.indexOf("chkChecar") > -1) {
                    div.checked = true;
                }
            }
        }
    </script>
    <div class="tituloPaginas" valign="top">
        Andamento da Fábrica
    </div>
    <div>
        <table>
            <tr class="rotulos">
                <td>&nbsp;<br />
                    <asp:CheckBox runat="server" ID="chkAtivo" Text="Ativo" />
                </td>
                <td>Nome:<br />
                    <asp:TextBox runat="server" ID="txtNome"></asp:TextBox>
                </td>
                <td>Produto(s)Id(s)<br />
                    <asp:TextBox runat="server" ID="idsProdutos"></asp:TextBox>
                </td>
                <td>Categoria:<br />
                    <asp:DropDownList runat="server" ID="ddlCategoria" DataValueField="categoriaId" DataTextField="categoriaNome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
                <td>Processos:<br />
                    <asp:DropDownList runat="server" ID="ddlProcessosFabrica" DataValueField="idSubProcessoFabrica" DataTextField="nome" AppendDataBoundItems="True">
                        <Items>
                            <asp:ListItem Text="Selecione" Value=""></asp:ListItem>
                        </Items>
                    </asp:DropDownList>
                </td>
                <td>&nbsp;<br />
                    <asp:Button runat="server" ID="btnFiltrar" Text="Filtrar" OnClick="btnFiltrar_OnClick" />
                </td>
                <td>&nbsp;<br />
                    Total: 
                    <asp:Literal runat="server" ID="litTotal"></asp:Literal>
                </td>
            </tr>
            <tr class="rotulos">
                <td>&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
    </div>
    <div>
        <table class="rotulos tabela" width="1300px">
            <tr>
                <td></td>
                <td>Checar
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstEstoqueC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstMateriaC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstCorteC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstBordadoC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstCosturaC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstColaC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstArremateC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstEnchimentoC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstEmbalagemC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
                <td>
                    <asp:ListView runat="server" ID="lstEntregaC">
                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="btnChecar" OnCommand="btnChecar_OnCommand" CommandArgument='<%#Eval("idSubProcessoFabrica") %>' Text='<%# Eval("nome") %>'></asp:LinkButton><br />
                        </ItemTemplate>
                    </asp:ListView>
                </td>
            </tr>
            <tr>
                <th style="width: 206px;">Foto
                </th>
                <th style="width: 85px;">Nome
                </th>
                <th style="width: 75px;">Estoque
                </th>
                <th style="width: 99px;">Separação Matéria Prima
                </th>
                <th style="width: 120px;">Corte
                </th>
                <th style="width: 133px;">Bordado
                </th>
                <th style="width: 134px;">Costura
                </th>
                <th style="width: 108px">Cola
                </th>
                <th style="width: 66px;">Arremate
                </th>
                <th style="width: 84px;">Enchimento
                </th>
                <th style="width: 85px;">Embalagem
                </th>
                <th>Entrega
                </th>
            </tr>
            <asp:ListView runat="server" ID="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound" OnDataBound="lstProdutos_OnDataBound" Visible="False">
                <ItemTemplate>
                    <tr>
                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                        <asp:HiddenField runat="server" ID="hdfFoto" Value='<%# Eval("fotoDestaque") %>' />
                        <td>
                            <asp:Image runat="server" ID="imgFoto" Width="200" />
                        </td>
                        <td>
                            <%# Eval("produtoNome") + " </br>Ref.:" + Eval("produtoId") %>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstEstoque">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkEstoque" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstMateria">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkMateria" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstCorte">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkCorte" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstBordado">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkBordado" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstCostura">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkCostura" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstCola">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkCola" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstArremate">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkArremate" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstEnchimento">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkEnchimento" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstEmbalagem">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkEmbalagem" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                        <td>
                            <asp:ListView runat="server" ID="lstEntrega">
                                <ItemTemplate>
                                    <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                    <asp:CheckBox runat="server" ID="chkEntrega" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                                </ItemTemplate>
                            </asp:ListView>
                        </td>
                    </tr>
                </ItemTemplate>
                <%--<LayoutTemplate>
                    <asp:DataPager runat="server" ID="ItemDataPager" PagedControlID="lstProdutos" PageSize="20">
                        <Fields>
                            <asp:NumericPagerField ButtonCount="10" />
                        </Fields>
                    </asp:DataPager>
                    <tr id="itemPlaceholder" runat="server"></tr>
                </LayoutTemplate>--%>
            </asp:ListView>




        </table>
        <asp:GridView ID="GridView1" CssClass="rotulos tabela" runat="server" Width="1300" DataKeyNames="produtoId" AutoGenerateColumns="False"
            AllowPaging="True" OnDataBound="GridView1_OnDataBound" OnRowDataBound="GridView1_OnRowDataBound" OnPageIndexChanging="GridView1_OnPageIndexChanging" ShowHeader="False"
            PageSize="40">
            <Columns>

                <asp:TemplateField HeaderText="Foto" ItemStyle-Width="202">
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>' />
                        <asp:HiddenField runat="server" ID="hdfFoto" Value='<%# Eval("fotoDestaque") %>' />
                        <asp:Image runat="server" ID="imgFoto" Width="200" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Nome" ItemStyle-Width="82">
                    <ItemTemplate>
                        <%# Eval("produtoNome") + " </br>Ref.:" + Eval("produtoId") %><br /><br />
                        <b style="width: 1.2em"><%# Eval("produtoAtivo").ToString().ToLower() == "true" ? "Ativo" : "Inativo" %></b>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Estoque" ItemStyle-Width="73px">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstEstoque">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkEstoque" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Separação Matéria Prima" ItemStyle-Width="96px">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstMateria">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkMateria" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Corte" ItemStyle-Width="117px">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstCorte">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkCorte" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Bordado" ItemStyle-Width="130px">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstBordado">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkBordado" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Costura" ItemStyle-Width="130px">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstCostura">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkCostura" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Cola" ItemStyle-Width="105">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstCola">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkCola" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Arremate" ItemStyle-Width="63">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstArremate">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkArremate" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Enchimento" ItemStyle-Width="81">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstEnchimento">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkEnchimento" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Embalagem" ItemStyle-Width="81">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstEmbalagem">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkEmbalagem" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Entrega" ItemStyle-Width="64">
                    <ItemTemplate>
                        <asp:ListView runat="server" ID="lstEntrega">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdfSubProcesso" Value='<%#Eval("idSubProcessoFabrica") %>' />
                                <asp:CheckBox runat="server" ID="chkEntrega" Text='<%# Eval("nome") %>'></asp:CheckBox><br />
                            </ItemTemplate>
                        </asp:ListView>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: right; padding-top: 15px">
        <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
    </div>



</asp:Content>
