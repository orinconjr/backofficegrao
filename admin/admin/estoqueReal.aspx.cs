﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_estoqueReal : System.Web.UI.Page
{
    private static int produtoId;
    protected void Page_Load(object sender, EventArgs e)
    {
        sql.SelectMethod = "produtoEstoqueRealNegativo";

        if (Request.QueryString["reposicao"] != null)
        {
            sql.SelectMethod = "produtosReposicaoEstoque";
        }
        if (Request.QueryString["negativo"] != null)
        {
            sql.SelectMethod = "produtoEstoqueRealNegativo";
        }
    }

    protected void grdDetalhes_BeforePerformDataSelect(object sender, EventArgs e)
    {
        Session["produtoId"] = (sender as ASPxGridView).GetMasterRowKeyValue();
        produtoId = Convert.ToInt32((sender as ASPxGridView).GetMasterRowKeyValue());
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
        if (e.Column.FieldName == "idPedidoFornecedor")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int produtoId = Convert.ToInt32(Session["produtoId"]);
            var pedidoDc = new dbCommerceDataContext();
            var pedidoFornecedor =
                (from c in pedidoDc.tbPedidoFornecedorItems
                    where c.idPedido == pedidoId && c.idProduto == produtoId
                    select c).FirstOrDefault();
            if (pedidoFornecedor != null)
            {
                e.Value = pedidoFornecedor.idPedidoFornecedor;
                if (!pedidoFornecedor.entregue && (pedidoFornecedor.tbPedidoFornecedor.dataLimite.Date <= DateTime.Now.Date))
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Atrasado";
                }
                if (pedidoFornecedor.entregue)
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Entregue";
                }
                if (pedidoFornecedor.tbPedidoFornecedor.confirmado == null | pedidoFornecedor.tbPedidoFornecedor.confirmado == false)
                {
                    e.Value = pedidoFornecedor.idPedidoFornecedor + " - Não confirmado";
                }
            }
        }
        if (e.Column.FieldName == "reserva")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            int produtoId = Convert.ToInt32(Session["produtoId"]);
            var pedidoDc = new dbCommerceDataContext();
            var reserva =
                (from c in pedidoDc.tbProdutoReservaEstoques
                 where c.idPedido == pedidoId && c.idProduto == produtoId
                 select c).FirstOrDefault();
            if (reserva != null)
            {
                e.Value = reserva.idProdutoReservaEstoque;
            }

        }
    }

    protected void grd1_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "ultimoPedido")
        {
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                e.Value = rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows[0]["dataDaCriacao"];
            }
            else
            {
                e.Value = "";
            }
        }
        if (e.Column.FieldName == "ultimaCompra")
        {
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                e.Value = rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows[0]["dataPedido"];
            }
            else
            {
                e.Value = "";
            }
        }
        if (e.Column.FieldName == "diasEstocado")
        {
            bool possuiValores = true;
            int produtoId = Convert.ToInt32(e.GetListSourceFieldValue("produtoId"));
            if (rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows.Count > 0)
            {
                try
                {
                    var dataPedido = Convert.ToDateTime(rnProdutos.dataUltimoPedidoDoProduto(produtoId).Tables[0].Rows[0]["dataDaCriacao"]);
                    if (rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows.Count > 0)
                    {
                        try
                        {
                            var dataCompra = Convert.ToDateTime(rnProdutos.dataUltimaCompraDoProduto(produtoId).Tables[0].Rows[0]["dataPedido"]);
                            if (dataCompra > dataPedido)
                            {
                                e.Value = (DateTime.Now - dataCompra).Days;
                            }
                            else
                            {
                                e.Value = (DateTime.Now - dataPedido).Days;
                            }
                        }
                        catch (Exception)
                        {
                            possuiValores = false;
                        }
                    }
                }
                catch (Exception)
                {
                    possuiValores = false;
                }
            }
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}