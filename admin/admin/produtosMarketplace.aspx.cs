﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using System.Collections.Generic;
using GraoDeGente.Infrastructure.Services.Marketplace.Interfaces;
using GraoDeGente.Infrastructure.Services.Marketplace;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.Products;
using GraoDeGente.Infrastructure.Services.Marketplace.Models;
using DevExpress.Web.ASPxEditors;
using System.Linq.Expressions;
using GraoDeGente.Infrastructure.Services.Marketplace.Models.SellerItems;
using GraoDeGente.Infrastructure.Services.Marketplace.SellerItems;
using GraoDeGente.Infrastructure.Services.Marketplace.Products;

public partial class admin_produtosMarketplace : System.Web.UI.Page
{
    private readonly ISyncResource _syncResource = new SyncResource();
    private readonly ISellerItemsResource _sellerItemsResource = new SellerItemsResource();

    protected void Page_Load(object sender, EventArgs e)
    {
        ASPxGridView.RegisterBaseScript(Page);

        if (!Page.IsPostBack)
        {
            PopularRooLevelCategoriasFiltros();
            PopulaRooLevelCategoriasAtivasNoSite();
        }

        PopularGrid();
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnExportarXls_Click(object sender, EventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }

    protected void btnEnviarProdutos_Click(object sender, EventArgs e)
    {
        var produtosSelecionados = this.obterProdutosSelecionados();

        if (!produtosSelecionados.Any())
        {
            this.Alert("Você deve selecionar algum produto.");
            return;
        }

        var request = new CreateProductsRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado(),
            Products = produtosSelecionados.Select(i => new Product { Id = i.ProdutoId }).ToList()
        };

        var response = this._syncResource.PostProducts(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao enviar produtos. ", response.Exception.Message));
            return;
        }

        this.Alert("Produtos enviados com sucesso!");

        PopularGrid();
    }

    protected void btnAtualizarPrecos_Click(object sender, EventArgs e)
    {
        var produtosSelecionados = this.obterProdutosSelecionados();

        if (!produtosSelecionados.Any())
        {
            this.Alert("Você deve selecionar algum produto.");
            return;
        }

        var request = new UpdateSellerItemsPricesRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado(),
            Products = produtosSelecionados.Select(i => new Product { Id = i.ProdutoId }).ToList()
        };

        var response = this._syncResource.PostSellerItemsPrices(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao atualizar preços. ", response.Exception.Message));
            return;
        }

        this.Alert("Preços atualizados com sucesso!");

        PopularGrid();
    }

    protected void btnAtualizarEstoque_Click(object sender, EventArgs e)
    {
        var produtosSelecionados = this.obterProdutosSelecionados();

        if (!produtosSelecionados.Any())
        {
            this.Alert("Você deve selecionar algum produto.");
            return;
        }

        this.AtualizarDataUltimaAtualizacaoDoEstoque(produtosSelecionados);

        var request = new SyncSellerItemsStockRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado()
        };

        var response = this._syncResource.PostSellerItemsStock(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao atualizar estoque. ", response.Exception.Message));
            return;
        }

        this.Alert("Estoque atualizado com sucesso!");

        PopularGrid();
    }

    private void AtualizarDataUltimaAtualizacaoDoEstoque(List<ProdutoDTO> produtosSelecionados)
    {
        var idsProdutos = produtosSelecionados.Select(i => i.ProdutoId);

        using (var dbContext = new dbCommerceDataContext())
        {
            var produtos = dbContext.tbProdutos
             .Where(i => idsProdutos.Contains(i.produtoId));

            foreach (var produto in produtos)
            {
                produto.ultimaAtualizacaoDoEstoque = DateTime.Now;
            }

            dbContext.SubmitChanges();
        }
    }

    protected void btnAlterarDisponibilidade_OnCommand(object sender, CommandEventArgs e)
    {
        hdfProdutoId.Value = e.CommandArgument.ToString();
        popDisponibilidade.ShowOnPageLoad = true;
        
        var marketplace = this.obterMarketplaceSelecionado();
        var produtoId = Convert.ToInt32(e.CommandArgument);
        var disponibilidade = this.obterDisponibilidade(marketplace, produtoId);

        chkDisponibilidade.Items.Clear();
        foreach (var item in disponibilidade)
            chkDisponibilidade.Items.Add(new ListItem {  Value = item.Codigo, Text = item.Nome, Selected = item.Ativo });
    }

    protected void btnAtualizarDisponibilidade_Click(object sender, EventArgs e)
    {
        var produtoId = Convert.ToInt32(hdfProdutoId.Value);

        var status = new Dictionary<string, bool>();
        foreach (ListItem item in chkDisponibilidade.Items)
            status.Add(item.Value, item.Selected);

        if (!status.Any())
        {
            this.Alert("Você deve selecionar algum site.");
            return;
        }

        var request = new UpdateSellerItemsStatusRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado(),
            Status = new List<SellerItemStatus>
            {
                new SellerItemStatus
                {
                    ProductId = produtoId,
                    Status = status.Select(i => new SiteStatus { SiteCode = i.Key,  Active = i.Value }).ToList()
                }
            } 
        };

        var response = this._sellerItemsResource.PostStatus(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao atualizar status do produto. ", response.Exception.Message));
            return;
        }

        this.popDisponibilidade.ShowOnPageLoad = false;
        this.Alert("Status atualizado com sucesso!");
    }

    protected void btnAtualizarStatusIntegracao_Click(object sender, EventArgs e)
    {
        var request = new SyncProductsStatusRequest()
        {
            Marketplace = this.obterMarketplaceSelecionado()
        };

        var response = this._syncResource.PostProductsStatus(request);
        if (!response.Success)
        {
            this.Alert(string.Concat("Ocorreu um erro ao atualizar status da integração. ", response.Exception.Message));
            return;
        }

        PopularGrid();

        this.Alert("Status atualizado com sucesso!");
    }

    private void PopularGrid()
    {
        grd.DataSource = this.obterProdutos();
        grd.DataBind();
    }

    private List<ProdutoDTO> obterProdutos()
    {

        int? categoriaId = null;
        int? filtroId = null;
        byte? marketplace = null;
        byte? statusIntegracao = null;
        string statusProduto = null;

        if (treeCategoriasAtivasNoSite.SelectedValue != "0")
            categoriaId = Convert.ToInt32(treeCategoriasAtivasNoSite.SelectedValue);

        if (treeCategorias.SelectedValue != "0")
            filtroId = Convert.ToInt32(treeCategorias.SelectedValue);

        if (!string.IsNullOrEmpty(ddlMarketplace.SelectedValue))
            marketplace = Convert.ToByte(ddlMarketplace.SelectedValue);

        if (!string.IsNullOrEmpty(ddlStatusIntegracao.SelectedValue))
            statusIntegracao = Convert.ToByte(ddlStatusIntegracao.SelectedValue);

        if (!string.IsNullOrEmpty(ddlStatusProduto.SelectedValue))
            statusProduto = ddlMarketplace.SelectedValue == "1" ? "True" : "False";

        Expression<Func<tbProduto, bool>> predicate = c =>
            (!categoriaId.HasValue || c.tbJuncaoProdutoCategorias.Any(a => a.categoriaId == categoriaId)) &&
            (!filtroId.HasValue || c.tbJuncaoProdutoCategorias.Any(a => a.categoriaId == filtroId)) &&
            (!marketplace.HasValue || !c.Marketplace_Produtos.Any() || c.Marketplace_Produtos.Any(a => a.Marketplace == marketplace)) &&
            (string.IsNullOrEmpty(statusProduto) || c.produtoAtivo == statusProduto);

        using (var dbContext = new dbCommerceDataContext())
        {
            var produtos = dbContext.tbProdutos
             .Where(predicate);

            if (statusIntegracao.HasValue)
            {
                produtos = statusIntegracao == 99
                    ? produtos.Where(c => !c.Marketplace_Produtos.Any())
                    : produtos.Where(c => c.Marketplace_Produtos.Any(a => a.Status == statusIntegracao));
            }

            return produtos
                .Select(s => new ProdutoDTO
                {
                    ProdutoId = s.produtoId,
                    Categoria = s.tbJuncaoProdutoCategorias.Select(c => c.tbProdutoCategoria.categoriaUrl).FirstOrDefault(),
                    ProdutoUrl = s.produtoUrl,
                    ProdutoNome = s.produtoNome,
                    ProdutoFornecedor = s.tbProdutoFornecedor.fornecedorNome,
                    FornecedorId = s.tbProdutoFornecedor.fornecedorId,
                    ProdutoMarca = s.tbMarca.marcaNome,
                    StatusIntegracao = obterStatusIntegracao(s.Marketplace_Produtos.ToList())
                })
                .ToList();
        }
    }

    private string obterStatusIntegracao(List<Marketplace_Produto> marketplaces)
    {
        var marketplace = marketplaces.FirstOrDefault(a => a.Marketplace == (byte)this.obterMarketplaceSelecionado());
        if (marketplace == null)
            return "Não Enviado";

        switch (marketplace.Status)
        {
            case (byte)IntegrationStatus.Pending:
                return "Pendente";
            case (byte)IntegrationStatus.Integrated:
                return "Integrado";
            case (byte)IntegrationStatus.Error:
                return "Erro";
            default:
                break;
        }

        return null;
    }

    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulaRooLevelCategoriasAtivasNoSite()
    {
        using (var data = new dbCommerceDataContext())
        {
            var categoriasAtivasNoSite = (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c).ToList();

            foreach (var categoria in categoriasAtivasNoSite)
            {
                TreeNode tn = new TreeNode { Text = categoria.categoriaNome, Value = categoria.categoriaId.ToString() };

                treeCategoriasAtivasNoSite.Nodes.Add(tn);
            }
        }
    }

    private void PopularRooLevelCategoriasFiltros()
    {
        using (var data = new dbCommerceDataContext())
        {
            List<int> categoriasFiltrosPai = (from c in data.tbProdutoCategorias
                                              where c.categoriaPaiId == 0 && c.exibirSite == false
                                              orderby c.categoriaNome
                                              select new { c.categoriaId }).Select(x => x.categoriaId).ToList();

            var categoriasComFiltros = (from c in data.tbProdutoCategorias
                                        where categoriasFiltrosPai.Contains(c.categoriaPaiId)
                                        orderby c.categoriaNome
                                        select new { c.categoriaPaiId }).Distinct().Select(x => x.categoriaPaiId).ToList();

            var categoriasFiltros =
                (from c in data.tbProdutoCategorias
                 where categoriasComFiltros.Contains(c.categoriaId)
                 orderby c.categoriaNome
                 select c);

            foreach (var categoria in categoriasFiltros)
            {
                TreeNode tn = new TreeNode { Text = categoria.categoriaNome, Value = categoria.categoriaId.ToString() };

                treeCategoriasFiltros.Nodes.Add(tn);

                tn.PopulateOnDemand = true;
            }
        }
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }


    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }

    private List<DisponibilidadeDTO> obterDisponibilidade(MarketplaceEnum marketplace, int produtoId)
    {

        var statusProduto = obterStatusProduto(marketplace, produtoId);
        var codigoDosSitesAtivos = statusProduto.Select(i => i.SiteCode).ToList();
        var disponibilidade = new List<DisponibilidadeDTO>();

        using (var dbContext = new dbCommerceDataContext())
        {
            disponibilidade = dbContext.Marketplace_Sites
                .Where(i => i.Marketplace == (byte)marketplace)
                .Select(s => new DisponibilidadeDTO
                {
                    Codigo = s.Codigo,
                    Nome = s.Nome,
                    Ativo = codigoDosSitesAtivos.Contains(s.Codigo)
                })
                .ToList();
        }

        return disponibilidade;
    }

    private List<SiteStatus> obterStatusProduto(MarketplaceEnum marketplace, int produtoId)
    {
        var request = new GetSellerItemStatusRequest()
        {
            Marketplace = marketplace,
            ProductId = produtoId
        };

        var response = _sellerItemsResource.GetStatus(request);
        return response.Status ?? new List<SiteStatus>();
    }

    private List<ProdutoDTO> obterProdutosSelecionados()
    {
        var produtosSelecionados = new List<ProdutoDTO>();
        for (int i = 0; i < grd.VisibleRowCount; i++)
        {
            ProdutoDTO produto = grd.GetRow(i) as ProdutoDTO;
            ASPxCheckBox chkSelecionado = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["selecionado"] as GridViewDataColumn, "chkSelecionado");
            if (chkSelecionado != null)
            {
                ((IPostBackDataHandler)chkSelecionado).LoadPostData(chkSelecionado.UniqueID, Request.Form);

                if (chkSelecionado.Checked)
                {
                    produtosSelecionados.Add(produto);
                }
            } 
        }
        return produtosSelecionados;
    }

    private MarketplaceEnum obterMarketplaceSelecionado()
    {
        var marketplace = Convert.ToByte(ddlMarketplace.SelectedValue);
        return (MarketplaceEnum)marketplace;
    }

    private void Alert(string message)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), string.Format("alert('{0}');", message), true);
    }

    public class ProdutoDTO
    {
        public int ProdutoId { get; set; }
        public string Categoria { get; set; }
        public string ProdutoUrl { get; set; }
        public string ProdutoNome { get; set; }
        public string ProdutoFornecedor { get; set; }
        public int? FornecedorId { get; set; }
        public string ProdutoMarca { get; set; }
        public string StatusIntegracao { get; set; }
        public bool Selecionado { get; set; }
    }

    public class DisponibilidadeDTO
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
    }
}
