﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_vincularEtiqueta : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnVincularEtiqueta_OnClick(object sender, EventArgs e)
    {

        try
        {
            int etiqueta = Convert.ToInt32(txtEtiqueta.Text);

            using (var data = new dbCommerceDataContext())
            {
                var verificacao = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == etiqueta select c).FirstOrDefault();

                if (verificacao != null)
                    if (verificacao.pedidoIdReserva == null && verificacao.idItemPedidoEstoqueReserva == null && verificacao.idItemPedidoEstoqueReserva == null)
                    {
                        ProcessoVincularEtiqueta(etiqueta);
                    }
                    else
                    {
                        string situacaoAtual = (from c in data.tbPedidos where c.pedidoId == verificacao.pedidoIdReserva select new { c.tbPedidoSituacao.situacao }).First().situacao;
                        string meuscript = @"if(confirm('A etiqueta " + etiqueta + " (" + verificacao.tbProduto.produtoNome + ") já está vinculada ao pedido " + verificacao.pedidoIdReserva + " que encontra-se na situação - " + situacaoAtual + " - deseja continuar com a vinculação? O pedido atual poderá ser atrasado, confirma?')){document.getElementById('" + btnAuxProcessoVincularEtiqueta.ClientID + "').click();}";
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "script", "$(function () {"+meuscript+" });", true);
                      
                    }
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

    }

    protected void btnAuxProcessoVincularEtiqueta_OnClick(object sender, EventArgs e)
    {
        int etiqueta = Convert.ToInt32(txtEtiqueta.Text);
        ProcessoVincularEtiqueta(etiqueta);
    }

    protected void ProcessoVincularEtiqueta(int etiqueta)
    {
        try
        {


            using (var data = new dbCommerceDataContext())
            {



                int itemPedido = Convert.ToInt32(txtIdItemPedido.Text);
                int idPedido = Convert.ToInt32(txtPedidoId.Text);
                var vincularEtiqueta = (from c in data.tbProdutoEstoques where c.idPedidoFornecedorItem == etiqueta select c).FirstOrDefault();
                vincularEtiqueta.pedidoIdReserva = idPedido;
                vincularEtiqueta.itemPedidoIdReserva = itemPedido;
                var itemPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                         where c.produtoId == vincularEtiqueta.produtoId && c.itemPedidoId == itemPedido && !c.reservado
                                         select c).FirstOrDefault();

                if (itemPedidoEstoque == null)
                {
                    var itemPedidoAtualizarStatus = (from c in data.tbItemPedidoEstoques
                                                     where c.produtoId == vincularEtiqueta.produtoId && c.itemPedidoId == itemPedido && c.reservado
                                                     select c).FirstOrDefault();

                    itemPedidoAtualizarStatus.reservado = false;
                    itemPedidoAtualizarStatus.dataReserva = null;
                    data.SubmitChanges();

                    itemPedidoEstoque = (from c in data.tbItemPedidoEstoques
                                         where c.produtoId == vincularEtiqueta.produtoId && c.itemPedidoId == itemPedido && !c.reservado
                                         select c).FirstOrDefault();

                    var desvincularEstoque =
                        (from c in data.tbProdutoEstoques
                         where c.idItemPedidoEstoqueReserva == itemPedidoEstoque.idItemPedidoEstoque
                         select c).FirstOrDefault();

                    if (desvincularEstoque != null)
                    {
                        desvincularEstoque.pedidoIdReserva = null;
                        desvincularEstoque.itemPedidoIdReserva = null;
                        desvincularEstoque.idItemPedidoEstoque = null;
                        desvincularEstoque.idItemPedidoEstoqueReserva = null;
                        data.SubmitChanges();
                    }
                }

                var removerReservaAtual = (from c in data.tbItemPedidoEstoques where c.idItemPedidoEstoque == vincularEtiqueta.idItemPedidoEstoqueReserva select c).FirstOrDefault();
                if (removerReservaAtual != null)
                {
                    removerReservaAtual.dataReserva = null;
                    removerReservaAtual.reservado = false;
                }

                vincularEtiqueta.idItemPedidoEstoqueReserva = itemPedidoEstoque.idItemPedidoEstoque;

                itemPedidoEstoque.reservado = true;
                itemPedidoEstoque.dataReserva = DateTime.Now;

                data.SubmitChanges();

                var checarCompleto = new tbQueue
                {
                    agendamento = DateTime.Now,
                    concluido = false,
                    andamento = false,
                    mensagem = "",
                    idRelacionado = idPedido,
                    tipoQueue = 8
                };
                data.tbQueues.InsertOnSubmit(checarCompleto);
                data.SubmitChanges();

            }

            Response.Write("<script>alert('Etiqueta vinculada com sucesso!');</script>");

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }


}