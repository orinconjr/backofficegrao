﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="clientesBack260417.aspx.cs" Inherits="admin_clientesBack260417"  Theme="Glass" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Clientes</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0" style="2">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                    DataSourceID="sqlCliente" KeyFieldName="clienteId" Width="834px" 
                    Cursor="auto" EnableCallBacks="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Cliente" FieldName="clienteId" 
                            VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="clienteNome" 
                            VisibleIndex="1">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="E-mail" FieldName="clienteEmail" VisibleIndex="2">
                            <Settings AutoFilterCondition="Contains" />
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RegularExpression ErrorText="Preencha corretamento o e-mail." 
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                    <RequiredField ErrorText="Preencha o email." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CPF/CNPJ" FieldName="clienteCPFCNPJ" 
                            VisibleIndex="2">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o CPF" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cliente desde" FieldName="dataDaCriacao" 
                            VisibleIndex="3">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Total em compras" FieldName="totalGasto" 
                            VisibleIndex="4">
                            <PropertiesTextEdit DisplayFormatString="c">
                            </PropertiesTextEdit>
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Histórico" VisibleIndex="5">
                            <EditFormSettings Visible="False" />
                            <DataItemTemplate>
                                <asp:HyperLink ID="hplHistorico" runat="server" 
                                    ImageUrl="~/admin/images/btNovo.jpg" 
                                    NavigateUrl='<%# "historicoDeCliente.aspx?clienteId=" + Eval("clienteId") %>'></asp:HyperLink>
                            </DataItemTemplate>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaHistorico.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn ButtonType="Image" 
                            VisibleIndex="6" Width="30px">
                            <EditButton Visible="True">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </EditButton>
                            <CancelButton>
                                <Image Url="~/admin/images/btCancelar.jpg" />
                            </CancelButton>
                            <UpdateButton>
                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                            </UpdateButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaEditar.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome da empresa" 
                            FieldName="clienteNomeDaEmpresa" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="RG/IE" FieldName="clienteRGIE" 
                            Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Senha" FieldName="clienteSenha" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha a senha." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn FieldName="clienteSexo" Visible="False" 
                            VisibleIndex="7">
                            <PropertiesComboBox ValueType="System.String">
                                <Items>
                                    <dxe:ListEditItem Text="Masculino" Value="masculino" />
                                    <dxe:ListEditItem Text="Feminino" Value="feminino" />
                                </Items>
                            </PropertiesComboBox>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Data de nascimento" 
                            FieldName="clienteDataNascimento" Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Estado civil" 
                            FieldName="clienteEstadoCivil" Visible="False" VisibleIndex="7">
                            <PropertiesComboBox ValueType="System.String">
                                <Items>
                                    <dxe:ListEditItem Text="Casado(a)" Value="Casado(a)" />
                                    <dxe:ListEditItem Text="Divirciado(a)" Value="Divirciado(a)" />
                                    <dxe:ListEditItem Text="Solteiro(a)" Value="Solteiro(a)" />
                                    <dxe:ListEditItem Text="Viuvo(a)" Value="Viuvo(a)" />
                                </Items>
                            </PropertiesComboBox>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fone residencial" 
                            FieldName="clienteFoneResidencial" Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <%--<ValidationSettings>
                                    <RequiredField ErrorText="Preencha o fone residencial." IsRequired="True" />
                                </ValidationSettings>--%>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fone comercial" 
                            FieldName="clienteFoneComercial" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fone celular" 
                            FieldName="clienteFoneCelular" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CEP" FieldName="clienteCep" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RegularExpression ErrorText="Preencha corretamente o cep." 
                                        ValidationExpression="^\d{5}(-\d{3})?$" />
                                    <RequiredField ErrorText="Preencha o cep." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Rua" FieldName="clienteRua" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha a rua." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Bairro" FieldName="clienteBairro" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o bairro." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Número" FieldName="clienteNumero" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha o número." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Complemento" 
                            FieldName="clienteComplemento" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Cidade" FieldName="clienteCidade" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="Preencha a cidade." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Estado" FieldName="clienteEstado" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesComboBox DataSourceID="sqlEstado" TextField="estadoSigla" 
                                ValueField="estadoSigla" ValueType="System.String">
                            </PropertiesComboBox>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Pais" FieldName="clientePais" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesComboBox DataSourceID="sqlPaises" TextField="paisNome" 
                                ValueField="paisId" ValueType="System.String">
                            </PropertiesComboBox>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Referencia para entrega" 
                            FieldName="clienteReferenciaParaEntrega" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Onde conheceu a loja" FieldName="ondeConheceuALoja" 
                            Visible="False" VisibleIndex="7">
                            <PropertiesComboBox TextField="paisNome" 
                                ValueField="paisId" ValueType="System.String">
                                <Items>
                                    <dxe:ListEditItem Text="Buscapé" Value="Buscapé" />
                                    <dxe:ListEditItem Text="Google" Value="Google" />
                                    <dxe:ListEditItem Text="Indicação" Value="Indicação" />
                                    <dxe:ListEditItem Text="Já sou cliente" Value="Já sou cliente" />
                                    <dxe:ListEditItem Text="Mídia impressa" Value="Mídia impressa" />
                                    <dxe:ListEditItem Text="Rádio" Value="Rádio" />
                                    <dxe:ListEditItem Text="Terra" Value="Terra" />
                                    <dxe:ListEditItem Text="TV" Value="TV" />
                                    <dxe:ListEditItem Text="Uol" Value="Uol" />
                                    <dxe:ListEditItem Text="Yahoo" Value="Yahoo" />
                                </Items>
                            </PropertiesComboBox>
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        
                        <dxwgv:GridViewDataTextColumn Caption="Melhor horário para contato" 
                            FieldName="melhorHorarioContato" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn Caption="Telefone para contato" 
                            FieldName="telRecado" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>
                        
                        <dxwgv:GridViewDataTextColumn Caption="Nome do contato" 
                            FieldName="nomeRecado" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataTextColumn>

                        <dxwgv:GridViewDataCheckColumn Caption="Aceita receber nossos informativos?" 
                            FieldName="clienteRecebeInformativo" Visible="False" VisibleIndex="7">
                            <EditFormSettings CaptionLocation="Top" Visible="True" />
                        </dxwgv:GridViewDataCheckColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
<%--                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>--%>
                <asp:ObjectDataSource ID="sqlCliente" runat="server" 
                    InsertMethod="clienteInclui" SelectMethod="clienteSeleciona" 
                    TypeName="rnClientes" UpdateMethod="clienteAlterar">
                    <UpdateParameters>
                        <asp:Parameter Name="clienteNome" Type="String" />
                        <asp:Parameter Name="clienteNomeDaEmpresa" Type="String" />
                        <asp:Parameter Name="clienteEmail" Type="String" />
                        <asp:Parameter Name="clienteCPFCNPJ" Type="String" />
                        <asp:Parameter Name="clienteRGIE" Type="String" />
                        <asp:Parameter Name="clienteSenha" Type="String" />
                        <asp:Parameter Name="ondeConheceuALoja" Type="String" />
                        <asp:Parameter Name="clienteSexo" Type="String" DefaultValue=" " />
                        <asp:Parameter Name="clienteDataNascimento" Type="String" DefaultValue=" " />
                        <asp:Parameter Name="clienteEstadoCivil" Type="String" />
                        <asp:Parameter Name="clienteFoneResidencial" Type="String" DefaultValue=" "/>
                        <asp:Parameter Name="clienteFoneComercial" Type="String" />
                        <asp:Parameter Name="clienteFoneCelular" Type="String" />
                        <asp:Parameter Name="clienteCep" Type="String" />
                        <asp:Parameter Name="clienteRua" Type="String" />
                        <asp:Parameter Name="clienteBairro" Type="String" />
                        <asp:Parameter Name="clienteNumero" Type="String" />
                        <asp:Parameter Name="clienteComplemento" Type="String" />
                        <asp:Parameter Name="clienteCidade" Type="String" />
                        <asp:Parameter Name="clienteEstado" Type="String" />
                        <asp:Parameter Name="clientePais" Type="String" />
                        <asp:Parameter Name="clienteReferenciaParaEntrega" Type="String" />
                        <asp:Parameter Name="clienteRecebeInformativo" Type="String" DefaultValue="False" />
                        <asp:Parameter Name="clienteId" Type="Int32" />
                        <asp:Parameter Name="melhorHorarioContato" Type="String" DefaultValue=" "/>
                        <asp:Parameter Name="telRecado" Type="String" DefaultValue=" "/>
                        <asp:Parameter Name="nomeRecado" Type="String" DefaultValue=" "/>
                    </UpdateParameters>
                    <InsertParameters>
                        <asp:Parameter Name="clienteNome" Type="String" />
                        <asp:Parameter Name="clienteNomeDaEmpresa" Type="String" />
                        <asp:Parameter Name="clienteEmail" Type="String" />
                        <asp:Parameter Name="clienteCPFCNPJ" Type="String" />
                        <asp:Parameter Name="clienteRGIE" Type="String" />
                        <asp:Parameter Name="clienteSenha" Type="String" />
                        <asp:Parameter Name="ondeConheceuALoja" Type="String" />
                        <asp:Parameter Name="clienteSexo" Type="String" />
                        <asp:Parameter Name="clienteDataNascimento" Type="String" />
                        <asp:Parameter Name="clienteEstadoCivil" Type="String" />
                        <asp:Parameter Name="clienteFoneResidencial" Type="String" />
                        <asp:Parameter Name="clienteFoneComercial" Type="String" />
                        <asp:Parameter Name="clienteFoneCelular" Type="String" />
                        <asp:Parameter Name="clienteCep" Type="String" />
                        <asp:Parameter Name="clienteRua" Type="String" />
                        <asp:Parameter Name="clienteBairro" Type="String" />
                        <asp:Parameter Name="clienteNumero" Type="String" />
                        <asp:Parameter Name="clienteComplemento" Type="String" />
                        <asp:Parameter Name="clienteCidade" Type="String" />
                        <asp:Parameter Name="clienteEstado" Type="String" />
                        <asp:Parameter Name="clientePais" Type="String" />
                        <asp:Parameter Name="clienteReferenciaParaEntrega" Type="String" />
                        <asp:Parameter Name="clienteRecebeInformativo" Type="String" />
                        <asp:Parameter Name="melhorHorarioContato" Type="String" DefaultValue=" "/>
                        <asp:Parameter Name="telRecado" Type="String" DefaultValue=" "/>
                        <asp:Parameter Name="nomeRecado" Type="String" DefaultValue=" "/>
                    </InsertParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlEstado" runat="server" 
                    SelectMethod="estadoSeleciona" TypeName="rnEstados">
                </asp:ObjectDataSource>
                    <asp:ObjectDataSource ID="sqlPaises" runat="server" 
                                            SelectMethod="paisesSeleciona" TypeName="rnPais">
                    </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>


