﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="contasParaDeposito.aspx.cs" Inherits="admin_contasParaDeposito" Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Contas para depósito</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlContasParaDeposito" KeyFieldName="idDaConta" Width="834px" 
                    Cursor="auto" onrowinserting="grd_RowInserting" EnableCallBacks="False" 
                    onhtmlrowcreated="grd_HtmlRowCreated" onrowupdating="grd_RowUpdating" 
                    onrowdeleted="grd_RowDeleted" onrowinserted="grd_RowInserted" 
                    onrowupdated="grd_RowUpdated">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Nome do banco" 
                            FieldName="nomeDanco" VisibleIndex="0">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o nome do banco." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <editformsettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Agência" FieldName="agencia" 
                            VisibleIndex="1">
                            <PropertiesTextEdit>
                                <ValidationSettings ErrorText="" SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a agência." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings captionlocation="Top" columnspan="2" visible="True" 
                                VisibleIndex="1" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Conta corrente" 
                            FieldName="contaCorrente" VisibleIndex="2">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a conta corrente." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Favorecido" FieldName="favorecido" 
                            VisibleIndex="3" Width="500px">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o favorecido." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CPF" FieldName="cpf" VisibleIndex="4">
                            <PropertiesTextEdit>
                                <ValidationSettings>
                                    <RequiredField ErrorText="" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="CNPJ" FieldName="cnpj" VisibleIndex="5">
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Visible="False" VisibleIndex="2" Name="imagens">
                            <EditFormSettings Visible="True" CaptionLocation="Top" columnspan="4" />
                            <EditCellStyle Font-Strikeout="False">
                            </EditCellStyle>
                            <EditItemTemplate>
                                <table cellpadding="0" cellspacing="0" style="width: 600px">
                                    <tr>
                                        <td class="rotulos" style="width: 415px">
                                            Imagem 1<br />
                                            <asp:FileUpload ID="flu1" runat="server" CssClass="campos" Width="400px" />
                                            <br />
                                            <asp:RegularExpressionValidator ID="rgeFlu1" runat="server" 
                                                ControlToValidate="flu1" ErrorMessage="A imagem precisa ser jpg." 
                                                ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 115px">
                                            <asp:Image ID="Imagem1" runat="server" 
                                                ImageUrl='<%# "../images/" + Eval("logoDobanco") %>' />
                                        </td>
                                    </tr>
                                </table>
                            </EditItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="6" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelar.jpg" />
                            </CancelButton>
                            <UpdateButton Text="Salvar">
                                <Image Url="~/admin/images/btSalvarPeq.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:LinqDataSource ID="sqlContasParaDeposito" runat="server" 
                    ContextTypeName="tbContasParaDepositoDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbContasParaDepositos">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>

