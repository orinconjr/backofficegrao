﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_grid : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string usuario = "";
        try
        {
            usuario = rnUsuarios.retornaNomeUsuarioLogado();
        }
        catch (Exception ex)  {}
       
        if(String.IsNullOrEmpty(usuario))
            Response.Redirect("default.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        //Proteção abaixo transferida pro lado cliente
        //if (txtselect.Text.ToLower().IndexOf("where") <= 0)
        //{
        //    lblregistros.Text = "Query sem nenhuma cláusula 'where'";
        //    GridView1.DataSource = null;
        //    GridView1.DataBind();
        //    return;
        //}
        string sqlquery = "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; "+ txtselect.Text;
        //Response.Write(sqlquery);
        
        using (
            SqlConnection conn =
                new SqlConnection(
                    @"Data Source=graodegente.cl6j8caodqlm.sa-east-1.rds.amazonaws.com;Initial Catalog=graodegente;User ID=graodegente;Password=Bark152029;Min Pool Size=10; Max Pool Size=1000")
                   // @"Data Source=192.168.1.30;Initial Catalog=graodegenteAtual;User ID=sa;Password=Bark152029;Min Pool Size=10; Max Pool Size=1000")
            )
        {

            SqlCommand command = conn.CreateCommand();
            command.Connection.Open();
            command.CommandType = CommandType.Text;
            command.CommandText = sqlquery;
            try
            {
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                DataTable table = new DataTable();

                table.Load(reader);

                if (table.Rows.Count > 0)
                {
                    lblregistros.Text = table.Rows.Count + " registros encontrados";
                    GridView1.DataSource = table;
                    GridView1.DataBind();
                }
                else
                {
                    lblregistros.Text = "Nenhum registro encontrado";
                    GridView1.DataSource = null;
                    GridView1.DataBind();
                }

                var log = new rnLog();
                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                log.descricoes.Add("Query no grid.aspx:" + txtselect.Text);
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = 0, tipoRelacionado = new rnEnums.TipoRegistroRelacionado(){}  });
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Pedido);
                log.InsereLog();
            }
            catch (Exception ex)
            {
                lblregistros.Text = ex.Message;
                GridView1.DataSource = null;
                GridView1.DataBind();
            }
 

        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
    }
}