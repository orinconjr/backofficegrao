﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rakutenPedidos;

public partial class admin_produtoAjusteEstoque : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtQuantidade.Attributes.Add("onkeypress", "return soNumero(event);");
        if (!Page.IsPostBack)
        {
            fillGrid(false);
        }
    }

    private void fillGrid(bool rebind)
    {
        var produtoDc = new dbCommerceDataContext();
        var movimentos = (from c in produtoDc.tbProdutoAjusteEstoques
            orderby c.data descending
            select new
            {
                c.idProdutoAjusteEstoque,
                c.data,
                c.idProduto,
                c.tbProduto.produtoNome,
                c.tbProduto.produtoIdDaEmpresa,
                c.tbProduto.complementoIdDaEmpresa,
                c.quantidade,
                c.motivo,
                c.usuario,
                c.observacao,
                tipo = c.tipo == 1 ? "Entrada" : "Saída"
            });
        grd.DataSource = movimentos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void btnAdicionar_OnClick(object sender, EventArgs e)
    {
        if (ddlComplementoIdDaEmpresa.Visible == false)
        {
            var produtosDc = new dbCommerceDataContext();
            var produto = (from c in produtosDc.tbProdutos where c.produtoIdDaEmpresa == txtIdDaEmpresa.Text select c);
            if (produto.Count() > 1)
            {
                ddlComplementoIdDaEmpresa.DataSource = produto;
                ddlComplementoIdDaEmpresa.DataBind();
                ddlComplementoIdDaEmpresa.Visible = true;
                trComplementoId.Visible = true;
                Response.Write("<script>alert('Produto com opções. Favor selecionar o complemento e clicar em adicionar novamente.');</script>");
            }
            else if (produto.Count() == 1)
            {
                var produtoItem = produto.First();
                var produtosFilho =
                            (from c in produtosDc.tbProdutoRelacionados
                             where c.idProdutoPai == produtoItem.produtoId
                             select c);
                if (produtosFilho.Any())
                {
                    Response.Write("<script>alert('Para adicionar um combo, adicione os produtos individualmente.');</script>");
                    return;
                }
                else
                {
                    HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                    usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                    if (usuarioLogadoId == null)
                    {
                        return;
                    }

                    var produtoAdd = produto.First();
                    var produtoAjuste = new tbProdutoAjusteEstoque();
                    produtoAjuste.idProduto = produtoAdd.produtoId;
                    produtoAjuste.data = DateTime.Now;
                    produtoAjuste.motivo = ddlMotivo.SelectedValue;
                    produtoAjuste.quantidade = Convert.ToInt32(txtQuantidade.Text);
                    produtoAjuste.tipo = Convert.ToInt32(ddlTipo.SelectedValue);
                    produtoAjuste.observacao = txtObservacao.Text;
                    produtoAjuste.usuario =
                        rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0]
                            .Rows[0]["usuarioNome"].ToString();
                    if (produtoAjuste.tipo == 1)
                    {
                        produtoAdd.estoqueReal += produtoAjuste.quantidade;
                    }
                    else
                    {
                        produtoAdd.estoqueReal -= produtoAjuste.quantidade;
                    }
                    produtosDc.tbProdutoAjusteEstoques.InsertOnSubmit(produtoAjuste);
                    produtosDc.SubmitChanges();
                    txtQuantidade.Text = "0";
                    txtIdDaEmpresa.Text = "0";
                    ddlComplementoIdDaEmpresa.Visible = false;
                    trComplementoId.Visible = false;
                    fillGrid(true);
                }
            }
            else
            {
                Response.Write("<script>alert('Produto não encontrado.');</script>");
            }
        }
        else
        {
            int produtoId = (Convert.ToInt32(ddlComplementoIdDaEmpresa.SelectedValue));
            var produtosDc = new dbCommerceDataContext();


            var produtoItem = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
            var produtosFilho =
                        (from c in produtosDc.tbProdutoRelacionados
                         where c.idProdutoPai == produtoItem.produtoId
                         select c);
            if (produtosFilho.Any())
            {
                Response.Write("<script>alert('Para adicionar um combo, adicione os produtos individualmente.');</script>");
                return;
            }
            else
            {
                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
                if (usuarioLogadoId == null)
                {
                    return;
                }

                var produtoAdd = produtoItem;
                var produtoAjuste = new tbProdutoAjusteEstoque();
                produtoAjuste.idProduto = produtoAdd.produtoId;
                produtoAjuste.data = DateTime.Now;
                produtoAjuste.motivo = ddlMotivo.SelectedValue;
                produtoAjuste.quantidade = Convert.ToInt32(txtQuantidade.Text);
                produtoAjuste.tipo = Convert.ToInt32(ddlTipo.SelectedValue);
                produtoAjuste.usuario =
                    rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0]
                        .Rows[0]["usuarioNome"].ToString();
                if (produtoAjuste.tipo == 1)
                {
                    produtoAdd.estoqueReal += produtoAjuste.quantidade;
                }
                else
                {
                    produtoAdd.estoqueReal -= produtoAjuste.quantidade;
                }
                produtosDc.tbProdutoAjusteEstoques.InsertOnSubmit(produtoAjuste);
                produtosDc.SubmitChanges();
                txtQuantidade.Text = "0";
                txtIdDaEmpresa.Text = "0";
                ddlComplementoIdDaEmpresa.Visible = false;
                trComplementoId.Visible = false;
                fillGrid(true);
            }
        }
    }
}