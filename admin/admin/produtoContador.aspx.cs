﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_produtoContador : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregaCategorias();
           Carrega_ddlEstiloContador();
        }
    }

    private void CarregaCategorias()
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbProdutoCategorias where c.idSite == 1 && c.exibirSite == true || (c.categoriaPaiId == 774) orderby c.categoriaNome select c).ToList();
                ddlCategoria.DataSource = dados;
                ddlCategoria.DataBind();
            }
            ddlCategoria.Items.Insert(0, new ListItem("Selecione", "0"));
            ddlCategoria.Items.Insert(1, new ListItem("Todos", "99999999"));
        }
        catch (Exception ex)
        {

        }
    }
    

    protected void btnSalvar_OnClick(object sender, ImageClickEventArgs e)
    {
        try
        {
            #region validação

            if (ddlCategoria.SelectedValue == "0" && string.IsNullOrEmpty(txtIdsProdutos.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe os produtos que terão contador!');", true);
                return;
            }

            if (string.IsNullOrEmpty(txtInicioContadorData.Text) || string.IsNullOrEmpty(txtFimContadorData.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe corretamente as datas do contador!');", true);
                return;
            }

            var dataInicioContador = Convert.ToDateTime(Convert.ToDateTime(txtInicioContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay);
            var dataFimContador = Convert.ToDateTime(Convert.ToDateTime(txtFimContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtFimContadorHora.Text).TimeOfDay);

            if (dataFimContador < dataInicioContador || dataFimContador < DateTime.Now )
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe corretamente as datas do contador!');", true);
                return;
            }
            List<int> produtoIds = new List<int>();

            if (!string.IsNullOrEmpty(txtIdsProdutos.Text))
            {
                bool validarProdutoIds;
                try
                {
                    produtoIds = txtIdsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
                    validarProdutoIds = true;
                }
                catch (Exception)
                {
                    validarProdutoIds = false;
                }

                if (!validarProdutoIds)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Verifique os produtoId informados!');", true);
                    return;
                }
            }

            #endregion validação

            List<int> listaDeProdutos = new List<int>();

            if (ddlCategoria.SelectedValue != "0")
            {
                int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);

                using (var data = new dbCommerceDataContext())
                {
                    listaDeProdutos = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == categoriaId select (int)c.produtoId).Distinct().ToList();
                }
            }

            if (!string.IsNullOrEmpty(txtIdsProdutos.Text))
            {
                using (var data = new dbCommerceDataContext())
                {
                    listaDeProdutos.AddRange((from c in data.tbProdutos where produtoIds.Contains(c.produtoId) select c.produtoId).Distinct(). ToList());
                }
            }

            listaDeProdutos = listaDeProdutos.Distinct().ToList();

            List<int> jaCadastradoNoPeriodo = new List<int>();
            bool cadastroValido = false;
            string nomeUsuario = rnUsuarios.retornaNomeUsuarioLogado();
            using (var data = new dbCommerceDataContext())
            {
                foreach (var produto in listaDeProdutos)
                {
                    try
                    {

                        var pesquisaProduto = (from c in data.tbProdutos where c.produtoId == produto select c.produtoId).FirstOrDefault();

                        if (pesquisaProduto != null)
                        {
                            var jaExiste = (from c in data.tbProdutoContadors
                                            where
                                                c.produtoId == produto &&
                                                (c.inicioContador == dataInicioContador && c.fimContador == dataFimContador)
                                            select c).Any();

                            if (!jaExiste)
                            {

                                var validarIntervalo = (from c in data.tbProdutoContadors
                                                        where
                                                            c.produtoId == produto && c.inicioContador.Value.Date == dataInicioContador.Date
                                                        select c);
                                bool intervaloValido = true;

                                foreach (var contador in validarIntervalo)
                                {
                                    if (contador.inicioContador >= dataInicioContador || dataInicioContador <= contador.fimContador && (contador.fimContador > DateTime.Now))
                                    {
                                        intervaloValido = false;
                                        jaCadastradoNoPeriodo.Add(produto);
                                    }
                                }

                                if (intervaloValido)
                                {
                                    var produtoContador = new tbProdutoContador
                                    {
                                        produtoId = produto,
                                        inicioContador = dataInicioContador,
                                        fimContador = dataFimContador,
                                        estilo = Convert.ToInt32(ddlLayoutContador.SelectedValue),
                                        //estilo = 1,
                                        ativo = true,
                                        dataCadastro = DateTime.Now,
                                        usuarioCadastro = nomeUsuario
                                    };
                                    data.tbProdutoContadors.InsertOnSubmit(produtoContador);
                                    data.SubmitChanges();

                                    cadastroValido = true;
                                }
                                else
                                {

                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string erro = ex.Message;
                    }
                  
                }
            }

            if (cadastroValido) ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Contador informado com sucesso!');", true);

            grd.DataBind();

            if (jaCadastradoNoPeriodo.Count > 0)
            {
                string produtoInvalidos = jaCadastradoNoPeriodo.Distinct().Aggregate("", (current, p) => current + (p + ", "));

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Os produtos : " + produtoInvalidos + " não tiveram contador informado pois já existe intervalo do mesmo tempo cadastrado para eles!');", true);
            }

        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO:" + ex.Message + "');", true);
        }
    }

    protected void btnRemover_OnClick(object sender, EventArgs e)
    {
        try
        {
            #region validação

            if (ddlCategoria.SelectedValue == "0" && string.IsNullOrEmpty(txtIdsProdutos.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe os produtos que serão removidos o contador!');", true);
                return;
            }

            //if (ddlCategoria.SelectedValue == "99999999" )
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "return confirm('Atenção : Todos os registros serão excluídos !!!\n Confirma?');", true);
                
            //}
           /* if (string.IsNullOrEmpty(txtInicioContadorData.Text) || string.IsNullOrEmpty(txtFimContadorData.Text))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe corretamente as datas do contador!');", true);
                return;
            }
            */

           

            //if (dataFimContador < dataInicioContador || dataFimContador < DateTime.Now)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Informe corretamente as datas do contador!');", true);
            //    return;
            //}
            List<int> produtoIds = new List<int>();

            if (!string.IsNullOrEmpty(txtIdsProdutos.Text))
            {
                bool validarProdutoIds;
                try
                {
                    produtoIds = txtIdsProdutos.Text.Split(',').Select(x => Convert.ToInt32(x.Trim())).ToList();
                    validarProdutoIds = true;
                }
                catch (Exception)
                {
                    validarProdutoIds = false;
                }

                if (!validarProdutoIds)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Verifique os produtoId informados!');", true);
                    return;
                }
            }

            #endregion validação

            List<int> listaDeProdutos = new List<int>();

            if (ddlCategoria.SelectedValue != "0" && ddlCategoria.SelectedValue != "99999999")
            {
                int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);

                using (var data = new dbCommerceDataContext())
                {
                   // listaDeProdutos = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == categoriaId select c).Distinct().Select(x => x.produtoId).ToList();

                    listaDeProdutos = (from pc in data.tbProdutoContadors
                                       join jpc in data.tbJuncaoProdutoCategorias on pc.produtoId equals jpc.produtoId
                                       where jpc.tbProdutoCategoria.categoriaId == categoriaId
                                       select (int)pc.produtoId).ToList();
                }
            }

            if (ddlCategoria.SelectedValue == "99999999")
            {
                int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
                using (var data = new dbCommerceDataContext())
                {
                    listaDeProdutos = (from c in data.tbProdutoContadors select (int)c.produtoId).Distinct().ToList();
                  //  listaDeProdutos = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == categoriaId select c).Distinct().Select(x => x.produtoId).ToList();
                }
            }
            if (!string.IsNullOrEmpty(txtIdsProdutos.Text) && ddlCategoria.SelectedValue == "99999999")
            {
                using (var data = new dbCommerceDataContext())
                {
                    listaDeProdutos.AddRange((from c in data.tbProdutos where produtoIds.Contains(c.produtoId) select c.produtoId).Distinct().ToList());
                }
            }
            bool removido = false;

            using (var data = new dbCommerceDataContext())
            {
                foreach (var produto in listaDeProdutos)
                {
                    var pesquisaProduto = (from c in data.tbProdutos where c.produtoId == produto select c.produtoId).FirstOrDefault();

                    if (pesquisaProduto != null)
                    {
                        DateTime dataInicioContador = new DateTime();
                        DateTime dataFimContador = new DateTime();
                        if (!String.IsNullOrEmpty(txtInicioContadorData.Text) && !String.IsNullOrEmpty(txtFimContadorData.Text))
                        {
                            dataInicioContador = Convert.ToDateTime(Convert.ToDateTime(txtInicioContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay);
                            dataFimContador = Convert.ToDateTime(Convert.ToDateTime(txtFimContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtFimContadorHora.Text).TimeOfDay);

                            var checarIntervalo = (from c in data.tbProdutoContadors
                                                   where
                                                       c.produtoId == produto &&
                                                       (c.inicioContador == dataInicioContador && c.fimContador == dataFimContador)
                                                   select c).FirstOrDefault();

                            if (checarIntervalo != null)
                            {
                                data.tbProdutoContadors.DeleteOnSubmit(checarIntervalo);
                                data.SubmitChanges();

                                removido = true;
                            }
                        }
                        else
                        {
                            var checarIntervalo = (from c in data.tbProdutoContadors
                                                   where
                                                       c.produtoId == produto // &&
                                                     //  (c.inicioContador == dataInicioContador && c.fimContador == dataFimContador)
                                                   select c).FirstOrDefault();

                            if (checarIntervalo != null)
                            {
                                data.tbProdutoContadors.DeleteOnSubmit(checarIntervalo);
                                data.SubmitChanges();

                                removido = true;
                            }
                        }
                        
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(),
                removido
                    ? "alert('Remoção concluida!');"
                    : "alert('A partir dos dados informados não foi possivel realizar nenhuma remoção de contador!');",
                true);

            grd.DataBind();
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }




    private void Carrega_ddlEstiloContador()
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbEstilos where c.idTipoEstilo == 3 orderby c.nome select c).ToList();
                ddlLayoutContador.DataSource = dados;
                ddlLayoutContador.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }


}