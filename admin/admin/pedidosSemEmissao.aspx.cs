﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils.About;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosSemEmissao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            fillGrid(true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Falha ao carregar dados! ERRO: " + ex.Message + "');", true);
        }

    }
    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now;
        var pedidos = (from c in data.tbPedidos
                       join pac in data.tbPedidoPacotes on c.pedidoId equals pac.idPedido
                       join env in data.tbPedidoEnvios on pac.idPedidoEnvio equals env.idPedidoEnvio
                       where
                        (pac.rastreio ?? "") == "" &&
                        (env.nfeImpressa ?? false) == false && (pac.rastreamentoConcluido ?? false) == false
                        && env.idCentroDeDistribuicao > 3 && env.dataFimEmbalagem != null && env.formaDeEnvio != ""
                       select new
                       {
                           pac.tbPedido.pedidoId,
                           pac.tbPedido.dataHoraDoPedido,
                           pac.tbPedidoEnvio.dataFimEmbalagem,
                           pac.tbPedido.prazoMaximoPostagemAtualizado,
                           transportadora = env.formaDeEnvio,
                           pac.numeroVolume,
                           dif = (hoje - pac.tbPedidoEnvio.dataFimEmbalagem.Value).TotalMinutes,
                       }).Distinct().ToList().OrderByDescending(x => x.dif);

        var pedidosFormatados = (from c in pedidos
                                 select new
                                 {
                                     c.pedidoId,
                                     c.dataHoraDoPedido,
                                     c.dataFimEmbalagem,
                                     c.prazoMaximoPostagemAtualizado,
                                     c.transportadora,
                                     c.numeroVolume,
                                     dif = c.dif < 60 ? c.dif.ToString("0.##") + " min" : c.dif < (60 * 24) ? (c.dif / 60).ToString("0.##") + " hr" : (c.dif / 60 / 24).ToString("0.##") + " dias",
                                 }).ToList();
        grd.DataSource = pedidosFormatados;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        DateTime? dataFimEmbalagem = (DateTime?)grd.GetRowValues(e.VisibleIndex, new string[] { "dataFimEmbalagem" });

        if (dataFimEmbalagem.HasValue)
        {
            if (System.DateTime.Now.Subtract(dataFimEmbalagem.Value).Minutes > 0 && (System.DateTime.Now.Subtract(dataFimEmbalagem.Value).Minutes <= 60))
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#ffffe5");
                    i++;
                }
            }

            if (System.DateTime.Now.Subtract(dataFimEmbalagem.Value).Minutes >= 120)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
        }
    }


    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
        
    protected void lnkEnderecamento_Command(object sender, CommandEventArgs e)
    {
        var data = new dbCommerceDataContext();

        var pedidoId = e.CommandArgument.ToString();
        popEnderecamento.ShowOnPageLoad = true;


        var envios = (from c in data.tbPedidoEnvios
                      join d in data.tbPedidoPacotes on c.idPedidoEnvio equals d.idPedidoEnvio
                      join h in data.tbPedidoPacoteEnderecoHistoricos on d.idPedidoPacote equals h.idPedidoPacote
                      join u in data.tbUsuarioExpedicaos on h.idUsuarioExpedicao equals u.idUsuarioExpedicao
                      where c.idPedido == Convert.ToInt32(pedidoId) select new
                      {
                          d.idPedidoPacote,
                          h.dataHora,
                          u.nome,
                          d.numeroVolume,
                          enderecoPacote = d.tbEnderecoPacote.enderecoPacote
                      }).OrderBy(x=>x.dataHora).ToList();

        grdHistoricoEndereco.DataSource = envios;
        grdHistoricoEndereco.DataBind();

        

    }
}