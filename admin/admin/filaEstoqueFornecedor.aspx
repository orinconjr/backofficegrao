﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" Theme="Glass" AutoEventWireup="true" CodeFile="filaEstoqueFornecedor.aspx.cs" Inherits="admin_filaEstoqueFornecedor" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Fila de Estoque</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                <asp:DropDownList runat="server" ID="ddlFornecedor" DataValueField="fornecedorId" DataTextField="fornecedorNome"></asp:DropDownList>
                <asp:Button runat="server" ID="btnFiltrar" OnClick="btnFiltrar_OnClick" Text="Filtrar" />
            </td>
        </tr>
        <tr>
            <td class="rotulos">
                &nbsp;
            </td>
        </tr>
        
        <asp:ListView runat="server" id="lstProdutos" OnItemDataBound="lstProdutos_OnItemDataBound">
            <ItemTemplate>
                <tr>
                    <td class="rotulos">
                        <%# Eval("produtoNome") %>
                    </td>
                </tr>
        
                <tr>
                    <td>
                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                            OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData" KeyFieldName="pedidoId">
                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                            <Styles>
                                <Footer Font-Bold="True">
                                </Footer>
                            </Styles>
                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                            <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                            </SettingsPager>
                            <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                            <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                            <Columns>
                                <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                    <DataItemTemplate>
                                        <%# Container.VisibleIndex + 1 %>
                                    </DataItemTemplate>
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="idItemPedidoAguardandoEstoque" FieldName="idItemPedidoAguardandoEstoque" VisibleIndex="0" Width="50" Visible="False">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Romaneio Previsto" FieldName="idPedidoFornecedor" VisibleIndex="0">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataTextColumn Caption="Etiqueta Prevista" FieldName="idPedidoFornecedorItem" VisibleIndex="0">
                                </dxwgv:GridViewDataTextColumn>
                                <dxwgv:GridViewDataDateColumn Caption="Data Limite" FieldName="dataLimiteReserva" VisibleIndex="0">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataDateColumn Caption="Data Prevista" FieldName="dataLimiteFornecedor" VisibleIndex="0">
                                </dxwgv:GridViewDataDateColumn>
                                <dxwgv:GridViewDataHyperLinkColumn Caption="Editar" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                                    <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                    </PropertiesHyperLinkEdit>
                                    <Settings AllowAutoFilter="False" />
                                    <HeaderTemplate>
                                        <img alt="" src="images/legendaEditar.jpg" />
                                    </HeaderTemplate>
                                </dxwgv:GridViewDataHyperLinkColumn>
                            </Columns>
                            <StylesEditors>
                                <Label Font-Bold="True">
                                </Label>
                            </StylesEditors>
                        </dxwgv:ASPxGridView>        
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
        <tr>
            <td style="text-align: right;">

            </td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>