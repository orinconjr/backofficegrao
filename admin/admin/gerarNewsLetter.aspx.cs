﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxHtmlEditor.Internal;

public partial class admin_gerarNewsLetter : System.Web.UI.Page
{
    public string mensagem = "";
    public string utm = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        // Response.Write(Server.MapPath("~/file.txt"));
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        salvarDefault();
    }

    protected void salvarDefault()
    {

        string dia = DateTime.Now.Day.ToString();
        string mes = DateTime.Now.Month.ToString();
        string hora = DateTime.Now.Hour.ToString();
        string minuto = DateTime.Now.Minute.ToString();
        string segundo = DateTime.Now.Second.ToString();
        string ano = DateTime.Now.Year.ToString();

        dia = dia.Length < 2 ? "0" + dia : dia;
        mes = mes.Length < 2 ? "0" + mes : mes;
        hora = hora.Length < 2 ? "0" + hora : hora;
        minuto = minuto.Length < 2 ? "0" + minuto : minuto;
        segundo = segundo.Length < 2 ? "0" + segundo : segundo;
        string arquivoDiaHora = ano + mes + dia + "_" + hora + minuto + segundo;

        /**** Tratar erros*******/
        bool temErro = false;

        bool temArquivo = flupFoto.HasFile;

        if (String.IsNullOrEmpty(txtnomeCampanha.Text))
        {
            temErro = true;
            mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> Informe o nome da campanha </div>";
        }

        //if (String.IsNullOrEmpty(txtlinkbanner.Text))
        //{
        //    temErro = true;
        //    mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> Informe o link do Banner</div>";
        //}
        //if (!temArquivo)
        //{
        //    temErro = true;
        //    mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> Faça upload do Banner</div>";
        //}

        if (temErro)
        {
            ltmensagem.Text = mensagem;
            return;
        }

        utm = "?c3ch=Email&c3nid=" + txtnomeCampanha.Text.Replace(" ", "-").Trim().ToLower() + "_" + ano + mes + dia;

        /**** Tratar erros fim*******/
        string pastaHtml = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/")).Parent.FullName + @"\admin\admin\envio\" +
                           arquivoDiaHora;

        Directory.CreateDirectory(pastaHtml);

        string arquivo = pastaHtml + @"\index.html";
        string arquivoLink = "http://www.graodegente.com.br/admin/admin/envio/" + arquivoDiaHora + "/index.html";

        string nomeDoArquivo = "";
        string nomeDoArquivoSelo = "";
        string caminhoBanner = "";
        string caminhoSelo = "";
        string extensao = "";
        bool temBanner = false;
        bool temSelo = false;
        if (flupFoto.HasFile)
        {
            try
            {
                // string fileName = Path.GetFileName(flupFoto.FileName);
                extensao = Path.GetExtension(flupFoto.FileName);// Verificar extensoes a validar
                nomeDoArquivo = "banner" + arquivoDiaHora + extensao;
                //  caminhoBanner = MapPath("~/site/banners/newsletter/") + nomeDoArquivo;
                caminhoBanner = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/")).Parent.FullName + @"\admin\admin\banners\newsletter\" +
                            nomeDoArquivo;
                flupFoto.SaveAs(caminhoBanner);
                temBanner = true;


                // ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmacao", "alert('Imagem alterada com sucesso!');", true);
                // Response.Redirect("background_home.aspx");

            }
            catch (Exception ex)
            {
                Response.Write("<139> " + ex.Message + "<br/>");
            }
        }

        if (flupSelo.HasFile)
        {
            try
            {
                // string fileName = Path.GetFileName(flupSelo.FileName);
                extensao = Path.GetExtension(flupSelo.FileName);// Verificar extensoes a validar
                nomeDoArquivoSelo = "Selo" + arquivoDiaHora + extensao;
                //  caminhoSelo = MapPath("~/site/Selos/newsletter/") + nomeDoArquivo;
                caminhoSelo = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/")).Parent.FullName + @"\admin\admin\banners\newsletter\" +
                            nomeDoArquivoSelo;
                flupSelo.SaveAs(caminhoSelo);
                temSelo = true;


                // ScriptManager.RegisterStartupScript(this, typeof(Page), "confirmacao", "alert('Imagem alterada com sucesso!');", true);
                // Response.Redirect("background_home.aspx");

            }
            catch (Exception ex)
            {
                Response.Write("<139> " + ex.Message + "<br/>");
            }
        }

        string[] destaques = txtprodutosdestaque.Text.Split(',');
        string[] vejaTambem = txtprodutosvejatambem.Text.Split(',');

        int qtd = destaques.Length;
        int posicao = 1;
        int contador = 1;
        string produtos = "";
        foreach (var item in destaques)
        {
            if (!String.IsNullOrEmpty(item))
            {
                bool ultimo = false;
                if (contador == qtd)
                    ultimo = true;
                produtos += inseriProduto(Convert.ToInt32(item), posicao, ultimo, nomeDoArquivoSelo);
                posicao++;
                contador++;
                if (posicao > 3)
                    posicao = 1;
            }
        }


        int qtd2 = vejaTambem.Length;
        int posicao2 = 1;
        int contador2 = 1;
        string produtos2 = "";
        foreach (var item in vejaTambem)
        {
            if (!String.IsNullOrEmpty(item))
            {
                bool ultimo = false;
                if (contador2 == qtd2)
                    ultimo = true;
                produtos2 += inseriProduto(Convert.ToInt32(item), posicao2, ultimo, nomeDoArquivoSelo);
                posicao2++;
                contador2++;
                if (posicao2 > 3)
                    posicao2 = 1;
            }
        }
        #region HTML

        string meuHtml = "";
        
        meuHtml += "<!DOCTYPE html PUBLIC //W3C//DTD XHTML 1.0 Transitional//EN'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html xmlns='http://www.w3.org/1999/xhtml'><head><title>Grão de Gente</title><meta http-equiv='Content-Type' content='text/html charset=UTF-8' /></head><body bgcolor='#FFFFFF'><center><table border='0' cellpadding='0' cellspacing='0' width='600'><tbody>";
        meuHtml += "<tr><td></td></tr><tr><td><table border='0' cellpadding='0' cellspacing='0' width='100%'><tr><td width='350' valign='top'><p style='font-size:11px;color:#000;font-family:Arial;text-align: left;display: block;'><span style='color: rgb(0, 0, 0); font-family: Arial; font-size: 11px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;'>Adicione o texto aqui</span></p></td>   <td width='250' valign='top'><p style='font-size:11px;color:#000;font-family:Arial;text-align: right;display: block;'><a href='##preview##' target='_blank' style='color:#000000'>Visualize no navegador</a></p></td></tr></table></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='17' alt='Grão de Gente' style='display:block;border:none;' /></td></tr><tr><td><table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td><a href='https://www.graodegente.com.br/' title='Grão de Gente' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r2_c1.jpg' width='238' height='61' alt='Grão de Gente' style='display:block;border:none;' /></a></td><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='181' height='61' alt='Grão de Gente' style='display:block;border:none;' /></td><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r2_c8.jpg' width='21' height='61' alt='Grão de Gente' style='display:block;border:none;' /></td><td valign='center' align='left'><span style='font-size: 22px;font-family: arial;font-weight: bold;color: #666666;'>11 3522 8379</span></td></tr></tbody></table></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='23' alt='Grão de Gente' style='display:block;border:none;' /></td></tr><tr><td valign='top' style='border-top: solid 1px #e1e1e1;border-bottom: solid 1px #e1e1e1;padding-top: 13px;'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td valign='top' width='68' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/kit-berco' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Kit Berço' target='_blank'>Kit Berço</a></td><td valign='top' width='99' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/kit-cama-baba' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Kit Cama Babá' target='_blank'>Kit Cama Babá</a></td><td valign='top' width='116' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/quarto-completo' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Quarto Completo' target='_blank'>Quarto Completo</a></td><td valign='top' width='116' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/quarto-completo-sem-cama-baba' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Quarto Completo sem cama babá' target='_blank'>Quarto Completo<span style='font-size: 9px;color: #8c8c8c;display: block;'>(sem cama babá)</span></a></td><td valign='top' width='67' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/cortinas' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Cortinas' target='_blank'>Cortinas</a></td><td valign='top' width='76' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/roupinhas' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Roupinhas' target='_blank'>Roupinhas</a></td><td valign='top' width='58' height='28' style='text-align: center;'><a href='https://www.graodegente.com.br/moveis' style='font-family: arial,sans-serif; text-decoration: none; color:#141414; font-size:12px;' title='Móveis' target='_blank'>Móveis</a></td></tr></tbody></table></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='12' alt='Grão de Gente' style='display:block;border:none;' /></td></tr><tr><td><a href='https://www.graodegente.com.br/regrasparafretegratis.aspx' title='Frete para todo o Brasil' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r5_c1.jpg' width='600' height='24' alt='Frete para todo o Brasil' style='display:block;border:none;' /></a></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='17' alt='Grão de Gente' style='display:block;border:none;' /></td></tr><tr><td><table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r7_c1.jpg' width='130' height='34' alt='Em até 12 vezes sem juros' style='display:block;border:none;' /></td><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r7_c5.jpg' width='303' height='34' alt='30 dias para efetuar a troca ou devolução de 100% do seu dinheiro' style='display:block;border:none;' /></td><td><a href='https://www.graodegente.com.br/site-seguro' title='Nosso site é seguro, entenda porque' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r7_c20.jpg' width='167' height='34' alt='Nosso site é seguro, entenda porque' title='Nosso site é seguro, entenda porque' style='display:block;border:none;' /></a></td></tr></tbody></table></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='22' alt='Grão de Gente' style='display:block;border:none;' /></td></tr>";
                                      
        // if(!String.IsNullOrEmpty(txtlinkbanner.Text)&& flupFoto.HasFile) {
            meuHtml += "<tr><td>";
                meuHtml += "<a href='" + txtlinkbanner.Text + utm + "' title='' target='_blank'>";
                    meuHtml += "<img src='http://admin.graodegente.com.br/admin/banners/newsletter/" + nomeDoArquivo + "' width='600' alt='Grão de Gente' style='display:block;border:none;' />";
                meuHtml += "</a>";
            meuHtml += "</a><td></tr>";
        // }
                                    
        meuHtml += "</tr><td style='background: #FFFFFF;'><table  border='0' cellpadding='0' cellspacing='0' width='100%'><tbody>";
        meuHtml += produtos;
        meuHtml += "</tbody></table></td></tr>";
                                    
        if (!String.IsNullOrEmpty(txtprodutosvejatambem.Text)){
            meuHtml += "<tr><td style='padding: 40px 0 0 0;font-size: 24px;color: #666666;text-align: center;font-family: Arial;'><p style='margin:0;'>VEJA TAMBÉM</p></td></tr>";
        }

        meuHtml += "</tr><td style='background: #FFFFFF;'><table  border='0' cellpadding='0' cellspacing='0' width='100%'><tbody>";
        meuHtml += produtos2;
        meuHtml += "</tbody></table></td></tr>";
        
        meuHtml += "<tr> <td height='30'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='30' alt='Grão de Gente' style='display:block;border:none;' /> </td> </tr> <tr> <td valign='top' height='15'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' width='600' height='15' alt='Grão de Gente' style='display:block;border:none;' /> </td> </tr> <tr> <td> <table border='0' cellpadding='0' cellspacing='0'> <tbody> <tr> <td width='74'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' width='74' height='38' alt='Grão de Gente' style='display:block;border:none;' /> </td> <td> <img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r12_c1_cinza.jpg' width='22' height='38' style='display:block;border:none;' /> </td> <td valign='top' width='242' align='left' style='font-size: 16px;font-family: arial;font-weight: bold;color: #000000;padding-top: 13px;text-transform: uppercase;' bgcolor='#f2f2f2'>SAC 11 3522 8379</td> <td> <img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r12_c14_cinza.jpg' alt='Grão de Gente' width='37' style='display:block;border:none;' /> </td> <td valign='top' align='left' width='173' style='padding-top: 14px;' bgcolor='#f2f2f2'> <a href='https://www.graodegente.com.br/central-de-atendimento' title='Central de atendimento' style='text-decoration: none;font-size: 12px;font-family: arial;font-weight: bold;color: #000000;text-transform: uppercase;' target='_blank'>Central de atendimento</a> </td> <td> <img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r12_c28_cinza.jpg' width='8' height='38' alt='Grão de Gente' style='display:block;border:none;' /> </td> <td width='44'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' width='44' height='38' alt='Grão de Gente' style='display:block;border:none;' /> </td> </tr> </tbody> </table> </td> </tr> <tr> <td height='15'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' width='600' height='15' alt='Grão de Gente' style='display:block;border:none;' /> </td> </tr> <tr> <td valign='top' height='15'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' width='600' height='15' style='display:block;border:none;' /> </td> </tr> <tr> <td> <table border='0' cellpadding='0' cellspacing='0' width='100%'> <tbody> <tr> <td valign='top' width='300'> <table border='0' cellpadding='0' cellspacing='0' width='100%'> <tbody> <tr> <td valign='top' width='37'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' width='100%' height='70' style='display:block;border:none;' /> </td> <td valign='top' width='201'> <a href='https://www.facebook.com/graodegente/app/141780475997483/?ref=page_internal' title='Mais de 2.5 Milhões de pessoas curtem a Grão de Gente - Veja aqui o elas falam' target='_blank'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r14_c2.jpg' width='100%' height='70' alt='Mais de 2.5 Milhões de pessoas curtem a Grão de Gente' style='display:block;border:none;'> </a> </td> <td valign='top' width='62'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' width='100%' height='70' style='display:block;border:none;' /></td></tr></tbody></table></td><td valign='top' width='300'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td valign='top' align='left' width='300' style='color: #666666; font-family: arial, sans-serif;font-weight: bold;font-size: 14px;text-transform: uppercase;'><table border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td width='130' bgcolor='#f2f2f2' style='color: #666666; font-family: arial, sans-serif;font-weight: bold;font-size: 14px;text-transform: uppercase;background: url(imagens/spacer.jpg) repeat #f2f2f2; line-height: 90%;'>SIGA TAMBÉM NO:</td><td width='170'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' width='100%' height='16' style='display:block;border:none;' /></td></tr></tbody></table></td></tr><tr><td valign='top' height='17' width='300'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='17' width='100%' style='display:block;border:none;' /></td></tr><tr><td valign='top' width='300'><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td valign='top' width='29'><a href='https://www.instagram.com/graodegente/' title='Instagram Grão de Gente' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r16_c11.png' width='100%' height='28' alt='Instagram Grão de Gente' title='Instagram Grão de Gente' style='display:block;border:none;' /></a></td><td valign='top' width='30'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='28' width='100%' style='display:block;border:none;' /></td><td valign='top' width='28'><a href='https://twitter.com/graodegente' title='Grão de Gente Twitter' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r16_c15.png' width='100%' height='28' alt='Grão de Gente Twitter' title='Grão de Gente Twitter' style='display:block;border:none;' /></a></td><td valign='top' width='31'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='28' width='100%' style='display:block;border:none;' /></td><td valign='top' width='27'><a href='https://plus.google.com/+GraodegenteBr/about' title='Grão de Gente G+' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r16_c19.png' width='100%' height='28' alt='Grão de Gente G+' title='Grão de Gente G+' style='display:block;border:none;' /></a></td><td valign='top' width='32'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='28' width='100%' style='display:block;border:none;' /></td><td valign='top' width='27'><a href='https://www.pinterest.com/graodegente/' title='Grão de Gente Pinterest' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r16_c24.jpg' width='100%' height='28' alt='Grão de Gente Pinterest' title='Grão de Gente Pinterest' style='display:block;border:none;' /></a></td><td valign='top' width='31'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='28' width='100%' style='display:block;border:none;' /></td><td valign='top' width='26'><a href='http://www.bloggraodegente.com.br/' title='Blog Grão de Gente' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r16_c27.jpg' width='100%' height='28' alt='Blog Grão de Gente' title='Blog Grão de Gente' style='display:block;border:none;' /></a></td><td valign='top' width='39'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='28' width='100%' style='display:block;border:none;' /></td></tr></tbody></table></td></tr><tr><td valign='top' height='9'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='9' width='300' style='display:block;border:none;' /></td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer.jpg' alt='Grão de Gente' height='15' width='600' style='display:block;border:none;' /></td></tr><tr><td valign='top' height='23'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='23' alt='Grão de Gente' style='display:block;border:none;' /></td></tr><tr><td><p style='text-align: center;color: #a3a3a3;font-family: arial,sans-serif;font-size: 11px;'> <span style='font-weight: bold;'>Grão de Gente - Lindsay Ferrando ME</span> <br>CNPJ: 10.924.051/0001-63 - Escritório Administrativo: Av. Ana Costa, 416, conj 21 <br>11060-002- Gonzaga - Santos SP (não temos showroom)</p></td></tr><tr><td><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td valign='top' width='215'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='215' height='62' alt='Grão de Gente' style='display:block;border:none;' /></td><td><a href='http://trustedcompany.com/br/graodegente.com.br-opini%C3%B5es?rating=5' style='text-decoration: none' title='Trusted Company' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r20_c7.jpg' width='76' height='62' alt='Trusted Company' title='Trusted Company' style='display:block;border:none;' /></a></td><td valign='top' width='27'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='27' height='62' alt='Grão de Gente' style='display:block;border:none;' /></td><td><a href='http://www.fundabrinq.org.br/' style='text-decoration: none' title='Fundação Abrinq' target='_blank'><img src='http://emkt.graodegente.com.br/email-novo/imagens/cab_rodape_novos_r20_c12.jpg' width='69' height='62' alt='Fundação Abrinq' title='Fundação Abrinq' style='display:block;border:none;' /></a></td><td valign='top' width='213'><img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='213' height='62' alt='Grão de Gente' style='display:block;border:none;' /></td></tr></tbody></table></td></tr> <tr> <td> <p style='text-align: center;color: #a3a3a3;font-family: arial,sans-serif;font-size: 10px;margin:0 0 5px;'> As imagens encontradas neste e-mail são meramente ilustrativas. Preços e condições válidos somente para compra por meio do site graodegente.com.br. As condições de venda podem ser alteradas após o envio dessa mensagem.<br> </p> <p style='text-align: center;color: #a3a3a3;font-family: arial,sans-serif;font-size: 10px;margin:0;'> Por isso, confira sempre o preço na página do produto na loja antes de comprar. Caso haja diferença de preço ou descrição do produto entre este e-mail e o do site, a condição válida e praticada será a do site. </p> <p style='text-align: center;color: #a3a3a3;font-family: arial,sans-serif;font-size: 10px;margin:0 0 5px;'> Para garantir que você receberá o melhor conteúdo da Grão de Gente em seu e-mail, sugerimos que adicione o e-mail <span style='text-decoration: none;font-weight: bold;color:#a3a3a3;'>webmaster@graodegente.com.br</span> à sua lista de remetentes confiáveis. </p><p style='text-align: center;color: #a3a3a3;font-family: arial,sans-serif;font-size: 10px;margin:0;'> Para esclarecer suas dúvidas ou enviar sugestões, entre em contato com nosso <a href='mailto:atendimento@graodegente.com.br' title='atendimento@graodegente.com.br' style='text-decoration: none;font-weight: bold;color:#a3a3a3'>Atendimento clicando aqui.</a><br> De 2ª a 6ª feira, das 9h às 17h. </p> </td> </tr> <tr><td valign='top' height='19' width='600'></td></tr> <tr> <td> <p style='font-size:11px;color:#000;font-family:Arial;text-align: center;display: block;'> </p> </td> </tr> <tr><td valign='top' height='19'> <img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='600' height='19' alt='Grão de Gente' style='display:block;border:none;' /> </td></tr><tr><td><p style='font-size:11px;color:#000;font-family:Arial;text-align: center;display: block;'>Quer descadastrar o seu e-mail? <a href='##optout##' target='_blank' style='color:#000000'>Clique aqui!</a></p></td></tr></tbody></table></center></body></html>";

        #endregion
        File.WriteAllText(arquivo, meuHtml);
        mensagem += "<div style='color:green;font:bold 12px verdana;float:left;clear:left;'> Produtos cadastrados</div>";
        mensagem += "<div style='color:#444;font:bold 12px verdana;float:left;clear:left;'> Link da página : </div>";
        mensagem += "<div style='color:#444;font:normal 12px verdana;float:left;clear:left;cursor:pointer;' onclick=\"window.open('"
                   + arquivoLink + "');\">" + arquivoLink + "</div>";
        // Response.Write(arquivo);
        ltmensagem.Text = mensagem;
    }

    string inseriProduto(int produtoId, int posicao, bool ultimo, string nomeDoArquivoSelo)
    {
        string retorno = "";
        var data = new dbCommerceDataContext();
        string fotoDestaque = "";
        var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoId orderby c.produtoId select c).ToList();
        if (fotos.Count <= 0)
        {
            mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> O produto " + produtoId +
                        " não foi cadastrado por não possuir fotos</div>";
            return mensagem;
        }
    
       
        if (posicao == 1)
            retorno += "<tr>";

        var produto = (from p in data.tbProdutos
                       join jpc in data.tbJuncaoProdutoCategorias on p.produtoId equals jpc.produtoId
                       join pc in data.tbProdutoCategorias on jpc.categoriaId equals pc.categoriaId
                       where p.produtoId == produtoId
                       select new
                       {
                           p,
                           pc
                       }).FirstOrDefault();
       
        if(!String.IsNullOrEmpty(produto.p.fotoDestaque))
        {
            fotoDestaque = produto.p.fotoDestaque;
        }
        else
        {
            if (fotos.Any(x => x.produtoFotoDestaque.ToLower() == "true"))
            {
                fotoDestaque = fotos.First(x => x.produtoFotoDestaque.ToLower() == "true").produtoFoto;

            }
            else
            {
                fotoDestaque = fotos.First().produtoFoto;

            }
        }
        string fotoDestaqueMedia = "http://dmhxz00kguanp.cloudfront.net/fotos/" + produtoId + "/media_" + fotoDestaque + ".jpg";
        string urlProduto = "http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + utm;
        string produtoNome = produto.p.produtoNome;
        string produtoIdString = produto.p.produtoId.ToString();
        var precoExibicao = produto.p.produtoPrecoPromocional > 0 && produto.p.produtoPrecoPromocional < produto.p.produtoPreco ? produto.p.produtoPrecoPromocional : produto.p.produtoPreco;
        precoExibicao = precoExibicao - ((precoExibicao / 100) * 15);
        decimal porcentagemOff = (decimal)(100 - ((precoExibicao * 100) / produto.p.produtoPreco));
        string porcentagemOffString = Math.Floor(porcentagemOff).ToString();
        string parcelamento = produto.p.parcelamentoMaximo + "x de " + produto.p.parcelamentoMaximoValorParcela.ToString("C");
        string produtoPreco = (produto.p.produtoPreco).ToString("C");
        string precoAVista = "";
        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0) precoAVista = Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85)).ToString("C");
        else precoAVista = Convert.ToDecimal(produto.p.produtoPreco * Convert.ToDecimal(0.85)).ToString("C");
        string condicaoParcelamento = produto.p.parcelamentoMaximo + "x de " + produto.p.parcelamentoMaximoValorParcela.ToString("C");

        string templateProduto = "<td width='190' valign='top' style='padding-top: 55px;'><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td><a href='{urlProduto}' title='{produtoNome}' target='_blank'><img src='{fotoDestaqueMedia}' width='190' height='190' alt='{produtoNome}' style='display:block;border:none;'></a></td></tr><tr><td style='position:relative;height: 185px;padding-top: 10px;text-align:left;' valign='top' width='190'><span style='display: block;font-size:12px;color:#666666;font-family:Arial;display: block;margin-bottom:10px;width: 100%;text-transform:uppercase;'>{produtoNome}<br>Ref: {produtoId}</span>{templatePreco}<span style='color:#eb466c;font-size: 18px;font-family:Arial;font-weight:bold;'>{condicaoParcelamento}</span><span style='color:#999999;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>s/ juros</span>{templateDesconto}</td></tr><tr><td><table border='0' cellpadding='0' cellspacing='0'><tbody><tr><td valign='top' width='190'><a href='{urlProduto}' title='{produtoNome}' target='_blank' style='float: left;width: 90%;background: #ff456e;text-decoration: none;padding: 8px 5%;font-family: Arial;color: #FFF;text-align: center;border-radius: 4px;font-weight: bold;font-size: 14px;'>Ver Detalhes &gt;</a></td></tr></tbody></table></td></tr></tbody></table></td>";
        string templatePreco = "";
        string templateBrindes = "";
        string templateDesconto = "";


        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0)
        {
            templatePreco = "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;text-decoration:line-through;'> de " + produtoPreco + " por" + "</span>" +
            "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" + precoAVista + " à vista" + "</span>";

            templateDesconto = "<span style='color:#666666;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>" +
                              "desconto de" +
                              "<span style='margin-top:5px;height: 25px;padding: 0 5px;color: #ffffff;font-size: 18px;font-weight: bold;line-height: 25px;padding-left: 6px;font-family: Arial;border-radius: 5px;display: inline-block;margin-left:6px;background-color: #E94A6E;'>" +
                              "- " + porcentagemOffString + "%" +
                              "</span>" +
                            "</span>";
        }
        else
        {
            templatePreco =
            "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" +
             precoAVista + " à vista" +
            "</span>";
        }


        if (Convert.ToInt32(produto.p.produtoBrindes == "" ? "0" : produto.p.produtoBrindes) > 0)
        {
            string plural = Convert.ToInt32(produto.p.produtoBrindes) > 1 ? "S" : "";
            templateBrindes = "<span style='clear:left; float: left; color: #ffffff;padding: 0 8px;font-size: 14px;margin: 10px 0 5px 0px;line-height: 20px;border-radius: 20px;background-color: #1a5481; font-family: arial; font-weight: bold;'> GANHE " + produto.p.produtoBrindes + " BRINDE" + plural + "!</span>";
        }

        templateProduto = templateProduto.Replace("{urlProduto}", urlProduto);
        templateProduto = templateProduto.Replace("{templatePreco}", templatePreco);
        templateProduto = templateProduto.Replace("{fotoDestaqueMedia}", fotoDestaqueMedia);
        templateProduto = templateProduto.Replace("{produtoNome}", produtoNome);
        templateProduto = templateProduto.Replace("{produtoId}", produtoIdString);
        templateProduto = templateProduto.Replace("{condicaoParcelamento}", condicaoParcelamento);
        templateProduto = templateProduto.Replace("{templateBrindes}", templateBrindes);
        templateProduto = templateProduto.Replace("{templateDesconto}", templateDesconto);
        retorno += templateProduto;
        
        if (!String.IsNullOrEmpty(nomeDoArquivoSelo))
        {
            retorno += "<tr><td><img src='../../banners/newsletter/" + nomeDoArquivoSelo + "' style='margin-bottom:12px;'/></td></tr>";
        }
        
        if (ultimo)
        {
            if (posicao == 1)
            {
                retorno += "<td width='15'>"+
                              "<img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='15' height='100%' alt='' style='display:block;border:none;' />" +
                            "</td>" +
                     "<td width='190' valign='top'>" +
                        "<td width='15'>"+
                              "<img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='15' height='100%' alt='' style='display:block;border:none;' />" +
                            "</td>" +
                        "<td width='190' valign='top'>" +
                        "</tr>";
            }
            if (posicao == 2)
            {
                retorno += "<td width='15'>"+
                              "<img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='15' height='100%' alt='' style='display:block;border:none;' />" +
                            "</td>" +
                            "<td width='190' valign='top'>" +
                          "</tr>";
            }
            if (posicao == 3)
            {
                retorno += "</tr>";
            }
        }
        else
        {
            if (posicao <= 2)
                retorno += "<td width='15'>"+
                              "<img src='http://emkt.graodegente.com.br/email-novo/imagens/spacer-white.jpg' width='15' height='100%' alt='' style='display:block;border:none;' />" +
                            "</td>";

            if (posicao == 3)
                retorno += "</tr>";
        }


        return retorno;
    }

    string inseriProdutoResponsivo(int produtoId, int posicao, bool ultimo, string nomeDoArquivoSelo)
    {
        string retorno = "";
        var data = new dbCommerceDataContext();
        string fotoDestaque = "";
        var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoId orderby c.produtoId select c).ToList();
        if (fotos.Count <= 0)
        {
            mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> O produto " + produtoId +
                        " não foi cadastrado por não possuir fotos</div>";
            return mensagem;
        }
        if (fotos.Any(x => x.produtoFotoDestaque.ToLower() == "true"))
        {
            fotoDestaque = fotos.First(x => x.produtoFotoDestaque.ToLower() == "true").produtoFoto;

        }
        else
        {
            fotoDestaque = fotos.First().produtoFoto;

        }
        string fotoDestaqueMedia = "http://dmhxz00kguanp.cloudfront.net/fotos/" + produtoId + "/media_" + fotoDestaque + ".jpg";

        var produto = (from p in data.tbProdutos
                       join jpc in data.tbJuncaoProdutoCategorias on p.produtoId equals jpc.produtoId
                       join pc in data.tbProdutoCategorias on jpc.categoriaId equals pc.categoriaId
                       where p.produtoId == produtoId
                       select new
                       {
                           p,
                           pc
                       }).FirstOrDefault();

        retorno += "<td  valign='top'>" +
                   //retorno += "<td width='200px' valign='top'>" +
                   "<table border='0' cellpadding='0' cellspacing='0'>" +
                   "<tbody>"+
                   "<!-- IMAGEM DO PRODUTO -->" +

                   "<tr>" +
                   "<td>" +
                   "<a href='http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + "" + utm + "' title='" + produto.p.produtoNome + "' target='_blank'>" +
                   "<img src='" + fotoDestaqueMedia +
                   "' width='190' height='190' alt='Quarto para Bebê Realeza Marinho' style='display:block;border:none;' />" +
                   "</a>" +
                   "</td>" +
                   "</tr>" +
                   //"<tr>" +
                   //"<td>" +
                   //"<a href='http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + "' title='" + produto.p.produtoNome + "' target='_blank'>" +
                   //"<img src='" + fotoDestaqueMedia +
                   //"' width='185' height='198' alt='Quarto para Bebê Realeza Marinho' style='display:block;border:none;' />" +
                   //"</a>" +
                   //"</td>" +
                   //"</tr>" +

                   "<!-- INFORMAÇÕES DO PRODUTO -->" +
                   "<tr>" +
                   "<td style='position:relative;height: 185px;padding-top: 10px;text-align:left;' valign='top'>" +

                   "<span style='display: block;font-size:12px;color:#666666;font-family:Arial;display: block;margin-bottom:10px;width:80%px;text-transform:uppercase;'>" +
                   produto.p.produtoNome + "<br />" +

                   "Ref: " + produto.p.produtoId +
                   "</span> ";
        //"<span style='display: block;font-size:12px;color:#666666;font-family:Arial;display: block;margin-bottom:10px;width:140px;text-transform:uppercase;'>" +
        //produto.p.produtoNome + "<br />" +

        //"Ref: " + produto.p.produtoId +
        //"</span> ";

        var precoExibicao = produto.p.produtoPrecoPromocional > 0 && produto.p.produtoPrecoPromocional < produto.p.produtoPreco ? produto.p.produtoPrecoPromocional : produto.p.produtoPreco;
        precoExibicao = precoExibicao - ((precoExibicao / 100) * 15);
        decimal porcentagemOff = (decimal)(100 - ((precoExibicao * 100) / produto.p.produtoPreco));
        string porcentagemOffString = Math.Floor(porcentagemOff).ToString();

        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0)
        {
            retorno +=
          "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;text-decoration:line-through;'>" +
          "de " + (produto.p.produtoPreco).ToString("C") + " por" +
          "</span>" +
          "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" +
          Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85)).ToString("C") + " à vista" +
          "</span>";
        }
        else
        {
            retorno +=
            "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" +
            Convert.ToDecimal(produto.p.produtoPreco * Convert.ToDecimal(0.85)).ToString("C") + " à vista" +
            "</span>";
        }


        retorno += "<span style='color:#eb466c;font-size: 18px;font-family:Arial;font-weight:bold;'>" +
                   produto.p.parcelamentoMaximo + "x de " + produto.p.parcelamentoMaximoValorParcela.ToString("C") +

                   "</span>" +
                   "<span style='color:#999999;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>" +
                   "s/ juros" +
                   "</span>";

        if (Convert.ToInt32(produto.p.produtoBrindes == "" ? "0" : produto.p.produtoBrindes) > 0)
        {

            //decimal desconto = ((( produto.p.produtoPreco ) -
            //                     Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85))) / (produto.p.produtoPreco )) * 100;
            //int descontoint = Convert.ToInt32(desconto);
            string plural = Convert.ToInt32(produto.p.produtoBrindes) > 1 ? "S" : "";

            retorno += "<span style='clear:left; float: left; color: #ffffff;padding: 0 8px;font-size: 14px;margin: 10px 0 5px 0px;line-height: 20px;border-radius: 20px;background-color: #1a5481; font-family: arial; font-weight: bold;'> GANHE " + produto.p.produtoBrindes + " BRINDE" + plural + "!</span>";
        }

        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0)
        {

            //decimal desconto = ((( produto.p.produtoPreco ) -
            //                     Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85))) / (produto.p.produtoPreco )) * 100;
            //int descontoint = Convert.ToInt32(desconto);
            retorno += "<span style='color:#666666;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>" +
                "desconto de" +
                "<span style='margin-top:5px;height: 25px;padding: 0 5px;color: #ffffff;font-size: 18px;font-weight: bold;line-height: 25px;padding-left: 6px;font-family: Arial;border-radius: 5px;display: inline-block;margin-left:6px;background-color: #E94A6E;'>" +
                "- " + porcentagemOffString + "%" +
                "</span>" +
                "</span>";
        }


        retorno += "</td>" +
                   "</tr>" +
                   "<tr>" +
                   "<td>" +
                   "<img src='http://www.graodegente.com.br/admin/admin/envio/imagensResponsivo/index_r11_c2.jpg' width='100%' alt='" +
                   produto.p.produtoNome + "' style='display:block;border:none;' />" +
                   //"<img src='http://www.graodegente.com.br/admin/admin/envio/imagensResponsivo/index_r11_c2.jpg' width='185' height='14' alt='" +
                   //produto.p.produtoNome + "' style='display:block;border:none;' />" +
                   "</td>" +
                   "</tr>";
        if (!String.IsNullOrEmpty(nomeDoArquivoSelo))
        {
            retorno += "<tr><td><img src='../../banners/newsletter/" + nomeDoArquivoSelo + "' style='margin-bottom:12px;'/></td></tr></tr>";
        }
        retorno += "<!-- VER DETALHES -->" +
                     "<tr>" +
                     "<td>" +
                     "<table border='0' cellpadding='0' cellspacing='0'>" +
                        "<tbody>"+
                          "<tr>" +
                           "<td valign='top' width='190'>" +
                              "<a href='http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + "" + utm + "' title='" + produto.p.produtoNome + "' target='_blank' style='float: left;width: 90%;background: #ff456e;text-decoration: none;padding: 8px 5%;font-family: Arial;color: #FFF;text-align: center;border-radius: 4px;font-weight: bold;font-size: 14px;'>" +
                               "Ver Detalhes >" +
                             "</a>" +
                           "</td>" +
                         "</tr>" +
                        "</tbody>"+
                      "</table>" +
                     "</td>" +
                     "</tr>" +
                     "</tbody>"+
                    "</table>" +
                    "</td>";
        if (ultimo)
        {
            if (posicao == 1)
            {
                retorno += "<td valign='top'> <td valign='top'>";
            }

            if (posicao == 2)
            {
                retorno += "<td valign='top'>";
            }
        }


        return retorno;
    }

    string inseriProduto600(int produtoId, int posicao, bool ultimo, string nomeDoArquivoSelo)
    {
        string retorno = "";
        var data = new dbCommerceDataContext();
        string fotoDestaque = "";
        var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoId orderby c.produtoId select c).ToList();
        if (fotos.Count <= 0)
        {
            mensagem += "<div style='color:red;font:bold 12px verdana;float:left;clear:left;'> O produto " + produtoId +
                        " não foi cadastrado por não possuir fotos</div>";
            return mensagem;
        }
        if (fotos.Any(x => x.produtoFotoDestaque.ToLower() == "true"))
        {
            fotoDestaque = fotos.First(x => x.produtoFotoDestaque.ToLower() == "true").produtoFoto;

        }
        else
        {
            fotoDestaque = fotos.First().produtoFoto;

        }
        string fotoDestaqueMedia = "http://dmhxz00kguanp.cloudfront.net/fotos/" + produtoId + "/media_" + fotoDestaque + ".jpg";
        if (posicao == 1)
            retorno += "<tr>";

        var produto = (from p in data.tbProdutos
                       join jpc in data.tbJuncaoProdutoCategorias on p.produtoId equals jpc.produtoId
                       join pc in data.tbProdutoCategorias on jpc.categoriaId equals pc.categoriaId
                       where p.produtoId == produtoId
                       select new
                       {
                           p,
                           pc
                       }).FirstOrDefault();

        retorno += "<td width='170px' valign='top'>" +
                   "<table border='0' cellpadding='0' cellspacing='0'>" +
                   "<tbody>"+
                   "<!-- IMAGEM DO PRODUTO -->" +
                   "<tr>" +
                   "<td>" +
                   "<a href='http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + "" + utm + "' title='" + produto.p.produtoNome + "' target='_blank'>" +
                   "<img src='" + fotoDestaqueMedia +
                   "' width='190' height='190'  alt='Quarto para Bebê Realeza Marinho' style='display:block;border:none;' />" +
                   "</a>" +
                   "</td>" +
                   "</tr>" +
                   "<!-- INFORMAÇÕES DO PRODUTO -->" +
                   "<tr>" +
                   "<td style='position:relative;height: 185px;padding-top: 10px;text-align:left;' valign='top'>" +

                   "<span style='display: block;font-size:12px;color:#666666;font-family:Arial;display: block;margin-bottom:10px;width:140px;text-transform:uppercase;'>" +
                   produto.p.produtoNome + "<br />" +

                   "Ref: " + produto.p.produtoId +
                   "</span> ";
        var precoExibicao = produto.p.produtoPrecoPromocional > 0 && produto.p.produtoPrecoPromocional < produto.p.produtoPreco ? produto.p.produtoPrecoPromocional : produto.p.produtoPreco;
        precoExibicao = precoExibicao - ((precoExibicao / 100) * 15);
        decimal porcentagemOff = (decimal)(100 - ((precoExibicao * 100) / produto.p.produtoPreco));
        string porcentagemOffString = Math.Floor(porcentagemOff).ToString();

        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0)
        {
            retorno +=
          "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;text-decoration:line-through;'>" +
          "de " + (produto.p.produtoPreco).ToString("C") + " por" +
          "</span>" +
          "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" +
          Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85)).ToString("C") + " à vista" +
          "</span>";
        }
        else
        {
            retorno +=
            "<span style='font-size:14px;color:#7d8043;font-family:Arial;display: block;font-weight:bold;margin-bottom: 10px;'>" +
            Convert.ToDecimal(produto.p.produtoPreco * Convert.ToDecimal(0.85)).ToString("C") + " à vista" +
            "</span>";
        }


        retorno += "<span style='color:#eb466c;font-size: 18px;font-family:Arial;font-weight:bold;'>" +
                   produto.p.parcelamentoMaximo + "x de " + produto.p.parcelamentoMaximoValorParcela.ToString("C") +

                   "</span>" +
                   "<span style='color:#999999;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>" +
                   "s/ juros" +
                   "</span>";

        if (Convert.ToInt32(produto.p.produtoBrindes == "" ? "0" : produto.p.produtoBrindes) > 0)
        {

            //decimal desconto = ((( produto.p.produtoPreco ) -
            //                     Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85))) / (produto.p.produtoPreco )) * 100;
            //int descontoint = Convert.ToInt32(desconto);
            string plural = Convert.ToInt32(produto.p.produtoBrindes) > 1 ? "S" : "";

            retorno += "<span style='clear:left; float: left; color: #ffffff;padding: 0 8px;font-size: 14px;margin: 10px 0 5px 0px;line-height: 20px;border-radius: 20px;background-color: #1a5481; font-family: arial; font-weight: bold;'> GANHE " + produto.p.produtoBrindes + " BRINDE" + plural + "!</span>";
        }

        if (Convert.ToDecimal(produto.p.produtoPrecoPromocional) > 0)
        {

            //decimal desconto = ((( produto.p.produtoPreco ) -
            //                     Convert.ToDecimal(produto.p.produtoPrecoPromocional * Convert.ToDecimal(0.85))) / (produto.p.produtoPreco )) * 100;
            //int descontoint = Convert.ToInt32(desconto);
            retorno += "<span style='color:#666666;display: block;font-size: 14px;font-family:Arial;font-weight:bold;'>" +
                "desconto de" +
                "<span style='margin-top:5px;height: 25px;padding: 0 5px;color: #ffffff;font-size: 18px;font-weight: bold;line-height: 25px;padding-left: 6px;font-family: Arial;border-radius: 5px;display: inline-block;margin-left:6px;background-color: #E94A6E;'>" +
                "- " + porcentagemOffString + "%" +
                "</span>" +
                "</span>";
        }


        retorno += "</td>" +
                   "</tr>" +
                   "<tr>" +
                   "<td>" +
                   "<img src='http://www.graodegente.com.br/admin/admin/envio/imagens600/index_r11_c2.jpg' width='170' height='14' alt='" +
                   produto.p.produtoNome + "' style='display:block;border:none;' />" +
                   "</td>" +
                   "</tr>";
        if (!String.IsNullOrEmpty(nomeDoArquivoSelo))
        {
            retorno += "<tr><td><img src='../../banners/newsletter/" + nomeDoArquivoSelo + "' style='margin-bottom:12px;'/></td></tr>";
        }
        retorno += "<!-- VER DETALHES -->" +  
                     "<tr>" +
                       "<td>" +
                         "<table width='190' border='0' cellpadding='0' cellspacing='0'>" +
                            "<tbody>"+
                            "<tr>" +
                             "<td valign='top' width='190'>" +
                                "<a href='http://www.graodegente.com.br/" + produto.pc.categoriaUrl + "/" + produto.p.produtoUrl + "" + utm + "' title='" + produto.p.produtoNome + "' target='_blank' style='float: left;width: 90%;background: #ff456e;text-decoration: none;padding: 8px 5%;font-family: Arial;color: #FFF;text-align: center;border-radius: 4px;font-weight: bold;font-size: 14px;'>" +
                                 "Ver Detalhes >" +
                               "</a>" +
                             "</td>" +
                           "</tr>" +
                          "</tbody>"+
                        "</table>" +
                       "</td>" +
                      "</tr>" +
                      "</tbody>"+
                    "</table>" +
                  "</td>";
        if (ultimo)
        {
            if (posicao == 1)
            {
                retorno += "<td width='15'></td>" +
                     "<td width='170px' valign='top'>" +
                        "<td width='61px'></td>" +
                        "<td width='170px' valign='top'>" +
                        "</tr>";
            }
            if (posicao == 2)
            {
                retorno += "<td width='61px'></td>" +
                     "<td width='170px' valign='top'>" +
                        "</tr>";
            }
            if (posicao == 3)
            {
                retorno += "</tr>";
            }
        }
        else
        {
            if (posicao <= 2)
                retorno += "<td width='61px'></td>";

            if (posicao == 3)
                retorno += "</tr>";
        }


        return retorno;
    }
}