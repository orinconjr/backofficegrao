﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_despesa : System.Web.UI.Page
{

    DateTemplate dateTemplate = null;


    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            TextBox txtValor = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valor"], "txtValor");
            txtValor.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            TextBox txtValorPago = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorPago"], "txtValorPago");
            txtValorPago.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");

            TextBox txtPedido = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["idPedido"], "txtPedido");
            txtPedido.Attributes.Add("onkeyPress", "return soNumero(event);");
        }
        
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        e.NewValues["idUsuario"] = usuarioLogadoId.Value.ToString();

        TextBox txtValor = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valor"], "txtValor");
        e.NewValues["valor"] = txtValor.Text.Replace(".", "");
        TextBox txtValorPago = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorPago"], "txtValorPago");
        if (!string.IsNullOrEmpty(txtValorPago.Text))
        {
            e.NewValues["valorPago"] = txtValorPago.Text.Replace(".", "");
        }
        
    }

    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {

        TextBox txtValor = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valor"], "txtValor");
        e.NewValues["valor"] = txtValor.Text.Replace(".", "");

        TextBox txtValorPago = (TextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["valorPago"], "txtValorPago");
        if (!string.IsNullOrEmpty(txtValorPago.Text))
        {
            e.NewValues["valorPago"] = txtValorPago.Text.Replace(".", "");
        }
        

    }


    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;

            dateTemplate = new DateTemplate();

            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|")
            return;
        if (e.Column.FieldName == "data")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["data"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("data") >= dateFrom) &
                             (new OperandProperty("data") <= dateTo);
            }
            else
            {
                if (Session["data"] != null)
                    e.Value = Session["data"].ToString();
            }
        }
    }

    protected void grd_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
    {
        try
        {

            int inicio = 0;
            int fim = 0;

            if (grd.PageIndex == 0)
            {
                inicio = 0;
                fim = inicio + grd.GetCurrentPageRowValues("idDespesa").Count;
            }
            else
            {
                inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
                fim += inicio + grd.GetCurrentPageRowValues("idDespesa").Count;
            }

            for (int i = inicio; i < fim; i++)
            {
                int idDespesa = Convert.ToInt32(grd.GetCurrentPageRowValues("idDespesa")[i]);
                ASPxCheckBox ckbPago = (ASPxCheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["pago"] as GridViewDataColumn, "ckbPago");

                var data = new dbCommerceDataContext();
                var despesa = (from c in data.tbDespesas where c.idDespesa == idDespesa select c).First();
                despesa.pago = ckbPago.Checked;
                data.SubmitChanges();
            }
        }
        catch (Exception)
        {

        }
        grd.DataBind();
    }
}
