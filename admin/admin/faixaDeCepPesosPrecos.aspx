﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="faixaDeCepPesosPrecos.aspx.cs" Inherits="admin_faixaDeCepPesosPrecos"   Theme="Glass" MaintainScrollPositionOnPostback=true  %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td style="border: 1px solid #7EACB1; height: 19px">
                <table cellpadding="0" cellspacing="0" style="width: 834px" 
                                    __designer:mapid="3f6">
                    <tr __designer:mapid="3f7">
                        <td height="50" style="border-right: 1px solid #7EACB1; width: 336px;" 
                                            __designer:mapid="3f8">
                            <table align="right" cellpadding="0" cellspacing="0" 
                                                style="width: 275px; height: 15px" __designer:mapid="3f9">
                                <tr __designer:mapid="3fa">
                                    <td class="rotulos" __designer:mapid="3fb">
                                        Peso inicial<br __designer:mapid="3fc" />
                                        <asp:TextBox ID="txtPesoInicial" runat="server" CssClass="campos" 
                                                            Text='<%# Bind("faixaDeCepPesoInicial") %>' ValidationGroup="grpInsert" 
                                                            Width="98%"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqvPesoInicial" runat="server" 
                                                            ControlToValidate="txtPesoInicial" Display="Dynamic" 
                                                            ErrorMessage="Preencha o peso inicial." SetFocusOnError="True" 
                                                            ValidationGroup="grpInsert"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td class="rotulos" 
                                            style="border-right: 1px solid #7EACB1; width: 361px; padding-left: 9px;" 
                                            __designer:mapid="400">
                            Peso final<br __designer:mapid="401" />
                            <asp:TextBox ID="txtPesoFinal" runat="server" CssClass="campos" 
                                                Text='<%# Bind("faixaDeCepPesoFinal") %>' ValidationGroup="grpInsert" 
                                                Width="97%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvPesoFinal" runat="server" 
                                                ControlToValidate="txtPesoFinal" Display="Dynamic" 
                                                ErrorMessage="Preencha o peso final." SetFocusOnError="True" 
                                                ValidationGroup="grpInsert"></asp:RequiredFieldValidator>
                        </td>
                        <td class="rotulos" 
                                            style="border-right: 1px solid #7EACB1; width: 257px; padding-left: 9px;" 
                                            __designer:mapid="404">
                            Preço<br __designer:mapid="405" />
                            <asp:TextBox ID="txtPreco" runat="server" CssClass="campos" 
                                                Text='<%# Bind("faixaDeCepPreco") %>' ValidationGroup="grpInsert" 
                                                Width="97%"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqvPreco0" runat="server" 
                                                ControlToValidate="txtPreco" Display="Dynamic" ErrorMessage="Preencha o preço." 
                                                SetFocusOnError="True" ValidationGroup="grpInsert"></asp:RequiredFieldValidator>
                        </td>
                        <td align="center" style="width: 50px;" __designer:mapid="408">
                            <asp:ImageButton ID="imbInsert" runat="server" 
                                                ImageUrl="~/admin/images/btNovo1.jpg" 
                                ValidationGroup="grpInsert" onclick="imbInsert_Click" />
                        </td>
                    </tr>
                </table>
                </td>
        </tr>
        <tr>
            <td style="height: 19px">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlFaixaDeCepPesoPreco" 
                    KeyFieldName="faixaDeCepPesoPrecoId" Width="834px" 
                    Cursor="auto" 
                    onhtmlrowcreated="grd_HtmlRowCreated" ClientInstanceName="grd" 
                    EnableCallBacks="False" oncustomcallback="grd_CustomCallback">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" 
                            FieldName="faixaDeCepPesoPrecoId" ReadOnly="True" VisibleIndex="0" 
                            Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                            <DataItemTemplate>
                                <asp:TextBox ID="txtFaixaDeCepPesoPrecoId" runat="server" BorderStyle="None" 
                                    CssClass="campos" Text='<%# Eval("faixaDeCepPesoPrecoId") %>' Width="100%"></asp:TextBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso inicial" FieldName="faixaDeCepPesoInicial" 
                            VisibleIndex="1">
                            <PropertiesTextEdit MaxLength="8">
                                <ValidationSettings>
                                    <RegularExpression ErrorText="" />
                                    <RequiredField ErrorText="Preencha o peso inicial." IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <DataItemTemplate>
                                <asp:TextBox ID="txtPesoInicial" runat="server" BorderStyle="None" 
                                    CssClass="campos" Text='<%# Eval("faixaDeCepPesoInicial") %>' Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqvPreco" runat="server" 
                                    ControlToValidate="txtPesoInicial" Display="Dynamic" 
                                    ErrorMessage="Preencha o peso inicial." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Peso final" FieldName="faixaDeCepPesoFinal" 
                            VisibleIndex="2">
                            <PropertiesTextEdit MaxLength="2">
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha o prazo de entrega." IsRequired="True" />
                                    <RegularExpression ErrorText="" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <DataItemTemplate>
                                <asp:TextBox ID="txtPesoFinal" runat="server" BorderStyle="None" 
                                    CssClass="campos" Text='<%# Eval("faixaDeCepPesoFinal") %>' Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqvPreco" runat="server" 
                                    ControlToValidate="txtPesoFinal" Display="Dynamic" 
                                    ErrorMessage="Preencha o peso final." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Preço" FieldName="faixaDeCepPreco" 
                            VisibleIndex="3">
                            <PropertiesTextEdit MaxLength="8">
                            <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a faixa final." IsRequired="True" />
                                    <RegularExpression ErrorText="" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                            <DataItemTemplate>
                                <asp:TextBox ID="txtPreco" runat="server" BorderStyle="None" CssClass="campos" 
                                    Text='<%# Eval("faixaDeCepPreco", "{0:C}").Replace("R$","").Trim() %>' 
                                    Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rqvpreco" runat="server" 
                                    ControlToValidate="txtPreco" Display="Dynamic" ErrorMessage="Preencha o preço." 
                                    Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Excluir" VisibleIndex="4">
                            <DataItemTemplate>
                                <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" 
                                    ValueType="System.String" ValueUnchecked="False">
                                </dxe:ASPxCheckBox>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:ObjectDataSource ID="sqlFaixaDeCepPesoPreco" runat="server" 
                    SelectMethod="faixaDeCepPesoPrecoSeleciona_PorFaixaDeCepId" 
                    TypeName="rnFaixaDeCepPesoPreco">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="faixaDeCepId" QueryStringField="faixaDeCepId" 
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="right">
<img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);"/></td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>

