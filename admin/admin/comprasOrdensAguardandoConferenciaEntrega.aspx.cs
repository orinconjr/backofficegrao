﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_comprasOrdensAguardandoConferenciaEntrega : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillGrid(false);
    }

    protected void btnPesquisar_Click(object sender, ImageClickEventArgs e)
    {
        FillGrid(true);
    }

    private void FillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbComprasOrdems
                       join d in data.tbComprasOrdemProdutos on c.idComprasOrdem equals d.idComprasOrdem into produtos
                       where c.statusOrdemCompra == 6
                       select new
                       {
                           c.idComprasOrdem,
                           c.dataCriacao,
                           c.idUsuarioCadastroOrdem,
                           c.idComprasEmpresa,
                           c.idComprasFornecedor,
                           produtos = produtos.Count(),
                           produtosConferidos = produtos.Count(x => x.quantidadeRecebida != null),
                           valor = produtos.Count(x => x.status == 1) > 0 ? produtos.Where(x => x.status == 1).Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade)) : 0,
                           colCondPag = c.idComprasCondicaoPagamento == null ? "Nenhuma selecionada" : c.idComprasCondicaoPagamento.ToString()
                       });
        grd.DataSource = pedidos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }


    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void btnEnviarAprovacao_OnCommand(object sender, CommandEventArgs e)
    {
        int idComprasOrdem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).First();
        pedido.statusOrdemCompra = 3; //Enviado para Financeiro
        pedido.idUsuarioLancamentoFinanceiro = RetornaIdUsuarioLogado();
        if (pedido.dataEnvioFinanceiro == null) pedido.dataEnvioFinanceiro = DateTime.Now;

        var pedidoHistorico = new tbComprasOrdemHistorico();
        pedidoHistorico.data = DateTime.Now;
        pedidoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        pedidoHistorico.idComprasOrdem = pedido.idComprasOrdem;
        pedidoHistorico.descricao = "Pedido enviado para Lançamento Financeiro";
        data.tbComprasOrdemHistoricos.InsertOnSubmit(pedidoHistorico);
        data.SubmitChanges();

        FillGrid(true);
    }

    protected void btnCancelar_OnCommand(object sender, CommandEventArgs e)
    {
        int idComprasOrdem = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido = (from c in data.tbComprasOrdems where c.idComprasOrdem == idComprasOrdem select c).First();
        pedido.statusOrdemCompra = 5; //Cancelado
        pedido.idUsuarioEnvioAprovacao = RetornaIdUsuarioLogado();

        var pedidoHistorico = new tbComprasOrdemHistorico();
        pedidoHistorico.data = DateTime.Now;
        pedidoHistorico.usuario = rnUsuarios.retornaNomeUsuarioLogado();
        pedidoHistorico.idComprasOrdem = pedido.idComprasOrdem;
        pedidoHistorico.descricao = "Ordem de compra cancelada";
        data.tbComprasOrdemHistoricos.InsertOnSubmit(pedidoHistorico);
        data.SubmitChanges();

        FillGrid(true);
    }
}