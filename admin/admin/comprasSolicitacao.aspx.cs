﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Web.ASPxEditors;

public partial class admin_comprasSolicitacao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ASPxComboBox_OnItemsRequestedByFilterCondition_SQL(object source, ListEditItemsRequestedByFilterConditionEventArgs e)
    {
        ASPxComboBox comboBox = (ASPxComboBox)source;
        SqlDataSource1.SelectCommand = @"select * from tbComprasProduto where produto like @filter order by produto ";
        SqlDataSource1.SelectParameters.Clear();
        SqlDataSource1.SelectParameters.Add("filter", TypeCode.String, string.Format("%{0}%", e.Filter));
        comboBox.DataSource = SqlDataSource1;
        comboBox.DataBind();
    }
    protected void ASPxComboBox_OnItemRequestedByValue_SQL(object source, ListEditItemRequestedByValueEventArgs e)
    {
    
    }
    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var lista = new List<tbComprasSolicitacaoProduto>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        if (lista.Count == 0)
        {
            Response.Write("<script>alert('Favor adicionar produtos à solicitação de compra.');</script>");
            return;
        }
        if (string.IsNullOrEmpty(txtMotivo.Text))
        {
            Response.Write("<script>alert('Favor preencher o motivo da solicitação.');</script>");
            txtMotivo.Focus();
            return;
        }

        var data = new dbCommerceDataContext();
        var solicitacao = new tbComprasSolicitacao();
        solicitacao.idUsuario = RetornaIdUsuarioLogado();
        solicitacao.dataSolicitacao = DateTime.Now;
        solicitacao.motivo = txtMotivo.Text;
        data.tbComprasSolicitacaos.InsertOnSubmit(solicitacao);
        data.SubmitChanges();

        foreach (var produtoSolicitacao in lista)
        {
            var produto = new tbComprasSolicitacaoProduto();
            produto.idComprasProduto = produtoSolicitacao.idComprasProduto == 0
                ? null
                : produtoSolicitacao.idComprasProduto;
            produto.quantidade = produtoSolicitacao.quantidade;
            produto.statusSolicitacao = 0;
            produto.idComprasSolicitacao = solicitacao.idComprasSolicitacao;
            produto.observacao = "";
            produto.produto = produtoSolicitacao.produto;
            data.tbComprasSolicitacaoProdutos.InsertOnSubmit(produto);
            data.SubmitChanges();
        }
        Response.Redirect("comprasMinhasSolicitacoes.aspx");
    }


    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }
    protected void btnGravarProduto_OnClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txtQuantidade.Value) > 0)
        {
            var produto = new tbComprasSolicitacaoProduto();
            int idComprasProduto = 0;
            try
            {
                idComprasProduto = Convert.ToInt32(ddlProduto.SelectedItem.Value);
            }
            catch (Exception)
            {

            }
            if (idComprasProduto > 0) produto.idComprasProduto = idComprasProduto;
            produto.quantidade = Convert.ToInt32(txtQuantidade.Value);
            produto.statusSolicitacao = 0;
            produto.idComprasSolicitacao = 0;
            produto.observacao = "";
            produto.produto = ddlProduto.Text;

            AdicionaItemListaTemporariaFornecedores(produto);
        }
    }

    private void AdicionaItemListaTemporariaFornecedores(tbComprasSolicitacaoProduto item)
    {
        var lista = new List<tbComprasSolicitacaoProduto>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        int nextId = lista.Count == 0 ? 1 : lista.OrderByDescending(x => x.idComprasSolicitacaoProduto).First().idComprasSolicitacaoProduto + 1;
        item.idComprasSolicitacaoProduto = nextId;
        lista.Add(item);
        hdfLista.Text = Serialize(lista);
        FillGridProdutos();
    }
    private void RemoveItemListaTemporariaFornecedores(int idComprasSolicitacaoProduto)
    {
        var lista = new List<tbComprasSolicitacaoProduto>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        lista.RemoveAll(x => x.idComprasSolicitacaoProduto == idComprasSolicitacaoProduto);
        hdfLista.Text = Serialize(lista);
        FillGridProdutos();
    }

    private void FillGridProdutos()
    {
        var lista = new List<tbComprasSolicitacaoProduto>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        grdProdutos.DataSource = lista;
        grdProdutos.DataBind();
    }



    public static string Serialize(List<tbComprasSolicitacaoProduto> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasSolicitacaoProduto>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbComprasSolicitacaoProduto> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasSolicitacaoProduto>));

        TextReader reader = new StringReader(tData);

        return (List<tbComprasSolicitacaoProduto>)serializer.Deserialize(reader);
    }

    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        RemoveItemListaTemporariaFornecedores(Convert.ToInt32(e.CommandArgument));
    }
}