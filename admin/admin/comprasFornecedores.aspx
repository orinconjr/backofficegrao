﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasFornecedores.aspx.cs" Inherits="admin_comprasFornecedores" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Compras - Fornecedores</asp:Label></td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"  CausesValidation="false" OnClick="btXsl_Click"
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td>
                                <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="relatorioPagamentos" GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
            </dxwgv:ASPxGridViewExporter>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlComprasFornecedor" 
                    KeyFieldName="idComprasFornecedor" Width="834px" 
                    Cursor="auto" EnableCallBacks="False">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedor" ReadOnly="True" VisibleIndex="0" Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedor" VisibleIndex="1" Width="200px">
                            <PropertiesTextEdit>
                                <ValidationSettings SetFocusOnError="True">
                                    <RequiredField ErrorText="Preencha a categoria" IsRequired="True" />
                                </ValidationSettings>
                            </PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Contato" FieldName="contato" VisibleIndex="1" Width="200px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Email" FieldName="email" VisibleIndex="1" Width="200px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Telefone" FieldName="telefone" VisibleIndex="1" Width="200px">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Prazo de Entrega" FieldName="prazo" Visible="True" VisibleIndex="1">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="1" Width="37px">
                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg" NavigateUrlFormatString="produtoCad.aspx">
                            </PropertiesHyperLinkEdit>
                            <DataItemTemplate>
                                <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btNovo.jpg" ToolTip="Novo" NavigateUrl="comprasFornecedorCadastro.aspx">Novo</asp:HyperLink>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataHyperLinkColumn>
                        <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1" Width="45px">
                            <DataItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="images/btEditar.jpg" ToolTip="Alterar" NavigateUrl='<%# "comprasFornecedorCadastro.aspx?id=" + Eval("idComprasFornecedor") %>'>Alterar</asp:HyperLink>
                                    <asp:HiddenField ID="hfIdBanner" runat="server" Value='<%# Eval("idComprasFornecedor") %>' />
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataHyperLinkColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--              <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:LinqDataSource ID="sqlComprasFornecedor" runat="server" 
                    ContextTypeName="dbCommerceDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbComprasFornecedors">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>