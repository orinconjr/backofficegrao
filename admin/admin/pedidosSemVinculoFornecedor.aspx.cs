﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_pedidosSemVinculoFornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var lista = new StringBuilder();

        var dataPedidos = new dbCommerceDataContext();
        var pedidos = (from c in dataPedidos.tbPedidos where c.statusDoPedido == 11 orderby c.dataConfirmacaoPagamento select c);
        foreach (var tbPedido in pedidos)
        {
            var itensPedidoDc = new dbCommerceDataContext();
            var itens = (from c in itensPedidoDc.tbItensPedidos where c.pedidoId == tbPedido.pedidoId select c);
            var produtosDc = new dbCommerceDataContext();
            foreach (var item in itens)
            {
                bool cancelado = item.cancelado == null ? false : (bool) item.cancelado;
                if (!cancelado)
                {
                    var produtosFilho = (from c in produtosDc.tbItensPedidoCombos where c.idItemPedido == item.itemPedidoId select c);
                    if (produtosFilho.Any())
                    {
                        foreach (var tbProdutoRelacionado in produtosFilho)
                        {
                            var produtoRelacionadoFilho =
                                (from c in produtosDc.tbProdutos
                                    where c.produtoId == tbProdutoRelacionado.produtoId
                                    select c).First();
                                
                            var itemPedido = (from c in dataPedidos.tbPedidoFornecedorItems
                                              where c.idItemPedido == tbProdutoRelacionado.idItemPedido &&  c.idPedido == tbPedido.pedidoId && (c.idProduto == tbProdutoRelacionado.produtoId | c.idProduto == produtoRelacionadoFilho.produtoAlternativoId)
                                              select c).Count();
                            var itemPedidoReserva = (from c in produtosDc.tbProdutoReservaEstoques
                                                     where c.idItemPedido == tbProdutoRelacionado.idItemPedido && c.idPedido == tbPedido.pedidoId && c.idProduto == tbProdutoRelacionado.produtoId
                                                     select c).Count();
                            if ((itemPedido + itemPedidoReserva) < item.itemQuantidade)
                            {
                                //reservaProdutoOuPedeFornecedor(tbPedido.pedidoId, tbProdutoRelacionado.idProdutoFilho, Convert.ToInt32(item.itemQuantidade), item.itemPedidoId);
                                lista.Append("Pedido: " + tbPedido.pedidoId + "<br>Confirmacao: " + tbPedido.dataConfirmacaoPagamento + "<br>Produto: " + tbProdutoRelacionado.produtoId +  "<br>Item: " + tbProdutoRelacionado.idItemPedido + "<br>" + tbProdutoRelacionado.tbProduto.produtoNome + "<br>ID da Empresa: " + tbProdutoRelacionado.tbProduto.produtoIdDaEmpresa + " - " + tbProdutoRelacionado.tbProduto.complementoIdDaEmpresa + "<br><br>");

                            }
                        }
                    }
                    else
                    {
                        var itemPedido = (from c in dataPedidos.tbPedidoFornecedorItems
                                          where c.idPedido == tbPedido.pedidoId && (c.idProduto == item.produtoId | c.idProduto == item.tbProduto.produtoAlternativoId)
                                          select c).Count();
                        var itemPedidoReserva = (from c in produtosDc.tbProdutoReservaEstoques
                                                 where c.idPedido == tbPedido.pedidoId && c.idProduto == item.produtoId
                                                 select c).Count();
                        if ((itemPedido + itemPedidoReserva) < item.itemQuantidade)
                        {
                            //reservaProdutoOuPedeFornecedor(tbPedido.pedidoId, item.produtoId, Convert.ToInt32(item.itemQuantidade), item.itemPedidoId);
                            var produtoNome = (from c in produtosDc.tbProdutos where c.produtoId == item.produtoId select c).FirstOrDefault();
                            lista.Append("Pedido: " + tbPedido.pedidoId + "<br>Confirmacao: " + tbPedido.dataConfirmacaoPagamento + "<br>Produto: " + item.produtoId + "<br>Item: " + item.itemPedidoId + "<br>" + produtoNome.produtoNome + "<br>ID da Empresa: " + produtoNome.produtoIdDaEmpresa + " - " + produtoNome.complementoIdDaEmpresa + "<br><br>");
                        }
                    }
                }
                
            }
        }

        litListaPedidos.Text = lista.ToString();
    }



    public static int reservaProdutoOuPedeFornecedor(int pedidoId, int produtoId, int quantidade, int itemPedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = HttpContext.Current.Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        int totalDeItens = 0;
        int totalEmEstoque = 0;

        var produtosDc = new dbCommerceDataContext();
        var produtosFilho = (from c in produtosDc.tbProdutoRelacionados where c.idProdutoPai == produtoId select c);
        int totalProdutosFilho = produtosFilho.Count();
        if (totalProdutosFilho > 0)
        {
            foreach (var produtoRelacionado in produtosFilho)
            {
                totalDeItens++;
                int fornecedorId = produtoRelacionado.tbProduto.produtoFornecedor;
                var fornecedorDc = new dbCommerceDataContext();
                bool estoqueConferido = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).FirstOrDefault().estoqueConferido ?? false;
                var estoqueDoItem = produtosDc.admin_produtoEmEstoque(produtoRelacionado.idProdutoFilho).FirstOrDefault();
                int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);

                if (estoqueTotalDoItem >= quantidade && estoqueConferido)
                {
                    totalEmEstoque++;
                }
            }
        }
        else
        {
            totalDeItens++;

            var produto = (from c in produtosDc.tbProdutos where c.produtoId == produtoId select c).First();
            var estoqueDoItem = produtosDc.admin_produtoEmEstoque(produto.produtoId).FirstOrDefault();
            int estoqueTotalDoItem = estoqueDoItem == null ? 0 : Convert.ToInt32(estoqueDoItem.estoqueLivre);


            int fornecedorId = produto.produtoFornecedor;
            var fornecedorDc = new dbCommerceDataContext();
            bool estoqueConferido = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == fornecedorId select c).FirstOrDefault().estoqueConferido ?? false;
            if (estoqueTotalDoItem >= quantidade && estoqueConferido)
            {
                totalEmEstoque++;
            }
        }

        if (totalDeItens == totalEmEstoque)
        {
            if (totalProdutosFilho > 0)
            {
                foreach (var produtoRelacionado in produtosFilho)
                {
                    var reserva = new tbProdutoReservaEstoque();
                    reserva.idPedido = pedidoId;
                    reserva.idProduto = produtoRelacionado.idProdutoFilho;
                    reserva.dataHora = DateTime.Now;
                    reserva.quantidade = quantidade;
                    reserva.idItemPedido = itemPedidoId;
                    produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                    produtosDc.SubmitChanges();
                }
            }
            else
            {
                var reserva = new tbProdutoReservaEstoque();
                reserva.idPedido = pedidoId;
                reserva.idProduto = produtoId;
                reserva.dataHora = DateTime.Now;
                reserva.idItemPedido = itemPedidoId;
                reserva.quantidade = quantidade;
                produtosDc.tbProdutoReservaEstoques.InsertOnSubmit(reserva);
                produtosDc.SubmitChanges();
            }
            return 1; // Reservado
        }

        return 2; // Não pedido Pedido ao Fornecedor
    }

}