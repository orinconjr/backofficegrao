﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_transportadorasLiberadas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var liberadas = (from c in data.tbTransportadoraLiberadas select c).First();
            chkTNT.Checked = liberadas.tnt;
            chkJadlog.Checked = liberadas.jadlog;
            chkCorreios.Checked = liberadas.correios;
        }
    }
    protected void btnGravar_Click(object sender, EventArgs e)
    {
        if(!chkTNT.Checked && !chkJadlog.Checked)
        {
            Response.Write("<script>alert('Favor selecionar ao menos uma transportadora.');</script>");
            return;
        }
        var data = new dbCommerceDataContext();
        var liberadas = (from c in data.tbTransportadoraLiberadas select c).First();
        liberadas.tnt = chkTNT.Checked;
        liberadas.jadlog = chkJadlog.Checked;
        liberadas.correios = chkCorreios.Checked;
        data.SubmitChanges();
        Response.Write("<script>alert('Transportadoras gravadas com sucesso.');</script>");
    }
}