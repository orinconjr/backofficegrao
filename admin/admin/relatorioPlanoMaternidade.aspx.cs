﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_relatorioPlanoMaternidade : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();
        var dataFiltro = Convert.ToDateTime(DateTime.Now.AddDays(-2).ToShortDateString());
        var pedidos = (from c in data.tbPedidos
                       join d in data.tbClientes on c.clienteId equals d.clienteId
                       join e in data.tbPedidoPagamentos on c.pedidoId equals e.pedidoId into pagamentos
                       join g in data.tbPedidoFornecedorItems on c.pedidoId equals g.idPedido into pedidosFornecedor
                       join h in data.tbProdutoReservaEstoques on c.pedidoId equals h.idPedido into estoques
                       where c.tipoDePagamentoId == 11
                       select new
                       {
                           c.pedidoId,
                           c.statusDoPedido,
                           c.tbPedidoSituacao.situacao,
                           c.dataHoraDoPedido,
                           c.valorCobrado,
                           d.clienteNome,
                           vencimentoParcela = pagamentos.Where(x => x.pago == false && x.cancelado == false && x.pagamentoPrincipal == true).OrderBy(x => x.dataVencimento).Count() == 0 ? null : (DateTime?)pagamentos.Where(x => x.pago == false).OrderBy(x => x.dataVencimento).FirstOrDefault().dataVencimento,
                           totalParcelas = pagamentos.Where(x => x.cancelado == false && x.pagamentoPrincipal == true).OrderBy(x => x.dataVencimento).Count(),
                           parcelasPagas = pagamentos.Where(x => x.pago == true && x.cancelado == false && x.pagamentoPrincipal == true).OrderBy(x => x.dataVencimento).Any(),
                           totalParcelasPagas = pagamentos.Where(x => x.pago == true && x.cancelado == false && x.pagamentoPrincipal == true).OrderBy(x => x.dataVencimento).Count(),
                           parcelasPendentes = pagamentos.Where(x => x.pago == false && x.cancelado == false && x.pagamentoPrincipal == true).Count(),
                           realizadoPedidoFornecedor = pedidosFornecedor.Any(),
                           utilizadoEstoque = estoques.Any(),
                           c.dataConfirmacaoPagamento
                       }).OrderByDescending(x => x.pedidoId);
        if (chkTodos.Checked)
        {
            grd.DataSource = pedidos;
        }
        else
        {
            var pedidosFiltrados = pedidos.Where(x => x.pedidoId == 0).ToList();
            if (chkPago.Checked)
            {
                var pedidosF = pedidos.Where(x => x.parcelasPendentes == 0 && x.parcelasPagas && x.statusDoPedido != 6).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            if (chkAguardandoNoPrazo.Checked)
            {
                var pedidosF = pedidos.Where(x => x.parcelasPagas == false && x.vencimentoParcela >= dataFiltro && x.statusDoPedido != 6).OrderBy(x => x.vencimentoParcela).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            if (chkAguardandoVencidas.Checked)
            {
                var pedidosF = pedidos.Where(x => x.parcelasPagas == false && x.vencimentoParcela < dataFiltro && x.parcelasPendentes > 0 && x.statusDoPedido != 6).OrderBy(x => x.vencimentoParcela).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            if (chkParcialmenteNoPrazo.Checked)
            {
                var pedidosF = pedidos.Where(x => x.parcelasPagas == true && x.parcelasPendentes > 0 && x.vencimentoParcela >= dataFiltro && x.statusDoPedido != 6).OrderBy(x => x.vencimentoParcela).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            if (chkParcialmenteVencidas.Checked)
            {
                var pedidosF = pedidos.Where(x => x.parcelasPagas == true && x.parcelasPendentes > 0 && x.vencimentoParcela < dataFiltro && x.statusDoPedido != 6).OrderBy(x => x.vencimentoParcela).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            if (chkCancelados.Checked)
            {
                var pedidosF = pedidos.Where(x => x.statusDoPedido == 6).OrderBy(x => x.vencimentoParcela).ToList();
                pedidosFiltrados = pedidosFiltrados.Union(pedidosF).ToList();
            }
            grd.DataSource = pedidosFiltrados.Distinct().ToList();
        }
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
        {
            grd.DataBind();
        }

    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }

    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataConfirmacaoPagamento")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
        //    dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
        //    dde.ReadOnly = true;
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataHoraDoPedido'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'vencimentoParcela'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataConfirmacaoPagamento")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataConfirmacaoPagamento'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            //dde.ClientSideEvents.DropDown = String.Format("function (s, e) {{ OnDropDown(s, {0}, {1}); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        //if (e.Column.FieldName == "dataHoraDoPedido")
        //{
        //    if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
        //    {
        //        Session["dataHoraDoPedido"] = e.Value;
        //        String[] dates = e.Value.Split('|');
        //        DateTime dateFrom = Convert.ToDateTime(dates[0]),
        //            dateTo = Convert.ToDateTime(dates[1]);
        //        e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
        //                     (new OperandProperty("dataHoraDoPedido") <= dateTo);
        //    }
        //    else
        //    {
        //        if (Session["dataHoraDoPedido"] != null)
        //            e.Value = Session["dataHoraDoPedido"].ToString();
        //    }
        //}
        if (e.Column.FieldName == "dataHoraDoPedido")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataHoraDoPedido"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataHoraDoPedido") >= dateFrom) &
                             (new OperandProperty("dataHoraDoPedido") <= dateTo);
            }
            else
            {
                if (Session["dataHoraDoPedido"] != null)
                    e.Value = Session["dataHoraDoPedido"].ToString();
            }
        }
        if (e.Column.FieldName == "vencimentoParcela")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["vencimentoParcela"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("vencimentoParcela") >= dateFrom) &
                             (new OperandProperty("vencimentoParcela") <= dateTo);
            }
            else
            {
                if (Session["vencimentoParcela"] != null)
                    e.Value = Session["vencimentoParcela"].ToString();
            }
        }
        if (e.Column.FieldName == "dataConfirmacaoPagamento")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataConfirmacaoPagamento"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataConfirmacaoPagamento") >= dateFrom) &
                             (new OperandProperty("dataConfirmacaoPagamento") <= dateTo);
            }
            else
            {
                if (Session["dataConfirmacaoPagamento"] != null)
                    e.Value = Session["dataConfirmacaoPagamento"].ToString();
            }
        }
    }


    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void rdbTodos_OnCheckedChanged(object sender, EventArgs e)
    {
        if (chkAguardandoNoPrazo.Checked | chkAguardandoVencidas.Checked | chkParcialmenteNoPrazo.Checked |
            chkParcialmenteVencidas.Checked | chkPago.Checked | chkCancelados.Checked)
        {
            chkTodos.Checked = false;
        }
        else
        {
            chkTodos.Checked = true;
        }
        fillGrid(true);
    }

    protected void chkTodos_OnCheckedChanged(object sender, EventArgs e)
    {
        if (chkTodos.Checked)
        {
            chkAguardandoNoPrazo.Checked = false;
            chkAguardandoVencidas.Checked = false;
            chkParcialmenteNoPrazo.Checked = false;
            chkParcialmenteVencidas.Checked = false;
            chkPago.Checked = false;
            chkCancelados.Checked = false;
        }
        fillGrid(true);
    }

    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}
