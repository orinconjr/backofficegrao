﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_importarFaturaTransportadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnImportar_Click(object sender, EventArgs e)
    {
        try
        {
            lblMensagem.Text = string.Empty;

            string caminho = MapPath("~/upload/faturatransportadora/");
            string nomeArquivo = Guid.NewGuid().ToString() + Path.GetExtension(fupPlanilha.FileName).ToLower();
            string caminhoPlanilha = caminho + nomeArquivo;
            fupPlanilha.SaveAs(caminhoPlanilha);
            int idTransportadora = Convert.ToInt32(ddlTransportadora.SelectedValue);

            var data = new dbCommerceDataContext();
            using (SLDocument sl = new SLDocument(caminhoPlanilha, "Remessas"))
            {
                bool headers = false;
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int iStartColumnIndex = stats.StartColumnIndex;
                for (int row = stats.StartRowIndex + 1; row <= stats.EndRowIndex; ++row)
                {
                    if(sl.GetCellValueAsString(row, 1).ToString().Trim().ToLower() == "data")
                    {
                        headers = true;
                    }
                    else
                    {
                        if (headers)
                        {
                            string dataEnvio = sl.GetCellValueAsString(row, 1).ToString().Trim();
                            string cte = sl.GetCellValueAsString(row, 2).ToString().Trim();
                            string peso = sl.GetCellValueAsString(row, 6).ToString().Trim();
                            string nfe = sl.GetCellValueAsString(row, 7).ToString().Trim();
                            string valorFrete = sl.GetCellValueAsString(row, 8).ToString().Trim();
                            string valorMercadoria = sl.GetCellValueAsString(row, 9).ToString().Trim();
                        }
                    }
                }
            }

            AlertShow("Tabela atualizada com sucesso");
        }
        catch (Exception ex)
        {
            AlertShow("Ocorreu um erro");
        }
    }


    public void AlertShow(string message)
    {
        base.ClientScript.RegisterStartupScript(base.GetType(), "myalert", "alert('" + message + "'); ", true);
    }
}