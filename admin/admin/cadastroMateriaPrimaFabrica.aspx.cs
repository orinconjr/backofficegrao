﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_cadastroMateriaPrimaFabrica : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var categorias =
                (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c).ToList
                    ();
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

            var produtos =
                (from c in data.tbComprasProdutos where c.materiaPrima orderby c.produto select c).ToList
                    ();
            ddlMateriaPrima.DataSource = produtos;
            ddlMateriaPrima.DataBind();

            var processos =
                (from c in data.tbSubProcessoFabricas
                 select new
                 {
                     c.idSubProcessoFabrica,
                     nome = c.tbProcessoFabrica.processo + " - " + c.nome
                 }).ToList
                    ();
            ddlProcessosFabrica.DataSource = processos.OrderBy(x => x.nome);
            ddlProcessosFabrica.DataBind();
        }

        //fillListProcesso();
    }

    private void filtrar()
    {
        if (string.IsNullOrEmpty(ddlMateriaPrima.SelectedValue))
        {
            lstProdutos.DataSource = null;
            lstProdutos.DataBind();
            return;
        }
        if (chkAtivo.Checked == false && string.IsNullOrEmpty(txtNome.Text) &&
            string.IsNullOrEmpty(ddlCategoria.SelectedValue) && string.IsNullOrEmpty(ddlProcessosFabrica.SelectedValue))
        {
            return;
        }
        var data = new dbCommerceDataContext();
        var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias
                                 where c.tbProduto.produtoFornecedor == 82 && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).ToList();
        var produtosProcessos = (from c in data.tbJuncaoProdutoProcessoFabricas
                                 where c.tbProduto.produtoFornecedor == 82
                                 select c).ToList();
        var produtos = (from c in data.tbProdutos
                        where c.produtoFornecedor == 82
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoAtivo,
                            c.produtoIdDaEmpresa
                        });
        if (!string.IsNullOrEmpty(txtNome.Text))
        {
            produtos = produtos.Where(x => x.produtoNome.Contains(txtNome.Text));
        }
        if (ddlCategoria.SelectedValue != "")
        {
            int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
            produtosCategoria = produtosCategoria.Where(x => x.categoriaId == categoriaId).ToList();
            produtos = (from c in produtos where produtosCategoria.Select(x => x.produtoId).Contains(c.produtoId) select c);
        }
        if (ddlProcessosFabrica.SelectedValue != "")
        {
            int idProcesso = Convert.ToInt32(ddlProcessosFabrica.SelectedValue);
            produtosProcessos = produtosProcessos.Where(x => x.idSubProcessoFabrica == idProcesso).ToList();
            produtos = (from c in produtos where produtosProcessos.Select(x => x.idProduto).Contains(c.produtoId) select c);
        }
        if (chkAtivo.Checked)
        {
            produtos = produtos.Where(x => x.produtoAtivo.ToLower() == "true");
        }
        var listaFinal = produtos.OrderBy(x => x.produtoNome).ToList();
        lstProdutos.DataSource = listaFinal;
        lstProdutos.DataBind();

        litTotal.Text = listaFinal.Count.ToString();

    }
    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        filtrar();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");
        var hdfFoto = (HiddenField)e.Item.FindControl("hdfFoto");
        var imgFoto = (Image)e.Item.FindControl("imgFoto");
        var txtQuantidade = (TextBox)e.Item.FindControl("txtQuantidade");
        imgFoto.ImageUrl = "http://www.graodegente.com.br/fotos/" + hdfProdutoId.Value + "/" + hdfFoto.Value + ".jpg";

        txtQuantidade.Attributes.Add("onkeypress", "return soNumeroVirgulaPonto(event);");


    }

    protected void lstProdutos_OnDataBound(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(ddlMateriaPrima.SelectedValue)) return;

        var data = new dbCommerceDataContext();


        foreach (var item in lstProdutos.Items)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var txtQuantidade = (TextBox)item.FindControl("txtQuantidade");
            //var lstMateriasPrimas = (ListView)item.FindControl("lstMateriasPrimas");
            int produtoId = Convert.ToInt32(hdfProdutoId.Value);

            var materias = (from c in data.tbProdutoMateriaPrimas
                            where c.produtoId == produtoId
                            select new
                            {
                                c.tbComprasProduto.produto,
                                c.consumo,
                                c.idComprasProduto
                            }).ToList();
            //lstMateriasPrimas.DataSource = materias;
            //lstMateriasPrimas.DataBind();

            int materiaPrimaId = Convert.ToInt32(ddlMateriaPrima.SelectedValue);
            var materia = (from c in materias where c.idComprasProduto == materiaPrimaId select new
            {
                c.produto,
                c.consumo
            }).FirstOrDefault();
            if (materia != null)
            {
                txtQuantidade.Text = materia.consumo.ToString();
            }
        }
    }

    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();

        int materiaPrimaId = Convert.ToInt32(ddlMateriaPrima.SelectedValue);

        foreach (var item in lstProdutos.Items)
        {
            var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");
            var txtQuantidade = (TextBox)item.FindControl("txtQuantidade");
            decimal quantidade = 0;

            decimal.TryParse(txtQuantidade.Text, out quantidade);

            int produtoId = Convert.ToInt32(hdfProdutoId.Value);
            var materia = (from c in data.tbProdutoMateriaPrimas
                           where c.idComprasProduto == materiaPrimaId && c.produtoId == produtoId
                           select c).FirstOrDefault();
            if (materia != null)
            {
                if (quantidade > 0)
                {
                    materia.consumo = quantidade;
                    data.SubmitChanges();
                }
                else
                {
                    data.tbProdutoMateriaPrimas.DeleteOnSubmit(materia);
                    data.SubmitChanges();
                }
            }
            else
            {
                if (quantidade > 0)
                {
                    var materiaAdicionar = new tbProdutoMateriaPrima();
                    materiaAdicionar.consumo = quantidade;
                    materiaAdicionar.produtoId = produtoId;
                    materiaAdicionar.idComprasProduto = materiaPrimaId;
                    data.tbProdutoMateriaPrimas.InsertOnSubmit(materiaAdicionar);
                    data.SubmitChanges();
                }
            }
        }

        filtrar();
    }

    protected void ddlMateriaPrima_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        filtrar();
    }
}