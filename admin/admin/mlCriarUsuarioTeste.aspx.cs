﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using MercadoLibre.SDK;
using Org.BouncyCastle.Ocsp;
using RestSharp;

public partial class admin_mlCriarUsuarioTeste : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var meli = mlIntegracao.verificaAutenticacao("mlCriarUsuarioTeste.aspx", Request.QueryString["code"]);
        var p = new Parameter();
        p.Name = "access_token";
        p.Value = mlIntegracao.retornaAccessToken();

        var ps = new List<Parameter> ();
        ps.Add (p);
        var objReq = new {site_id="MLB"};
        //IRestResponse r = meli.Post("/users/test_user", ps, objReq);
        IRestResponse r = meli.Post("/users/test_user", ps, objReq);

    }
}