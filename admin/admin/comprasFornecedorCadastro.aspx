﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="comprasFornecedorCadastro.aspx.cs" Inherits="admin_comprasFornecedorCadastro" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register assembly="CuteEditor" namespace="CuteEditor" tagprefix="CE" %>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Compras - Cadastro de Fornecedores</asp:Label>
        </td>
    </tr>
        
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td>
                <asp:TextBox runat="server" ID="hdfLista" Visible="False" />
                <asp:HiddenField ID="hdfIdFornecedor" runat="server" />
                &nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
                Fornecedor:<br/>
                <asp:TextBox runat="server" ID="txtFornecedor" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
               CPF ou CNPJ (Somente Números):<br/>
                <asp:TextBox runat="server" ID="txtfornecedorCPFCNPJ" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
                Contato:<br/>
                <asp:TextBox runat="server" ID="txtContato" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
                Email:<br/>
                <asp:TextBox runat="server" ID="txtEmail" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr class="campos">
            <td>
                Telefone:<br/>
                <asp:TextBox runat="server" ID="txtTelefone" Width="300"></asp:TextBox>&nbsp;
            </td>
        </tr>        
        <tr class="campos">
            <td>
                Prazo de Entrega:<br/>
                <dxe:ASPxTextBox ID="txtPrazoDeEntrega" runat="server" Width="100">
                    <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                    <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                </dxe:ASPxTextBox>
            </td>
        </tr>   
        
        <tr class="rotulos">
            <td  style="font-weight: bold; padding-top: 30px;">
                Condições de Pagamento:
            </td>
        </tr>
        <tr class="campos">
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                            Condição de Pagamento:<br/>
                            <dxe:ASPxComboBox  DataSourceID="sqlComprasCondicoes" ID="ddlCondicaoPagamento" runat="server" ValueField="idComprasCondicaoPagamento" TextField="nome" Width="400px" DropDownStyle="DropDown">
                            </dxe:ASPxComboBox>
                        </td>
                        <td>
                            Desconto (%):<br/>
                            <dxe:ASPxTextBox ID="txtDesconto" runat="server" Width="100">
                                <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                            </dxe:ASPxTextBox>
                        </td>
                        <td>
                            Acréscimo (%):<br/>
                            <dxe:ASPxTextBox ID="txtAcrescimo" runat="server" Width="100">
                                <MaskSettings Mask="<0..999999g>" IncludeLiterals="None" />
                                <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                            </dxe:ASPxTextBox>
                        </td>
                        <td style="text-align: right">
                            &nbsp;<br/>
                            <asp:Button runat="server" ID="btnGravarCondicao" Text="Adicionar Condição de Pagamento" OnClick="btnGravarCondicao_OnClick" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grdCondicoes" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" KeyFieldName="idComprasFornecedorCondicaoPagamento">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="Bottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idComprasFornecedorCondicaoPagamento" Visible="False" VisibleIndex="0">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataComboBoxColumn Caption="Condição" FieldName="idComprasCondicaoPagamento">
                            <PropertiesComboBox DataSourceID="sqlComprasCondicoes" TextField="nome" ValueField="idComprasCondicaoPagamento">
                            </PropertiesComboBox>
                            <Settings AllowAutoFilter="True" AllowAutoFilterTextInputTimer="True" FilterMode="DisplayText" />
                        </dxwgv:GridViewDataComboBoxColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Acréscimo" FieldName="acrescimo">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Desconto" FieldName="desconto">
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Remover" Name="remover" Width="80">
                            <DataItemTemplate>
                                <asp:ImageButton ImageUrl="~/admin/images/btExcluir.jpg" runat="server" OnCommand="btnRemover_OnCommand" ID="btnRemover" CommandArgument='<%# Eval("idComprasCondicaoPagamento") %>' EnableViewState="False">
                                </asp:ImageButton>
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView> 
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button runat="server" ID="btnGravar" Text="Gravar Condição de Pagamento" OnClick="btnGravar_OnClick"/>
            </td>
        </tr>
        
    </table>
        </td>
    </tr>
</table>

    <asp:LinqDataSource ID="sqlComprasCondicoes" runat="server" ContextTypeName="dbCommerceDataContext" OrderBy="nome" TableName="tbComprasCondicaoPagamentos">
    </asp:LinqDataSource>
</asp:Content>