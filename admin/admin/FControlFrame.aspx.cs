﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_FControlFrame : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        //Seu login e sus senha de acesso ao fcontrol (são fixos)
        string login = "LINDSAY";
        string password = "srenxovais";

        //Abaixo os dados estão fixos mas devem ser substituídos pelos dados de seu pedido
        //Faça aqui a consulta em seu Banco de Dados para pegar os dados que devem ser adicionados ao Redirecionamento abaixo.

        //Dados da compra

        //Data em que o pedido foi feito
        string date_entered = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Request["codPedido"])).Tables[0].Rows[0]["dataHoraDoPedido"].ToString(); //A data deve estar no formato aaaa-mm-dd hh:mm:ss

        //Código do pedido
        string Order_Id = Request["codPedido"]; //Máximo de 26 caracteres, deve ser único pois ele identifica o pedido.

        //Ip do usuário que realizou a transação
        string ip = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["ipDoCliente"].ToString();// pode ser para um * caso você não tenha o ip

        // número de itens (ex.: 5 lápis)
        string items_tot = rnPedidos.itensPedidoConta(int.Parse(Order_Id)).Tables[0].Rows[0]["quantidadeTotalDeItens"].ToString();

        // número de itens DISTINTOS (ex.: 5 lápis, só o tipo lápis então  = 1)
        string items_dif = int.Parse(rnPedidos.itensPedidoConta(int.Parse(Order_Id)).Tables[0].Rows[0]["quantidadeDeItensDestinstos"].ToString()).ToString();

        /*
        * Valor do pedido Deve ser multiplicado por 100 para remover as casas
        * decimais. Ex.: R$ 123,99 deve ser convertido para 12399
        */
        //string total = (double.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["valorCobrado"].ToString()) * 100).ToString().Replace(",","");
        //string total = decimal.Parse(().ToString().Replace(",", "")).ToString("##,###.00").Replace(",", "").Replace(".", "");

        string total = String.Format("{0:c}", double.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["valorCobrado"].ToString())).Replace(",", "").Replace(".", "").Replace("R$", "");

        /*
        * Método de pagamento:
        * Código Método de Pagamentos
        * 1 Cartão de Crédito - Temporário
        * 2 Cartão Visa
        * 3 Cartão MasterCard
        * 4 Cartão Diners
        * 5 Cartão American Express
        * 6 Cartão HiperCard
        * 10 Pagamento na Entrega
        * 11 Débito/Transferência Eletrônica
        * 12 Boleto Bancário
        * 13 Financiamento
        * 14 Cheque
        * 15 Depósito
        * Lembrando que alguns métodos de pagamento não
        * necessitam da analise do fcontrol (Ex.: boleto), pois o pagamento
        * ocorre antes do envio do produto (e não é possível o charge-back no
        * caso do boleto).
        */
        string payment_method_id = string.Empty;
        string condicaoDePagamentoDoPedido = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["condDePagamentoId"].ToString();

        switch (rnCondicoesDePagamento.condicaoDePagamentoSeleciona_PorCondicaoId(int.Parse(condicaoDePagamentoDoPedido)).Tables[0].Rows[0]["condicaoNome"].ToString())
        {
            case "Visa":
                payment_method_id = "2";
                break;
            case "MasterCard":
                payment_method_id = "3";
                break;
            case "Dinners":
                payment_method_id = "4";
                break;
            case "American Express":
                payment_method_id = "5";
                break;
        }

        string num_parcelas = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["numeroDeParcelas"].ToString();

        //Dados do comprador

        int clienteId = int.Parse(rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["clienteId"].ToString());

        //Nome do comprador
        string shopper_name = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

        //Rua do comprador
        string shopper_street = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

        //Número da rua do comprador
        string shopper_street_number = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

        //Complemento do endereço do comprador
        string shopper_street_compl = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

        //Bairro do endereço do comprador
        string shopper_street_district = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

        //Cidade do comprador
        string shopper_city = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

        //Estado do comprador
        string shopper_state = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

        //C.E.P. do comprador
        string shopper_zip = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString().Replace("-", "");

        //País do comprador
        string shopper_country = "BRA"; // Os países devem estar no formato ISO 3166 Alpha 3

        //Telefone do comprador
        string shopper_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");

        //DDD do celular do comprador
        string shopper_ddd_cel_phone = "*"; //Caso não o tenha deve-se passar um *.

        //Celular do comprador
        string shopper_cel_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneCelular"].ToString().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");

        //O email do comprador
        string shopper_email = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEmail"].ToString();

        //CPF/CNPJ do comprador
        string shopper_cpf_cnpj = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCPFCNPJ"].ToString();

        //Senha usado pelo comprador para acessar a loja
        string shopper_password = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteSenha"].ToString();

        //Dados de entrega
        string ship_to_name;
        string ship_to_street;
        string ship_to_street_number;
        string ship_to_street_compl;
        string ship_to_street_district;
        string ship_to_city;
        string ship_to_state;
        string ship_to_zip;
        string ship_to_country;
        string ship_to_ddd_phone;
        string ship_to_phone;
        string ship_to_ddd_cel_phone;
        string ship_to_cel_phone;
        if (rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString() != string.Empty)
        {
            //Nome dos dados de entrega
            ship_to_name = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNomeDoDestinatario"].ToString();

            //Rua do endereço de entrega
            ship_to_street = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endRua"].ToString();

            //Número do endereço de entrega
            ship_to_street_number = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endNumero"].ToString();

            //Complemento do endereço de entrega
            ship_to_street_compl = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endComplemento"].ToString();

            //Bairro do endereço de entrega
            ship_to_street_district = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endBairro"].ToString();

            //Cidade do endereço de entrega
            ship_to_city = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endCidade"].ToString();

            //Estado do endereço de entrega
            ship_to_state = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endEstado"].ToString();

            //C.E.P. do endereço de entrega
            ship_to_zip = rnPedidos.pedidoSeleciona_PorPedidoId(int.Parse(Order_Id)).Tables[0].Rows[0]["endCep"].ToString().Replace("-", "");

            //País do endereço de entrega
            ship_to_country = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            ship_to_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");

            //DDD do telefone dos dados de entrega
            ship_to_ddd_cel_phone = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            ship_to_cel_phone = "*"; //Caso não tenha o dado passar um *.
        }
        else
        {
            //Nome dos dados de entrega
            ship_to_name = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNome"].ToString();

            //Rua do endereço de entrega
            ship_to_street = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteRua"].ToString();

            //Número do endereço de entrega
            ship_to_street_number = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteNumero"].ToString();

            //Complemento do endereço de entrega
            ship_to_street_compl = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteComplemento"].ToString(); ;

            //Bairro do endereço de entrega
            ship_to_street_district = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteBairro"].ToString(); ;

            //Cidade do endereço de entrega
            ship_to_city = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCidade"].ToString(); ;

            //Estado do endereço de entrega
            ship_to_state = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteEstado"].ToString();

            //C.E.P. do endereço de entrega
            ship_to_zip = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteCep"].ToString().Replace("-", "");

            //País do endereço de entrega
            ship_to_country = "BRA"; //Os países devem estar no formato ISO 3166 Alpha 3

            //Telefone dos dados de entrega
            ship_to_phone = rnClientes.clienteSelecionaAdmin_PorClienteId(clienteId).Tables[0].Rows[0]["clienteFoneResidencial"].ToString().Replace("-", "").Replace("(", "").Replace(")", "").Replace(" ", "");

            //DDD do telefone dos dados de entrega
            ship_to_ddd_cel_phone = "*"; //Caso não tenha o dado passar um *.

            //Celular dos dados de entrega
            ship_to_cel_phone = "*"; //Caso não tenha o dado passar um *.
        }

        //Esta consulta retorna um iframe do fcontrol, já prove a interface visual.
        string url = "https://secure.fcontrol.com.br/validatorframe/validatorframe.aspx?" +
                     "login=" + HttpUtility.UrlEncode(login) +
                     "&password=" + HttpUtility.UrlEncode(password) +
                     "&date_entered=" + HttpUtility.UrlEncode(date_entered) +
                     "&Order_Id=" + HttpUtility.UrlEncode(Order_Id) +
                     "&ip=" + HttpUtility.UrlEncode(ip) +
                     "&shopper_name=" + HttpUtility.UrlEncode(shopper_name) +
                     "&shopper_street=" + HttpUtility.UrlEncode(shopper_street) +
                     "&shopper_street_number=" + HttpUtility.UrlEncode(shopper_street_number) +
                     "&shopper_street_compl=" + HttpUtility.UrlEncode(shopper_street_compl) +
                     "&shopper_street_district=" + HttpUtility.UrlEncode(shopper_street_district) +
                     "&shopper_city=" + HttpUtility.UrlEncode(shopper_city) +
                     "&shopper_state=" + HttpUtility.UrlEncode(shopper_state) +
                     "&shopper_zip=" + HttpUtility.UrlEncode(shopper_zip) +
                     "&shopper_country=" + HttpUtility.UrlEncode(shopper_country) +
                     "&shopper_phone=" + HttpUtility.UrlEncode(shopper_phone) +
                     "&shopper_ddd_cel_phone=" + HttpUtility.UrlEncode(shopper_ddd_cel_phone) +
                     "&shopper_cel_phone=" + HttpUtility.UrlEncode(shopper_cel_phone) +
                     "&shopper_email=" + HttpUtility.UrlEncode(shopper_email) +
                     "&shopper_cpf_cnpj=" + HttpUtility.UrlEncode(shopper_cpf_cnpj) +
                     "&shopper_password=" + HttpUtility.UrlEncode(shopper_password) +
                     "&ship_to_name=" + HttpUtility.UrlEncode(ship_to_name) +
                     "&ship_to_street=" + HttpUtility.UrlEncode(ship_to_street) +
                     "&ship_to_street_number=" + HttpUtility.UrlEncode(ship_to_street_number) +
                     "&ship_to_street_compl=" + HttpUtility.UrlEncode(ship_to_street_compl) +
                     "&ship_to_street_district=" + HttpUtility.UrlEncode(ship_to_street_district) +
                     "&ship_to_city=" + HttpUtility.UrlEncode(ship_to_city) +
                     "&ship_to_state=" + HttpUtility.UrlEncode(ship_to_state) +
                     "&ship_to_zip=" + HttpUtility.UrlEncode(ship_to_zip) +
                     "&ship_to_country=" + HttpUtility.UrlEncode(ship_to_country) +
                     "&ship_to_phone=" + HttpUtility.UrlEncode(ship_to_phone) +
                     "&ship_to_ddd_cel_phone=" + HttpUtility.UrlEncode(ship_to_ddd_cel_phone) +
                     "&ship_to_cel_phone=" + HttpUtility.UrlEncode(ship_to_cel_phone) +
                     "&items_dif=" + HttpUtility.UrlEncode(items_dif) +
                     "&items_tot=" + HttpUtility.UrlEncode(items_tot) +
                     "&total=" + HttpUtility.UrlEncode(total) +
                     "&payment_method_id=" + HttpUtility.UrlEncode(payment_method_id) +
                     "&num_parcelas=" + HttpUtility.UrlEncode(num_parcelas);

        //*Observação: para retornar um documento XML, deve-se adicionar o parâmetro &formato=XML,
        //neste exemplo o retorno é um html com o frame para administração do pedido
        Response.Redirect(url);
    }
}
