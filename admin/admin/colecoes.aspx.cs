﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using Amazon.S3.Model;
using Amazon.S3;

public partial class admin_colecoes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    //protected void btPdf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WritePdfToResponse();
    //}
    //protected void btXsl_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteXlsToResponse();
    //}
    //protected void btRtf_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteRtfToResponse();
    //}
    //protected void btCsv_Click(object sender, ImageClickEventArgs e)
    //{
    //    this.grdEx.WriteCsvToResponse();
    //}

    protected void grd_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.EditForm) return;
        {
            Image image1 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem1");
            Image image2 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem2");
            Image image3 = (Image)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "imagem3");

            CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
            CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
            CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");
            DropDownList ddlSite = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["siteId"], "ddlSite");

            #region Oculta imagem quando for inserir um novo registro ou quando for editar um registro e não existir imagem no bd
            if (image1.ImageUrl.ToString() == "../colecoes//" || image1.ImageUrl.ToString() == "../colecoes/" + e.KeyValue + "/")
            {
                image1.Visible = false;
                ckbExcluir1.Visible = false;
            }

            if (image2.ImageUrl.ToString() == "../colecoes//" || image2.ImageUrl.ToString() == "../colecoes/" + e.KeyValue + "/")
            {
                image2.Visible = false;
                ckbExcluir2.Visible = false;
            }

            if (image3.ImageUrl.ToString() == "../colecoes//" || image3.ImageUrl.ToString() == "../colecoes/" + e.KeyValue + "/")
            {
                image3.Visible = false;
                ckbExcluir3.Visible = false;
            }
            #endregion
        }
    }

    protected void grd_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        sqlCategorias.InsertParameters.Clear();

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["colecaoNome"], "txtNome");
        DropDownList ddlSite = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["siteId"], "ddlSite");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");
        FileUpload flu360 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu360");
        FileUpload fluVideo = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "fluVideo");
        ASPxTextBox txtNomeSite = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["colecaoNomeSite"], "txtNomeSite");

        string nomeParaUrl = GerarPermalinkColecao(0, string.IsNullOrEmpty(txtNomeSite.Text) ? txtNome.Text : txtNomeSite.Text);
        sqlCategorias.InsertParameters.Add("colecaoUrl", rnFuncoes.limpaString(nomeParaUrl).Trim().ToLower());
        sqlCategorias.InsertParameters.Add("colecaoNome", txtNome.Text);
        sqlCategorias.InsertParameters.Add("siteId", ddlSite.SelectedValue);
        sqlCategorias.InsertParameters.Add("colecaoNomeSite", txtNomeSite.Text);

        if (flu1.HasFile)
            sqlCategorias.InsertParameters.Add("imagem1", "imagem1.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem1", string.Empty);

        if (flu2.HasFile)
            sqlCategorias.InsertParameters.Add("imagem2", "imagem2.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem2", string.Empty);

        if (flu3.HasFile)
            sqlCategorias.InsertParameters.Add("imagem3", "imagem3.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem3", string.Empty);

        if (flu360.HasFile)
            sqlCategorias.InsertParameters.Add("imagem360", "imagem360.jpg");
        else
            sqlCategorias.InsertParameters.Add("imagem360", string.Empty);

        if (fluVideo.HasFile)
            sqlCategorias.InsertParameters.Add("video", "video." + System.IO.Path.GetExtension(fluVideo.FileName));
        else
            sqlCategorias.InsertParameters.Add("video", string.Empty);
    }

    protected void grd_RowInserted(object sender, DevExpress.Web.Data.ASPxDataInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");
            FileUpload flu360 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu360");
            FileUpload fluVideo = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "fluVideo");

            int ultimoId = int.Parse(rnColecoes.retornaUltimoId().Tables[0].Rows[0]["colecaoId"].ToString());
            Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId);

            if (flu1.HasFile)
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId + "\\" + "imagem1.jpg");

            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId + "\\" + "imagem3.jpg");

            if (flu360.HasFile)
            {
                flu360.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId + "\\" + "imagem360.jpg");
                uploadImagemAmazonS3("colecoes\\" + ultimoId + "\\", "colecoes/" + ultimoId + "/", "imagem360.jpg");
            }

            if (fluVideo.HasFile)
            {
                fluVideo.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + ultimoId + "\\" + "video." + System.IO.Path.GetExtension(fluVideo.FileName));
                //uploadImagemAmazonS3("colecoes\\" + ultimoId + "\\", "colecoes/" + ultimoId + "/", "video." + System.IO.Path.GetExtension(fluVideo.FileName));
            }


            try
            {
                //Atualiza Cache
                //string url = ConfigurationManager.AppSettings["caminhoVirtual"] + "atualizaCache.aspx";
                //WebRequest request = WebRequest.Create(url);
                //WebResponse response = request.GetResponse();
            }
            catch (Exception ex)
            {

            }

            Response.Write("<script>window.location=('colecoes.aspx');</script>");
        }
    }




    private void uploadImagemAmazonS3(string caminhoLocal, string caminhoRemoto, string foto)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoLocal + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = caminhoRemoto + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Response.Write(s3Exception.Message + "<br>" + s3Exception.InnerException);

            //Console.ReadKey();
        }
    }


    protected void grd_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        sqlCategorias.UpdateParameters.Clear();

        ASPxTextBox txtNome = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["colecaoNome"], "txtNome");
        DropDownList ddlSite = (DropDownList)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["siteId"], "ddlSite");
        FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
        FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
        FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");
        FileUpload flu360 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu360");
        FileUpload fluVideo = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "fluVideo");
        ASPxTextBox txtNomeSite = (ASPxTextBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["colecaoNomeSite"], "txtNomeSite");

        CheckBox ckbExcluir1 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir1");
        CheckBox ckbExcluir2 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir2");
        CheckBox ckbExcluir3 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir3");
        CheckBox ckbExcluir360 = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluir360");
        CheckBox ckbExcluirVideo = (CheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "ckbExcluirVideo");

        int colecaoId = Convert.ToInt32(e.Keys[0]);

        var data = new dbCommerceDataContext();
        string nomeParaUrl = GerarPermalinkColecao(colecaoId, string.IsNullOrEmpty(txtNomeSite.Text) ? txtNome.Text : txtNomeSite.Text);
        sqlCategorias.UpdateParameters.Add("colecaoUrl", rnFuncoes.limpaString(nomeParaUrl).Trim().ToLower());
        var colecao = (from c in data.tbColecaos where c.colecaoId == colecaoId select c).FirstOrDefault();
        sqlCategorias.UpdateParameters.Add("colecaoNome", txtNome.Text);
        sqlCategorias.UpdateParameters.Add("siteId", ddlSite.SelectedValue);
        sqlCategorias.UpdateParameters.Add("colecaoNomeSite", txtNomeSite.Text);

        if (colecao != null)
        {
            if (flu1.HasFile)
                sqlCategorias.UpdateParameters.Add("imagem1", "imagem1.jpg");
            else
                sqlCategorias.UpdateParameters.Add("imagem1", string.Empty);

            if (flu2.HasFile)
                sqlCategorias.UpdateParameters.Add("imagem2", "imagem2.jpg");
            else
                sqlCategorias.UpdateParameters.Add("imagem2", string.Empty);

            if (flu3.HasFile)
                sqlCategorias.UpdateParameters.Add("imagem3", "imagem3.jpg");
            else
                sqlCategorias.UpdateParameters.Add("imagem3", string.Empty);


            if (flu360.HasFile)
            {
                sqlCategorias.UpdateParameters.Add("imagem360", "imagem360.jpg");
            }
            else if (ckbExcluir360.Checked)
            {
                sqlCategorias.UpdateParameters.Add("imagem360", string.Empty);
            }
            else if (!string.IsNullOrEmpty(colecao.imagem360))
            {
                sqlCategorias.UpdateParameters.Add("imagem360", colecao.imagem360);
            }
            else
            {
                sqlCategorias.UpdateParameters.Add("imagem360", string.Empty);
            }


            if (fluVideo.HasFile)
            {
                sqlCategorias.UpdateParameters.Add("video", "video." + System.IO.Path.GetExtension(fluVideo.FileName));
            }
            else if (ckbExcluirVideo.Checked)
            {
                sqlCategorias.UpdateParameters.Add("video", string.Empty);
            }
            else if (!string.IsNullOrEmpty(colecao.video))
            {
                sqlCategorias.UpdateParameters.Add("video", colecao.video);
            }
            else
            {
                sqlCategorias.UpdateParameters.Add("video", string.Empty);
            }
        }

        if (ckbExcluir1.Checked == true)
            rnCategorias.excluiFoto(colecaoId, "imagem1.jpg");

        if (ckbExcluir2.Checked == true)
            rnCategorias.excluiFoto(colecaoId, "imagem2.jpg");

        if (ckbExcluir3.Checked == true)
            rnCategorias.excluiFoto(colecaoId, "imagem3.jpg");
    }


    public string GerarPermalinkColecao(int colecaoId, string colecaoNome)
    {
        var data = new dbCommerceDataContext();
        string nomeLimpo = rnFuncoes.limpaString(colecaoNome.Trim()).ToLower();
        string nomeLimpoAjustado = nomeLimpo;
        bool jaExiste = true;
        int contagem = 1;
        while (jaExiste)
        {
            bool possuiColecao = (from c in data.tbColecaos where c.colecaoUrl == nomeLimpoAjustado && c.colecaoId != colecaoId select c).Any();
            if (possuiColecao)
            {
                nomeLimpoAjustado = nomeLimpo + "-" + contagem;
                contagem++;
            }
            else
            {
                jaExiste = false;
            }
        }
        return nomeLimpoAjustado;
    }

    protected void grd_RowUpdated(object sender, DevExpress.Web.Data.ASPxDataUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            FileUpload flu1 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu1");
            FileUpload flu2 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu2");
            FileUpload flu3 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu3");
            FileUpload flu360 = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "flu360");
            FileUpload fluVideo = (FileUpload)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["imagens"], "fluVideo");

            int categoriaId = Convert.ToInt32(e.Keys[0]);

            if (flu1.HasFile)
                flu1.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + categoriaId + "\\" + "imagem1.jpg");

            if (flu2.HasFile)
                flu2.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + categoriaId + "\\" + "imagem2.jpg");

            if (flu3.HasFile)
                flu3.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + categoriaId + "\\" + "imagem3.jpg");

            if (flu360.HasFile)
            {
                flu360.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + categoriaId + "\\" + "imagem360.jpg");
                uploadImagemAmazonS3("colecoes\\" + categoriaId + "\\", "colecoes/" + categoriaId + "/", "imagem360.jpg");

                var data = new dbCommerceDataContext();
                var produtosColecao = (from c in data.tbJuncaoProdutoColecaos where c.colecaoId == categoriaId select c);
                foreach(var produtoColecao in produtosColecao)
                {
                    var queue = new tbQueue()
                    {
                        agendamento = DateTime.Now,
                        andamento = false,
                        concluido = false,
                        idRelacionado = produtoColecao.produtoId,
                        mensagem = "",
                        tipoQueue = 26
                    };
                    data.tbQueues.InsertOnSubmit(queue);
                    data.SubmitChanges();
                }
            }

            if (fluVideo.HasFile)
            {
                fluVideo.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + categoriaId + "\\" + "video." + System.IO.Path.GetExtension(fluVideo.FileName));
            }


            try
            {
                //Atualiza Cache
                /*string url = ConfigurationManager.AppSettings["caminhoVirtual"] + "atualizaCache.aspx";
                WebRequest request = WebRequest.Create(url);
                WebResponse response = request.GetResponse();*/
            }
            catch (Exception ex)
            {

            }

            Response.Write("<script>window.location=('colecoes.aspx');</script>");
        }
    }

    protected void grd_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        
    }

    protected void grd_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        int colecaoId = Convert.ToInt32(e.Keys[0]);

        if (Directory.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + colecaoId))
        {
            Directory.Delete(ConfigurationManager.AppSettings["caminhoFisico"] + "colecoes\\" + colecaoId, true);
        }

        Response.Write("<script>window.location=('colecoes.aspx');</script>");
    }
}
