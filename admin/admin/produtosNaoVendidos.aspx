﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtosNaoVendidos.aspx.cs" Inherits="admin_produtosNaoVendidos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script src="js/jquery.min.js"></script>
    <style>
        .fieldsetatualizarprecos {
            width: 760px;
            float: left;
            clear: left;
            border-radius: 5px;
            margin-left: 25px;
        }

            .fieldsetatualizarprecos input[type=checkbox] {
                margin-top: 15px;
            }

            .fieldsetatualizarprecos label {
                margin-left: 5px;
                margin-right: 20px;
            }

        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            /* white-space: nowrap;*/
            font-weight: normal;
            text-align: left;
            width: 834px;
            border-collapse: collapse;
            float: left;
            clear: left;
            margin-top: 10px;
            margin-bottom: 10px;
            min-height: 500px;
            margin-left: 25px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:nth-child(4) {
                text-align: center;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }

        .tabrotulos {
            width: 848px;
            margin: auto;
        }

        .materiaprima {
            width: 250px;
        }

            .materiaprima ul {
                margin: 0px;
                padding-left: 10px;
            }

                .materiaprima ul li {
                    max-width: 230px;
                    white-space: pre-wrap;
                }

        .meugrid tr td:last-of-type {
            text-align: center;
        }

        .nome {
            width: 237px;
        }

            .nome div {
                max-width: 230px;
                white-space: pre-wrap;
            }

        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }

        /*****************************/
        .width150 {
        }

        .height150 {
        }

        .height80 {
        }

        .btn {
            -webkit-border-radius: 7px;
            -moz-border-radius: 7px;
            border-radius: 2px;
            font-family: Arial;
            color: #ffffff;
            font-size: 14px;
            background: #63c269;
            padding: 5px 10px 5px 10px;
            text-decoration: none;
            cursor: pointer;
            border: 0px;
        }

            .btn:hover {
                background: #a4dbab;
                text-decoration: none;
                color: black;
                font-weight: bold;
            }


        .marginTop5porcento {
            margin: 6%;
            font-size: 15px;
        }

        .quadroPrincipal {
            float: left;
            width: 150px;
            height: 155px;
            text-align: center;
            position: relative;
            font-family: monospace;
            border: 1px solid #B1B1B1;
            margin: 0 14px 5px 14px;
            border-radius: 15px;
        }

        .quadroInteriorQtds {
            margin-top: 5%;
            text-align: center;
            font-size: 30px;
        }

        .AlinharAoRodapeCentralizar {
            position: absolute;
            bottom: 5px;
            text-align: center;
            width: 150px;
        }
        .meubotao {
            cursor: pointer;
            font: bold 14px tahoma;
        }
    </style>

    <div class="tituloPaginas" valign="top">
        Produtos não Vendidos
    </div>
    <div>
        <fieldset class="fieldsetatualizarprecos">
            <legend style="font-weight: bold;">Filtro</legend>

            <table class="tabrotulos">
                <tr class="rotulos">

                    <td>Data Inicial:<br />
                        <asp:TextBox runat="server" ID="txtDataInicial"></asp:TextBox>
                    </td>
                    <td>Data Final:<br />
                        <asp:TextBox runat="server" ID="txtDataFinal"></asp:TextBox>
                    </td>
                    <td>Fornecedor:<br />
                        <asp:DropDownList runat="server" ID="ddlfornecedor" Width="370px" CssClass="campos">
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr class="rotulos">
                    <td>Qtd. Máxima de Vendas:<br />
                        <asp:TextBox runat="server" ID="txtQtdMaxima"></asp:TextBox>
                    </td>

                     <td>Produto Id:<br />
                        <asp:TextBox runat="server" ID="txtProdutoId"></asp:TextBox>
                    </td>

                     <td>Produto Nome:<br />
                        <asp:TextBox runat="server" ID="txtProdutoNome"  Width="370px" ></asp:TextBox>
                    </td>


                </tr>
                <tr class="rotulos">

                    <td>Preco de:<br />
                        <asp:TextBox runat="server" ID="txtPrecoDe"></asp:TextBox>
                    </td>
                    <td>Preço até:<br />
                        <asp:TextBox runat="server" ID="txtPrecoAte"></asp:TextBox>
                    </td>
                    <td>Marca:<br />
                        <asp:DropDownList runat="server" ID="ddlMarca" Width="370px" CssClass="campos">
                        </asp:DropDownList>
                    </td>
                </tr>

                <tr class="rotulos">

                    <td>Preço promocional de:<br />
                        <asp:TextBox runat="server" ID="txtPrecoPromocionalDe"></asp:TextBox>
                    </td>
                    <td>Preço promocional até:<br />
                        <asp:TextBox runat="server" ID="txtPrecoPromocionalAte"></asp:TextBox>
                    </td>
                    <td>  Ordenar por:<br />
                        <asp:DropDownList runat="server" ID="ddlordenarpor" Width="370px" CssClass="campos">
                            <asp:ListItem>Selecione</asp:ListItem>
                            <asp:ListItem Value="produtoId">produto Id</asp:ListItem>
                            <asp:ListItem Value="produtoNome">produto Nome</asp:ListItem>
                            <asp:ListItem Value="fornecedorNome">Fornecedor</asp:ListItem>
                            <asp:ListItem Value="dataDaCriacao">Criação do Produto</asp:ListItem>
                            <asp:ListItem Value="quantidadeVendas">Itens Vendidos</asp:ListItem>
                            <asp:ListItem Value="produtoPrecoDeCusto">Preço de Custo</asp:ListItem>
                            <asp:ListItem Value="margem">Margem a prazo</asp:ListItem>
                            <asp:ListItem Value="margemavista">Margem a vista</asp:ListItem>
                            <asp:ListItem Value="produtoEstoqueAtual">Estoque</asp:ListItem>
                            <asp:ListItem Value="marcaNome">Marca</asp:ListItem>
                            <asp:ListItem Value="produtoPreco">Preço</asp:ListItem>
                            <asp:ListItem Value="produtoPrecoPromocional">Preço Promocional</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr class="rotulos">
                    <td>Produto Ativo<br />
                        <asp:RadioButton ID="rbAtivoSim" runat="server" GroupName="rbAtivos" Checked="True" /> Sim &nbsp;  
                        <asp:RadioButton ID="rbAtivoNao" runat="server" GroupName="rbAtivos" />  Não &nbsp;  
                        <asp:RadioButton ID="rbAtivoTotos" runat="server" GroupName="rbAtivos" /> Todos  
                    </td>

                    <td>&nbsp;
                    </td>

                    <td>
                      &nbsp;
                    </td>

                </tr>
                <tr class="rotulos">
                    <td>&nbsp;
                    </td>

                    <td>&nbsp;
                    </td>

                    <td>
                        <div style="width: 370px; text-align: right">
                            <asp:Button runat="server" ID="btnFiltrar" CssClass="meubotao" Text="Filtrar" OnClick="btnFiltrar_Click" />
                        </div>
                    </td>

                </tr>
            </table>
        </fieldset>

        <div style="float: left; clear: left; width: 868px; text-align: right; margin-left: 25px; margin-top: 15px;">
            <asp:Button runat="server" ID="btnExportarPlanilha" CssClass="meubotao" Text="Exportar Planilha" OnClick="btnExportarPlanilha_Click" />
        </div>
    </div>
    <div align="center" style="min-height: 500px; float: left; clear: left; width: 834px;">
        <asp:GridView ID="grdprodnaovendidos" CssClass="meugrid" runat="server" Width="834px" DataKeyNames="produtoId" AutoGenerateColumns="False"
            AllowPaging="True" OnPageIndexChanging="grdprodnaovendidos_PageIndexChanging" PageSize="20" OnRowDataBound="grdprodnaovendidos_RowDataBound">
            <Columns>

                <asp:BoundField DataField="produtoId" ItemStyle-Width="50px" HeaderText="Id do Produto">
                    <ItemStyle Width="50px"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="produtoNome" HeaderText="Produto" />
                <asp:BoundField DataField="fornecedorNome" HeaderText="Fornecedor" />
                <asp:BoundField DataField="produtoEstoqueAtual" HeaderText="Estoque" />
                <asp:TemplateField HeaderText="Itens Vendidos" ItemStyle-Width="50px" ShowHeader="False">
                    <ItemTemplate>
                        <asp:Label ID="lblQtdVendas" runat="server" Text="Label"></asp:Label>
                    </ItemTemplate>

                    <ItemStyle Width="50px"></ItemStyle>
                </asp:TemplateField>
                <asp:BoundField DataField="dataDaCriacao" HeaderText="Criação do Produto" />
                <asp:BoundField DataField="produtoPrecoDeCusto" DataFormatString="{0:C}" HeaderText="Preço de Custo" />
                <asp:BoundField DataField="margemavista" HeaderText="Margem a Vista" />
                <asp:BoundField DataField="margem" HeaderText="Margem a Prazo" />

                <asp:CheckBoxField DataField="produtoAtivo" HeaderText="Ativo" />
                <asp:TemplateField HeaderText="Editar Produto" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center" ShowHeader="False">
                    <ItemTemplate>
                        <a id="linkeditarproduto" runat="server" target="blank">
                            <img src="images/btEditar.jpg" alt="Editar" style="border-style: none;" />
                        </a>
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center" Width="80px"></ItemStyle>
                </asp:TemplateField>



            </Columns>
        </asp:GridView>
    </div>
    <div style="float: left; clear: left; width: 868px; margin-left: 25px; margin-top: 15px;">
        <strong>Quantidade de itens encontrados nesta busca:
        <span style="text-decoration: underline">
            <asp:Label ID="lblitensencontrados" runat="server" Text=""></asp:Label></span>
           
        </strong>
    </div>
</asp:Content>

