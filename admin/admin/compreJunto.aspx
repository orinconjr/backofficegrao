﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="compreJunto.aspx.cs" Inherits="admin_compreJunto"  Theme="Glass" MaintainScrollPositionOnPostback=true %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Compre junto</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        <tr>
            <td>
                <dxwgv:aspxgridview ID="grd" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="sqlCompreJunto" KeyFieldName="idDoGrupo" Width="834px" 
                    Cursor="auto">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir? " 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        showheaderfilterbutton="True" />
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" mode="Inline" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="ID" 
                            FieldName="idDoGrupo" ReadOnly="True" VisibleIndex="0" Width="30px">
                            <Settings AutoFilterCondition="Contains" />
                            <editformsettings visible="False" CaptionLocation="Top" ColumnSpan="2" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Nome do grupo" FieldName="nomeDoGrupo" 
                            VisibleIndex="1" Width="500px">
                            <PropertiesTextEdit>
                                
<ValidationSettings SetFocusOnError="True">
                                    
<RequiredField ErrorText="Preencha a descrição" IsRequired="True" />
                                
</ValidationSettings>
                            
</PropertiesTextEdit>
                            <settings autofiltercondition="Contains" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Desconto em %" FieldName="porcentagemDeDesconto" 
                            VisibleIndex="2" Width="100px">
                            <PropertiesTextEdit MaxLength="4">
                            
<ValidationSettings SetFocusOnError="True">
                                    
<RequiredField ErrorText="Preencha o prazo de entrega." IsRequired="True" />
                                    

<RegularExpression ErrorText="Preencha apenas com números e virgula." 
                                        ValidationExpression="^\d*\,?\d*$" />
                                
</ValidationSettings>
                            
</PropertiesTextEdit>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Incluir" VisibleIndex="3">
                            <DataItemTemplate>
                                <asp:HyperLink ID="hpl" runat="server" ImageUrl="~/admin/images/btIncluirIcon.jpg" 
                                    
                                    
                                    NavigateUrl='<%# "compreJuntoItens.aspx?idDoGrupo=" + Eval("idDoGrupo")  + "&nomeDoGrupo=" + Eval("nomeDoGrupo") %>'></asp:HyperLink>
                            </DataItemTemplate>
                            <CellStyle HorizontalAlign="Center">
                            </CellStyle>
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewCommandColumn VisibleIndex="4" Width="90px" ButtonType="Image">
                            <editbutton visible="True" Text="Editar">
                                <Image Url="~/admin/images/btEditar.jpg" />
                            </editbutton>
                            <NewButton Visible="True" Text="Novo">
                                <Image Url="~/admin/images/btNovo.jpg" />
                            </NewButton>
                            <deletebutton visible="True" Text="Excluir">
                                <Image Url="~/admin/images/btExcluir.jpg" />
                            </deletebutton>
                            <CancelButton Text="Cancelar">
                                <Image Url="~/admin/images/btCancelarIcon.jpg" />
                            </CancelButton>
                            <UpdateButton>
                                <Image Url="~/admin/images/btSalvarIcon.jpg" />
                            </UpdateButton>
                            <ClearFilterButton Visible="True" Text="Limpar filtro">
                                <Image Url="~/admin/images/btLimparFiltro.jpg" />
                            </ClearFilterButton>
                            <HeaderTemplate>
                                <img alt="" src="images/legendaIcones.jpg" />
                            </HeaderTemplate>
                        </dxwgv:GridViewCommandColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:aspxgridview>
<%--                <dxwgv:aspxgridviewexporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:aspxgridviewexporter>--%>
                <asp:LinqDataSource ID="sqlCompreJunto" runat="server" 
                    ContextTypeName="tbCompreJuntoDataContext" EnableDelete="True" 
                    EnableInsert="True" EnableUpdate="True" TableName="tbCompreJuntoGrupos">
                </asp:LinqDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    </asp:Content>


