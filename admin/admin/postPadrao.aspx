﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="postPadrao.aspx.cs" Theme="Glass" Inherits="admin_postPadrao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <link rel="stylesheet" href="css/postPadrao.css">
    <link rel="stylesheet" href="css/assets/reset.css">
    <link rel="stylesheet" href="css/assets/font.css">

    <script type="text/javascript">
        function pop(div) {
            document.getElementById(div).style.display = 'block';
        }
        function hide(div) {
            document.getElementById(div).style.display = 'none';
        }
        //To detect escape button
        document.onkeydown = function (evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                hide('popDiv');
            }
        };

        function optionPost(opcao) {
            if (opcao.value == "1") {
                document.getElementById('tbDadosPostPadrao').style.display = "block";
                document.getElementById('tbDadosPostProduto').style.display = "none";
            } else {
                document.getElementById('tbDadosPostProduto').style.display = "block";
                document.getElementById('tbDadosPostPadrao').style.display = "none";
            }
        }

        function subirImagem() {
            if (document.getElementById('panel1').style.display == "block") {
                var posicao = document.getElementById('imgFoto').style.marginTop == "" ? 0 : parseInt(document.getElementById('imgFoto').style.marginTop.replace("px", ""));
                if ((posicao < 0 || posicao > 0 || posicao == 0) && posicao != -148) {
                    posicao -= 2;
                    document.getElementById('imgFoto').style.marginTop = posicao + "px";
                }
            }

        }

        function baixarImagem() {
            if (document.getElementById('panel1').style.display == "block") {
                var posicao = document.getElementById('imgFoto').style.marginTop == "" ? 0 : parseInt(document.getElementById('imgFoto').style.marginTop.replace("px", ""));
                if (posicao > 0 || posicao < 0) {
                    posicao += 2;
                    document.getElementById('imgFoto').style.marginTop = posicao + "px";
                }
            }
        }

        function mostraOcultaEdicaoNomeProduto(ckb) {
            if (ckb.childNodes[0].checked == true) {
                document.getElementById('<%=lblProdutoNomePostProduto.ClientID%>').style.display = "none";
                document.getElementById('<%=txtProdutoNomePostProduto.ClientID%>').style.display = "block";
            } else {
                document.getElementById('<%=lblProdutoNomePostProduto.ClientID%>').style.display = "block";
                document.getElementById('<%=txtProdutoNomePostProduto.ClientID%>').style.display = "none";
            }

            document.getElementById('<%=divVisualizarPostProduto.ClientID%>').style.display = 'none';
        }

        function trocandoProduto() {
            document.getElementById('<%=ckbEditarNomeProduto.ClientID%>').checked = false;
            document.getElementById('<%=lblProdutoNomePostProduto.ClientID%>').style.display = "block";
            document.getElementById('<%=txtProdutoNomePostProduto.ClientID%>').style.display = "none";
        }

        function visualizarPostPadrao() {
            document.getElementById("light").style.display = "block";
            document.getElementById("fade").style.display = "block";

            var listImgs = document.getElementsByClassName("rbt_img");

            for (var i = 0, x = listImgs.length; i < x; i++) {
                if (listImgs[i].childNodes[1].checked === true) {
                    document.getElementById("imgFoto").src = listImgs[i].childNodes[3].src;
                }
            }

        }
    </script>

    <script type="text/javascript">

        document.querySelector('body').addEventListener('keydown', function (event) {


            var tecla = event.keyCode;

            if (tecla == 13) {

                // tecla ENTER

            } else if (tecla == 27) {

                // tecla ESC

            } else if (tecla == 37) {

                // seta pra ESQUERDA

            } else if (tecla == 38) {

                // seta pra CIMA
                subirImagem();

            } else if (tecla == 39) {

                // seta pra DIREITA

            } else if (tecla == 40) {

                // seta pra BAIXO
                baixarImagem();
            }

        });

    </script>

    <style type="text/css">
        
        .rbt_img {
            float: left;
            margin: 5px 5px 0 0;
        }

         .rbt_img > input { /* HIDE RADIO */
             visibility: visible; /* Makes input not-clickable */
             position: absolute; /* Remove input from document flow */
         }

        .rbt_img > input + img { /* IMAGE STYLES */
            cursor: pointer;
            border: 2px solid #ccc;
        }

        .rbt_img > input:checked + img { /* (RADIO CHECKED) IMAGE STYLES */
            border: 2px solid #21bf59;
        }
    </style>

    <asp:ScriptManager runat="server" ScriptMode="Release"></asp:ScriptManager>
    <div class="tituloPaginas">
        <asp:Label ID="lblAcao" runat="server">Post Padrão</asp:Label>
    </div>

    <div style="font-family: Tahoma; font-size: 14px; margin-left: 7%;">
        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblOpcaoPost">
            <asp:ListItem Text="Post Padrão" Value="1" Selected="True" onclick="optionPost(this);"></asp:ListItem>
            <asp:ListItem Text="Post Campanha" Value="2" onclick="optionPost(this);"></asp:ListItem>
        </asp:RadioButtonList>
        <br />
        <table id="tbDadosPostPadrao">
            <tr>
                <td>Produto ID:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtProdutoId" Width="109" MaxLength="7"></asp:TextBox>
                    <asp:Label runat="server" ID="lblProdutoNome"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Valor R$:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtValorProduto" Width="197" MaxLength="8"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Layout Valor:
                </td>
                <td>
                    <asp:DropDownList ID="ddlOplayoutValor" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="Boleto" Value="0"></asp:ListItem>
                        <asp:ListItem Text="De Por" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Parcelado" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Sem Valor" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <%--<tr>
                <td>COR DO BALÃO:
                </td>
                <td>
                    <asp:DropDownList ID="ddlOplayoutBalao" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="ROSA" Value="0" Selected></asp:ListItem>
                        <asp:ListItem Text="AZUL" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td>Cor do logo:
                </td>
                <td>
                    <asp:DropDownList ID="ddlOplayoutLogo" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="Nenhum" Value="nenhum" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Branco" Value="branco"></asp:ListItem>
                        <asp:ListItem Text="Cinza" Value="cinza"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Selo Rodapé
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpSelo" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="Nenhum" Value="nenhum" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ultimas horas" Value="selo-1-ultimas-horas"></asp:ListItem>
                        <asp:ListItem Text="Corra, só neste final de semana" Value="selo-2-corra"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Layout do Post:
                </td>
                <td>
                    <asp:DropDownList ID="ddlOplayoutTipo" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="Padrão" Value="padrao" Selected="padrao"></asp:ListItem>
                        <%--<asp:ListItem Text="Black Friday" Value="blackfriday"></asp:ListItem>--%>
                        <asp:ListItem Text="Promocional 'Corre, é só hoje'" Value="promosohj"></asp:ListItem>
                        <asp:ListItem Text="Promocional Deco Madeira" Value="decoMadeira"></asp:ListItem>
                        <asp:ListItem Text="Promocional Todo Site Off" Value="todoSiteOff"></asp:ListItem>
                        <asp:ListItem Text="Promocional Dia de Ofertas" Value="diaDeOfertas"></asp:ListItem>
                        <asp:ListItem Text="Promocional Queridinhos da Mamãe" Value="queridinhoMamaes"></asp:ListItem>
                        <asp:ListItem Text="Promocional FDS de descontos Todo Site" Value="fdsDescontos"></asp:ListItem>
                        <asp:ListItem Text="Promocional Ultima Chance 2017" Value="ultimaChance"></asp:ListItem>
                        <asp:ListItem Text="Promocional Domingo de Ofertas" Value="domingoOfertas"></asp:ListItem>
                        <asp:ListItem Text="Promocional Sábado Imperdível" Value="sabadoImperdivel"></asp:ListItem>
                        <asp:ListItem Text="Promocional Alerta de Descontos" Value="alertaDeDescontos"></asp:ListItem>
                        <asp:ListItem Text="Promocional Pequenos Preços - Grandes Ofertas" Value="pPrecosGDescontos"></asp:ListItem>
                        <asp:ListItem Text="Promocional Novidades para decorar" Value="novidadesDecorar"></asp:ListItem>
                        <asp:ListItem Text="Combo Kit + Berço Mel" Value="comboKitBerco"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Rede Social
                </td>
                <td>
                    <asp:DropDownList ID="ddlOpTipoPost" runat="server" Height="21" Width="200" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPost').style.display='none';">
                        <asp:ListItem Text="Facebook" Value="facebook" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Instagram" Value="instagram"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr class="opcaoInsta">
                <td>Ajustar imagem:</td>
                <td>
                    <asp:TextBox runat="server" ID="sobeImagemPx" Width="109" MaxLength="7"></asp:TextBox>px (para cima)
                    <!-- <%= sobeImagemPx.Text %> -->
                </td>
            </tr>
            <%--<tr>
                <td>Tarja:
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fupImagem" />
                </td>
            </tr>
            <tr>
                <td>Foto 222:
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fupFoto" />
                </td>
            </tr>--%>
            <tr>
                <td></td>
                <td style="text-align: left;">
                    <div style="float: left;">
                        <asp:Button runat="server" ID="btnMontarImagem" Text="Montar Imagem" Width="113" OnClick="btnMontarImagem_OnClick" />
                    </div>
                    <div style="float: left; width: 150px; height: 21px; text-align: center;" runat="server" id="divVisualizarPost" visible="False">
                        <a href="javascript:void(0)" onclick="visualizarPostPadrao();">Visualizar Post</a>
                    </div>
                </td>
            </tr>
        </table>

        <asp:Panel ID="pnlListaFotos" runat="server" Visible="False" GroupingText="Opção de Foto" Width="93%" Style="margin-top: 20px;">
            <asp:Repeater ID="rptFotos" runat="server">
                <ItemTemplate>
                    <div class="rbt_img">
                        <input type="radio" name="selecionarFoto" value='<%# Eval("produtoFoto") %>' id='selecionarFoto' runat="server"/>
                        <label for='<%# Eval("produtoFoto") %>'>
                            <img src='<%# "/admin/postPadrao/fotos/" + Eval("produtoId") + "/" + Eval("produtoFoto") + ".jpg" %>' alt="Foto" width="100" />
                        </label>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </asp:Panel>

        <table id="tbDadosPostProduto" style="display: none;">
            <tr>
                <td style="width: 120px;">Produto ID:
                </td>
                <td class="paddingBottomTd"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtProdutoIdPostProduto" Width="109" MaxLength="7" onblur="trocandoProduto();"></asp:TextBox>

                </td>
                <td colspan="4">
                    <div style="float: left; border: 1px solid #a9a9a9;">
                        <asp:Label runat="server" ID="lblProdutoNomePostProduto" Width="475" Style="background-color: #e6e6e6;"></asp:Label>
                        <asp:TextBox runat="server" ID="txtProdutoNomePostProduto" Width="475" Style="display: none;"></asp:TextBox>
                    </div>
                    <div style="float: left; padding: 4px 0 0 5px;">
                        <asp:CheckBox runat="server" Text="Editar Nome Produto" ID="ckbEditarNomeProduto" onchange="mostraOcultaEdicaoNomeProduto(this);" Visible="False" />
                    </div>
                </td>
                <td></td>
            </tr>
            <%--<tr>
                <td>Desconto (%):
                </td>
                <td class="paddingBottomTd">
                    <asp:DropDownList ID="ddlLayoutPorcetagemDescontoPostProduto" runat="server" Height="21" Width="113" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPostProduto').style.display='none';">
                        <asp:ListItem Text="Não Mostrar" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Pequeno" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Grande" Value="2" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>--%>
            <tr>
                <td>Preço Parcela:
                </td>
                <td style="width: 120px;">Cor Esquerda:</td>
                <td style="width: 120px;">Cor Balão:</td>
                <td style="width: 120px;">Cor Borda Balão:</td>
                <td>Título:</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlLayoutPrecoParcelaPostProduto" runat="server" Height="21" Width="113" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPostProduto').style.display='none';">
                        <asp:ListItem Text="Não Mostrar" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Pequeno" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Médio" Value="2" Selected="True"></asp:ListItem>
                        <%--<asp:ListItem Text="Grande" Value="3"></asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRgbBgPostProduto" runat="server" Width="109" MaxLength="7" Text="#422955"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRgbBgCorBalaoPostProduto" runat="server" Width="109" MaxLength="7" Text="#422955"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtCorBordaBalaoPostProduto" runat="server" Width="109" MaxLength="7" Text="#FFF"></asp:TextBox>
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fupImagemTituloPostProduto" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Preço A Vista:
                </td>
                <td>Cor Direita:
                </td>
                <td>Cor Texto Balão:</td>
                <td>Borda Bolão:</td>
                <td>Tarja:</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlLayoutPrecoAVistaPostProduto" runat="server" Height="21" Width="113" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPostProduto').style.display='none';">
                        <asp:ListItem Text="Não Mostrar" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Pequeno" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Médio" Value="2" Selected="True"></asp:ListItem>
                        <%--<asp:ListItem Text="Grande" Value="3"></asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtRgbBgCorDireitaPostProduto" runat="server" Width="109" MaxLength="7" Text="#422955"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtRgbBgCorTextoBalaoPostProduto" runat="server" Width="109" MaxLength="7" Text="#FFF"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtEspessuraBordaBalaoPostProduto" runat="server" Width="109" MaxLength="2" Text="10"></asp:TextBox>
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fupImagemTarjaPostProduto" />
                </td>
                <td></td>
            </tr>
            <%--<tr>
                <td>Balão Desconto:
                </td>
                <td class="paddingBottomTd">
                    <asp:FileUpload runat="server" ID="fupImagemBalaoPostProduto" />
                </td>
            </tr>--%>
            <tr>
                <td>Tamanho Nome
                </td>
                <td>Cor Nome
                </td>
                <td>Cor Valores
                </td>
                <td>Cor Logo:
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList ID="ddlTamanhoTituloPostProduto" runat="server" Height="21" Width="113" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPostProduto').style.display='none';">
                        <asp:ListItem Text="Normal" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pequena" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:TextBox ID="txtCorTituloPostProduto" runat="server" Width="109" MaxLength="7" Text="#FFF"></asp:TextBox>
                </td>
                <td>
                    <asp:TextBox ID="txtCorValoresPostProduto" runat="server" Width="109" MaxLength="7" Text="#FFF"></asp:TextBox>
                </td>
                <td>
                    <asp:DropDownList ID="ddlCorLogoPostProduto" runat="server" Height="21" Width="113" onchange="document.getElementById('ctl00_conteudoPrincipal_divVisualizarPostProduto').style.display='none';">
                        <asp:ListItem Text="Logo Preto" Value="logo-preto.png"></asp:ListItem>
                        <asp:ListItem Text="Logo Rosa" Value="logo-rosa.png" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Logo Verde" Value="logo-verde.png"></asp:ListItem>
                        <asp:ListItem Text="Logo Branco" Value="logo-branco.png"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:CheckBox runat="server" Text="Limpar Imagens" ID="ckbLimparImagens" />

                </td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4" style="padding-bottom: 10px;"></td>
                <td style="padding-bottom: 10px;"></td>
                <td style="padding-bottom: 10px;"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div style="float: left;">
                        <asp:Button runat="server" ID="btnMontarImagemPostProduto" Text="Montar Imagem" Width="113" OnClick="btnMontarImagem_OnClick" />
                    </div>
                    <div style="float: left; width: 150px; height: 21px; text-align: center;" runat="server" id="divVisualizarPostProduto" visible="False">
                        <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block';">Visualizar Post</a>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
        <br />
    </div>


    <div id="light" class="white_content">
        <div id="panel1">
            <div id="postPadrao" class="postPadrao">
            	<div class="porcentagem">
                    <p class="center">
                    	<span>
	                        <asp:Literal ID="litPorcentagem" runat="server" Text="24"></asp:Literal></span><span class="porcento">%</span>
	                    <span class="lower">OFF</span>
                    </p>
                </div>
                <div class="logoGrao"></div>
                <img src='<%=ImgUrl %>' id="imgFoto" alt="" style="float: left; width: 100%; cursor: pointer;bottom: <%= sobeImagemPx.Text %>px" onclick="document.getElementById('ctl00_conteudoPrincipal_btnExport').click();" />
                <%--<img src="../fotos/produto.png" id="imgFoto" alt="" style="float: left; width: 100%; cursor: pointer;" onclick="document.getElementById('ctl00_conteudoPrincipal_btnExport').click();" />--%>
                <div class="boxInfo">
                   <div class="bloco-flutuante">
                   		<div class="bloco-conteudo">
                   			 <%--<div id="boxImagem">
			                    <p class="exclusivo">
			                        <img src="../imagens/tarja-lançamento.png" alt="">
			                    </p>
			                </div>--%>
			                <div id="divTarjaExclusivo" style="display: none;">
			                    <img id="imgTarjaExclusivo" runat="server" src="" alt="" />
			                </div>
			                <p class="preco precoParcelado">
			                    <span class="padrao">
			                    	<span class="apenas">Apenas</span>
                                    <span class="parcelas">12x de</span>
                                    <span class="valorInteiro"><span class="rs">R$</span> <asp:Literal ID="litPrecoParceladoInteiro" runat="server" Text="29"></asp:Literal></span>
                                    <span class="moedaTopo">,<asp:Literal ID="litPrecoParceladoCentavos" runat="server" Text="11"></asp:Literal></span>
			                    </span>
			                </p>
			                <p class="preco Avista">
			                    <span class="padrao">
			                    	<span class="apenas">Apenas</span>
			                        <span class="valorInteiro"><span class="rs">R$ </span><asp:Literal ID="litprecoBoletoInteiro" runat="server" Text="29"></asp:Literal></span><span class="moedaTopo">,<asp:Literal ID="precoBoletoCentavos" runat="server" Text="11"></asp:Literal></span>
                                    <span class="textBoleto">à vista</span>
			                    </span>
			                </p>
			                <div class="preco precoDePor">
			                	<div class="padrao">
				                    <p class="de">
				                        <span class="minText">De:</span> <span class="rs">R$</span> <span class="cutLine">
				                            <asp:Literal ID="litPrecoDe" runat="server" Text="70,90"></asp:Literal>
				                        </span>
				                    </p>
				                    <p class="por">
				                        <span class="minText">Por:</span> <span class="moeda none">R$</span>
				                        <span class="valorInteiro"><asp:Literal ID="litPrecoPorInteiro" runat="server" Text="53"></asp:Literal></span>
				                        <span class="moedaTopo">,<asp:Literal ID="litPrecoPorCentavos" runat="server" Text="47"></asp:Literal></span>
				                        <span class="textBoleto">à vista</span>
				                    </p>
				                </div>
			                </div>
			                <!-- <span style="float: left; position: absolute; bottom: 5px; font-size: 16px; font-family: Arial; right: 36px; color: rgb(68, 68, 68);" class="promocaoTempoLimitado">* Promoção por tempo limitado</span> -->
                   		</div>
                   </div>
                   <!-- <img src="../imagens/rodape-1.png" alt="www.graodegente.com.br" class="rodape"> -->
                   <div class="rodape"></div>
                   <div class="seloRodape"></div>
                </div>
                <div style="width: 50px; height: 110px; position: absolute; right: 20px; top: 40%; display: none;">
                    <div style="width: 58px;">
                        <img src="images/arrow-up-icon.png" alt="" id="imgSetaUp" title="subir imagem" width="50" style="cursor: pointer;" onclick="subirImagem(this)" />
                    </div>
                    <div style="width: 58px; padding-top: 10px;">
                        <img src="images/arrow-down-icon.png" alt="" id="imgSetaDown" title="baixar imagem" width="50" style="cursor: pointer;" onclick="baixarImagem(this)" />
                    </div>
                </div>
            </div>

        </div>
        <div id="panel2">
            <div id="postPadraoProduto" class="bgAnuncio">
                <div class="blocoLeft">
                    <div class="tarja" runat="server">
                        <img id="imgTarjaPostProduto" runat="server" src="" alt="" />
                    </div>
                    <div id="imgTituloPostProduto" runat="server"></div>
                    <img src='<%= ImgUrl %>' class="produto" alt="Berço" width="488" title="Download Post" onclick=" document.getElementById('ctl00_conteudoPrincipal_btnExportPostProduto').click();" style="cursor: pointer;" />
                    <%--<img src="../fotos/produto.png" class="produto" alt="Berço" width="488" title="Download Post" onclick=" document.getElementById('ctl00_conteudoPrincipal_btnExportPostProduto').click();" />--%>
                    <%--<img src="../imagens/logo-post-produto.png" alt="Grão de Gente" title="Gente" class="logo" />--%>
                    <img src='<%= ImgUrlLogoRodape %>' alt="Grão de Gente" title="Gente" class="logo" />
                </div>
                <div class="blocoRight">
                    <h1 id="h1TituloPostproduto">
                        <asp:Literal runat="server" ID="litNomeProdutoPostProduto"></asp:Literal>
                    </h1>
                    <div class="desconto" style="border-radius: 100%;">
                        <span class="porcento">
                            <span class="div">
                                <span class="blocoPorcento">
                                    <span style="left: -4px;">
                                        <asp:Literal runat="server" ID="litPorcentagemDesconto1PostProduto" Text="40"></asp:Literal></span><span style="left: -12px;"><asp:Literal runat="server" ID="litPorcentagemDesconto2PostProduto" Text="40"></asp:Literal></span>
                                </span>
                            </span>
                            <br>
                            <span class="texto">OFF</span>
                            <span class="sinal">%</span>
                        </span>

                    </div>

                    <div class="pagamento">
                        <div class="flutua">
                            <span class="vezes">
                                <asp:Literal runat="server" ID="liMaximoParcelasProduto" Text="12"></asp:Literal>x</span>
                            <span class="rs">R$</span>
                        </div>
                        <span class="parcela">
                            <asp:Literal runat="server" ID="litValorInteiroPostProduto" Text="30"></asp:Literal><span class="cp"> ,<asp:Literal runat="server" ID="litValorCentavosPostProduto" Text="27"></asp:Literal></span></span>
                        <div style="position: absolute; width: 158px; margin-top: 104px;" id="divOuParcela">
                            <p style="margin-left: 90px" runat="server" id="pOuParcela"></p>
                        </div>
                    </div>

                    <p style="width: 100%; height: 31px;"></p>
                    <div class="avista">
                        <span class="rs">R$</span><span class="parcela"><asp:Literal runat="server" ID="litValorAVistaInterioPostProduto" Text="308"></asp:Literal><span class="cb"> ,<asp:Literal runat="server" ID="litValorAVistaCentavosPostProduto" Text="76"></asp:Literal></span></span>
                        <div style="position: absolute; width: 120px; margin-top: 60px;" id="divNoBotelo">
                            <p class="boleto" style="height: 21px;">no boleto</p>
                        </div>
                    </div>
                    <p class="boleto" style="height: 21px;"></p>

                </div>


            </div>
        </div>
    </div>

     <div id="fade" class="black_overlay" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none';" title="Fechar">
    </div>

    <div style="width: 840px;">
        <asp:HiddenField ID="hfImageData" runat="server" />
        <asp:Button ID="btnExport" Text="Export to Image" runat="server" UseSubmitBehavior="false" Style="display: none;"
            OnClick="ExportToImage" OnClientClick="return capture(this)" />
        <asp:Button ID="Button1" Text="Export to Image v2" runat="server" UseSubmitBehavior="false" Style="display: none;"
            OnClick="ExportToImage" OnClientClick="return capture(this)" />
        <asp:Button ID="btnExportPostProduto" Text="Export to Image" runat="server" UseSubmitBehavior="false" Style="display: none;"
            OnClick="ExportToImage" OnClientClick="return ConvertToImagePostProduto(this)" />
    </div>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">
    </script>
    <!-- <script type="text/javascript" src="http://cdn.rawgit.com/niklasvh/html2canvas/master/dist/html2canvas.min.js"></script> 404-->
    <script type="text/javascript" src="http://admin.graodegente.com.br/admin/js/html2canvas.min.js"></script>

    <script type="text/javascript">
        function ConvertToImage(btnExport) {
            html2canvas($("#postPadrao")[0]).then(function (canvas) {
                var base64 = canvas.toDataURL();
                $("[id*=hfImageData]").val(base64);
                __doPostBack(btnExport.name, "");
            });
            return false;
        }

        function ConvertToImagePostProduto(btnExportPostProduto) {
            html2canvas($("#postPadraoProduto")[0]).then(function (canvas) {
                var base64 = canvas.toDataURL();
                $("[id*=hfImageData]").val(base64);
                __doPostBack(btnExportPostProduto.name, "");
            });
            return false;
        }

        function capture(btnExport) {
            $('#postPadrao').focus();
            console.log($('#postPadrao')[0]);
            html2canvas($('#postPadrao'), {
                logging: true,
                useCORS: true,
                onrendered: function (canvas) {
                    //Set hidden field's value to image data (base-64 string)
                    //$('#img_val').val(canvas.toDataURL("image/jpg"));
                    //Submit the form manually
                    //document.getElementById("myForm").submit();

                    var base64 = canvas.toDataURL();
                    $("[id*=hfImageData]").val(base64);
                    __doPostBack(btnExport.name, "");
                }
            });
        }

        function capturev2() {
            html2canvas($('#postPadrao'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL();
                    $("#img-out").append(canvas);
                }
            });
        }
    </script>

    <script type="text/javascript">
        function pop(div) {
            document.getElementById(div).style.display = 'block';
        }
        function hide(div) {
            document.getElementById(div).style.display = 'none';
        }
        //To detect escape button
        document.onkeydown = function (evt) {
            evt = evt || window.event;
            if (evt.keyCode == 27) {
                hide('popDiv');
            }
        };
    </script>
</asp:Content>

