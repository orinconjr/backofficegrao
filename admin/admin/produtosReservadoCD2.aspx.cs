﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_produtosReservadoCD2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
            fillGrid(false);
        
    }

    private void fillGrid(bool rebind)
    {
        var data = new dbCommerceDataContext();

        var pedidos = (from c in data.tbProdutoEstoques
                       join d in data.tbItemPedidoEstoques on c.pedidoIdReserva equals d.tbItensPedido.pedidoId into itens
            where
                c.idCentroDistribuicao == 2 && c.pedidoIdReserva != null && c.enviado == false &&
                itens.Count(
                    x =>
                        x.idCentroDistribuicao == 2 && x.reservado == false && x.cancelado == false &&
                        x.enviado == false) > 0
            select new
            {
                c.tbPedidoFornecedorItem.idPedidoFornecedor,
                c.idPedidoFornecedorItem,
                c.tbProduto.produtoNome,
                pedidoId = c.pedidoIdReserva ?? 0,
                dataLimite = itens.OrderByDescending(x => x.dataLimite).FirstOrDefault().dataLimite
            }).ToList().OrderBy(x => x.dataLimite).ThenBy(x => x.pedidoId);

        grd.DataSource = pedidos;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();

        var pedidosCompletos = (from c in data.tbProdutoEstoques
                                join d in data.tbItemPedidoEstoques on c.pedidoIdReserva equals d.tbItensPedido.pedidoId into itens
            where
                c.idCentroDistribuicao == 2 && c.pedidoIdReserva != null && c.enviado == false &&
                itens.Count( x => x.idCentroDistribuicao == 2 && x.reservado == false && x.cancelado == false && x.enviado == false) == 0
            select new
            {
                c.tbPedidoFornecedorItem.idPedidoFornecedor,
                c.idPedidoFornecedorItem,
                c.tbProduto.produtoNome,
                pedidoId = c.pedidoIdReserva ?? 0,
                dataLimite = itens.OrderByDescending(x => x.dataLimite).FirstOrDefault().dataLimite
            }).ToList().OrderBy(x => x.dataLimite).ThenBy(x => x.pedidoId); ;

        grdCompletos.DataSource = pedidosCompletos;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdCompletos.DataBind();

        var pedidosEmissao = (from c in data.tbProdutoEstoques
                              join d in data.tbItemPedidoEstoques on c.pedidoIdReserva equals d.tbItensPedido.pedidoId into itens
                              join e in data.tbPedidoPacotes on c.idPedidoEnvio equals e.idPedidoEnvio into pacotes
            where
                c.idCentroDistribuicao == 2 && c.pedidoIdReserva != null && c.enviado == true &&
                pacotes.Count( x => x.rastreio == "" && (x.despachado ?? false) == false) > 0
            select new
            {
                c.tbPedidoFornecedorItem.idPedidoFornecedor,
                c.idPedidoFornecedorItem,
                c.tbProduto.produtoNome,
                pedidoId = c.pedidoIdReserva ?? 0,
                dataLimite = itens.OrderByDescending(x => x.dataLimite).FirstOrDefault().dataLimite
            }).ToList().OrderBy(x => x.dataLimite).ThenBy(x => x.pedidoId); ;

        grdAguardandoEmissao.DataSource = pedidosEmissao;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grdAguardandoEmissao.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        try
        {
            var dataLimite = grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimite" });
            if (dataLimite != null)
            {
                DateTime dataLimiteR = DateTime.Now;
                DateTime.TryParse(dataLimite.ToString(), out dataLimiteR);
           
                if (dataLimiteR.Date < DateTime.Now.Date)
                {
                    int i = 0;
                    while ((i < e.Row.Cells.Count))
                    {
                        e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                        i++;
                    }
                }
            }
        }
        catch (Exception)
        {

        }

    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            if (pedidoId > 0) e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
}