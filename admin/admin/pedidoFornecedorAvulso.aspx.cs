﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidoFornecedorAvulso : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                int id = Convert.ToInt32(Request.QueryString["id"]);
                enviaExcelEmailFornecedor(id);
                Response.Redirect("pedidoFornecedorAvulso.aspx");
            }
        }
    }


    private void fillGrid(bool rebind)
    {
        grd.DataSource = sql;
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }
    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int idPedidoFornecedor = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idPedidoFornecedor" }).ToString(), out idPedidoFornecedor);
        //Literal litEntregue = (Literal)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["entregue"] as GridViewDataColumn, "litEntregue");
        LinkButton btnExportar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["exportar"] as GridViewDataColumn, "btnExportar");
        LinkButton btnReenviar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["reenviar"] as GridViewDataColumn, "btnReenviar");
        LinkButton btnEditar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["editar"] as GridViewDataColumn, "btnEditar");
        LinkButton btnVer = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["ver"] as GridViewDataColumn, "btnVer");
        LinkButton btnConfirmar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["confirmado"] as GridViewDataColumn, "btnConfirmar");

        if (idPedidoFornecedor == 0)
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFFFFF");
                i++;
            }
            //litEntregue.Text = "-";
            btnExportar.Visible = false;
            btnEditar.Visible = false;
            btnVer.Visible = false;
        }
        else
        {
            var pedidoDc = new dbCommerceDataContext();
            var pedido = (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
            if (pedido.confirmado == true)
            {
                btnConfirmar.Visible = false;
            }
            else
            {
                btnConfirmar.Visible = true;
            }
            var itensDoPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c);
            int totalItens = itensDoPedido.Count();
            if (totalItens > itensDoPedido.Where(x => x.entregue == true).Count() && (pedido.dataLimite.Date <= DateTime.Now.Date))
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
            else if (totalItens == itensDoPedido.Where(x => x.entregue == false).Count())
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFFFBB");
                    i++;
                }
                //litEntregue.Text = "Não";
            }
            else if (totalItens > itensDoPedido.Where(x => x.entregue == true).Count())
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
                //litEntregue.Text = "Parcialmente";
            }
            else
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#D2E9FF");
                    i++;
                }
                //litEntregue.Text = "Sim";
            }
        }

    }

    private void exportaExcelPedidoFornecedor(int pedidoFornecedorId)
    {
        /*#region cabecalhoTabela
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ClearContent();
        HttpContext.Current.Response.ClearHeaders();
        HttpContext.Current.Response.Buffer = true;
        HttpContext.Current.Response.ContentType = "application/ms-excel";
        HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=Reports.xls");

        HttpContext.Current.Response.Charset = "UTF-8";
        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.Default; 
        //HttpContext.Current.Response.Charset = "utf-8";
       // HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
        //sets font
        HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
        HttpContext.Current.Response.Write("<BR><BR><BR>");
        //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
        HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " + "borderColor='#000000' cellSpacing='0' cellPadding='0' " + "style='font-size:10.0pt; font-family:Calibri; background:white;'>");
        #endregion

        var pedidosDc = new dbCommerceDataContext();

        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td><b>");
        HttpContext.Current.Response.Write("Pedido: " + pedidoFornecedorId);
        HttpContext.Current.Response.Write("</b></td>");
        HttpContext.Current.Response.Write("<td><b>");
        HttpContext.Current.Response.Write("Data de Solicitação: " + pedido.data.ToShortDateString());
        HttpContext.Current.Response.Write("</b></td>");
        HttpContext.Current.Response.Write("<td><b>");
        HttpContext.Current.Response.Write("Data limite de entrega: " + pedido.dataLimite.ToShortDateString());
        HttpContext.Current.Response.Write("</b></td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("</tr>");

        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("</tr>");

        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td>ID do Produto</td>");
        HttpContext.Current.Response.Write("<td>Complemento</td>");
        HttpContext.Current.Response.Write("<td>Nome</td>");
        HttpContext.Current.Response.Write("<td>Quantidade</td>");
        HttpContext.Current.Response.Write("<td>Valor</td>");
        HttpContext.Current.Response.Write("<td>Total</td>");
        HttpContext.Current.Response.Write("</tr>");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();
                HttpContext.Current.Response.Write("<tr>");
                HttpContext.Current.Response.Write("<td>" + produto.produtoIdDaEmpresa + "</td>");
                HttpContext.Current.Response.Write("<td>" + produto.complementoIdDaEmpresa + "</td>");
                HttpContext.Current.Response.Write("<td>" + produto.produtoNome + "</td>");
                HttpContext.Current.Response.Write("<td>" + quantidade + "</td>");
                HttpContext.Current.Response.Write("<td>" + fornecedorItem.custo.ToString("C") + "</td>");
                HttpContext.Current.Response.Write("<td>" + (fornecedorItem.custo * quantidade).ToString("C") + "</td>");
                HttpContext.Current.Response.Write("</tr>");
                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtosIds.Add(produtoId);
            }
        }

        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("</tr>");

        HttpContext.Current.Response.Write("<tr>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td>&nbsp;</td>");
        HttpContext.Current.Response.Write("<td><b>Total do Pedido: " + totalDoPedido.ToString("C") + "</b></td>");
        HttpContext.Current.Response.Write("</tr>");

        HttpContext.Current.Response.Write("</Table>");
        HttpContext.Current.Response.Write("</font>");
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();*/


        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");



        WorksheetRow cabecalho = sheet.Table.Rows.Add();
        cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
        cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
        cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");


        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtosIds.Add(produtoId);
            }
        }

        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");

        using (MemoryStream memoryStream = new MemoryStream())
        {
            var fornecedorDc = new dbCommerceDataContext();
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();
            string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";


            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    /*
    private void enviaExcelEmailFornecedor(int pedidoFornecedorId)
    {
        var excel = new StringBuilder();
        #region cabecalhoTabela
        excel.Append("<font style='font-size:10.0pt; font-family:Calibri;'>");
        excel.Append("<BR><BR><BR>");
        excel.Append("<Table border='1' bgColor='#ffffff' " + "borderColor='#000000' cellSpacing='0' cellPadding='0' " + "style='font-size:10.0pt; font-family:Calibri; background:white;'>");
        #endregion

        var pedidosDc = new dbCommerceDataContext();

        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        excel.Append("<tr>");
        excel.Append("<td><b>");
        excel.Append("Pedido: " + pedidoFornecedorId);
        excel.Append("</b></td>");
        excel.Append("<td><b>");
        excel.Append("Data de Solicitação: " + pedido.data.ToShortDateString());
        excel.Append("</b></td>");
        excel.Append("<td><b>");
        excel.Append("Data limite de entrega: " + pedido.dataLimite.ToShortDateString());
        excel.Append("</b></td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("</tr>");

        excel.Append("<tr>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("</tr>");

        excel.Append("<tr>");
        excel.Append("<td>ID do Produto</td>");
        excel.Append("<td>Complemento</td>");
        excel.Append("<td>Nome</td>");
        excel.Append("<td>Quantidade</td>");
        excel.Append("<td>Valor</td>");
        excel.Append("<td>Total</td>");
        excel.Append("</tr>");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();
                excel.Append("<tr>");
                excel.Append("<td>" + produto.produtoIdDaEmpresa + "</td>");
                excel.Append("<td>" + produto.complementoIdDaEmpresa + "</td>");
                excel.Append("<td>" + produto.produtoNome + "</td>");
                excel.Append("<td>" + quantidade + "</td>");
                excel.Append("<td>" + fornecedorItem.custo.ToString("C") + "</td>");
                excel.Append("<td>" + (fornecedorItem.custo * quantidade).ToString("C") + "</td>");
                excel.Append("</tr>");
                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtosIds.Add(produtoId);
            }
        }

        excel.Append("<tr>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("</tr>");

        excel.Append("<tr>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td>&nbsp;</td>");
        excel.Append("<td><b>Total do Pedido: " + totalDoPedido.ToString("C") + "</b></td>");
        excel.Append("</tr>");

        excel.Append("</Table>");
        excel.Append("</font>");

        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();

        string fornecedorEmail = fornecedor.fornecedorEmail;
        if (!string.IsNullOrEmpty(fornecedorEmail)) fornecedorEmail += ",";
        fornecedorEmail += "compras@graodegente.com.br";

        string assunto = DateTime.Now.ToShortDateString() + " Pedido " + fornecedor.fornecedorNome + " " + pedido.idPedidoFornecedor + " entrega até " + pedido.dataLimite.ToShortDateString();
        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + " Pedido " + fornecedor.fornecedorNome + " " + pedido.idPedidoFornecedor + " entrega até " + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";
        string mensagem = "Segue em anexo o pedido, favor confirmar recebimento e enviar o romaneio no email.";


        rnEmails.EnviaEmailComAnexo("compras@graodegente.com.br", fornecedorEmail, "compras@graodegente.com.br", mensagem, assunto, excel.ToString(), nomeArquivo, "application/ms-excel");
    }
    */


    private void enviaExcelEmailFornecedor(int pedidoFornecedorId)
    {
        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");



        WorksheetRow cabecalho = sheet.Table.Rows.Add();
        cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
        cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
        cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");


        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtosIds.Add(produtoId);
            }
        }

        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");


        var fornecedorDc = new dbCommerceDataContext();
        var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();

        string fornecedorEmail = fornecedor.fornecedorEmail;
        if (!string.IsNullOrEmpty(fornecedorEmail)) fornecedorEmail += ",";
        fornecedorEmail += "compras@graodegente.com.br";

        string assunto = DateTime.Now.ToShortDateString() + " Pedido " + fornecedor.fornecedorNome + " " + pedido.idPedidoFornecedor + " entrega até " + pedido.dataLimite.ToShortDateString();
        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";
        string mensagem = "Segue em anexo o pedido, favor confirmar recebimento e enviar o romaneio no email.";

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress("compras@graodegente.com.br");
        var emailsPara = fornecedorEmail.Split(',');
        foreach (var emailPara in emailsPara)
        {
            mailMessage.To.Add(new MailAddress(emailPara));
        }
        mailMessage.ReplyTo = new MailAddress("compras@graodegente.com.br");
        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient(ConfigurationManager.AppSettings["emailSmtp"]);
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["emailLogin"], ConfigurationManager.AppSettings["emailSenha"]);

        using (MemoryStream memoryStream = new MemoryStream())
        {
            book.Save(memoryStream);
            memoryStream.Seek(0, SeekOrigin.Begin);

            // Create attachment
            ContentType contentType = new ContentType();
            contentType.MediaType = "application/ms-excel";
            contentType.Name = nomeArquivo;
            contentType.CharSet = "UTF-8";
            Attachment attachment = new Attachment(memoryStream, contentType);

            // Add the attachment
            mailMessage.Attachments.Add(attachment);

            // Send Mail via SmtpClient
            smtpClient.Send(mailMessage);
        }
    }



    private class fornecedorCusto
    {
        public decimal custo { get; set; }
        public int produtoId { get; set; }
    }

    protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void timer1_OnTick(object sender, EventArgs e)
    {
        if (Session[hiddenExportarId.Value] != "1")
        {
            Session[hiddenExportarId.Value] = "1";
            int id = Convert.ToInt32(hiddenExportarId.Value);
            exportaExcelPedidoFornecedor(id);
        }
        else
        {
            timer1.Enabled = false;
        }
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        exportaExcelPedidoFornecedor(Convert.ToInt32(e.CommandArgument));
    }
    protected void btnReenviar_OnCommand(object sender, CommandEventArgs e)
    {
        enviaExcelEmailFornecedor(Convert.ToInt32(e.CommandArgument));
        Response.Write("<script>alert('Pedido reenviado ao fornecedor com sucesso.');</script>");
    }

    protected void btnEditar_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("pedidoFornecedorEditar.aspx?idPedidoFornecedor=" + e.CommandArgument);
    }

    protected void btnVer_OnCommand(object sender, CommandEventArgs e)
    {
        Response.Redirect("pedidoFornecedorVisualizar.aspx?idPedidoFornecedor=" + e.CommandArgument);
    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void btnConfirmar_OnCommand(object sender, CommandEventArgs e)
    {
        int idPedidoFornecedor = Convert.ToInt32(e.CommandArgument);
        var data = new dbCommerceDataContext();
        var pedido =
            (from c in data.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();
        pedido.confirmado = true;

        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return;
        }
        var interacaoObj = new tbPedidoFornecedorInteracao();
        interacaoObj.data = DateTime.Now;
        interacaoObj.idPedidoFornecedor = idPedidoFornecedor;
        interacaoObj.interacao = "Pedido confirmado com o Fornecedor";
        interacaoObj.usuario = rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString();
        data.tbPedidoFornecedorInteracaos.InsertOnSubmit(interacaoObj);
        data.SubmitChanges();

        fillGrid(true);
    }

    protected void btnNovo_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("pedidoFornecedorAvulsoNovo.aspx");
    }

}