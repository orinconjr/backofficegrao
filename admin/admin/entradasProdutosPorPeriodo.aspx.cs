﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_entradasProdutosPorPeriodo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");

            ddlFornecedores.SelectedValue = "82";
            ddlFornecedores.Enabled = false;
        }

        if (IsPostBack && Session["data"] != null)
        {
            rebindGridDetalhes();
        }
    }

    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    protected void lbtDetalhesData_Command(object sender, CommandEventArgs e)
    {
        try
        {
            var data = e.CommandArgument;

            linhaDetalhes.Visible = true;
            grd.Visible = false;

            grdDetalhes.DataSource = rnRelatorios.entradaDeProdutoPorData(data.ToString(), ddlFornecedores.SelectedValue);
            grdDetalhes.DataBind();

            Session["data"] = data;
        }
        catch (Exception ex)
        {

            Session["data"] = null;
        }



    }
    private void rebindGridDetalhes()
    {
        var data = Session["data"];
        grdDetalhes.DataSource = rnRelatorios.entradaDeProdutoPorData(data.ToString(), ddlFornecedores.SelectedValue);
        grdDetalhes.DataBind();
    }
    protected void btnVoltar_Click(object sender, EventArgs e)
    {
        linhaDetalhes.Visible = false;
        grd.Visible = true;
        Session["data"] = null;
    }
    protected void grd_CustomSummaryCalculate(object sender, DevExpress.Data.CustomSummaryEventArgs e)
    {
        //var entregues = e.GetValue("QtdEntregues");
        //var previstos = e.GetValue("QtdEntregaPrevista");
        //e.TotalValue = "Qtd. Entrega Faltando " + (Convert.ToInt32(previstos) - Convert.ToInt32(entregues));
    }
}