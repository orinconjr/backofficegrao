﻿<%@ Page Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="produtos.aspx.cs" Inherits="admin_produtos" Theme="Glass" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Data.Linq" Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxMenu" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>

<%--<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>--%>


<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script language="javascript" type="text/javascript">
        function grid_ContextMenu(s, e) {
            if (e.objectType == "header")
                pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), 780);
        }

        function OnCustomizationWindowCloseUp(s, e) {
            btnMoveColumns.SetEnabled(false);
        }
        function OnBtnToggleCustomWindowClick(s, e) {
            if (grd.IsCustomizationWindowVisible())
                grd.HideCustomizationWindow();
            else
                grd.ShowCustomizationWindow();
        }

        function OnEndCallBack(s, e) {
            if (s.cpColumnsInHeader != undefined) {
                UpdateBtnMoveColumnsText(!s.cpColumnsInHeader);
                hiddenField.Set('columnsInWindow', !s.cpColumnsInHeader);
                delete s.cpColumnsInHeader;
            }
        }
    </script>

    <dxe:ASPxPopupMenu ID="pmColumnMenu" runat="server" ClientInstanceName="pmColumnMenu">
        <Items>
            <dxe:MenuItem Name="cmdShowCustomization" Text="Escolher colunas">
            </dxe:MenuItem>
            <%--<dxe:MenuItem Name="cmdShowCustomization" Text="Mostrar todas colunas">
            </dxe:MenuItem>--%>
        </Items>
        <ClientSideEvents ItemClick="function(s, e) { if(e.item.name == 'cmdShowCustomization') grd.ShowCustomizationWindow(); }" />
    </dxe:ASPxPopupMenu>

    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Produtos</asp:Label>
            </td>
        </tr>
        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">

                    <tr>
                        <td class="rotulos">
                            <div style="width: 415px; float: left;">
                                <div style="font-size: 16px; font-weight: bold; width: 395px; text-align: center;">Filtros</div>
                                <div style="width: 415px; height: 400px; overflow: auto; float: left;">
                                    <asp:TreeView ID="treeCategorias" runat="server" Visible="False"
                                        ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                    <asp:TreeView ID="treeCategoriasFiltros" runat="server"
                                        ShowLines="True" OnTreeNodePopulate="TreeView1_TreeNodePopulate" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                </div>
                            </div>

                            <div style="width: 415px; float: left;">
                                <div style="font-size: 16px; font-weight: bold; width: 395px; text-align: center;">Categorias Ativas no Site</div>
                                <div style="width: 415px; height: 400px; overflow: auto; float: left;">

                                    <asp:TreeView ID="treeCategoriasAtivasNoSite" runat="server"
                                        ShowLines="True" ExpandDepth="0"
                                        Font-Names="Tahoma" Font-Size="12px" ForeColor="Black">
                                        <SelectedNodeStyle BackColor="#D7EAEE" />

                                        <Nodes>
                                            <asp:TreeNode Selected="True" Text="Categorias" Value="0"></asp:TreeNode>
                                        </Nodes>
                                    </asp:TreeView>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table cellpadding="0" cellspacing="0" style="width: 834px">
                                <tr>
                                    <td align="left">
                                        <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblAtivos" AutoPostBack="True">
                                            <asp:ListItem Text="Ativos" Value="True" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Inativos" Value="False"></asp:ListItem>
                                            <asp:ListItem Text="Todos" Value="Todos"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td height="38"
                                        style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;"
                                        valign="bottom" width="231">
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="width: 97px">&nbsp;</td>
                                                <td style="width: 33px">
                                                    <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                        Style="margin-left: 5px" OnClick="btPdf_Click" EnableViewState="true" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 31px">
                                                    <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                        Style="margin-left: 5px" OnClick="btXsl_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td style="width: 36px">
                                                    <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                        Style="margin-left: 5px" OnClick="btRtf_Click" ValidationGroup="exportar" />
                                                </td>
                                                <td valign="bottom">
                                                    <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                        Style="margin-left: 5px" OnClick="btCsv_Click" ValidationGroup="exportar" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <dx:ASPxButton ID="buttonToggleCustomWindow" runat="server" ClientInstanceName="btnToggleCustomWindow"
                                            Text="Selecionar colunas" AutoPostBack="false" Width="250px">
                                            <ClientSideEvents Click="OnBtnToggleCustomWindowClick" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Horizontal" Width="834px">
                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                    KeyFieldName="produtoId" Settings-ShowFilterBar="Visible"
                                    Cursor="auto" OnHtmlRowCreated="grd_HtmlRowCreated" OnCustomCallback="grd_CustomCallback"
                                    ClientInstanceName="grd" EnableCallBacks="False" OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                        AllowFocusedRow="True" />
                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                        EmptyDataRow="Nenhum registro encontrado."
                                        GroupPanel="Arraste uma coluna aqui para agrupar." />
                                    <SettingsPager Position="TopAndBottom" PageSize="25"
                                        ShowDisabledButtons="False" AlwaysShowPager="True">
                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                            Text="Página {0} de {1} ({2} registros encontrados)" />
                                    </SettingsPager>
                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                    <TotalSummary>
                                        <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                            ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                            SummaryType="Average" />
                                    </TotalSummary>
                                    <SettingsEditing EditFormColumnCount="4"
                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                        PopupEditFormWidth="700px" Mode="Inline" />
                                    <ClientSideEvents EndCallback="OnEndCallBack" CustomizationWindowCloseUp="OnCustomizationWindowCloseUp" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                    <Columns>
                                        <dxwgv:GridViewDataTextColumn Caption="Processos Fabrica" Name="processosFabrica" VisibleIndex="0" Width="80px" Visible="False">
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="btnChecarItens" CommandArgument='<%# Eval("produtoId") %>' OnCommand="btnAlterarProcessosFabrica_OnCommand">
                                        Processos Fábrica
                                                </asp:LinkButton>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Ver no Site" Name="verNoSite" VisibleIndex="3" Width="72px">
                                            <DataItemTemplate>
                                                <a href='<%# "http://www.graodegente.com.br/" + Eval("produtoUrl") +"/" + Eval("produtoUrl") %>' target="_black" style='display: <%# string.IsNullOrEmpty(Eval("categoria").ToString()) ? "none" : "block" %>'>Ver no Site</a>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="ID Produto" FieldName="produtoId"
                                            ReadOnly="True" VisibleIndex="4" Width="70px">
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Nome" FieldName="produtoNome"
                                            VisibleIndex="5" Width="210px">
                                            <Settings AutoFilterCondition="Contains" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <%--<dxwgv:GridViewDataTextColumn Caption="Fornecedor" FieldName="fornecedorNome"
                                            VisibleIndex="6" Width="160px">
                                        </dxwgv:GridViewDataTextColumn>--%>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Fornecedor" FieldName="produtoFornecedor" VisibleIndex="6" Width="160px">
                                            <PropertiesComboBox DataSourceID="sqlFornecedor" TextField="fornecedorNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                ValueField="fornecedorId" ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="CMV Cartão" Visible="false" FieldName="cmvCartao" VisibleIndex="8" Width="160px" UnboundType="Decimal">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="CMV Boleto" Visible="false" FieldName="cmvBoleto" VisibleIndex="9" Width="160px" UnboundType="Decimal">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="CMV Médio" Visible="false" FieldName="cmvMedio" Width="160px" VisibleIndex="10" UnboundType="Decimal">
                                            <EditFormSettings Visible="False" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <%--<dxwgv:GridViewDataTextColumn Caption="" FieldName="" Visible="False" VisibleIndex="10" Width="160px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtCmvMedio" runat="server" BorderStyle="None"
                                                    CssClass="campos" Text='<%# Bind("produtoCmvMedio") %>' Width="100%"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>--%>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Marca" FieldName="produtoMarca" VisibleIndex="7" Width="160px">
                                            <PropertiesComboBox DataSourceID="sqlMarcas" TextField="marcaNome" IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                ValueField="marcaId" ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="ID da Empresa" Visible="False"
                                            FieldName="produtoIdDaEmpresa" Name="idDaEmpresa" VisibleIndex="8"
                                            Width="100px">
                                            <Settings AutoFilterCondition="Contains" />
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtIdDaEmpresa" runat="server" BorderStyle="None"
                                                    CssClass="campos" Text='<%# Bind("produtoIdDaEmpresa") %>' Width="100%"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Complemento ID" Visible="False"
                                            FieldName="complementoIdDaEmpresa" Name="complementoIdDaEmpresa" VisibleIndex="9"
                                            Width="100px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtcomplementoIdDaEmpresa" runat="server" BorderStyle="None"
                                                    CssClass="campos" Text='<%# Bind("complementoIdDaEmpresa") %>' Width="100%"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>


                                        <dxwgv:GridViewDataTextColumn Caption="Preço de Custo" Visible="False"
                                            FieldName="produtoPrecoDeCusto" VisibleIndex="10" Width="100px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtPrecoDeCusto" runat="server" BorderStyle="None" ReadOnly="True"
                                                    CssClass="campos"
                                                    Text='<%# Eval("produtoPrecoDeCusto", "{0:C}").Replace("R$","").Trim() %>'
                                                    Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvpreco" runat="server"
                                                    ControlToValidate="txtPrecoDeCusto" Display="Dynamic"
                                                    ErrorMessage="Preencha o preço de custo." Font-Size="10px"
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Preço" FieldName="produtoPreco" Visible="False"
                                            VisibleIndex="11" Width="90px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtPreco" runat="server" BorderStyle="None" CssClass="campos"
                                                    Text='<%# Eval("produtoPreco", "{0:C}").Replace("R$","").Trim() %>'
                                                    Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvpreco" runat="server"
                                                    ControlToValidate="txtPreco" Display="Dynamic" ErrorMessage="Preencha o preço."
                                                    Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Preço Promocional" Visible="False"
                                            FieldName="produtoPrecoPromocional" VisibleIndex="12" Width="110px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtPrecoPromocional" runat="server" BorderStyle="None"
                                                    CssClass="campos"
                                                    Text='<%# Eval("produtoPrecoPromocional", "{0:C}").Replace("R$","").Trim() %>'
                                                    Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvPrecoPromocional" runat="server"
                                                    ControlToValidate="txtPrecoPromocional" Display="Dynamic"
                                                    ErrorMessage="Preencha o preço promocional." Font-Size="10px"
                                                    SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Margem" FieldName="margem" Visible="False"
                                            VisibleIndex="13" Width="90px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtMargem" runat="server" BorderStyle="None"
                                                    CssClass="campos" ReadOnly="True" Text='<%# Eval("margem", "{0:C}").Replace("R$","").Trim() %>' Width="82px"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Margem a Vista" FieldName="margem" Visible="False"
                                            VisibleIndex="14" Width="90px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtMargemAvista" runat="server" BorderStyle="None"
                                                    CssClass="campos" ReadOnly="True" Text='<%# Eval("margemavista", "{0:C}").Replace("R$","").Trim() %>' Width="82px"></asp:TextBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Peso" FieldName="produtoPeso" Visible="False"
                                            VisibleIndex="15" Width="80px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtPeso" runat="server" BorderStyle="None" CssClass="campos"
                                                    Text='<%# Bind("produtoPeso") %>' Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvPreco" runat="server"
                                                    ControlToValidate="txtPeso" Display="Dynamic" ErrorMessage="Preencha o peso."
                                                    Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn Caption="Estoque" FieldName="produtoEstoqueAtual" Visible="False"
                                            VisibleIndex="16" Width="90px">
                                            <DataItemTemplate>
                                                <asp:TextBox ID="txtEstoque" runat="server" BorderStyle="None"
                                                    CssClass="campos" Text='<%# Bind("produtoEstoqueAtual") %>' Width="100%"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rqvEstoque" runat="server"
                                                    ControlToValidate="txtEstoque" Display="Dynamic"
                                                    ErrorMessage="Preencha o estoque." Font-Size="10px" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Fora de Linha" FieldName="foraDeLinha" Visible="False"
                                            VisibleIndex="17" Width="80px">
                                            <DataItemTemplate>
                                                <dxe:ASPxCheckBox ID="ckbForaDeLinha" runat="server" Value='<%# Bind("foraDeLinha") %>'>
                                                </dxe:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>


                                        <dxwgv:GridViewDataTextColumn Caption="Criacao Propria (3d)" FieldName="criacaoPropria" Visible="False"
                                            VisibleIndex="17" Width="80px">
                                            <DataItemTemplate>
                                                <dxe:ASPxCheckBox ID="ckbCriacaoPropria" runat="server" Value='<%# Bind("criacaoPropria") %>'>
                                                </dxe:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn VisibleIndex="18" Caption="Fotos" FieldName="fotos" Visible="False"
                                            Width="160px">
                                            <Settings AllowHeaderFilter="False" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn VisibleIndex="19" Caption="Categorias" FieldName="categorias" Visible="False"
                                            Width="160px">
                                            <Settings AllowHeaderFilter="False" />
                                        </dxwgv:GridViewDataTextColumn>
                                        <dxwgv:GridViewDataTextColumn VisibleIndex="19" Caption="Categoria" FieldName="categoria" Visible="False"
                                            Width="160px">
                                            <Settings AllowHeaderFilter="False" />
                                        </dxwgv:GridViewDataTextColumn>


                                        <dxwgv:GridViewDataTextColumn VisibleIndex="19" Caption="Coleções" FieldName="colecao" Visible="false"
                                            Width="160px">
                                            <Settings AllowHeaderFilter="False" />
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataComboBoxColumn Caption="Site" FieldName="siteId" VisibleIndex="20" Width="130px" Visible="False">
                                            <PropertiesComboBox DataSourceID="sqlSite" TextField="nome" ValueField="idSite" ValueType="System.String">
                                            </PropertiesComboBox>
                                        </dxwgv:GridViewDataComboBoxColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="produtoAtivo" Visible="False"
                                            UnboundType="Boolean" VisibleIndex="21" Width="60px">
                                            <DataItemTemplate>
                                                <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                    Value='<%# Bind("produtoAtivo") %>' ValueChecked="True"
                                                    ValueType="System.String" ValueUnchecked="False">
                                                </dxe:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataTextColumn Caption="Excluir" Name="excluir" VisibleIndex="22" Width="42px" Visible="False">
                                            <DataItemTemplate>
                                                <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                                </dxe:ASPxCheckBox>
                                            </DataItemTemplate>
                                        </dxwgv:GridViewDataTextColumn>

                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Novo" VisibleIndex="0"
                                            Width="37px">
                                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btNovo.jpg"
                                                NavigateUrlFormatString="produtoCad.aspx">
                                            </PropertiesHyperLinkEdit>
                                            <DataItemTemplate>
                                                <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btNovo.jpg"
                                                    NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=0" + "&acao=incluir" %>'>Novo</asp:HyperLink>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                            Width="45px">
                                            <DataItemTemplate>
                                                <asp:HyperLink ID="hplNovo" runat="server" ImageUrl="images/btEditar-v2.jpg"
                                                    NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=" + Eval("produtoPaiId") + "&acao=alterar" %>'>Alterar</asp:HyperLink>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dxwgv:GridViewDataHyperLinkColumn>
                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Incluir" VisibleIndex="2"
                                            Width="70px">
                                            <DataItemTemplate>
                                                <asp:HyperLink ID="hplDuplicar" runat="server" ImageUrl="images/btIncluirIcon.jpg"
                                                    NavigateUrl='<%# "produtoAlt.aspx?produtoId=" + Eval("produtoId") + "&produtoPaiId=" + Eval("produtoPaiId") + "&acao=duplicar" %>'>Incluir no grupo</asp:HyperLink>
                                                <asp:ImageButton runat="server" ImageUrl="images/btDuplicar.jpg" CommandArgument='<%#Eval("produtoId")%>' OnCommand="ibtDuplicarAll_OnCommand" OnClientClick="return confirm('Este processo irá copiar todos dos dados do produto e gerar um novo (cópia exata), confirma?')" ToolTip="Duplicar exatamente todos os dados do produto" />
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center">
                                            </CellStyle>
                                        </dxwgv:GridViewDataHyperLinkColumn>

                                    </Columns>
                                    <ClientSideEvents ContextMenu="grid_ContextMenu" />
                                    <SettingsCustomizationWindow Enabled="True" />
                                </dxwgv:ASPxGridView>
                            </asp:Panel>
                            <%--<asp:ObjectDataSource ID="sqlProdutos" runat="server"
                                SelectMethod="produtoAdminSeleciona" TypeName="rnProdutos"
                                DeleteMethod="produtoExclui">
                                <DeleteParameters>
                                    <asp:Parameter Name="produtoId" Type="String" />
                                </DeleteParameters>
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="treeCategorias" Name="categoriaId"
                                        PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>--%>


                            <asp:ObjectDataSource ID="sqlProdutosPorCategoriaFiltros" runat="server"
                                SelectMethod="produtosAdminLista" TypeName="rnProdutos">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="treeCategoriasFiltros" Name="categoriaId"
                                        PropertyName="SelectedValue" Type="String" />
                                    <asp:ControlParameter ControlID="rblAtivos" Name="ativo"
                                        PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlProdutosPorCategoriaAtivasNoSite" runat="server"
                                SelectMethod="produtosAdminLista" TypeName="rnProdutos">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="treeCategoriasAtivasNoSite" Name="categoriaId"
                                        PropertyName="SelectedValue" Type="String" />
                                    <asp:ControlParameter ControlID="rblAtivos" Name="ativo"
                                        PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlFornecedor" runat="server" SelectMethod="GetFornecedores"
                                TypeName="rnProdutos"></asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="sqlMarcas" runat="server" SelectMethod="GetMarcas"
                                TypeName="rnProdutos"></asp:ObjectDataSource>

                            <asp:LinqDataSource ID="sqlSite" runat="server"
                                ContextTypeName="dbCommerceDataContext" EnableDelete="True"
                                EnableInsert="True" EnableUpdate="True" TableName="tbSites">
                            </asp:LinqDataSource>

                            <dx:LinqServerModeDataSource ID="sqlProdutosServer" runat="server" ContextTypeName="dbCommerceDataContext"
                                OnSelecting="sqlProdutosServer_OnSelecting"></dx:LinqServerModeDataSource>

                            <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                            </dxwgv:ASPxGridViewExporter>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <img src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);" style="display: none;" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>



    <dx:ASPxPopupControl ID="popChecarItens" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
        ClientInstanceName="popChecarItens" HeaderText="Processos da Fabrica" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="75" AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl10" runat="server">
                <div style="width: 800px; height: 500px; overflow: scroll;" id="itensDoRomaneio">
                    <asp:HiddenField runat="server" ID="hdfIdProduto" />
                    <asp:CheckBoxList ID="chkProcessosFabrica" runat="server" DataSourceID="sqlProcessosFabrica" DataTextField="processo" DataValueField="idProcessoFabrica" OnDataBound="chkProcessosFabrica_OnDataBound" Font-Size="12px"></asp:CheckBoxList>
                    <asp:SqlDataSource ID="sqlProcessosFabrica" runat="server" ConnectionString="<%$ ConnectionStrings:connectionString %>" SelectCommand="SELECT idProcessoFabrica, processo FROM tbProcessoFabrica"></asp:SqlDataSource>
                </div>
                <div style="width: 800px; height: 35px; text-align: right;">
                    <table style="width: 100%">
                        <tr>
                            <td style="text-align: right;">
                                <asp:Button runat="server" ID="btnGravarProcessoFabrica" Text="Gravar Processo" OnClick="btnGravarProcessoFabrica_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>


    <dx:ASPxPopupControl ID="popEditarProdutoDuplicado" runat="server" CloseAction="CloseButton" Modal="True" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides"
        ClientInstanceName="popEditarProdutoDuplicado" HeaderText="Completar dados do produto duplicado" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Height="660"
        AutoUpdatePosition="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl6" runat="server">
                <asp:HiddenField runat="server" ID="hdfIdProdutoNovo" />
                <div style="width: 850px; height: 660px;">
                    <table width="100%">
                        <tr class="rotulos">
                            <td></td>
                        </tr>
                        <tr class="rotulos">
                            <td>Nome do Produto
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td>
                                <asp:TextBox runat="server" ID="txtNomeProdutoRecemDuplicado" Width="788px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos"></td>
                        </tr>
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 834px">
                                    <tr class="rotulos">
                                        <td style="width: 213px">Preço de Custo<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoDeCusto"
                                            ErrorMessage="Preencha o pre&#231;o de custo." Display="None" SetFocusOnError="True"
                                            ID="rqvPrecoDeCusto"></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPrecoDeCusto">0</asp:TextBox>
                                        </td>
                                        <td style="width: 213px">Preço<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPreco"
                                            ErrorMessage="Preencha o pre&#231;o." Display="None" SetFocusOnError="True" ID="rqvPreco"></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPreco"></asp:TextBox>
                                        </td>
                                        <td style="width: 213px">Preço Promocional<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoPromocional"
                                            ErrorMessage="Preencha o pre&#231;o promocional." Display="None" SetFocusOnError="True"
                                            ID="rqvPrecoPromocional"></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:TextBox runat="server" CssClass="campos" Width="190px" ID="txtProdutoPrecoPromocional">0</asp:TextBox>
                                        </td>
                                        <td>Preço para Atacado<asp:RequiredFieldValidator runat="server" ControlToValidate="txtProdutoPrecoAtacado"
                                            ErrorMessage="Preencha o pre&#231;o de atacado." Display="None" SetFocusOnError="True"
                                            ID="rqvPrecoAtacado"></asp:RequiredFieldValidator>
                                            <br />
                                            <asp:TextBox runat="server" CssClass="campos" Width="191px" ID="txtProdutoPrecoAtacado">0</asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td style="width: 213px">Margem:<br />
                                            <asp:TextBox ID="txtMargem" runat="server" CssClass="campos" Width="190px">0</asp:TextBox>
                                        </td>
                                        <td style="width: 213px">Preço Venda:
                                                    <br />
                                            <asp:Label ID="lblPrecoVenda" runat="server" Text=""></asp:Label>
                                        </td>
                                        <td style="width: 213px">Preço Promocional:
                                                    <br />
                                            <asp:Label ID="lblPrecoPromocional" runat="server" Text=""></asp:Label><br />
                                        </td>
                                        <td style="width: 213px"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos"></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="pnlFotos" runat="server">
                                    <table cellpadding="0" cellspacing="0" class="rotulos"
                                        style="width: 100%">
                                        <tr>
                                            <td>
                                                <table cellpadding="0" cellspacing="0" style="width: 100%">
                                                    <tr>
                                                        <td class="rotulos" style="width: 291px">Foto<asp:RegularExpressionValidator ID="rgeFlu1" runat="server"
                                                            ControlToValidate="upl1" Display="None"
                                                            ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                            ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                            <br />
                                                            <asp:FileUpload ID="upl1" runat="server" CssClass="campos" Width="262px" />
                                                        </td>
                                                        <td class="rotulos" style="width: 292px">Foto<asp:RegularExpressionValidator ID="rgeFlu2" runat="server"
                                                            ControlToValidate="upl2" Display="None"
                                                            ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                            ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                            <br />
                                                            <asp:FileUpload ID="upl2" runat="server" CssClass="campos" Width="262px" />
                                                        </td>
                                                        <td class="rotulos">Foto<asp:RegularExpressionValidator ID="rgeFlu3" runat="server"
                                                            ControlToValidate="upl3" Display="None"
                                                            ErrorMessage="A imagem precisa ser jpg." SetFocusOnError="True"
                                                            ValidationExpression="(.*\.jpe?g|.*\.JPE?G)"></asp:RegularExpressionValidator>
                                                            <br />
                                                            <asp:FileUpload ID="upl3" runat="server" CssClass="campos" Width="245px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="rotulos" style="width: 291px">Descrição da foto<br />
                                                            <asp:TextBox ID="txtFotoDescricao1" runat="server" CssClass="campos"
                                                                Width="262px"></asp:TextBox>
                                                        </td>
                                                        <td class="rotulos" style="width: 292px">Descrição da foto<br />
                                                            <asp:TextBox ID="txtFotoDescricao2" runat="server" CssClass="campos"
                                                                Width="262px"></asp:TextBox>
                                                        </td>
                                                        <td class="rotulos">Descrição da foto<br />
                                                            <asp:TextBox ID="txtFotoDescricao3" runat="server" CssClass="campos"
                                                                Width="245px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr class="rotulos">
                            <td class="rotulos"></td>
                        </tr>
                        <tr>
                            <td class="rotulos">Produtos Relacionados: 0
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="width: 100%">
                                    <tr class="rotulos">
                                        <td style="width: 160px;">
                                            <b>Produtos Relacionados:</b>
                                            <asp:Literal runat="server" ID="litProdutosRelacionados"></asp:Literal>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                            <asp:CheckBox runat="server" ID="chkProdutoRelacionadoValidar" Text="Validar Duplicado" Checked="True" />
                                        </td>
                                        <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                        </td>
                                        <td style="padding-right: 5px; width: 100px;">&nbsp;<br />
                                        </td>
                                        <td>&nbsp;<br />
                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td style="padding-right: 5px; width: 100px;">ID da Empresa:<br>
                                            <asp:TextBox runat="server" ID="txtProdutoRelacionadoId" CssClass="campos" Width="480" TextMode="MultiLine" Height="65"></asp:TextBox>
                                        </td>
                                        <td style="padding-right: 5px; width: 100px;">Quantidade:<br>
                                            <asp:TextBox runat="server" ID="txtProdutoRelacionadoQuantidade" CssClass="campos" Width="60"></asp:TextBox>
                                        </td>
                                        <td style="padding-right: 5px; width: 50px;">Desconto:<br>
                                            <asp:TextBox runat="server" ID="txtProdutoRelacionadoDesconto" CssClass="campos" Text="0" Width="60">
                                            </asp:TextBox>
                                        </td>
                                        <td>&nbsp;<br />
                                            <asp:Button runat="server" ID="btnProdutoRelacionadoAdicionar" OnClick="btnProdutoRelacionadoAdicionar_OnClick" Text="Adicionar Produto" ValidationGroup="produtoRelacionado" />
                                        </td>
                                    </tr>
                                    <asp:ListView runat="server" ID="lstProdutoRelacionadoComplemento" OnItemDataBound="lstProdutoRelacionadoComplemento_OnItemDataBound" Visible="False">
                                        <ItemTemplate>
                                            <tr class="rotulos">
                                                <td>
                                                    <asp:Literal runat="server" ID="litProdutoRelacionadoId" Text='<%# Container.DataItem %>'></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:DropDownList runat="server" ID="ddlProdutoRelacionadoComplemento" />
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <tr class="rotulos">
                                        <td colspan="4" style="padding-top: 30px;">
                                            <div style="overflow: hidden; margin-bottom: 10px; font-weight: bold; text-align: center;">
                                                <div style="float: left; width: 80px;">
                                                    Foto
                                                </div>
                                                <div style="float: left; width: 100px; margin-right: 10px">
                                                    ID da Empresa
                                                </div>
                                                <div style="float: left; width: 100px; margin-right: 10px">
                                                    Complemento
                                                </div>
                                                <div style="float: left; width: 305px; margin-right: 10px;">
                                                    Nome
                                                </div>
                                                <div style="float: left; width: 100px; margin-right: 10px;">
                                                    Desconto
                                                </div>
                                                <div style="float: left; width: 100px;">
                                                    Remover
                                                </div>
                                            </div>
                                            <div style="overflow: scroll; width: 845px; height: 170px;">
                                                <asp:ListView runat="server" ID="lstProdutosRelacionados">
                                                    <ItemTemplate>
                                                        <div style="overflow: hidden; margin-bottom: 10px; text-align: center;">
                                                            <div style="float: left; width: 80px;">
                                                                <asp:HiddenField runat="server" ID="hiddenProdutoId" Value='<%#Eval("produtoId") %>' />
                                                                <asp:Image ID="fotoDestaque" runat="server" Height="70px" Width="70px" ImageUrl='<%# System.Configuration.ConfigurationManager.AppSettings["caminhoVirtual"] + "/fotos/" + Eval("produtoId") + "/pequena_" + Eval("fotoDestaque") + ".jpg" %>' />
                                                            </div>
                                                            <div style="float: left; width: 100px; margin-right: 10px;">
                                                                <asp:Label ID="lblIdDaEmpresa" runat="server" Text='<%# Bind("produtoIdDaEmpresa") %>'></asp:Label>
                                                            </div>
                                                            <div style="float: left; width: 100px; margin-right: 10px;">
                                                                <asp:Label ID="lblComplementoId" runat="server" Text='<%# Bind("complementoIdDaEmpresa") %>'></asp:Label>&nbsp;
                                                            </div>
                                                            <div style="float: left; width: 305px; margin-right: 10px;">
                                                                <asp:Label ID="lblProdutoNome" runat="server" Text='<%# Bind("produtoNome") %>'></asp:Label>
                                                            </div>
                                                            <div style="float: left; width: 100px; margin-right: 10px;">
                                                                <asp:Label ID="lblDesconto" runat="server" Text='<%# Bind("desconto") %>'></asp:Label>
                                                            </div>
                                                            <div style="float: left; width: 100px;">
                                                                <asp:Button runat="server" OnClientClick="return confirm('Deseja realmente remover este produto relacionado?')" ID="btnRemoverProdutoRelacionado" OnCommand="btnRemoverProdutoRelacionado_OnCommand" CommandArgument='<%# Bind("idProdutoRelacionado") %>' Text="Remover" />
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                            </div>

                                        </td>
                                    </tr>
                                    <tr class="rotulos">
                                        <td>
                                            <b>Custo do Combo:</b>
                                            <asp:Literal runat="server" ID="litCustoCombo"></asp:Literal>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="rotulos"></td>
                        </tr>
                        <tr>
                            <td class="rotulos" colspan="2" style="text-align: right">
                                <asp:Button runat="server" ID="btnGravar" Text="Gravar" OnClick="btnGravar_OnClick" />
                            </td>
                        </tr>
                    </table>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

</asp:Content>

