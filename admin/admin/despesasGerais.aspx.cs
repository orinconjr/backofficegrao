﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.DynamicData;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class admin_despesasGerais : System.Web.UI.Page
{
    //private string[] unidadeBasica = { "", "Grama", "Centímetro", "Unidade" };
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int id = 0;
            bool ok = Int32.TryParse(Request.QueryString["idDespesasGerais"], out id);

            if (id != 0)
            {
                fieldseteditarunidade.Visible = true;
                fieldsetnovaunidade.Visible = false;
                carregarDadosEditar(id);
            }

            carregaGrid();
        }


    }
    private void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        var despesasGerais = (from dg in data.tbDespesasGerais
                              select dg).ToList();

        if (!String.IsNullOrEmpty(txtiddespesasgerais.Text))
        {
            int idDespesasGerais = Convert.ToInt32(txtiddespesasgerais.Text);
            despesasGerais =
                (from dg in despesasGerais where dg.idDespesasGerais == idDespesasGerais select dg).ToList();

        }
        if (!String.IsNullOrEmpty(txtnomedespesasgerais.Text))
        {
            string nomeDespesasGerais = txtnomedespesasgerais.Text;
            despesasGerais =
                (from dg in despesasGerais where dg.nomeDespesasGerais.ToLower().Contains(nomeDespesasGerais.ToLower()) select dg).ToList();

        }
     
        if (!String.IsNullOrEmpty(txtporcentagemdespesasgerais.Text))
        {
            decimal porcentagemDespesasGerais = 0;
            bool ok = Decimal.TryParse(txtporcentagemdespesasgerais.Text, out porcentagemDespesasGerais);
            if (porcentagemDespesasGerais != 0)
            {
                despesasGerais =
                             (from dg in despesasGerais where dg.porcentagemDespesasGerais == porcentagemDespesasGerais select dg).ToList();
            }

        }
        var totalPorcentagem = despesasGerais.Sum(x => x.porcentagemDespesasGerais);
        lblTotalPorcentagem.Text = totalPorcentagem.ToString();
        GridView1.DataSource = despesasGerais;
        GridView1.DataBind();


    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        carregaGrid();
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        //var data = new dbCommerceDataContext();
        //var unidadeMedidas = (from um in data.tbComprasUnidadesDeMedidas
        //                      select um).ToList();

        //if (!String.IsNullOrEmpty(txtidunidade.Text))
        //{
        //    int unidadeId = Convert.ToInt32(txtidunidade.Text);
        //    unidadeMedidas =
        //        (from um in unidadeMedidas where um.idComprasUnidadesDeMedida == unidadeId select um).ToList();

        //}
        //if (!String.IsNullOrEmpty(txtunidademedida.Text))
        //{
        //    string unidademedida = txtunidademedida.Text;
        //    unidadeMedidas =
        //        (from um in unidadeMedidas where um.unidadeDeMedida.ToLower().Contains(unidademedida.ToLower()) select um).ToList();

        //}
        //if (ddlUnidadeBasica.SelectedItem.Value != "0")
        //{
        //    int idunidadebasica = Convert.ToInt32(ddlUnidadeBasica.SelectedItem.Value);
        //    unidadeMedidas = (from um in unidadeMedidas where um.idUnidadeBasica == idunidadebasica select um).ToList();

        //}

        //SLDocument sl = new SLDocument();
        //SLStyle style1 = sl.CreateStyle();
        //style1.Font.Bold = true;
        //sl.SetRowStyle(1, style1);

        //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //Response.AddHeader("Content-Disposition", "attachment; filename=unidadesDeMedida.xlsx");



        //sl.SetCellValue("A1", "Id");
        //sl.SetCellValue("B1", "Unidade de Medida");
        //sl.SetCellValue("C1", "Unidade Básica");
        //sl.SetCellValue("D1", "Fator de Conversão");

        //int linha = 2;
        //foreach (var item in unidadeMedidas)
        //{
        //    sl.SetCellValue(linha, 1, item.idComprasUnidadesDeMedida);
        //    sl.SetCellValue(linha, 2, item.unidadeDeMedida);
        //    sl.SetCellValue(linha, 3, item.idUnidadeBasica.ToString());
        //    sl.SetCellValue(linha, 4, item.fatorConversao.ToString());


        //    linha++;
        //}
        //sl.SaveAs(Response.OutputStream);
        //Response.End();

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //Label lblunidadebasica = (Label)e.Row.FindControl("lblunidadebasica");
            //int unidadebasicaindex = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "idUnidadeBasica") ?? 0);

            //lblunidadebasica.Text = unidadeBasica[unidadebasicaindex];


            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "despesasGerais.aspx?idDespesasGerais=" + DataBinder.Eval(e.Row.DataItem, "idDespesasGerais");

            LinkButton MyButton = (LinkButton)e.Row.FindControl("cmdDelete");
            MyButton.Attributes.Add("onclick", "javascript:return " +
            "confirm('Confirma a exclusão do item ?')");

        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        using (var data = new dbCommerceDataContext())
        {
            int idDespesasGerais = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Value);
            var despesasGerais = (from dg in data.tbDespesasGerais
                           where dg.idDespesasGerais == idDespesasGerais
                           select dg).FirstOrDefault();
            data.tbDespesasGerais.DeleteOnSubmit(despesasGerais);
            string meuscript = "";
            try
            {
                data.SubmitChanges();
                carregaGrid();
            }
            catch (Exception ex)
            {

                meuscript = @"alert('" + ex.Message + "');";
            }
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }


    }

    protected void carregarDadosEditar(int idDespesasGerais)
    {
        using (var data = new dbCommerceDataContext())
        {
            var despesasGerais = (from dg in data.tbDespesasGerais
                                  where dg.idDespesasGerais == idDespesasGerais
                                  select dg).FirstOrDefault();
            lblId.Text = despesasGerais.idDespesasGerais.ToString();
            //lblId.Text = unidadeMedida.idComprasUnidadesDeMedida.ToString();
            txtnomedespesasgeraiseditar.Text = despesasGerais.nomeDespesasGerais;
            txtporcentagemdespesasgeraiseditar.Text = despesasGerais.porcentagemDespesasGerais.ToString();
          
        }
    }
    protected void btnSalvar_Click(object sender, EventArgs e)
    {

        string erro = validaCampos(txtnomedespesasgeraisinserir.Text, 
           txtporcentagemdespesasgeraisinserir.Text);

        if (erro == "")
        {
            using (var data = new dbCommerceDataContext())
            {
                var despesasGerais = new tbDespesasGerai();
                despesasGerais.nomeDespesasGerais = txtnomedespesasgeraisinserir.Text;
                despesasGerais.porcentagemDespesasGerais = Convert.ToDecimal(txtporcentagemdespesasgeraisinserir.Text);
                data.tbDespesasGerais.InsertOnSubmit(despesasGerais);
                try
                {
                    data.SubmitChanges();
                }
                catch (Exception ex)
                {
                    string mensagem = "";
                    if (ex.Message.IndexOf("UQ_DespesasGerais") > 0)
                        mensagem = "Despesa geral já existe";
                    else
                    {
                        mensagem = ex.Message;
                    }
                    string meuscript = @"alert('" + mensagem + "');";
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
                }
               
            }
            fieldseteditarunidade.Visible = false;
            fieldsetnovaunidade.Visible = false;
            carregaGrid();
        }
        else
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }


    }

    string validaCampos(string nomeDespesasGerais,string porcentagemDespesasGerais)
    {
        string erro = "";
        if (String.IsNullOrEmpty(nomeDespesasGerais))
            erro = "Informe o nome da Despesa Geral. ";

        if (String.IsNullOrEmpty(porcentagemDespesasGerais))
            erro += "Informe a porcentagem. ";
        else
        {
            decimal porcDespesasGerais = 0;
            try
            {
                porcDespesasGerais = Convert.ToDecimal(porcentagemDespesasGerais);
            }
            catch (Exception ex)
            {

                erro += "Porcentagem precisa ser numérica. ";
            }
        }
        return erro;
    }
    protected void btnNovaDespesasGerais_Click(object sender, EventArgs e)
    {
        fieldseteditarunidade.Visible = false;
        fieldsetnovaunidade.Visible = true;
    }
    protected void btnAtualizar_Click(object sender, EventArgs e)
    {
        string erro = validaCampos(txtnomedespesasgeraiseditar.Text, txtporcentagemdespesasgeraiseditar.Text);
        //string erro = validaCampos(txtunidadedemedidaeditar.Text, ddlunidadebasicaeditar.SelectedValue,
        //  txtfatorconversaoeditar.Text);
        if (erro == "")
        {
            using (var data = new dbCommerceDataContext())
            {
                int idDespesasGerais = Convert.ToInt32(Request.QueryString["idDespesasGerais"]);
                var despesasGerais = (from dg in data.tbDespesasGerais
                                      where dg.idDespesasGerais == idDespesasGerais
                                      select dg).FirstOrDefault();
                despesasGerais.nomeDespesasGerais = txtnomedespesasgeraiseditar.Text;
                despesasGerais.porcentagemDespesasGerais = Convert.ToDecimal(txtporcentagemdespesasgeraiseditar.Text);
               data.SubmitChanges();


                int idUnidadeMedida = Convert.ToInt32(Request.QueryString["idUnidade"]);
                var unidadeMedida = (from um in data.tbComprasUnidadesDeMedidas
                                     where um.idComprasUnidadesDeMedida == idUnidadeMedida
                                     select um).FirstOrDefault();

                fieldseteditarunidade.Visible = false;
                fieldsetnovaunidade.Visible = false;
                carregaGrid();


            }
        }
        else
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);
        }

    }
}