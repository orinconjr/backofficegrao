﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;

public partial class admin_filaEstoqueFornecedor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var fornecedores = (from c in data.tbProdutoFornecedors orderby c.fornecedorNome select c).ToList();
            ddlFornecedor.DataSource = fornecedores;
            ddlFornecedor.DataBind();
        }
    }

    private void fillGrid(bool rebind, int produtoId)
    {

    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }
        var grd = (ASPxGridView) sender;
        bool completo = true;
        int pedidoId = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "pedidoId" }).ToString(), out pedidoId);


        var dataLimiteReserva = grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimiteReserva" });
        var dataLimiteFornecedor = grd.GetRowValues(e.VisibleIndex, new string[] { "dataLimiteFornecedor" });
        if (dataLimiteFornecedor != null && dataLimiteReserva != null)
        {
            DateTime dataLimiteR = DateTime.Now;
            DateTime.TryParse(dataLimiteReserva.ToString(), out dataLimiteR);
            DateTime dataLimiteF = DateTime.Now;
            DateTime.TryParse(dataLimiteFornecedor.ToString(), out dataLimiteF);
            if (dataLimiteR.Date < dataLimiteF.Date)
            {
                int i = 0;
                while ((i < e.Row.Cells.Count))
                {
                    e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                    i++;
                }
            }
        }

    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            if (pedidoId > 0) e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }


    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        int fornecedorId = 0;
        int.TryParse(ddlFornecedor.SelectedValue, out fornecedorId);
        var data = new dbCommerceDataContext();
        var produtos = (from c in data.tbItemPedidoEstoques
            where
                c.reservado == false && c.cancelado == false && c.enviado == false && c.dataLimite != null &&
                c.tbProduto.produtoFornecedor == fornecedorId
            select c.tbProduto).Distinct();
        lstProdutos.DataSource = produtos;
        lstProdutos.DataBind();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var valores = (tbProduto) e.Item.DataItem;
        int produtoId = valores.produtoId;
        ASPxGridView grd = (ASPxGridView)e.Item.FindControl("grd");


        var data = new dbCommerceDataContext();
        var aguardando = (from c in data.tbItemPedidoEstoques
                          where c.produtoId == produtoId && c.reservado == false && c.cancelado == false && c.dataLimite != null
                          orderby c.dataLimite
                          select new
                          {
                              c.idItemPedidoEstoque,
                              c.tbItensPedido.pedidoId,
                              dataLimiteReserva = c.dataLimite
                          }).ToList();
        var pedidos = (from c in data.tbPedidoFornecedorItems
                       where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) && c.idProduto == produtoId
                       orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
                       select new
                       {
                           c.idPedidoFornecedor,
                           c.idPedidoFornecedorItem,
                           dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite
                       }).ToList();
        if (aguardando.Count >= pedidos.Count)
        {
            var listaFinal = (from c in aguardando.Select((item, index) => new { item, index })
                              join d in pedidos.Select((item, index) => new { item, index }) on c.index equals d.index into
                                  pedidosFornecedor
                              from f in pedidosFornecedor.DefaultIfEmpty()
                              select new
                              {
                                  c.item.idItemPedidoEstoque,
                                  c.item.pedidoId,
                                  c.item.dataLimiteReserva,
                                  idPedidoFornecedor = f != null ? f.item.idPedidoFornecedor : 0,
                                  dataLimiteFornecedor = f != null ? (DateTime?)f.item.dataLimiteFornecedor : null,
                                  idPedidoFornecedorItem = f != null ? (int?)f.item.idPedidoFornecedorItem : null
                              }
                ).ToList();

            grd.DataSource = listaFinal;
            grd.DataBind();
        }
        else
        {
            var listaFinal = (from c in pedidos.Select((item, index) => new { item, index })
                              join d in aguardando.Select((item, index) => new { item, index }) on c.index equals d.index into aguardandos
                              from f in aguardandos.DefaultIfEmpty()
                              select new
                              {
                                  idItemPedidoEstoque = f != null ? f.item.idItemPedidoEstoque : 0,
                                  pedidoId = f != null ? f.item.pedidoId : 0,
                                  dataLimiteReserva = f != null ? f.item.dataLimiteReserva : null,
                                  c.item.idPedidoFornecedor,
                                  c.item.dataLimiteFornecedor,
                                  c.item.idPedidoFornecedorItem
                              }
                ).ToList();

            grd.DataSource = listaFinal;
            grd.DataBind();
        }
    }
}