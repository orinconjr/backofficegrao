﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxCallback;

public partial class admin_atualizarTabelaJadlog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        string caminho = MapPath("~/");
        fluArquivo.SaveAs(caminho + "tabelaJadlog.xls");



        Response.Write("<script>alert('Planilha Salva com Sucesso.');</script>");
    }

    protected void btnAtualizar_OnCallback(object source, CallbackEventArgs e)
    {
        string caminho = MapPath("~/");
        string connStr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + caminho + "relevancia.xls;Extended Properties=Excel 12.0;";

        OleDbConnection cnnExcel = new OleDbConnection(connStr);
        OleDbCommand cmmPlan = null;
        OleDbDataReader drPlan = null;

        cnnExcel.Open();
        cmmPlan = new OleDbCommand("select * from [relatorio$]", cnnExcel);
        drPlan = cmmPlan.ExecuteReader(CommandBehavior.CloseConnection);
        var data = new dbCommerceDataContext();

        int id = 13;

        int contagem = 0;
        while (drPlan.Read())
        {
            try
            {
                string cep = drPlan[2].ToString();
                int prazo = Convert.ToInt32(drPlan[4].ToString().Trim());
                string interiorizacao = drPlan[5].ToString();
                string faixaInicial = "";
                string faixaFinal = "";

                var cepSplit = cep.Split('a');
                if (cepSplit.Count() > 1)
                {
                    faixaInicial = cepSplit[0].Trim();
                    faixaFinal = cepSplit[1].Trim();
                }
                else
                {
                    faixaInicial = cepSplit[0].Trim();
                    faixaFinal = cepSplit[0].Trim();
                }

                var faixaDeCep = (from c in data.tbFaixaDeCeps where c.tipodeEntregaId == id && c.faixaInicial == faixaInicial && c.faixaFinal == faixaFinal select c).FirstOrDefault();

                if(faixaDeCep != null)
                {
                    if((prazo + 1) >= faixaDeCep.prazo)
                    {
                        faixaDeCep.prazo = prazo + 1;
                        faixaDeCep.porcentagemDeDesconto = 0;
                        faixaDeCep.porcentagemDoSeguro = 0;
                        faixaDeCep.dataDaCriacao = DateTime.Now;
                        if (interiorizacao.ToLower().Trim() == "interior")
                        {
                            faixaDeCep.interiorizacao = true;
                        }
                        else
                        {
                            faixaDeCep.interiorizacao = false;
                        }
                        data.SubmitChanges();
                    }
                }
            }
            catch (Exception)
            {

            }
        }


        cnnExcel.Close();
        cnnExcel.Dispose();

    }
}