﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxTabControl;
using DevExpress.Web.ASPxEditors;
using DevExpress.Data;
using DevExpress.Web.ASPxUploadControl;
using System.IO;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Reflection;
using Amazon.S3;
using Amazon.S3.Model;
using DevExpress.Web.ASPxPivotGrid;

public partial class admin_relatorioBiCustoPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
        bool datas = true;
        if (txtDataInicial.Text == "" | txtDataFinal.Text == "")
        {
            datas = false;
        }
        if (datas) fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        DateTime.TryParse(txtDataInicial.Text, out dataInicial);
        DateTime.TryParse(txtDataFinal.Text, out dataFinal);        
        dataFinal = dataFinal.AddDays(1);
        var data = new dbCommerceDataContext();
        using (var txn = new System.Transactions.TransactionScope( System.Transactions.TransactionScopeOption.Required, new System.Transactions.TransactionOptions
        {
            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
        }))
        {
            var listaStatusValidos = new List<int>() { 5 };

            var itensEnviados = (from c in data.tbItemPedidoEstoques
                                   join d in data.tbProdutoEstoques on c.idItemPedidoEstoque equals d.idItemPedidoEstoqueReserva
                                   where c.tbItensPedido.tbPedido.dataHoraDoPedido >= dataInicial && 
                                   c.tbItensPedido.tbPedido.dataHoraDoPedido <= dataFinal && c.reservado == true && 
                                   listaStatusValidos.Contains(c.tbItensPedido.tbPedido.statusDoPedido) && (c.tbItensPedido.brinde ?? false) == false
                                   select new {
                                       c.tbProduto.produtoPeso,
                                       c.idItemPedidoEstoque,
                                       c.tbItensPedido.tbPedido.dataHoraDoPedido,
                                       pedidoId = c.tbItensPedido.pedidoId,
                                       c.itemPedidoId,
                                       precoVenda = (c.tbProduto.produtoPrecoPromocional > 0 ? c.tbProduto.produtoPrecoPromocional : c.tbProduto.produtoPreco),
                                       custo = d.tbPedidoFornecedorItem.custo,
                                       c.tbProduto.produtoNome,
                                       c.produtoId,
                                       categoria =
                                         c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true) ==
                                         null
                                             ? "Sem Categoria"
                                             : c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                                 x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true)
                                                 .tbProdutoCategoria.categoriaNomeExibicao                                     
                                   }).ToList();
            var itensPedido = (from c in data.tbItensPedidos
                                   where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido) && (c.brinde ?? false) == false
                               select new {
                                       pedidoId = c.pedidoId,
                                   c.itemPedidoId,
                                   precoVenda = (c.itemValor * c.itemQuantidade),
                                       c.tbProduto.produtoNome,
                                       c.produtoId,
                                       categoria =
                                         c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true) ==
                                         null
                                             ? "Sem Categoria"
                                             : c.tbProduto.tbJuncaoProdutoCategorias.FirstOrDefault(
                                                 x => x.tbProdutoCategoria.categoriaPaiId == 0 && x.tbProdutoCategoria.exibirSite == true)
                                                 .tbProdutoCategoria.categoriaNomeExibicao
                                   }).ToList();           


            var totaisPagos = (from c in data.tbPedidoPagamentos
                               where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && c.pago == true && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                               select new { pedidoId = c.pedidoId, valor = c.valor }).ToList();

            var envios = (from c in data.tbPedidoEnvios
                          where c.tbPedido.dataHoraDoPedido >= dataInicial && c.tbPedido.dataHoraDoPedido <= dataFinal && listaStatusValidos.Contains(c.tbPedido.statusDoPedido)
                          select new { pedidoId = c.tbPedido.pedidoId, custo = c.valorSelecionado }).ToList();


            var pedidos = (from c in data.tbPedidos
                           where c.dataHoraDoPedido >= dataInicial && c.dataHoraDoPedido <= dataFinal && listaStatusValidos.Contains(c.statusDoPedido)
                           select new
                           {
                               c.pedidoId,
                               c.dataHoraDoPedido,
                               c.valorCobrado,
                               c.valorDoFrete,
                               c.valorDosItens
                           }).ToList();
            var relatorioMontagem = (from c in itensEnviados
                                     join d in itensPedido on c.itemPedidoId equals d.itemPedidoId into itemPedido
                                     join d in itensEnviados on c.pedidoId equals d.pedidoId into todosItensPedido
                                     join e in pedidos on c.pedidoId equals e.pedidoId into pedido
                                     join f in envios on c.pedidoId equals f.pedidoId into valoresEnvio
                                     join h in itensEnviados on c.itemPedidoId equals h.itemPedidoId into itemEnviado
                                     select new
                                     {
                                         c.pedidoId,
                                         c.idItemPedidoEstoque,
                                         c.itemPedidoId,
                                         categoriaItem = c.categoria,
                                         precoItem = c.precoVenda,
                                         categoriaProduto = itemPedido.First().categoria,
                                         valorItemPedido = itemPedido.Distinct().Select(x => x.precoVenda).DefaultIfEmpty(1).Sum(),
                                         valorTotalCombo = itemEnviado.Distinct().Select(x => x.precoVenda).DefaultIfEmpty(1).Sum(),
                                         pesoDoItem = c.produtoPeso < 1 ? 1 : c.produtoPeso,
                                         pesoTotalPedido = todosItensPedido.Distinct().Select(x => x.produtoPeso).DefaultIfEmpty(1).Sum(),
                                         c.custo,
                                         c.dataHoraDoPedido,
                                         c.produtoNome,
                                         totalPagoPedido = pedido.Distinct().Select(x => x.valorCobrado).DefaultIfEmpty(1).Sum(),
                                         totalItensPedido = pedido.Distinct().Select(x => x.valorDosItens).DefaultIfEmpty(1).Sum() < pedido.Distinct().Select(x => x.valorCobrado).DefaultIfEmpty(1).Sum() ? pedido.Distinct().Select(x => x.valorCobrado).DefaultIfEmpty(1).Sum() : pedido.Distinct().Select(x => x.valorDosItens).DefaultIfEmpty(1).Sum(),
                                         valorDoFreteCliente = pedido.Distinct().Select(x => x.valorDoFrete).DefaultIfEmpty(1).Sum(),
                                         valorDoFretePago = valoresEnvio.Select(x => x.custo).DefaultIfEmpty(1).Sum()
                                     }).ToList();
            var relatorioMontagem2 = (from c in relatorioMontagem
                                      select new
                                      {
                                          c.pedidoId,
                                          c.idItemPedidoEstoque,
                                          c.itemPedidoId,
                                          c.categoriaItem,
                                          c.categoriaProduto,
                                          percentualCombo = ((c.valorItemPedido * 100) / (c.valorTotalCombo == 0 ? 1 : (c.valorTotalCombo < 1 ? 1 : c.valorTotalCombo))),
                                          c.precoItem,
                                          precoEfetivoItem = (c.precoItem / 100) * ((c.valorItemPedido * 100) / (c.valorTotalCombo < 1 ? 1 : c.valorTotalCombo)),
                                          c.valorItemPedido,
                                          c.valorTotalCombo,
                                          c.pesoTotalPedido,
                                          c.custo,
                                          c.produtoNome,
                                          c.totalPagoPedido,
                                          c.totalItensPedido,
                                          valorDoFreteCliente = c.valorDoFreteCliente > 0 ? ((c.valorDoFreteCliente / 100) * (((decimal)c.pesoDoItem * 100) / (decimal)(c.pesoTotalPedido < 1 ? 1 : c.pesoTotalPedido))) : 0,
                                          valorDoFretePago = c.valorDoFretePago > 0 ? ((c.valorDoFretePago / 100) * (((decimal)c.pesoDoItem * 100) / (decimal)(c.pesoTotalPedido < 1 ? 1 : c.pesoTotalPedido))) : 0
                                      });
            var relatorioMontagem3 = (from c in relatorioMontagem2
                                      select new
                                      {
                                          c.pedidoId,
                                          c.idItemPedidoEstoque,
                                          c.itemPedidoId,
                                          c.categoriaItem,
                                          c.categoriaProduto,
                                          c.precoItem,
                                          precoEfetivoItem = (c.precoEfetivoItem ?? 0),
                                          c.valorItemPedido,
                                          c.valorTotalCombo,
                                          c.pesoTotalPedido,
                                          c.produtoNome,
                                          c.custo,
                                          precoEfetivoItemComDesconto = (c.precoEfetivoItem / 100) * ((c.totalPagoPedido * 100) / c.totalItensPedido),
                                          valorDesconto = c.precoEfetivoItem - ((c.precoEfetivoItem / 100) * ((c.totalPagoPedido * 100) / c.totalItensPedido)),
                                          c.totalPagoPedido,
                                          c.totalItensPedido,
                                          c.valorDoFreteCliente,
                                          c.valorDoFretePago
                                          /*pCustoFrete = Decimal.Round((decimal)((c.valorEnvio * 100) / c.valorCobrado), 2),
                                          pCustoFretePago = Decimal.Round((decimal)(((c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                          pCustoTotal = Decimal.Round((decimal)(((c.valorCusto + c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2)*/
                                      });


            /*var relatorio = (from c in relatorioMontagem
                             select new
                             {
                                 c.pedidoId,
                                 c.dataHoraDoPedido,
                                 c.valorCobrado,
                                 c.valorDoFrete,
                                 c.valorCusto,
                                 c.valorEnvio,
                                 pCustoProduto = Decimal.Round((decimal)((c.valorCusto * 100) / c.valorCobrado), 2),
                                 pCustoFrete = Decimal.Round((decimal)((c.valorEnvio * 100) / c.valorCobrado), 2),
                                 pCustoFretePago = Decimal.Round((decimal)(((c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2),
                                 pCustoTotal = Decimal.Round((decimal)(((c.valorCusto + c.valorEnvio - c.valorDoFrete) * 100) / c.valorCobrado), 2)
                             });

*/
            grd.DataSource = relatorioMontagem3;
            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                grd.DataBind();
            }
        }
    }
    protected void btnPequisar_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void chkHoraAtual_OnCheckedChanged(object sender, EventArgs e)
    {
        fillGrid(true);
    }



    protected void grd_CustomUnboundFieldData(object sender, CustomFieldDataEventArgs e)
    {
        //if (e.Field.FieldName == "variacaoQuantidade")
        //{
        //    string nome = e.GetListSourceColumnValue("produtoNome").ToString();
        //    if(nome.ToLower().Contains("orthoflex"))
        //    {
        //        var teste = "";
        //    }
        //    var source1 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo1");
        //    var source2 = e.GetListSourceColumnValue(e.ListSourceRowIndex, "itemQuantidadePeriodo2");
        //    decimal quantidade1 = Convert.ToDecimal(source1);
        //    decimal quantidade2 = Convert.ToDecimal(source2);
        //    if (quantidade1 == 0) { e.Value = 100; }
        //    else
        //    {
        //        e.Value = Math.Round((((quantidade2 * 100) / quantidade1) - 100), 2);
        //    }
        //}
    }
}