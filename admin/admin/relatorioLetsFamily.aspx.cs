﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpreadsheetLight;
using System.IO;
using System.Xml;
public partial class admin_relatorioLetsFamily : System.Web.UI.Page
{
    public class Item
    {
        public int pedidoId { get; set; }
        public string IdCupom { get; set; }
        public decimal? valorTotalGeral {get;set;}
        public string situacao{get;set;}
        public string idCupom{get;set;}
        public string campanhaNome { get; set; }
        public int? campanhaId { get; set; }
        public string clienteNome{get;set;}
        public string clienteEmail {get;set;}
        public DateTime? pedidoData { get; set; }
      
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (validarcampos())
            {
                carregaGrid();
            }
          
        }
    }
    private void carregaGrid()
    {
        List<Item> itens = getDados();
        GridView1.DataSource = itens;
        GridView1.DataBind();
        lblitensencontrados.Text = itens.Count().ToString();
        lblvalortotal.Text = Convert.ToDecimal(itens.Sum(x => x.valorTotalGeral)).ToString("C");
     }

    public List<Item> getDados()
    {
        var data = new dbCommerceDataContext();
         List<Item> itens = new List<Item>();
         List<int> pagos = new List<int>() { 3, 4, 5, 11 };
        if(rbPagos.Checked)
        {
            itens = (from cd in data.CuponsDeDescontos
                     join pe in data.tbPedidos on cd.ID equals pe.cupomDeDesconto
                     join cli in data.tbClientes on pe.clienteId equals cli.clienteId
                     join cc in data.CampanhaCupoms on cd.IDCampanha equals cc.ID
                     where cd.Usado == true && cd.IDCampanha > 1 && cd.IDCampanha <=6
                     && pagos.Contains(pe.statusDoPedido)
                     select new Item()
                     {
                         IdCupom = cd.ID,
                         pedidoId = pe.pedidoId,
                         valorTotalGeral = pe.valorCobrado,
                         situacao = pe.tbPedidoSituacao.situacao,
                         idCupom = cd.ID,
                         campanhaNome = cc.Nome,
                         campanhaId = cd.IDCampanha,
                         clienteNome = cli.clienteNome,
                         clienteEmail = cli.clienteEmail,
                         pedidoData = pe.dataHoraDoPedido
                     }).ToList<Item>();
        }
        if (rbNaoPagos.Checked)
        {
            itens = (from cd in data.CuponsDeDescontos
                     join pe in data.tbPedidos on cd.ID equals pe.cupomDeDesconto
                     join cli in data.tbClientes on pe.clienteId equals cli.clienteId
                     join cc in data.CampanhaCupoms on cd.IDCampanha equals cc.ID
                     where cd.Usado == true && cd.IDCampanha > 1 && cd.IDCampanha <= 6
                     && !pagos.Contains(pe.statusDoPedido)
                     select new Item()
                     {
                         IdCupom = cd.ID,
                         pedidoId = pe.pedidoId,
                         valorTotalGeral = pe.valorCobrado,
                         situacao = pe.tbPedidoSituacao.situacao,
                         idCupom = cd.ID,
                         campanhaNome = cc.Nome,
                         campanhaId = cd.IDCampanha,
                         clienteNome = cli.clienteNome,
                         clienteEmail = cli.clienteEmail,
                         pedidoData = pe.dataHoraDoPedido
                     }).ToList<Item>();
        }
        if(rbTodos.Checked)
        {
            
            itens = (from cd in data.CuponsDeDescontos
                     join pe in data.tbPedidos on cd.ID equals pe.cupomDeDesconto
                     join cli in data.tbClientes on pe.clienteId equals cli.clienteId
                     join cc in data.CampanhaCupoms on cd.IDCampanha equals cc.ID
                     where cd.Usado == true && cd.IDCampanha > 1 && cd.IDCampanha <= 6
                     select new Item()
                     {
                         IdCupom = cd.ID,
                         pedidoId = pe.pedidoId,
                         valorTotalGeral = pe.valorCobrado,
                         situacao = pe.tbPedidoSituacao.situacao,
                         idCupom = cd.ID,
                         campanhaNome = cc.Nome,
                         campanhaId = cd.IDCampanha,
                         clienteNome = cli.clienteNome,
                         clienteEmail = cli.clienteEmail,
                         pedidoData = pe.dataHoraDoPedido
                     }).ToList<Item>();
        }


        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            int pedidoId = Convert.ToInt32(txtNumPedido.Text);
            itens = (from i in itens where i.pedidoId == pedidoId select i).ToList();
        }

        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
        {
            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            DateTime dtFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");
            itens =
                itens.Where(
                    x => x.pedidoData >= dtInicial && x.pedidoData <= dtFinal).ToList();
        }
        if (ddlCampanha.SelectedItem.Value != "0")
        {
            int campanhaId = Convert.ToInt32(ddlCampanha.SelectedItem.Value);
            itens = (from i in itens where i.campanhaId == campanhaId select i).ToList();
        }
        if (ddlordenarpor.SelectedItem.Value == "pedidoData")
            itens = itens.OrderByDescending(x => x.pedidoData).ToList();
        if (ddlordenarpor.SelectedItem.Value == "pedidoId")
            itens = itens.OrderByDescending(x => x.pedidoId).ToList();
        if (ddlordenarpor.SelectedItem.Value == "campanhaNome")
            itens = itens.OrderBy(x => x.campanhaNome).ToList();
        return itens;

    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;
        if (!String.IsNullOrEmpty(txtNumPedido.Text))
        {
            try { int teste = Convert.ToInt32(txtNumPedido.Text); }
            catch (Exception)
            {
                erro += @"Numero do pedido invalido\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString();
            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "pedido.aspx?pedidoId=" + pedidoId;
        }
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            List<Item> itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=Relatorio_Lets_Family.xlsx");

            sl.SetCellValue("A1", "Id Pedido");
            sl.SetCellValue("B1", "Data");
            sl.SetCellValue("C1", "Status");
            sl.SetCellValue("D1", "Vl do Pedido");
            sl.SetCellValue("E1", "Cupom");
            sl.SetCellValue("F1", "Campanha");
            sl.SetCellValue("G1", "Cliente");
            sl.SetCellValue("H1", "E-mail");


            int linha = 2;
            foreach (var item in itens)
            {
                sl.SetCellValue(linha, 1, item.pedidoId.ToString());
                sl.SetCellValue(linha, 2, Convert.ToDateTime(item.pedidoData).ToShortDateString());
                sl.SetCellValue(linha, 3, item.situacao);
                sl.SetCellValue(linha, 4, Convert.ToDecimal(item.valorTotalGeral).ToString("C"));
                sl.SetCellValue(linha, 5, item.idCupom);
                sl.SetCellValue(linha, 6, item.campanhaNome);
                sl.SetCellValue(linha, 7, item.clienteNome);
                sl.SetCellValue(linha, 8, item.clienteEmail);
                linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }

  

}