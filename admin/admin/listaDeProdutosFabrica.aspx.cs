﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using serviceNfe;

public partial class admin_listaDeProdutosFabrica : System.Web.UI.Page
{

    public class meusprodutos
    {
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string fotoDestaque { get; set; }
        public string produtoAtivo { get; set; }
        public decimal? somaMateriaPrima { get; set; }
        public decimal? somaProcessos { get; set; }
        public decimal? somaProcessosExternos { get; set; }
        public decimal? indiceMargemDeLucro { get; set; }
        public decimal? indiceTributos { get; set; }
        public decimal? produtoPrecoDeCusto { get; set; }
        //public decimal subTotal1 { get; set; }
        //public decimal subTotal2 { get; set; }
        public decimal? subTotalComGGF { get; set; }
        //public decimal subTotalComMargemLucro { get; set; }
        public decimal? subTotalComTributos { get; set; }
        public decimal? soTributos { get; set; }
        public decimal? gastosGeraisFabricacao { get; set; }
        public decimal? margemLucro { get; set; }
        public decimal? lucroPorcentagem { get; set; }
        public decimal? lucroValor { get; set; }
        public IEnumerable<materiaPrima> materiasPrimas { get; set; }
        public IEnumerable<processo> processos { get; set; }
        public IEnumerable<processoExterno> processosExternos { get; set; }

    }


    public class materiaPrima
    {
        //public int produtoId { get; set; }
        //public int idComprasProduto { get; set; }
        //public int idProdutoMateriaPrima { get; set; }
        public string produtoNome { get; set; }
        //public decimal consumo { get; set; }
        //public decimal valorUnitario { get; set; }
        public decimal? valorTotal { get; set; }
        //public decimal totalGeral { get; set; }

    }

    public class processo
    {
        public decimal? valorTotal { get; set; }
    }
    public class processoExterno
    {
        public decimal? valorTotal { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            var data = new dbCommerceDataContext();
            var categorias =
                (from c in data.tbProdutoCategorias where c.categoriaPaiId == 0 && c.exibirSite == true orderby c.categoriaNome select c).ToList
                    ();
            ddlCategoria.DataSource = categorias;
            ddlCategoria.DataBind();

        }


    }

    private void filtrar()
    {
        // Stopwatch stopwatch = Stopwatch.StartNew();
        var data = new dbCommerceDataContext();
        var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias
                                 where c.tbProduto.produtoFornecedor == 82 && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).ToList();
        //=========================================
        IEnumerable<tbProduto> produtos1 = (from c in data.tbProdutos
                                            where c.produtoFornecedor == 82
                                            select c).ToList<tbProduto>();


        if (ddlCategoria.SelectedValue != "")
        {
            int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
            produtosCategoria = produtosCategoria.Where(x => x.categoriaId == categoriaId).ToList();
            produtos1 = (from c in produtos1 where produtosCategoria.Select(x => x.produtoId).Contains(c.produtoId) select c);
        }

        if (!string.IsNullOrEmpty(txtNome.Text))
        {

            produtos1 = produtos1.Where(x => x.produtoNome.ToLower().Contains(txtNome.Text.ToLower())).ToList();

        }
        int a = produtos1.Count();
        //=========================================

        IEnumerable<meusprodutos> produtos = (from c in produtos1
                                              where c.produtoFornecedor == 82
                                              select new meusprodutos
                                              {
                                                  produtoId = c.produtoId,
                                                  produtoNome = c.produtoNome,
                                                  fotoDestaque = c.fotoDestaque,
                                                  produtoAtivo = c.produtoAtivo,
                                                  indiceMargemDeLucro = c.indiceMargemDeLucro,
                                                  indiceTributos = c.indiceTributos,
                                                  produtoPrecoDeCusto = c.produtoPrecoDeCusto,
                                                  materiasPrimas = (from p in data.tbProdutoMateriaPrimas
                                                                    join tcp in data.tbComprasProdutos
                                                                        on p.idComprasProduto equals tcp.idComprasProduto
                                                                    join tcpo in data.tbComprasOrdemProdutos
                                                                        on p.idComprasProduto equals tcpo.idComprasProduto
                                                                        into _tcpo
                                                                    join tcpf in data.tbComprasProdutoFornecedors
                                                                       on _tcpo.FirstOrDefault().idComprasProduto equals tcpf.idComprasProduto
                                                                       into _tcpf
                                                                    join tcum in data.tbComprasUnidadesDeMedidas
                                                                         on _tcpf.FirstOrDefault().idComprasUnidadeMedida equals tcum.idComprasUnidadesDeMedida
                                                                         into _tcum
                                                                    where p.produtoId == c.produtoId
                                                                    select new materiaPrima()
                                                                    {
                                                                        produtoNome = tcp.produto,
                                                                        // valorTotal = p.consumo * _tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco,
                                                                        valorTotal = p.consumo * (_tcpo.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco / (_tcum.FirstOrDefault().fatorConversao ?? 1))
                                                                    }).ToList<materiaPrima>(),
                                                  processos = (from spf in data.tbSubProcessoFabricas
                                                               join pf in data.tbProcessoFabricas on
                                                                   spf.idProcessoFabrica equals pf.idProcessoFabrica
                                                               join jppf in data.tbJuncaoProdutoProcessoFabricas on
                                                                   new
                                                                   {
                                                                       idProcessoFabrica = spf.idProcessoFabrica,
                                                                       idSubProcessoFabrica = spf.idSubProcessoFabrica
                                                                   } equals
                                                                   new
                                                                   {
                                                                       idProcessoFabrica = jppf.idProcessoFabrica,
                                                                       idSubProcessoFabrica = jppf.idSubProcessoFabrica
                                                                   }
                                                               where jppf.idProduto == c.produtoId
                                                               select new processo
                                                               {
                                                                   valorTotal = jppf.minutos * spf.valorMinuto
                                                               }).ToList<processo>(),
                                                  processosExternos = (from pef in data.tbProcessoExternoFabricas

                                                                       join jppef in data.tbJuncaoProdutoProcessoExternoFabricas on
                                                                                pef.idProcessoExternoFabrica equals jppef.idProcessoExternoFabrica

                                                                       where jppef.idProduto == c.produtoId
                                                                       select new processoExterno
                                                                       {
                                                                           valorTotal = jppef.minutos * pef.valorMinuto

                                                                       }).ToList<processoExterno>()
                                              }).OrderBy(x => x.produtoNome).ToList();


        // var listaFinal = produtos.OrderBy(x => x.produtoNome).ToList();
        if (chkAtivo.Checked)
        {
            produtos = (from p in produtos where p.produtoAtivo.ToLower() == "true" select p);
        }
        if (ckbsemmateriaprima.Checked)
        {
            produtos = (from p in produtos where p.materiasPrimas.Count() == 0 select p);
        }

        ///// Porcentagem total despesas gerais a ser aplicada sobre a materia prima
        //var totalPorcentagemDespesasGerais = (from dg in data.tbDespesasGerais
        //                                      select dg.porcentagemDespesasGerais).Sum();
        decimal? totalPorcentagemDespesasGerais = 0;
        var totalPorcentagemDespesasGerais1 = (from dg in data.tbDespesasGerais
                                               select dg.porcentagemDespesasGerais).ToList();
        if (totalPorcentagemDespesasGerais1.Count > 0)
        {
            totalPorcentagemDespesasGerais = totalPorcentagemDespesasGerais1.Sum();
        }

        foreach (var item2 in produtos)
        {
            item2.somaMateriaPrima = item2.materiasPrimas.Sum(x => x.valorTotal);
            item2.somaProcessos = item2.processos.Sum(x => x.valorTotal);
            item2.somaProcessosExternos = item2.processosExternos.Sum(x => x.valorTotal);

            // Soma de Materia prima com processo internos e internos e Despesas Gerais:
            decimal? subtotalDespesasGerais = (item2.somaMateriaPrima / 100) * totalPorcentagemDespesasGerais;
            decimal? subTotal1 = item2.somaMateriaPrima + item2.somaProcessos + item2.somaProcessosExternos;
            //item2.gastosGeraisFabricacao = (subTotal1 / 100) * rnConfiguracoes.GGF;
            item2.gastosGeraisFabricacao = subtotalDespesasGerais;
            //Adiciona o GGF no Subtotal
            item2.subTotalComGGF = item2.gastosGeraisFabricacao + subTotal1;

            item2.margemLucro = (item2.subTotalComGGF / 100) * item2.indiceMargemDeLucro;
            //Adiciona a margem de lucro
            decimal? subTotalComMargemLucro = item2.margemLucro + item2.subTotalComGGF;

            //Tributos
            decimal? fatordivisao = (100 - item2.indiceTributos) / 100;
            item2.subTotalComTributos = subTotalComMargemLucro / fatordivisao;
            item2.soTributos = item2.subTotalComTributos - subTotalComMargemLucro;

            //Cálculo do lucro
            item2.lucroPorcentagem = 0;
            try // try/catch para prevenir possível divisão por zero
            {
                item2.lucroPorcentagem =
                    Convert.ToDecimal(((item2.produtoPrecoDeCusto - item2.subTotalComGGF) / item2.subTotalComGGF) * 100);
                item2.lucroValor = Convert.ToDecimal(item2.produtoPrecoDeCusto - item2.subTotalComGGF);
            }
            catch (Exception)
            {
            }


        }




        lstProdutos.DataSource = produtos;
        lstProdutos.DataBind();

        litTotal.Text = produtos.Count().ToString();
        //stopwatch.Stop();
        //Response.Write("<span style='color:#fff;'>tempo:" + stopwatch.ElapsedMilliseconds + "</span>");


    }

    private void carregaGrid()
    {
        // Stopwatch stopwatch = Stopwatch.StartNew();
        var data = new dbCommerceDataContext();
        var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias
                                 where c.tbProduto.produtoFornecedor == 82 && c.tbProdutoCategoria.categoriaPaiId == 0
                                 select c).ToList();
        //=========================================
        var produtos1 = (from c in data.tbProdutos
                         where c.produtoFornecedor == 82
                         select new
                         {
                             c.produtoId,
                             c.produtoNome,
                             c.produtoFornecedor,
                             c.fotoDestaque,
                             c.produtoAtivo,
                             c.indiceMargemDeLucro,
                             c.indiceTributos,
                             c.produtoPrecoDeCusto,
                             c.tbProdutoMateriaPrimas
                         }).ToList();


        if (ddlCategoria.SelectedValue != "")
        {
            int categoriaId = Convert.ToInt32(ddlCategoria.SelectedValue);
            produtosCategoria = produtosCategoria.Where(x => x.categoriaId == categoriaId).ToList();
            produtos1 = (from c in produtos1 where produtosCategoria.Select(x => x.produtoId).Contains(c.produtoId) select c).ToList();
        }

        if (!string.IsNullOrEmpty(txtNome.Text))
        {

            produtos1 = produtos1.Where(x => x.produtoNome.ToLower().Contains(txtNome.Text.ToLower())).ToList();

        }
        int a = produtos1.Count();
        //=========================================

        var produtos = (from c in produtos1
                        where c.produtoFornecedor == 82
                        select new
                        {
                            c.produtoId,
                            c.produtoNome,
                            c.fotoDestaque,
                            c.produtoAtivo,
                            c.indiceMargemDeLucro,
                            c.indiceTributos,
                            c.produtoPrecoDeCusto,
                            c.tbProdutoMateriaPrimas
                            //processos = (from spf in data.tbSubProcessoFabricas
                            //             join pf in data.tbProcessoFabricas on
                            //                 spf.idProcessoFabrica equals pf.idProcessoFabrica
                            //             join jppf in data.tbJuncaoProdutoProcessoFabricas on
                            //                 new
                            //                 {
                            //                     idProcessoFabrica = spf.idProcessoFabrica,
                            //                     idSubProcessoFabrica = spf.idSubProcessoFabrica
                            //                 } equals
                            //                 new
                            //                 {
                            //                     idProcessoFabrica = jppf.idProcessoFabrica,
                            //                     idSubProcessoFabrica = jppf.idSubProcessoFabrica
                            //                 }
                            //             where jppf.idProduto == c.produtoId
                            //             select new processo
                            //             {
                            //                 valorTotal = jppf.minutos * spf.valorMinuto
                            //             }).ToList<processo>(),
                            //processosExternos = (from pef in data.tbProcessoExternoFabricas

                            //                     join jppef in data.tbJuncaoProdutoProcessoExternoFabricas on
                            //                              pef.idProcessoExternoFabrica equals jppef.idProcessoExternoFabrica

                            //                     where jppef.idProduto == c.produtoId
                            //                     select new processoExterno
                            //                     {
                            //                         valorTotal = jppef.minutos * pef.valorMinuto

                            //                     }).ToList<processoExterno>()
                        }).OrderBy(x => x.produtoNome).ToList();


        // var listaFinal = produtos.OrderBy(x => x.produtoNome).ToList();
        if (chkAtivo.Checked)
        {
            produtos = (from p in produtos where p.produtoAtivo.ToLower() == "true" select p).ToList();
        }
        if (ckbsemmateriaprima.Checked)
        {
            produtos = (from p in produtos where !p.tbProdutoMateriaPrimas.Any() select p).ToList();
        }

        // Porcentagem total despesas gerais a ser aplicada sobre a materia prima
        //decimal? totalPorcentagemDespesasGerais = 0;
        //var totalPorcentagemDespesasGerais1 = (from dg in data.tbDespesasGerais
        //                                       select dg.porcentagemDespesasGerais).ToList();
        //if (totalPorcentagemDespesasGerais1.Count > 0)
        //{
        //    totalPorcentagemDespesasGerais = totalPorcentagemDespesasGerais1.Sum();
        //}

        //foreach (var item2 in produtos)
        //{
        //    item2.somaMateriaPrima = item2.materiasPrimas.Sum(x => x.valorTotal);
        //    item2.somaProcessos = item2.processos.Sum(x => x.valorTotal);
        //    item2.somaProcessosExternos = item2.processosExternos.Sum(x => x.valorTotal);

        //    // Soma de Materia prima com processo internos e internos e Despesas Gerais:
        //    decimal? subtotalDespesasGerais = (item2.somaMateriaPrima / 100) * totalPorcentagemDespesasGerais;
        //    decimal? subTotal1 = item2.somaMateriaPrima + item2.somaProcessos + item2.somaProcessosExternos;
        //    //item2.gastosGeraisFabricacao = (subTotal1 / 100) * rnConfiguracoes.GGF;
        //    item2.gastosGeraisFabricacao = subtotalDespesasGerais;
        //    //Adiciona o GGF no Subtotal
        //    item2.subTotalComGGF = item2.gastosGeraisFabricacao + subTotal1;

        //    item2.margemLucro = (item2.subTotalComGGF / 100) * item2.indiceMargemDeLucro;
        //    //Adiciona a margem de lucro
        //    decimal? subTotalComMargemLucro = item2.margemLucro + item2.subTotalComGGF;

        //    //Tributos
        //    decimal? fatordivisao = (100 - item2.indiceTributos) / 100;
        //    item2.subTotalComTributos = subTotalComMargemLucro / fatordivisao;
        //    item2.soTributos = item2.subTotalComTributos - subTotalComMargemLucro;

        //    //Cálculo do lucro
        //    item2.lucroPorcentagem = 0;
        //    try // try/catch para prevenir possível divisão por zero
        //    {
        //        item2.lucroPorcentagem =
        //            Convert.ToDecimal(((item2.produtoPrecoDeCusto - item2.subTotalComGGF) / item2.subTotalComGGF) * 100);
        //        item2.lucroValor = Convert.ToDecimal(item2.produtoPrecoDeCusto - item2.subTotalComGGF);
        //    }
        //    catch (Exception)
        //    {
        //    }


        //}

        GridView1.DataSource = produtos;
        GridView1.DataBind();

        litTotal.Text = produtos.Count().ToString();
    }

    protected void btnFiltrar_OnClick(object sender, EventArgs e)
    {
        //filtrar();
        carregaGrid();
    }

    protected void lstProdutos_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        meusprodutos _meusprodutos = (meusprodutos)dataItem.DataItem;

        var hdfProdutoId = (HiddenField)e.Item.FindControl("hdfProdutoId");

        var hdfFoto = (HiddenField)e.Item.FindControl("hdfFoto");
        var imgFoto = (Image)e.Item.FindControl("imgFoto");

        imgFoto.ImageUrl = "http://dmhxz00kguanp.cloudfront.net/fotos/" + hdfProdutoId.Value + "/" + hdfFoto.Value + ".jpg";
        var rptmateriaprima = (Repeater)e.Item.FindControl("rptmateriaprima");

        rptmateriaprima.DataSource = _meusprodutos.materiasPrimas;
        rptmateriaprima.DataBind();



    }

    //protected void lstProdutos_OnDataBound(object sender, EventArgs e)
    //{
    //    var data = new dbCommerceDataContext();

    //    var estoque = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 7 orderby c.nome select c);
    //    var materia = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 8 orderby c.nome select c);
    //    var corte = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 1 orderby c.nome select c);
    //    var bordado = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 2 orderby c.nome select c);
    //    var costura = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 3 orderby c.nome select c);
    //    var cola = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 4 orderby c.nome select c);
    //    var arremate = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 9 orderby c.nome select c);
    //    var enchimento = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 10 orderby c.nome select c);
    //    var embalagem = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 5 orderby c.nome select c);
    //    var entrega = (from c in data.tbSubProcessoFabricas where c.idProcessoFabrica == 6 orderby c.nome select c);



    //    foreach (var item in lstProdutos.Items)
    //    {
    //        var hdfProdutoId = (HiddenField)item.FindControl("hdfProdutoId");

    //        int produtoId = Convert.ToInt32(hdfProdutoId.Value);
    //        var subProcessos =
    //            (from c in data.tbJuncaoProdutoProcessoFabricas where c.idProduto == produtoId select c).ToList();

    //    }
    //}


    protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }

    protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int produtoId = Convert.ToInt32(((HiddenField)e.Row.FindControl("hdfProdutoId")).Value);

            var hdfFoto = (HiddenField)e.Row.FindControl("hdfFoto");
            var imgFoto = (Image)e.Row.FindControl("imgFoto");
            var lblDescMateriaPrima = (Label)e.Row.FindControl("lblDescMateriaPrima");
            decimal? indiceMargemDeLucro = Convert.ToDecimal(((HiddenField)e.Row.FindControl("hdfIndiceMargemDeLucro")).Value);
            decimal? indiceTributos = Convert.ToDecimal(((HiddenField)e.Row.FindControl("hdfIndiceTributos")).Value);
            decimal? produtoPrecoDeCusto = Convert.ToDecimal(((HiddenField)e.Row.FindControl("hdfProdutoPrecoDeCusto")).Value);

            imgFoto.ImageUrl = "http://dmhxz00kguanp.cloudfront.net/fotos/" + produtoId + "/" + hdfFoto.Value + ".jpg";
            var rptmateriaprima = (Repeater)e.Row.FindControl("rptmateriaprima");

            using (var data = new dbCommerceDataContext())
            {


                var materiasPrimas = (from p in data.tbProdutoMateriaPrimas
                                      where p.produtoId == produtoId
                                      select new materiaPrima
                                      {
                                          produtoNome = p.tbComprasProduto.produto,
                                          valorTotal =
                                              p.consumo *
                                              (p.tbComprasProduto.tbComprasOrdemProdutos.OrderByDescending(x => x.dataCadastro).FirstOrDefault().preco /
                                               (p.tbComprasProduto.tbComprasProdutoFornecedors.Select(x => x.tbComprasUnidadesDeMedida).FirstOrDefault().fatorConversao ?? 1))
                                      }).ToList();

                var processos = (from spf in data.tbSubProcessoFabricas
                                 join pf in data.tbProcessoFabricas on
                                     spf.idProcessoFabrica equals pf.idProcessoFabrica
                                 join jppf in data.tbJuncaoProdutoProcessoFabricas on
                                     new
                                     {
                                         spf.idProcessoFabrica,
                                         spf.idSubProcessoFabrica
                                     } equals
                                     new
                                     {
                                         jppf.idProcessoFabrica,
                                         jppf.idSubProcessoFabrica
                                     }
                                 where jppf.idProduto == produtoId
                                 select new processo
                                 {
                                     valorTotal = jppf.minutos * spf.valorMinuto
                                 }).ToList();

                var processosExternos = (from pef in data.tbProcessoExternoFabricas
                                         join jppef in data.tbJuncaoProdutoProcessoExternoFabricas on
                                             pef.idProcessoExternoFabrica equals jppef.idProcessoExternoFabrica

                                         where jppef.idProduto == produtoId
                                         select new processoExterno
                                         {
                                             valorTotal = jppef.minutos * pef.valorMinuto

                                         }).ToList();

                decimal? totalPorcentagemDespesasGerais = 0;
                var totalPorcentagemDespesasGerais1 = (from dg in data.tbDespesasGerais
                                                       select dg.porcentagemDespesasGerais).ToList();
                if (totalPorcentagemDespesasGerais1.Count > 0)
                {
                    totalPorcentagemDespesasGerais = totalPorcentagemDespesasGerais1.Sum();
                }


                rptmateriaprima.DataSource = materiasPrimas;
                rptmateriaprima.DataBind();

                decimal? somaMateriaPrimas = materiasPrimas.Sum(x => x.valorTotal);
                decimal? somaProcessos = processos.Sum(x => x.valorTotal);
                decimal? somaProcessosEnternos = processosExternos.Sum(x => x.valorTotal);

                lblDescMateriaPrima.Text = string.Format("MateriaPrima: {0:C}", somaMateriaPrimas) + "<br/>";
                lblDescMateriaPrima.Text += string.Format("Processos Internos: {0:C}", somaProcessos) + "<br/>";
                lblDescMateriaPrima.Text += string.Format("Processos Externos: {0:C}", somaProcessosEnternos) + "<br/>";


                // Soma de Materia prima com processo internos e internos e Despesas Gerais:
                decimal? subtotalDespesasGerais = (somaMateriaPrimas / 100) * totalPorcentagemDespesasGerais;
                decimal? subTotal1 = somaMateriaPrimas + somaProcessos + somaProcessosEnternos;
                //decimal? gastosGeraisFabricacao = (subTotal1 / 100) * rnConfiguracoes.GGF;
                decimal? gastosGeraisFabricacao = subtotalDespesasGerais;
                lblDescMateriaPrima.Text += string.Format("Gastos Gerais Fab. : {0:C}", subtotalDespesasGerais) + "<br/>";
                // Adiciona o GGF no Subtotal
                lblDescMateriaPrima.Text += string.Format("<strong>Subtotal com GGF : {0:C}", subtotalDespesasGerais + subTotal1) + "</strong><br/>";
                decimal? subTotalComGGF = gastosGeraisFabricacao + subTotal1;

                decimal? margemLucro = ((subtotalDespesasGerais + subTotal1) / 100) * indiceMargemDeLucro;
                lblDescMateriaPrima.Text += string.Format("Margem de Lucro : {0:C}", margemLucro) + "<br/>";
                //Adiciona a margem de lucro
                decimal? subTotalComMargemLucro = margemLucro + (subtotalDespesasGerais + subTotal1);

                //Tributos
                decimal? fatordivisao = (100 - indiceTributos) / 100;
                decimal? subTotalComTributos = subTotalComMargemLucro / fatordivisao;
                decimal? soTributos = subTotalComTributos - subTotalComMargemLucro;
                lblDescMateriaPrima.Text += string.Format("Tributos : {0:C}", soTributos) + "<br/>";
                lblDescMateriaPrima.Text += string.Format("<strong>Total : {0:C}", subTotalComTributos) + "</strong><br/><br/><br/>";
                lblDescMateriaPrima.Text += string.Format("Preço de custo : {0:C}", produtoPrecoDeCusto) + "<br/>";

                ////Cálculo do lucro
                try // try/catch para prevenir possível divisão por zero
                {
                    var lucroPorcentagem = Convert.ToDecimal(((produtoPrecoDeCusto - subTotalComGGF) / subTotalComGGF) * 100);
                    lblDescMateriaPrima.Text += string.Format("Lucro Atual(%) : {0:N2}", lucroPorcentagem) + "%<br/>";
                    decimal lucroValor = Convert.ToDecimal(produtoPrecoDeCusto - subTotalComGGF);
                    lblDescMateriaPrima.Text += string.Format("Lucro Atual(R$) : {0:C}", lucroValor) + "<br/>";
                }
                catch (Exception ex)
                {
                    lblDescMateriaPrima.Text += string.Format("Lucro Atual(%) : {0:N2}", 0) + "<br/>";
                    lblDescMateriaPrima.Text += string.Format("Lucro Atual(R$) : ") + "<br/>";
                }

            }
        }
    }
}