﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        if (rnUsuarios.usuarioSeleciona_PorUsuarioNomeUsuarioSenha(txtNome.Text, txtSenha.Text).Tables[0].Rows.Count > 0)
        {
            string usuarioId = rnUsuarios.usuarioSeleciona_PorUsuarioNomeUsuarioSenha(txtNome.Text, txtSenha.Text).Tables[0].Rows[0]["usuarioId"].ToString();
            if (!string.IsNullOrEmpty(usuarioId))
            {
                HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
                usuarioLogadoId.Value = usuarioId;
                usuarioLogadoId.Expires = DateTime.Now.AddHours(10);
                Response.Cookies.Add(usuarioLogadoId);

                rnUsuarios.usuarioAcessoInclui(int.Parse(usuarioId));

                //Response.Write("<script>window.location=('pedidos.aspx');</script>");
                Response.Write("<script>window.location=('bemvindo.aspx');</script>");
            }
        }
        else
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
    }
}
