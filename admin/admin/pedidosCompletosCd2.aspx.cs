﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Utils.About;
using DevExpress.Web.ASPxGridView;

public partial class admin_pedidosCompletosCd2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid(false);
    }
    private void fillGrid(bool rebind)
    {
        //grd.DataSource = sql;
        grd.DataSource = rnPedidos.verificaPedidosCompletos(2);
        if ((!Page.IsPostBack && !Page.IsCallback) | rebind) grd.DataBind();
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

    }

    protected void btnGravar_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    public void interacaoInclui(string interacao, string alteracaoDeEstatus, int pedidoId)
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId != null)
        {
            rnInteracoes.interacaoInclui(pedidoId, interacao, rnUsuarios.usuarioSeleciona_PorUsuarioId(int.Parse(usuarioLogadoId.Value.ToString())).Tables[0].Rows[0]["usuarioNome"].ToString(), alteracaoDeEstatus);
        }
    }

    protected void btnGravarPrioridade_OnClick(object sender, EventArgs e)
    {
        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            var pedidoDc = new dbCommerceDataContext();
            TextBox txtPrioridade = (TextBox)grd.FindRowCellTemplateControl(i, grd.Columns["prioridade"] as GridViewDataColumn, "txtPrioridade");
            DropDownList ddlEnviarPor = (DropDownList)grd.FindRowCellTemplateControl(i, grd.Columns["enviarPor"] as GridViewDataColumn, "ddlEnviarPor");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

            if (!string.IsNullOrEmpty(txtPrioridade.Text))
            {
                if (pedido.prioridadeEnvio != Convert.ToInt32(txtPrioridade.Text))
                {
                    interacaoInclui("Pedido priorizado para " + Convert.ToInt32(txtPrioridade.Text), "False", pedido.pedidoId);
                }

                pedido.prioridadeEnvio = Convert.ToInt32(txtPrioridade.Text);
            }
            else
            {
                pedido.prioridadeEnvio = null;
            }
            if (!string.IsNullOrEmpty(ddlEnviarPor.SelectedValue))
            {
                if (pedido.formaDeEnvio != ddlEnviarPor.SelectedValue)
                {
                    interacaoInclui("Forma de envio alterada para " + ddlEnviarPor.SelectedValue, "False", pedido.pedidoId);
                }
                pedido.formaDeEnvio = ddlEnviarPor.SelectedValue;
            }
            else
            {
                pedido.formaDeEnvio = null;
            }
            pedidoDc.SubmitChanges();
        }
        fillGrid(true);
        Response.Write("<script>alert('Prioridade gravada com sucesso.');</script>");
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }

    protected void btnGerarEnvio_OnClick(object sender, EventArgs e)
    {

        int inicio = 0;
        int fim = 0;

        if (grd.PageIndex == 0)
        {
            inicio = 0;
            fim = inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }
        else
        {
            inicio += inicio + grd.SettingsPager.PageSize * grd.PageIndex;
            fim += inicio + grd.GetCurrentPageRowValues("pedidoId").Count;
        }

        for (int i = inicio; i < fim; i++)
        {
            var pedidoDc = new dbCommerceDataContext();
            CheckBox chkGerarEnvio = (CheckBox)grd.FindRowCellTemplateControl(i, grd.Columns["gerarEnvio"] as GridViewDataColumn, "chkGerarEnvio");
            int pedidoId = (int)grd.GetRowValues(i, new string[] { "pedidoId" });
            var pedido = (from c in pedidoDc.tbPedidos where c.pedidoId == pedidoId select c).First();

            if (chkGerarEnvio.Checked)
            {

                var produtosEtiqueta = rnPedidos.retornaProdutosEtiqueta(pedido.pedidoId, 2);

                var pedidoEnvio = new tbPedidoEnvio();
                pedidoEnvio.idUsuarioSeparacao = 93;
                pedidoEnvio.dataInicioSeparacao = DateTime.Now;
                pedidoEnvio.dataFimSeparacao = DateTime.Now;
                pedidoEnvio.idPedido = pedidoId;
                pedidoEnvio.dataInicioEmbalagem = DateTime.Now;
                pedidoEnvio.dataFimEmbalagem = DateTime.Now;
                pedidoEnvio.idUsuarioEmbalagem = 93;
                pedidoEnvio.formaDeEnvio = "jadlog";
                pedidoEnvio.volumesEmbalados = produtosEtiqueta.Count;
                pedidoEnvio.idCentroDeDistribuicao = 2;

                if ((pedido.endEstado != "ES" && pedido.endEstado != "MG" && pedido.endEstado != "SP" && pedido.endEstado != "PR" && pedido.endEstado != "RS" && pedido.endEstado != "SC" && pedido.endEstado != "RJ" && pedido.endEstado != "GO" && pedido.endEstado != "DF") | pedido.tipoDePagamentoId == 10)
                {
                    pedidoEnvio.nfeObrigatoria = true;
                }
                if (pedido.endEstado == "RJ" && pedido.endCidade.Trim().ToLower() == "angra dos reis")
                {
                    pedidoEnvio.nfeObrigatoria = true;
                }

                pedidoDc.tbPedidoEnvios.InsertOnSubmit(pedidoEnvio);
                pedidoDc.SubmitChanges();

                int volume = 0;
                int pesoTotal = 0;
                foreach (var produtoEtiqueta in produtosEtiqueta)
                {
                    volume++;
                    var etiqueta = (from c in pedidoDc.tbProdutoEstoques
                        where c.itemPedidoIdReserva == produtoEtiqueta.itemPedidoId && c.produtoId == produtoEtiqueta.produtoId
                              && c.pedidoId == null && c.idCentroDistribuicao == 2
                        select c).First();
                    etiqueta.pedidoId = etiqueta.pedidoIdReserva;
                    etiqueta.itemPedidoId = etiqueta.itemPedidoIdReserva;
                    etiqueta.idItemPedidoEstoque = etiqueta.idItemPedidoEstoqueReserva;
                    etiqueta.enviado = true;
                    etiqueta.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                    pedidoDc.SubmitChanges();

                    var itemEstoque =
                        (from c in pedidoDc.tbItemPedidoEstoques
                            where c.idItemPedidoEstoque == etiqueta.idItemPedidoEstoqueReserva
                            select c).First();
                    itemEstoque.enviado = true;
                    pedidoDc.SubmitChanges();

                    var pedidoPacote = new tbPedidoPacote();
                    pedidoPacote.altura = 1;
                    pedidoPacote.codJadlog = "";
                    pedidoPacote.concluido = true;
                    pedidoPacote.formaDeEnvio = "jadlog";
                    pedidoPacote.idPedido = pedidoId;
                    pedidoPacote.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                    pedidoPacote.largura = 1;
                    pedidoPacote.numeroVolume = volume;
                    pedidoPacote.observacao = "";
                    pedidoPacote.peso = Convert.ToInt32(etiqueta.tbProduto.produtoPeso);
                    pedidoPacote.profundidade = 1;
                    pedidoPacote.rastreio = "";
                    pedidoPacote.codJadlog = "1";

                    pesoTotal += pedidoPacote.peso;
                    pedidoDc.tbPedidoPacotes.InsertOnSubmit(pedidoPacote);
                    pedidoDc.SubmitChanges();


                    var itensFilho = (from c in pedidoDc.tbItensPedidoCombos where c.idItemPedido == produtoEtiqueta.itemPedidoId select c).ToList();
                    if (itensFilho.Any())
                    {
                        var itemPedido = (from c in pedidoDc.tbItensPedidoCombos where c.idItemPedido == produtoEtiqueta.itemPedidoId && c.produtoId == produtoEtiqueta.produtoId select c).First();
                        itemPedido.enviado = true;
                        itemPedido.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                        pedidoDc.SubmitChanges();
                        pedidoDc.SubmitChanges();
                    }
                    else
                    {
                        var itemPedido = (from c in pedidoDc.tbItensPedidos where c.itemPedidoId == produtoEtiqueta.itemPedidoId select c).First();
                        itemPedido.enviado = true;
                        itemPedido.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                        pedidoDc.SubmitChanges();
                        pedidoDc.SubmitChanges();
                    }
                }

                var produtosEnvio = (from c in pedidoDc.tbProdutoEstoques where c.idPedidoEnvio == pedidoEnvio.idPedidoEnvio select c.produtoId).ToList();
                var produtosPrecos =
                (from c in pedidoDc.tbProdutos
                 where produtosEnvio.Contains(c.produtoId)
                 select new { preco = (c.produtoPrecoPromocional ?? 0) > c.produtoPreco ? c.produtoPrecoPromocional : c.produtoPreco, c.produtoPrecoDeCusto });
                    decimal valorTotalDoPedido = 0;
                    decimal valorTotalDoCustoDoPedido = 0;
                if (produtosPrecos.Any())
                {
                    valorTotalDoPedido = produtosPrecos.Sum(x => (decimal)x.preco);
                    valorTotalDoCustoDoPedido = produtosPrecos.Sum(x => (decimal)x.produtoPrecoDeCusto);
                }
                else
                {
                    valorTotalDoPedido = (decimal)pedido.valorCobrado;
                    valorTotalDoCustoDoPedido = (decimal)pedido.valorCobrado / 2;
                }
                long cep = Convert.ToInt64(pedido.endCep.Replace("-", "").Replace(".", "").Replace(" ", "").Trim());


                #region Calculo na Jadlog
                var valoresJadlog = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotal), pedido.endCep.Replace("-", "").Trim(), valorTotalDoPedido, 0, 0, 0);
                //var valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "", valorTotalDoPedido);
                if (valoresJadlog.valor <= 0)
                {
                    valoresJadlog = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, cep, "", valorTotalDoPedido);
                }

                if (valoresJadlog.valor <= 0)
                {
                    var faixasTotal = (from c in pedidoDc.tbFaixaDeCeps
                                       where c.tipodeEntregaId == 13
                                       select new
                                       {
                                           faixa = Convert.ToInt64(c.faixaInicial)
                                       }).ToList();
                    var proximaFaixa = (from c in faixasTotal where c.faixa > cep select c).FirstOrDefault();
                    if (proximaFaixa != null)
                    {
                        //var valoresJadlog2 = rnFrete.CalculaFreteWebserviceJadlog(pesoTotal, Convert.ToInt64(proximaFaixa.faixa), modalidadeJadlog, valorTotalDoPedido);
                        var valoresJadlog2 = rnFrete.CalculaFrete(13, Convert.ToInt32(pesoTotal), proximaFaixa.faixa.ToString(), valorTotalDoPedido, 0, 0, 0);
                        var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                        valorTransporteJadlog.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                        valorTransporteJadlog.prazo = valoresJadlog2.prazo;
                        valorTransporteJadlog.servico = "jadlog";
                        valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog2.valor.ToString("0.00"));
                        pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                        pedidoDc.SubmitChanges();
                    }
                }
                else
                {
                    var valorTransporteJadlog = new tbPedidoEnvioValorTransporte();
                    valorTransporteJadlog.idPedidoEnvio = pedidoEnvio.idPedidoEnvio;
                    valorTransporteJadlog.prazo = valoresJadlog.prazo;
                    valorTransporteJadlog.servico = "jadlog";
                    valorTransporteJadlog.valor = Convert.ToDecimal(valoresJadlog.valor.ToString("0.00"));
                    pedidoDc.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteJadlog);
                    pedidoDc.SubmitChanges();
                }
                #endregion

                pedidoEnvio.valorSelecionado = valoresJadlog.valor;
                pedidoEnvio.prazoSelecionado = valoresJadlog.prazo;
                pedidoDc.SubmitChanges();



                var parciaisPendentes = (from c in pedidoDc.tbItemPedidoEstoques
                                         where
                                         c.tbItensPedido.pedidoId == pedidoId && c.enviado == false
                                         && c.cancelado == false
                                         select c).ToList();
                if (!parciaisPendentes.Any())
                {
                    rnPedidos.pedidoAlteraStatus(5, pedidoId);
                    rnInteracoes.interacaoInclui(pedidoId, "Pedido Enviado via CD2", rnUsuarios.retornaNomeUsuarioLogado(), "True");
                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", cliente.clienteEmail);
                    //rnEmails.enviaAlteracaoDeStatus(cliente.clienteNome, pedido.pedidoId.ToString(), "Pedido Enviado", "andre@bark.com.br");
                }
                else
                {
                    foreach (var parcial in parciaisPendentes)
                    {
                        parcial.autorizarParcial = false;
                        pedidoDc.SubmitChanges();
                    }
                    rnPedidos.pedidoAlteraStatus(11, pedido.pedidoId);
                    rnInteracoes.interacaoInclui(pedidoId, "Pedido Enviado Parcialmente via CD2", rnUsuarios.retornaNomeUsuarioLogado(), "True");
                    pedidoDc.SubmitChanges();
                }


            }

            pedidoDc.SubmitChanges();
        }
        fillGrid(true);
        Response.Write("<script>alert('Prioridade gravada com sucesso.');</script>");
    }
}