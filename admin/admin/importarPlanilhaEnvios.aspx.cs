﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpreadsheetLight;

public partial class admin_importarPlanilhaEnvios : System.Web.UI.Page
{
    public class pacotesEnvios
    {
        public int idPedido { get; set; }
        public int? itensPedido { get; set; }
        public string statusPedido { get; set; }
        public string cepEntrega { get; set; }
        public string cidadeEntrega { get; set; }
        public string ufEntrega { get; set; }
        public string dataEnvio { get; set; }
        public string rastreio { get; set; }
        public string transportadora { get; set; }
        public int? peso { get; set; }
        public int? volumes { get; set; }
        public decimal? valorEnvio { get; set; }
        public decimal? valorFrete { get; set; }
        public decimal? pagoTranspSimulado { get; set; }
        public decimal? taxaExtra { get; set; }
        public string cepTransp { get; set; }
        public decimal? custoEnvio { get; set; }
        public string statusTransp { get; set; }
        public decimal? diferenca { get; set; }

        internal bool Contains()
        {
            throw new NotImplementedException();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            // Primeiro Dia: Criamos uma variavel DateTime com o ano atual, o mês atual e o dia igual a 1 
            DateTime primeiroDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

            // Ultimo Dia: Criamos uma variavel DateTime com o ano atual, o mês atual e o dia é a quantidade de dias que o mês corrente possui.
            //A função DateTime.DaysInMonth recebe como parametro o ano(int) e o mês(int) e retorna a quantidade de dias(int). 
            DateTime ultimoDia = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month));

            txtDataInicial.Text = primeiroDia.ToShortDateString();
            txtDataFinal.Text = ultimoDia.ToShortDateString();
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        try
        {

            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            var dtFinal = Convert.ToDateTime(txtDataFinal.Text);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=envios-Transportadoras.xls");

            sl.SetCellValue("A1", "Id Pedido");
            sl.SetCellValue("B1", "Itens pedido");
            sl.SetCellValue("C1", "Status");
            sl.SetCellValue("D1", "Cep Enviado");
            sl.SetCellValue("E1", "Cidade");
            sl.SetCellValue("F1", "Estado");
            sl.SetCellValue("G1", "Data");
            sl.SetCellValue("H1", "Rastreio");
            sl.SetCellValue("I1", "Transportadora");
            sl.SetCellValue("J1", "Peso");
            sl.SetCellValue("K1", "Volumes");
            sl.SetCellValue("L1", "Valor Pesagem");
            sl.SetCellValue("M1", "Pago cliente");
            sl.SetCellValue("N1", "Valor Transp Simulado");
            sl.SetCellValue("O1", "Taxa extra");
            sl.SetCellValue("P1", "Cep Transp");
            sl.SetCellValue("Q1", "Custo do envio");
            sl.SetCellValue("R1", "Status transportadora");
            sl.SetCellValue("S1", "Diferença");

            using (var data = new dbCommerceDataContext())
            {

                IEnumerable<pacotesEnvios> dados = null;

                dados = (from env in data.tbPedidoPacotes
                         where
                             (env.dataDeCarregamento.Value.Date.Day >= dtInicial.Date.Day && env.dataDeCarregamento.Value.Date.Month >= dtInicial.Date.Month && env.dataDeCarregamento.Value.Date.Year >= dtInicial.Date.Year) &&
                             (env.dataDeCarregamento.Value.Date.Day <= dtFinal.Date.Day && env.dataDeCarregamento.Value.Date.Month <= dtFinal.Date.Month && env.dataDeCarregamento.Value.Date.Year <= dtFinal.Date.Year)
                         select new pacotesEnvios
                         {
                             idPedido = env.idPedido,
                             itensPedido = env.tbPedido.tbItensPedidos.Count(x => !(x.cancelado ?? false)),
                             statusPedido = env.tbPedido.tbPedidoSituacao.situacao,
                             cepEntrega = env.tbPedido.endCep,
                             cidadeEntrega = env.tbPedido.endCidade,
                             ufEntrega = env.tbPedido.endEstado,
                             dataEnvio = env.dataDeCarregamento.Value.ToShortDateString(),
                             rastreio = env.rastreio,
                             transportadora = env.formaDeEnvio,
                             peso = env.peso,
                             volumes = env.tbPedidoEnvio.volumesEmbalados,
                             valorEnvio = env.tbPedidoEnvio.tbPedidoEnvioValorTransportes.FirstOrDefault(x => (x.servico ?? "") == (env.formaDeEnvio ?? "")).valor,
                             valorFrete = env.tbPedido.valorDoFrete,
                             pagoTranspSimulado = env.tbPedidoEnvio.valorSelecionado,
                             taxaExtra = env.taxaExtra,
                             cepTransp = env.tbPedido.endCep,
                             custoEnvio = env.tbPedidoEnvio.tbPedidoEnvioValorTransportes.FirstOrDefault(x => (x.servico ?? "") == (env.formaDeEnvio ?? "")).valor + (env.taxaExtra ?? 0),
                             statusTransp = env.statusAtual,
                             diferenca = env.tbPedidoEnvio.valorSelecionado - env.tbPedidoEnvio.tbPedidoEnvioValorTransportes.FirstOrDefault(x => (x.servico ?? "") == (env.formaDeEnvio ?? "")).valor + (env.taxaExtra ?? 0) //(env.tbPedidoEnvio.tbPedidoEnvioValorTransportes.FirstOrDefault(x => (x.servico ?? "") == (env.formaDeEnvio ?? "")).valor - env.tbPedido.valorDoFrete + (env.taxaExtra ?? 0))
                         }).ToList<pacotesEnvios>();

                int linha = 2;
                foreach (var item in dados)
                {
                    sl.SetCellValue(linha, 1, item.idPedido);
                    sl.SetCellValue(linha, 2, item.itensPedido ?? 0);
                    sl.SetCellValue(linha, 3, item.statusPedido);
                    sl.SetCellValue(linha, 4, item.cepEntrega);
                    sl.SetCellValue(linha, 5, item.cidadeEntrega);
                    sl.SetCellValue(linha, 6, item.ufEntrega);
                    sl.SetCellValue(linha, 7, item.dataEnvio);
                    sl.SetCellValue(linha, 8, item.rastreio);
                    sl.SetCellValue(linha, 9, item.transportadora);
                    sl.SetCellValue(linha, 10, item.peso.ToString());
                    sl.SetCellValue(linha, 11, item.volumes ?? 0);
                    sl.SetCellValue(linha, 12, (item.valorEnvio ?? 0).ToString("C"));
                    sl.SetCellValue(linha, 13, (item.valorFrete ?? 0).ToString("C"));
                    sl.SetCellValue(linha, 14, (item.pagoTranspSimulado ?? 0).ToString("C"));
                    sl.SetCellValue(linha, 15, item.taxaExtra ?? 0);
                    sl.SetCellValue(linha, 16, item.cepTransp);
                    sl.SetCellValue(linha, 17, (item.custoEnvio ?? 0).ToString("C"));
                    sl.SetCellValue(linha, 18, item.statusTransp);
                    sl.SetCellValue(linha, 19, (item.diferenca ?? 0).ToString("C"));

                    linha++;
                }

            }

            //Exporta 
            sl.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {


        }
    }
}