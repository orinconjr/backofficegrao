﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="processosinternos.aspx.cs" Inherits="admin_processosinternos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <style>
        .meugrid {
            background-color: #F4FAFB;
            border: 0px none;
            /* border-collapse: separate !important; */
            overflow: hidden;
            font: 9pt Tahoma;
            color: #000;
            cursor: pointer;
            white-space: nowrap;
            font-weight: normal;
            width: 805px;
            text-align: left;
            margin-left: 4px;
            width: 805px;
        }

            .meugrid th {
                background-image: url(../../App_Themes/Glass/GridView/gvHeaderBackground.gif);
                background-position: center top;
                background-repeat: repeat-x;
                background-color: #C0DDE0;
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                font-weight: normal;
                border: 1px solid #7EACB1;
                /* border-collapse: separate;*/
            }

            .meugrid td {
                overflow: hidden;
                height: 23px;
                line-height: 23px;
                padding: 4px 6px 5px;
                border: 1px solid #7EACB1;
            }

            .meugrid tr td:last-of-type {
                text-align: center;
            }
            .meugrid tr td:nth-child(4) {
                text-align: center;
               
            }
           .meugrid tr td:nth-child(4) input{
              
                 text-decoration: underline;
            }
        .rotulos td {
            font: bold 12px tahoma;
            padding-top: 20px;
            color: rgb(65, 105, 126);
            padding-left: 5px;
            padding-bottom: 5px;
        }
    </style>
    <script src="js/jquery.min.js"></script>
    <script type="text/javascript">

        function validaform() {
            var valido = true;
            var mensagem = "";
            var txtMinutos = $('input[id *= txtMinutos]').val().replace(",", ".");
            var processofabrica = $('select[id *= ddlProcessosFabrica]').val();
            if (txtMinutos == "0" || txtMinutos == "") {
                mensagem = "Informe a quantidade de minutos\n";
                valido = false;
            } else {
                if (isNaN(txtMinutos)) {
                    mensagem = "Informe apenas valores núméricos para os minutos\n";
                    valido = false;
                } else {
                    if (!(txtMinutos % 1 === 0)) {
                        mensagem = "Informe números inteiros para minutos\n";
                        valido = false;
                    }
                 }
            }
            if (processofabrica == "-1") {
                mensagem += "Selecione o processo / subprocesso";
                valido = false;
            }
            if (!valido) {
                alert(mensagem);
            }
            $('input[id *= txtMinutos]').val(txtMinutos.replace(".", ","));
            return valido;

        }

        function validaformExterno() {
            var valido = true;
            var mensagem = "";
            var txtMinutos = $('input[id *= txtMinutosExternos]').val().replace(",", ".");
            var processofabrica = $('select[id *= ddlProcessosExternosFabrica]').val();
            if (txtMinutos == "0" || txtMinutos == "") {
                mensagem = "Informe a quantidade de minutos\n";
                valido = false;
            } else {
                if (isNaN(txtMinutos)) {
                    mensagem = "Informe apenas valores núméricos para os minutos\n";
                    valido = false;
                } else {
                    if (!(txtMinutos % 1 === 0)) {
                        mensagem = "Informe números inteiros para minutos\n";
                        valido = false;
                    }
                }
            }
            if (processofabrica == "-1") {
                mensagem += "Selecione o processo externo";
                valido = false;
            }
            if (!valido) {
                alert(mensagem);
            }
            $('input[id *= txtMinutosExternos]').val(txtMinutos.replace(".", ","));
            return valido;

        }
    </script>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Processos por produto</asp:Label>
            </td>
        </tr>

        <tr>
            <td>

                <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td>
                            <div style="float: left; clear: left;">
                                <img id="imgfotodestaque" runat="server" style="width: 250px;"/>
                            </div>
                             <div style="float: left;font: bold 16px tahoma;color: #41697E;padding-left: 20px;">
                                 <asp:Label ID="lblprodutonome" runat="server" Text="Label"></asp:Label>
                            </div>
                        </td>
                    </tr>


                    <tr class="campos">
                        <td>
                            <table style="width: 100%" class="rotulos">
                                <tr>
                                    <td>Processo / Subprocesso:<br />

                                        <asp:DropDownList  Width="473px" ID="ddlProcessosFabrica" runat="server" AppendDataBoundItems="True"  DataTextField="nome" DataValueField="procsubproc" EnableViewState="True">
                                            <asp:ListItem Value="-1">
                                                -- Escolha um processo e subprocesso --
                                            </asp:ListItem>
                                        </asp:DropDownList>

                                  
                                    </td>
                                    <td>Minutos:<br />
                                        <asp:TextBox ID="txtMinutos" Width="100" runat="server"></asp:TextBox>
                                  
                                    </td>
                                    <td>&nbsp;<br />
                                        <asp:Button runat="server" ID="btnGravar" Text="Adicionar" OnClientClick="return validaform();" OnClick="btnGravar_Click"  />
                                     
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="rotulos">

                        <td>Processos internos:
                        </td>

                    </tr>
                    <tr>
                        <td>

                            <asp:GridView ID="GridView1" CssClass="meugrid" DataKeyNames="idJuncaoProdutoProcessoFabrica" BorderWidth="0px"
                                runat="server" AutoGenerateColumns="False" OnRowDeleting="GridView1_RowDeleting" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating" >
                                <Columns>
                                   
                                     <asp:TemplateField HeaderText="Processo">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("processo") %>'></asp:Label>
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="SubProcesso">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("nome") %>'></asp:Label>
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
  
                                

                                    <asp:TemplateField HeaderText="Minutos">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("minutos") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("minutos") %>'></asp:TextBox>
                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="TextBox1" ErrorMessage="Informe os minutos" ForeColor="Red">
                                            </asp:RequiredFieldValidator>
                                             <asp:CompareValidator ID="cv" runat="server" ControlToValidate="TextBox1" Type="Integer"  ForeColor="Red"  Operator="DataTypeCheck" ErrorMessage="Digite apenas números" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:CommandField CancelText="Cancela" EditText="Alterar" ControlStyle-Width="22px"
                                        ControlStyle-Height="24px" HeaderText="Editar" ShowEditButton="True" UpdateText="Confirma"
                                        ButtonType="Image" EditImageUrl="images/btEditar2.jpg" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle></ItemStyle>
                                        <ControlStyle Height="24px" Width="22px"></ControlStyle>
                                        <HeaderStyle />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                Text="Delete">
                                            <img  src="images/btExcluir.jpg" alt="Excluir" style="border-style:none;"/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">&nbsp;
                        </td>
                    </tr>
                    
                     <tr class="campos">
                        <td>
                            <table style="width: 100%" class="rotulos">
                                <tr>
                                    <td>Processo Externo:<br />

                                        <asp:DropDownList  Width="473px" ID="ddlProcessosExternosFabrica" runat="server" AppendDataBoundItems="True"  DataTextField="nome" DataValueField="proc" EnableViewState="True">
                                            <asp:ListItem Value="-1">
                                                -- Escolha um processo --
                                            </asp:ListItem>
                                        </asp:DropDownList>

                                  
                                    </td>
                                    <td>Minutos:<br />
                                        <asp:TextBox ID="txtMinutosExternos" Width="100" runat="server"></asp:TextBox>
                                  
                                    </td>
                                    <td>&nbsp;<br />
                                        <asp:Button runat="server" ID="btnGravarExterno" Text="Adicionar" OnClientClick="return validaformExterno();" OnClick="btnGravarExterno_Click"   />
                                     
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="rotulos">

                        <td>Processos externos:
                        </td>

                    </tr>
                    <tr>
                        <td>

                            <asp:GridView ID="GridView2" CssClass="meugrid" DataKeyNames="idJuncaoProdutoProcessoExternoFabrica" BorderWidth="0px"
                                runat="server" AutoGenerateColumns="False" OnRowDeleting="GridView2_RowDeleting" OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowEditing="GridView2_RowEditing" OnRowUpdating="GridView2_RowUpdating" >
                                <Columns>
                                   
                                     <asp:TemplateField HeaderText="Processo">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("processoExterno") %>'></asp:Label>
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
                                  
                                

                                    <asp:TemplateField HeaderText="Minutos">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("minutos") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("minutos") %>'></asp:TextBox>
                                            &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                ControlToValidate="TextBox1" ErrorMessage="Informe os minutos" ForeColor="Red">
                                            </asp:RequiredFieldValidator>
                                             <asp:CompareValidator ID="cv" runat="server" ControlToValidate="TextBox1" Type="Integer"  ForeColor="Red"  Operator="DataTypeCheck" ErrorMessage="Digite apenas números" />
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:CommandField CancelText="Cancela" EditText="Alterar" ControlStyle-Width="22px"
                                        ControlStyle-Height="24px" HeaderText="Editar" ShowEditButton="True" UpdateText="Confirma"
                                        ButtonType="Image" EditImageUrl="images/btEditar2.jpg" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle></ItemStyle>
                                        <ControlStyle Height="24px" Width="22px"></ControlStyle>
                                        <HeaderStyle />
                                    </asp:CommandField>
                                    <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                Text="Delete">
                                            <img  src="images/btExcluir.jpg" alt="Excluir" style="border-style:none;"/>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>

                            </asp:GridView>
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
    </table>

    
</asp:Content>
