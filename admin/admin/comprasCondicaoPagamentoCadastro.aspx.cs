﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;
using CarlosAg.ExcelXmlWriter;

public partial class admin_comprasCondicaoPagamentoCadastro : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["id"] != null)
            {
                hdfIdCondicao.Text = Request.QueryString["id"].ToString();
            }
            FillForm();
        }
    }

    private void FillForm()
    {
        if (!string.IsNullOrEmpty(hdfIdCondicao.Text))
        {
            int idCondicao = 0;
            int.TryParse(hdfIdCondicao.Text, out idCondicao);
            var data = new dbCommerceDataContext();
            var condicao = (from c in data.tbComprasCondicaoPagamentos where c.idComprasCondicaoPagamento == idCondicao select c).FirstOrDefault();
            if (condicao == null)
            {
                hdfIdCondicao.Text = "";
                return;
            }
            txtNome.Text = condicao.nome;
            rdbTipoDia.Checked = condicao.tipoPagamento == 1;
            rdbTipoFechamento.Checked = condicao.tipoPagamento == 0;
            rdbPrazoPedido.Checked = condicao.inicioPrazo == 1;
            rdbPrazoEntrega.Checked = condicao.inicioPrazo == 0;
            chkIgnorarMes.Checked = condicao.ignorarMesAtual ?? false;
            FillGridPrazos();
        }
    }

    private void FillGridPrazos()
    {
        if (!string.IsNullOrEmpty(hdfIdCondicao.Text))
        {
            int idCondicao = 0;
            int.TryParse(hdfIdCondicao.Text, out idCondicao);

            var data = new dbCommerceDataContext();
            var condicaoDias = (from c in data.tbComprasCondicaoPagamentoDias where c.idComprasCondicaoPagamento == idCondicao select c).ToList();
            grdPrazos.DataSource = condicaoDias;
            grdPrazos.DataBind();
        }
        else
        {
            var lista = new List<tbComprasCondicaoPagamentoDia>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }
            grdPrazos.DataSource = lista;
            grdPrazos.DataBind();
        }
    }


    private void AdicionaItemListaTemporariaFornecedores(tbComprasCondicaoPagamentoDia item)
    {
        var lista = new List<tbComprasCondicaoPagamentoDia>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }

        lista.RemoveAll(x => x.dia == item.dia);

        lista.Add(item);
        hdfLista.Text = Serialize(lista);
        FillGridPrazos();
    }


    protected void btnGravar_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        if (!string.IsNullOrEmpty(hdfIdCondicao.Text))
        {
            int idCondicao = Convert.ToInt32(hdfIdCondicao.Text);
            var condicao = (from c in data.tbComprasCondicaoPagamentos where c.idComprasCondicaoPagamento == idCondicao select c).First();
            condicao.nome = txtNome.Text;
            condicao.tipoPagamento = rdbTipoDia.Checked ? 1 : 0;
            condicao.inicioPrazo = rdbPrazoPedido.Checked ? 1 : 0;

            if (rdbTipoFechamento.Checked)
                condicao.ignorarMesAtual = chkIgnorarMes.Checked;
            else
                condicao.ignorarMesAtual = false;

            data.SubmitChanges();
        }
        else
        {
            var condicao = new tbComprasCondicaoPagamento();
            condicao.nome = txtNome.Text;
            condicao.tipoPagamento = rdbTipoDia.Checked ? 1 : 0;
            condicao.inicioPrazo = rdbPrazoPedido.Checked ? 1 : 0;
            data.tbComprasCondicaoPagamentos.InsertOnSubmit(condicao);

            if (rdbTipoFechamento.Checked)
                condicao.ignorarMesAtual = chkIgnorarMes.Checked;
            else
                condicao.ignorarMesAtual = false;

            data.SubmitChanges();

            var lista = new List<tbComprasCondicaoPagamentoDia>();
            if (!string.IsNullOrEmpty(hdfLista.Text))
            {
                lista = Deserialize(hdfLista.Text);
            }

            foreach (var cadastroDia in lista)
            {
                var condicaoDia = new tbComprasCondicaoPagamentoDia
                {
                    dia = cadastroDia.dia,
                    idComprasCondicaoPagamento = condicao.idComprasCondicaoPagamento
                };

                if (rdbTipoFechamento.Checked)
                    condicaoDia.ignorarMesAtual = chkIgnorarMes.Checked;
                else
                    condicaoDia.ignorarMesAtual = false;

                data.tbComprasCondicaoPagamentoDias.InsertOnSubmit(condicaoDia);
                data.SubmitChanges();
            }
        }
        Response.Redirect("comprasCondicaoPagamentoLista.aspx");
    }
    protected void btnGravarPrazo_OnClick(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        int dias = Convert.ToInt32(txtDias.Text);
        if (!string.IsNullOrEmpty(hdfIdCondicao.Text))
        {
            int idCondicao = Convert.ToInt32(hdfIdCondicao.Text);
            var diasCheck = (from c in data.tbComprasCondicaoPagamentoDias where c.idComprasCondicaoPagamento == idCondicao && c.dia == dias select c).Any();
            if (!diasCheck)
            {
                var diaCadastro = new tbComprasCondicaoPagamentoDia();
                diaCadastro.dia = dias;
                diaCadastro.idComprasCondicaoPagamento = idCondicao;
                data.tbComprasCondicaoPagamentoDias.InsertOnSubmit(diaCadastro);
                data.SubmitChanges();
            }
        }
        else
        {
            var diaCadastro = new tbComprasCondicaoPagamentoDia();
            diaCadastro.dia = dias;
            diaCadastro.idComprasCondicaoPagamento = 0;
            AdicionaItemListaTemporariaFornecedores(diaCadastro);
        }
    }

    private int RetornaIdUsuarioLogado()
    {
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (usuarioLogadoId == null)
        {
            return 0;
        }

        return Convert.ToInt32(usuarioLogadoId.Value);
    }

    public static string Serialize(List<tbComprasCondicaoPagamentoDia> tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasCondicaoPagamentoDia>));

        TextWriter writer = new StringWriter();
        serializer.Serialize(writer, tData);

        return writer.ToString();
    }

    public static List<tbComprasCondicaoPagamentoDia> Deserialize(string tData)
    {
        var serializer = new XmlSerializer(typeof(List<tbComprasCondicaoPagamentoDia>));

        TextReader reader = new StringReader(tData);

        return (List<tbComprasCondicaoPagamentoDia>)serializer.Deserialize(reader);
    }



    protected void btnRemover_OnCommand(object sender, CommandEventArgs e)
    {
        if (!string.IsNullOrEmpty(hdfIdCondicao.Text))
        {
            var data = new dbCommerceDataContext();
            int dias = Convert.ToInt32(e.CommandArgument);
            int idCondicao = Convert.ToInt32(hdfIdCondicao.Text);
            var diasCheck = (from c in data.tbComprasCondicaoPagamentoDias where c.idComprasCondicaoPagamento == idCondicao && c.dia == dias select c).FirstOrDefault();
            if (diasCheck != null)
            {
                data.tbComprasCondicaoPagamentoDias.DeleteOnSubmit(diasCheck);
                data.SubmitChanges();
            }
        }
        else
        {
            RemoveItemLista(Convert.ToInt32(e.CommandArgument));
        }
        FillGridPrazos();
    }
    private void RemoveItemLista(int dias)
    {
        var lista = new List<tbComprasCondicaoPagamentoDia>();
        if (!string.IsNullOrEmpty(hdfLista.Text))
        {
            lista = Deserialize(hdfLista.Text);
        }
        lista.RemoveAll(x => x.dia == dias);
        hdfLista.Text = Serialize(lista);
        FillGridPrazos();
    }
}