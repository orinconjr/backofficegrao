﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SpreadsheetLight;
using System.IO;
using System.Xml;
public partial class admin_pedidosMotivosInclusaoItem : System.Web.UI.Page
{
    public class Item
    {
        public int pedidoId { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
        public string motivoAdicao { get; set; }
        public DateTime? dataDaCriacao { get; set; }
        public decimal? itemValor { get; set; }
        public decimal? valorCusto { get; set; }
        public int? idItemPedidoTipoAdicao { get; set; }
        public int? idUsuarioAdicao { get; set; }
        public string usuarioNome { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            carregarDdl();
            if (validarcampos())
            {
                carregaGrid();
            }

        }
    }
    private void carregaGrid()
    {
        List<Item> itens = getDados();
        GridView1.DataSource = itens;
        GridView1.DataBind();
        lblitensencontrados.Text = itens.Count().ToString();
        lblvalortotalcusto.Text = Convert.ToDecimal(itens.Sum(x => x.valorCusto)).ToString("C");
        lblvalortotalvenda.Text = Convert.ToDecimal(itens.Sum(x => x.itemValor)).ToString("C");

    }

    void carregarDdl()
    {
        var data = new dbCommerceDataContext();
        var motivos = (from c in data.tbItemPedidoTipoAdicaos
                       select new
                       {
                           c.idItemPedidoTipoAdicao,
                           c.tipoDeAdicao
                       }).ToList();
        ddlMotivos.Items.Clear();
        ddlMotivos.Items.Add(new ListItem("Todos", "0"));
        foreach(var motivo in motivos)
        {
            ddlMotivos.Items.Add(new ListItem(motivo.tipoDeAdicao,motivo.idItemPedidoTipoAdicao.ToString()));
        }
    }
    public List<Item> getDados()
    {
        var data = new dbCommerceDataContext();
        List<Item> itens = new List<Item>();
        itens = (from c in data.tbItensPedidos
                 where c.idItemPedidoTipoAdicao != null
                 select new Item
                 {
                     pedidoId = c.pedidoId,
                     produtoId = c.produtoId,
                     dataDaCriacao =  c.dataDaCriacao,
                     produtoNome = c.tbProduto.produtoNome,
                     valorCusto = c.valorCusto,
                     itemValor = c.itemValor,
                     motivoAdicao = c.motivoAdicao,
                     idItemPedidoTipoAdicao = c.idItemPedidoTipoAdicao,
                     usuarioNome = (from u in data.tbUsuarios where u.usuarioId == c.idUsuarioAdicao select u.usuarioNome).FirstOrDefault()

                 }).ToList<Item>();


        if (txtDataInicial.Text != "" && txtDataFinal.Text != "")
        {
            var dtInicial = Convert.ToDateTime(txtDataInicial.Text);
            DateTime dtFinal = Convert.ToDateTime(txtDataFinal.Text + " 23:59:59");
            itens =
                itens.Where(
                    x => x.dataDaCriacao >= dtInicial && x.dataDaCriacao <= dtFinal).ToList();
        }
        if(ddlMotivos.SelectedItem.Value !="0")
        {
            itens = itens.Where(x => x.idItemPedidoTipoAdicao == Convert.ToInt32(ddlMotivos.SelectedItem.Value)).ToList();
        }

        if (ddlordenarpor.SelectedItem.Value == "dataDaCriacao")
            itens = itens.OrderByDescending(x => x.dataDaCriacao).ToList();

        if (ddlordenarpor.SelectedItem.Value == "pedidoId")
            itens = itens.OrderByDescending(x => x.pedidoId).ToList();

        if (ddlordenarpor.SelectedItem.Value == "produtoId")
            itens = itens.OrderBy(x => x.produtoId).ToList();

        if (ddlordenarpor.SelectedItem.Value == "produtoNome")
            itens = itens.OrderBy(x => x.produtoNome).ToList();

        if (ddlordenarpor.SelectedItem.Value == "produtoId")
            itens = itens.OrderBy(x => x.produtoId).ToList();

        if (ddlordenarpor.SelectedItem.Value == "valorCusto")
            itens = itens.OrderBy(x => x.valorCusto).ToList();

        if (ddlordenarpor.SelectedItem.Value == "itemValor")
            itens = itens.OrderBy(x => x.itemValor).ToList();

        if (ddlordenarpor.SelectedItem.Value == "motivoAdicao")
            itens = itens.OrderBy(x => x.motivoAdicao).ToList();

        return itens;

    }
    bool validarcampos()
    {
        string erro = "";
        bool valido = true;


        if (!String.IsNullOrEmpty(txtDataInicial.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataInicial.Text); }
            catch (Exception)
            {
                erro += @"Data inicial invalida\n";
                valido = false;
            }
        }

        if (!String.IsNullOrEmpty(txtDataFinal.Text))
        {
            try { DateTime teste = Convert.ToDateTime(txtDataFinal.Text); }
            catch (Exception)
            {
                erro += @"Data final invalida\n";
                valido = false;
            }
        }

        if (erro != "")
        {
            string meuscript = @"alert('" + erro + "');";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "MyScript", meuscript, true);

        }
        return valido;
    }
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        if (validarcampos())
            carregaGrid();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;

        carregaGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string pedidoId = DataBinder.Eval(e.Row.DataItem, "pedidoId").ToString();
            HtmlAnchor linkeditar = (HtmlAnchor)e.Row.FindControl("linkeditar");
            linkeditar.HRef = "pedido.aspx?pedidoId=" + pedidoId;
        }
    }
    protected void btnExportarPlanilha_Click(object sender, EventArgs e)
    {
        if (validarcampos())
        {
            List<Item> itens = getDados();
            SLDocument sl = new SLDocument();
            SLStyle style1 = sl.CreateStyle();
            style1.Font.Bold = true;
            sl.SetRowStyle(1, style1);

            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=Relatorio_Motivos_Inclusao.xlsx");

            sl.SetCellValue("A1", "Data");
            sl.SetCellValue("B1", "Id do Pedido");
            sl.SetCellValue("C1", "produto Id");
            sl.SetCellValue("D1", "produto Nome");
            sl.SetCellValue("E1", "Preço de custo");
            sl.SetCellValue("F1", "Preço de venda");
            sl.SetCellValue("G1", "Motivo");
            sl.SetCellValue("H1", "Usuário");

            int linha = 2;
            foreach (var item in itens)
            {
                sl.SetCellValue(linha, 1, Convert.ToDateTime(item.dataDaCriacao).ToShortDateString());
                sl.SetCellValue(linha, 2, item.pedidoId);
                sl.SetCellValue(linha, 3, item.produtoId);
                sl.SetCellValue(linha, 4, item.produtoNome);

                decimal valorCusto = 0;
                try { valorCusto = Convert.ToDecimal(item.valorCusto.ToString().Replace(".", ",")); }
                catch (Exception) { }

                decimal itemValor = 0;
                try { itemValor = Convert.ToDecimal(item.itemValor.ToString().Replace(".", ",")); }
                catch (Exception) { }

                sl.SetCellValue(linha, 5, valorCusto.ToString("C"));
                sl.SetCellValue(linha, 6, itemValor.ToString("C"));
                sl.SetCellValue(linha, 7, item.motivoAdicao);
                sl.SetCellValue(linha, 8, item.usuarioNome);
                linha++;
            }

            sl.SaveAs(Response.OutputStream);
            Response.End();
        }

    }



}