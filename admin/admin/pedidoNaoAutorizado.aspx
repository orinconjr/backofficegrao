﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="pedidoNaoAutorizado.aspx.cs" Inherits="admin_pedidoNaoAutorizado" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTimer" TagPrefix="dxt" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register Namespace="Controls" TagPrefix="uc" %>


<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web.ASPxMenu" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxPopupControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<%@ Register Assembly="CuteEditor" Namespace="CuteEditor" TagPrefix="CE" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">


    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblTitulo" runat="server" BorderColor="#FFFF99">Pedidos Nao Autorizados       </asp:Label>
                  <asp:ImageButton ID="btRefresh" runat="server" ImageUrl="~/admin/images/btAtualizarIcon.jpg" OnClick="btRefresh_OnClick" />
           </td>
        </tr>

        <tr>
            <td>
                <dx:ASPxPageControl ID="tabsPedidosNAuto" runat="server" ActiveTabIndex="0" Width="100%">
                    <TabPages>


                        <dx:TabPage Text="Agendados">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">

                                        <tr>
                                            <td>
                                                <dxwgv:ASPxGridView ID="grdpedidoNaoAutorizado" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="grdpedidoNaoAutorizado_RowCreated"
                                                    OnCustomUnboundColumnData="grdpedidoNaoAutorizado_CustomData" KeyFieldName="pedidoId">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />

                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                                            <DataItemTemplate>
                                                                <%# Container.VisibleIndex + 1 %>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataTextColumn Caption="Pedidos" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Observações/Comentários" FieldName="observacao" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Pedido" FieldName="dataPedido" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Atendimento" FieldName="dataAtendimento" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Próximo Atendimento" FieldName="dataProxAtendimento" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Atendente" FieldName="atendente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Nome Cliente" FieldName="nomeCliente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Contato Cliente" FieldName="contatoCliente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>


                                                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                                                            <DataItemTemplate>
                                                                <asp:Button runat="server" ID="btnAgendamento" Text="Agendamento" OnCommand="btnAgendar_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                                                            <DataItemTemplate>
                                                                <asp:Button runat="server" ID="btnHistorico" Text="Histórico" OnCommand="btnHistorico_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>


                                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                                                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                                            </PropertiesHyperLinkEdit>
                                                            <Settings AllowAutoFilter="False" />
                                                            <HeaderTemplate>
                                                                <img alt="" src="images/legendaEditar.jpg" />
                                                            </HeaderTemplate>
                                                        </dxwgv:GridViewDataHyperLinkColumn>

                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;"></td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>


                        <dx:TabPage Text="Sem Agendamento">
                            <ContentCollection>
                                <dx:ContentControl runat="server">
                                    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">

                                        <tr>
                                            <td>
                                                <dxwgv:ASPxGridView ID="grdSemAgendamento" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="grdSemAgendamento_RowCreated"
                                                    OnCustomUnboundColumnData="grdSemAgendamento_CustomData" KeyFieldName="pedidoId">
                                                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                    <Styles>
                                                        <Footer Font-Bold="True">
                                                        </Footer>
                                                    </Styles>
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                    <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />

                                                    <Columns>
                                                        <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                                            <DataItemTemplate>
                                                                <%# Container.VisibleIndex + 1 %>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataTextColumn Caption="Pedidos" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Pedido" FieldName="dataPedido" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Observações/Comentários" FieldName="observacao" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Atendente" FieldName="atendente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Nome Cliente" FieldName="nomeCliente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Contato Cliente" FieldName="contatoCliente" VisibleIndex="0" Width="50">
                                                        </dxwgv:GridViewDataTextColumn>


                                                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                                                            <DataItemTemplate>
                                                                <asp:Button runat="server" ID="btnAgendamento" Text="Agendamento" OnCommand="btnAgendar_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                                                            <DataItemTemplate>
                                                                <asp:Button runat="server" ID="btnHistorico" Text="Histórico" OnCommand="btnHistorico_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>


                                                        <dxwgv:GridViewDataHyperLinkColumn Caption="Pedido" FieldName="pedidoId" VisibleIndex="7" Width="30px">
                                                            <PropertiesHyperLinkEdit ImageUrl="~/admin/images/btEditar.jpg" NavigateUrlFormatString="pedido.aspx?pedidoId={0}" Target="_blank">
                                                            </PropertiesHyperLinkEdit>
                                                            <Settings AllowAutoFilter="False" />
                                                            <HeaderTemplate>
                                                                <img alt="" src="images/legendaEditar.jpg" />
                                                            </HeaderTemplate>
                                                        </dxwgv:GridViewDataHyperLinkColumn>

                                                    </Columns>
                                                    <StylesEditors>
                                                        <Label Font-Bold="True">
                                                        </Label>
                                                    </StylesEditors>
                                                </dxwgv:ASPxGridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;"></td>
                                        </tr>
                                    </table>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>




                    </TabPages>
                </dx:ASPxPageControl>



            </td>
        </tr>
    </table>


    <dx:ASPxPopupControl ID="frmAgendamento" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="frmAgendamento"
        HeaderText="Agendamento" AllowDragging="True" PopupAnimationType="None" EnableViewState="True" Width="569px">


        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <asp:HiddenField runat="server" ID="hdfPedidoId" />
                <div style="width: 489px; height: 200px; overflow: scroll;">
                    <div>
                        <table style="width: 100%">
                            <tr class="rotulos">
                                <td colspan="3">Agendamento de atendimento para pedidos não Autorizados:

                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox CssClass="campos"  TextMode="MultiLine" runat="server" ID="txtComentario" Width="94%" Height="82px"></asp:TextBox>
                                </td>
                            </tr>

                            <tr>
                                <td>


                                    <dxe:ASPxDateEdit runat="server" ID="txtDataProximoAtend"></dxe:ASPxDateEdit>


                                </td>
                                <td>


                                    <dxe:ASPxTimeEdit runat="server" ID="txtHoraProximoAtend"></dxe:ASPxTimeEdit>


                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;
                                </td>
                            </tr>

                            <tr>
                                <td style="padding-left: 35px; font-weight: bold;">
                                    <uc:OneClickButton ID="OneClickbtnAgendamento" runat="server" Text="Agendamento" ReplaceTitleTo="Aguarde..." OnClick="btnAgendamento_OnClick" Width="170px" />
                                </td>


                                <td style="padding-left: 35px; font-weight: bold;">
                                    <uc:OneClickButton ID="OneClickbtnComentar" runat="server" Text="Comentar" ReplaceTitleTo="Aguarde..." OnClick="btnComentar_OnClick" Width="172px" />
                                </td>

                            </tr>

                        </table>
                    </div>
                </div>

            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <dx:ASPxPopupControl ID="frmHistorico" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="frmHistorico"
        HeaderText="Historico" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">

        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <asp:HiddenField runat="server" ID="HiddenField1" />
                <div style="width: 710px; height: 300px; overflow: scroll;">
                    <div>
                        <table style="width: 100%">
                            <tr class="rotulos">
                                <td colspan="3">Histórico de atendimentos de pedidos não autorizados:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <dxwgv:ASPxGridView ID="grdHistorico" runat="server" AutoGenerateColumns="False" Width="660px" Cursor="auto" KeyFieldName="pedidoId">
                                        <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                        <Styles>
                                            <Footer Font-Bold="True">
                                            </Footer>
                                        </Styles>
                                        <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                        <SettingsPager Position="TopAndBottom" PageSize="500" ShowDisabledButtons="False" AlwaysShowPager="True">
                                            <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                        </SettingsPager>
                                        <Settings ShowFilterRow="false" ShowGroupButtons="False" ShowHeaderFilterButton="False" ShowFooter="True" />
                                        <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />

                                        <Columns>
                                            <dxwgv:GridViewDataTextColumn Caption="#" VisibleIndex="0" Width="50">
                                                <DataItemTemplate>
                                                    <%# Container.VisibleIndex + 1 %>
                                                </DataItemTemplate>
                                            </dxwgv:GridViewDataTextColumn>

                                            <dxwgv:GridViewDataTextColumn Caption="pedidos" FieldName="pedidoId" VisibleIndex="0" Width="50">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="Observação" FieldName="observacao" VisibleIndex="0" Width="50">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="dataAtendimento" FieldName="dataAtendimento" VisibleIndex="0" Width="50">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="dataProxAtendimento" FieldName="dataProxAtendimento" VisibleIndex="0" Width="50">
                                            </dxwgv:GridViewDataTextColumn>
                                            <dxwgv:GridViewDataTextColumn Caption="atendente" FieldName="atendente" VisibleIndex="0" Width="50">
                                            </dxwgv:GridViewDataTextColumn>

                                        </Columns>
                                        <StylesEditors>
                                            <Label Font-Bold="True">
                                            </Label>
                                        </StylesEditors>
                                    </dxwgv:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 20px; font-weight: bold;">&nbsp;</td>
                            </tr>


                        </table>
                    </div>
                </div>

            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

</asp:Content>
