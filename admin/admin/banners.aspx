﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="banners.aspx.cs" Theme="Glass" Inherits="admin_banners" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.v11.1.Linq, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxClasses" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxTabControl" Assembly="DevExpress.Web.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="js/jquery.plugin.js"></script>
    <script src="js/jquery.countdown.js"></script>


    <script>

        function setTimer(idElemento, data) {
            var dataAgendamento = new Date(data);
            //var austDay = new Date();
            //austDay = new Date(austDay.getFullYear() + 1, 1 - 1, 26);
            $('#' + idElemento + '').countdown({ until: dataAgendamento, compact: true, description: '' });
            $('#year').text(dataAgendamento.getFullYear());
        }

        function ApplyFilter(dde, dateFrom, dateTo, campo) {
            var colunaSplit = dde.name.split('_');
            var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
            var colunaFiltro = "";
            if (coluna == "3") colunaFiltro = "agendamento";

            var d1 = dateFrom.GetText();
            var d2 = dateTo.GetText();
            if (d1 == "" || d2 == "") return;
            dde.SetText(d1 + "|" + d2);
            grdAgendamentos.AutoFilterByColumn(colunaFiltro, d1 + "|" + d2);
        }
    </script>

    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Banners</asp:Label>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 30px;">

                <dx:ASPxPageControl ID="tabsBanners" runat="server" ActiveTabIndex="0" Width="845px">
                    <TabPages>
                        <dx:TabPage Text="Geral">
                            <ContentCollection>
                                <dx:ContentControl runat="server">

                                    <table style="width: 834px;">
                                        <tr>
                                            <td class="rotulos">
                                                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblAtivos" AutoPostBack="True">
                                                    <asp:ListItem Text="Ativos" Value="ativos" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Agendados" Value="agendados"></asp:ListItem>
                                                    <asp:ListItem Text="Inativos" Value="inativos"></asp:ListItem>
                                                    <asp:ListItem Text="Arquivados" Value="arquivados"></asp:ListItem>
                                                    <asp:ListItem Text="Todos" Value="todos"></asp:ListItem>
                                                </asp:RadioButtonList>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="bannerCad.aspx">
                                                    <div style="float:right;background-color:#316674;color:#ffffff;padding:5px;border-radius: 4px;">Cadastrar novo banner</div>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False"
                                                                DataSourceID="sqlBanners" KeyFieldName="Id" Cursor="auto"
                                                                ClientInstanceName="grd" EnableCallBacks="False" OnHtmlRowPrepared="grd_HtmlRowPrepared" OnCustomCallback="grd_CustomCallback">
                                                                <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                                    AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                                    AllowFocusedRow="True" />
                                                                <SettingsText ConfirmDelete="Tem certeza que deseja excluir?"
                                                                    EmptyDataRow="Nenhum registro encontrado."
                                                                    GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                                <SettingsPager Position="TopAndBottom" PageSize="30"
                                                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                                                </SettingsPager>
                                                                <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                                <TotalSummary>
                                                                    <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem"
                                                                        ShowInColumn="Margem" ShowInGroupFooterColumn="Margem"
                                                                        SummaryType="Average" />
                                                                </TotalSummary>
                                                                <Columns>
                                                                    <dxwgv:GridViewDataTextColumn Caption="ID Banner"
                                                                        FieldName="Id" Name="Id" VisibleIndex="3" Visible="false"
                                                                        Width="100px">
                                                                        <Settings AutoFilterCondition="Contains" />
                                                                        <DataItemTemplate>
                                                                            <asp:TextBox ID="txtIdDoBanner" runat="server" BorderStyle="None"
                                                                                CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Banner"
                                                                        FieldName="Id" Name="Id" VisibleIndex="3"
                                                                        Width="300">
                                                                        <DataItemTemplate>
                                                                            <asp:Image ID="imgPagamento" runat="server"
                                                                                ImageUrl='<%# ConfigurationManager.AppSettings["caminhoCDN"] + (Convert.ToInt32(Eval("local")) == 1 ? "banners\\home\\" : "banners\\") +Eval("foto") %>' Width="100%" />
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Link do Produto" FieldName="linkProduto"
                                                                        VisibleIndex="4" Width="210px">
                                                                        <Settings AutoFilterCondition="Contains" />
                                                                        <DataItemTemplate>
                                                                            <a href='<%#Eval("linkProduto") %>' target="_blank" title="Clique para abrir o link em uma nova aba"><asp:Label ID="linkProduto" Text='<%#Eval("linkProduto") %>' runat="server"></asp:Label></a>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Relevancia" FieldName="relevancia"
                                                                        VisibleIndex="4" Width="50px" SortIndex="0" SortOrder="Ascending">
                                                                        <Settings AutoFilterCondition="Contains" />
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Mobile" FieldName="mobile" CellStyle-HorizontalAlign="Center"
                                                                        UnboundType="Boolean" VisibleIndex="4" Width="40px">
                                                                        <DataItemTemplate>
                                                                            <dxe:ASPxCheckBox ID="ckbMobile" runat="server"
                                                                                Value='<%#Eval("mobile") %>' ValueChecked="True"
                                                                                ValueType="System.Boolean" ValueUnchecked="False">
                                                                            </dxe:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataComboBoxColumn Caption="Local" FieldName="local" VisibleIndex="4" Width="150px" CellStyle-HorizontalAlign="Center">
                                                                        <PropertiesComboBox DropDownStyle="DropDownList" TextField="Text" ValueField="Value">
                                                                            <Items>
                                                                                <dxe:ListEditItem Text="Home" Value="1" />
                                                                                <dxe:ListEditItem Text="Detalhes Produto" Value="2" />
                                                                                <dxe:ListEditItem Text="Busca" Value="3" />
                                                                                <dxe:ListEditItem Text="Lista Categoria" Value="4" />
                                                                            </Items>
                                                                        </PropertiesComboBox>
                                                                        <DataItemTemplate>
                                                                            <asp:DropDownList ID="ddlLocal" runat="server">
                                                                                <asp:ListItem Text="Home" Value="1" />
                                                                                <asp:ListItem Text="Detalhes Produto" Value="2" />
                                                                                <asp:ListItem Text="Busca" Value="3" />
                                                                                <asp:ListItem Text="Lista Categoria" Value="4" />
                                                                            </asp:DropDownList>
                                                                            <asp:HiddenField ID="hfLocal" runat="server" Value='<%# Eval("local") %>' />
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataComboBoxColumn>


                                                                    <dxwgv:GridViewDataComboBoxColumn Caption="Posição" FieldName="posicao" VisibleIndex="4" Width="130px" CellStyle-HorizontalAlign="Center">
                                                                        <PropertiesComboBox DropDownStyle="DropDownList" TextField="SelectStatus" ValueField="SelectStatus">
                                                                            <Items>
                                                                                <dxe:ListEditItem Text="Topo" Value="1" />
                                                                                <dxe:ListEditItem Text="Direita" Value="2" />
                                                                                <dxe:ListEditItem Text="Rodapé" Value="3" />
                                                                                <dxe:ListEditItem Text="Esquerda" Value="4" />
                                                                                <dxe:ListEditItem Text="Conteudo Home - Topo" Value="5" />
                                                                                <dxe:ListEditItem Text="Conteudo Home - Rodapé" Value="6" />
                                                                                <dxe:ListEditItem Text="Topo Split" Value="7" />
                                                                            </Items>
                                                                        </PropertiesComboBox>
                                                                        <DataItemTemplate>
                                                                            <asp:DropDownList ID="ddlPosicao" runat="server">
                                                                                <asp:ListItem Text="Topo" Value="1" />
                                                                                <asp:ListItem Text="Direita" Value="2" />
                                                                                <asp:ListItem Text="Rodapé" Value="3" />
                                                                                <asp:ListItem Text="Esquerda" Value="4" />
                                                                                <asp:ListItem Text="Conteudo Home - Topo" Value="5" />
                                                                                <asp:ListItem Text="Conteudo Home - Rodapé" Value="6" />
                                                                                <asp:ListItem Text="Topo Split" Value="7" />
                                                                            </asp:DropDownList>
                                                                            <asp:HiddenField ID="hfPosicao" runat="server" Value='<%# Eval("posicao") %>' />
                                                                        </DataItemTemplate>

                                                                    </dxwgv:GridViewDataComboBoxColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Ativo" FieldName="ativo" CellStyle-HorizontalAlign="Center"
                                                                        UnboundType="Boolean" VisibleIndex="2" Width="40px">
                                                                        <DataItemTemplate>
                                                                            <dxe:ASPxCheckBox ID="ckbAtivo" runat="server"
                                                                                Value='<%#Eval("ativo") %>' ValueChecked="True"
                                                                                ValueType="System.Boolean" ValueUnchecked="False">
                                                                            </dxe:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Arquivado" FieldName="arquivado" CellStyle-HorizontalAlign="Center"
                                                                        UnboundType="Boolean" VisibleIndex="10" Width="40px" Name="colArquivado">
                                                                        <DataItemTemplate>
                                                                            <dxe:ASPxCheckBox ID="ckbArquivado" runat="server"
                                                                                Value='<%#Eval("arquivado") %>' ValueChecked="True"
                                                                                ValueType="System.Boolean" ValueUnchecked="False">
                                                                            </dxe:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>

                                                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="1"
                                                                        Width="45px">
                                                                        <DataItemTemplate>
                                                                            <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="images/btEditar.jpg" ToolTip="Alterar"
                                                                                NavigateUrl='<%# "bannerAlt.aspx?bannerId=" + Eval("id") %>'>Alterar</asp:HyperLink>
                                                                            <asp:HiddenField ID="hfIdBanner" runat="server" Value='<%# Eval("id") %>' />
                                                                        </DataItemTemplate>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataHyperLinkColumn>
                                                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Arquivar" VisibleIndex="11" Width="40px" Name="colArquivar">
                                                                        <DataItemTemplate>
                                                                            <asp:ImageButton runat="server" ImageUrl="images/btExcluir.jpg" CommandArgument='<%#Eval("id")%>' ID="btArquivar" OnCommand="btArquivar_Command" OnClientClick="return confirm('Deseja arquivar esse banner?')" ToolTip="Arquivar Banner" />
                                                                        </DataItemTemplate>
                                                                        <CellStyle HorizontalAlign="Center">
                                                                        </CellStyle>
                                                                    </dxwgv:GridViewDataHyperLinkColumn>
                                                                    <dxwgv:GridViewDataTextColumn Caption="Excluir" Name="excluir" VisibleIndex="26" Width="42px" Visible="false">
                                                                        <DataItemTemplate>
                                                                            <dxe:ASPxCheckBox ID="ckbExcluir" runat="server" ValueChecked="True" ValueType="System.String" ValueUnchecked="False">
                                                                            </dxe:ASPxCheckBox>
                                                                        </DataItemTemplate>
                                                                    </dxwgv:GridViewDataTextColumn>
                                                                </Columns>
                                                            </dxwgv:ASPxGridView>

                                                <dx:LinqServerModeDataSource ID="sqlBanners" runat="server" ContextTypeName="dbCommerceDataContext" TableName="viewListaBanners" OnSelecting="sqlBanners_Selecting" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: right;">
                                                <img alt="Salvar" src="images/btSalvar.jpg" onclick="grd.PerformCallback(this.value);" style="cursor: pointer;" />
                                            </td>
                                        </tr>
                                    </table>

                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                        <dx:TabPage Text="Agendamentos">
                            <ContentCollection>
                                <dx:ContentControl runat="server">

                                    <div class="tituloPaginas">
                                        <asp:Label ID="Label1" runat="server">Ações a Executar</asp:Label>
                                    </div>

                                    <asp:UpdatePanel runat="server" ID="updGrdAgendamento">
                                        <ContentTemplate>
                                            <dxwgv:ASPxGridView ID="grdAgendamentos" runat="server" AutoGenerateColumns="False"
                                                    KeyFieldName="idBannerAgendamento" Cursor="auto" 
                                                    ClientInstanceName="grdAgendamentos" EnableCallBacks="False">
                                                    <SettingsBehavior AutoFilterRowInputDelay="10200" ConfirmDelete="True"
                                                        AutoExpandAllGroups="True" ColumnResizeMode="Control" AllowGroup="False"
                                                        AllowFocusedRow="True" />
                                                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." GroupPanel="Arraste uma coluna aqui para agrupar." />
                                                    <SettingsPager Position="TopAndBottom" PageSize="30" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" Text="Página {0} de {1} ({2} registros encontrados)" />
                                                    </SettingsPager>
                                                    <Settings ShowFilterRow="True" ShowGroupedColumns="True" ShowFooter="True" />
                                                    <TotalSummary>
                                                        <dxwgv:ASPxSummaryItem DisplayFormat="0" FieldName="margem" ShowInColumn="Margem" ShowInGroupFooterColumn="Margem" SummaryType="Average" />
                                                    </TotalSummary>
                                                    <SettingsEditing EditFormColumnCount="4"
                                                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                        PopupEditFormWidth="700px" Mode="Inline" />
                                                    <Columns>
                                                    <dxwgv:GridViewDataHyperLinkColumn Caption="Alterar" VisibleIndex="0"
                                                        Width="45px">
                                                        <DataItemTemplate>
                                                            <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="images/btEditar.jpg" ToolTip="Alterar"
                                                                NavigateUrl='<%# "bannerAlt.aspx?bannerId=" + Eval("id") %>'>Alterar</asp:HyperLink>
                                                            <asp:HiddenField ID="hfIdBanner" runat="server" Value='<%# Eval("id") %>' />
                                                        </DataItemTemplate>
                                                        <CellStyle HorizontalAlign="Center">
                                                        </CellStyle>
                                                    </dxwgv:GridViewDataHyperLinkColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="ID Banner"
                                                        FieldName="idBannerAgendamento" Name="Id" VisibleIndex="0" Visible="false"
                                                        Width="100px">
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <DataItemTemplate>
                                                            <asp:TextBox ID="txtIdDoBanner" runat="server" BorderStyle="None"
                                                                CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>
                                                        </DataItemTemplate>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="Banner"
                                                        FieldName="foto" Name="foto" VisibleIndex="0"
                                                        Width="135">
                                                        <DataItemTemplate>
                                                            <asp:Image ID="imgPagamento" runat="server"
                                                                ImageUrl='<%# ConfigurationManager.AppSettings["caminhoCDN"] + (Convert.ToInt32(Eval("local")) == 1 ? "banners\\home\\" : "banners\\") +Eval("foto") %>' Width="300px" />
                                                        </DataItemTemplate>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="ID Banner"
                                                        FieldName="Id" Name="Id" VisibleIndex="3" Visible="false"
                                                        Width="100px">
                                                        <Settings AutoFilterCondition="Contains" />
                                                        <DataItemTemplate>
                                                            <asp:TextBox ID="txtIdDoBanner" runat="server" BorderStyle="None"
                                                                CssClass="campos" Text='<%# Bind("id") %>' Width="100%"></asp:TextBox>

                                                        </DataItemTemplate>
                                                    </dxwgv:GridViewDataTextColumn>
                                                    <dxwgv:GridViewDataTextColumn Caption="Mobile" FieldName="mobile" CellStyle-HorizontalAlign="Center"
                                                        UnboundType="Boolean" VisibleIndex="4" Width="40px">
                                                        <DataItemTemplate>
                                                            <dxe:ASPxCheckBox ID="ckbMobile" runat="server"
                                                                Value='<%#Eval("mobile") %>' ValueChecked="True"
                                                                ValueType="System.Boolean" ValueUnchecked="False">
                                                            </dxe:ASPxCheckBox>
                                                        </DataItemTemplate>
                                                    </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Entrada" FieldName="dataEntrada" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("dataEntrada") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Data Saída" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("dataSaida") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Inicio Contador" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("inicioContador") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Fim Contador" FieldName="dataSaida" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("fimContador") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>
                                                        <dxwgv:GridViewDataTextColumn Caption="Posicao" FieldName="posicao" CellStyle-HorizontalAlign="Center" UnboundType="String"
                                                            Width="50px">
                                                            <DataItemTemplate>
                                                                <asp:Label ID="lblTempoParaAtivacao" runat="server" Text='<%# Eval("posicao") %>'></asp:Label>
                                                            </DataItemTemplate>
                                                        </dxwgv:GridViewDataTextColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Entrada" FieldName="queueConcluidoEntrada" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Saida" FieldName="queueConcluidoSaida" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>

                                                        <dxwgv:GridViewDataCheckColumn Caption="Contador" FieldName="queueConcluidoContador" CellStyle-HorizontalAlign="Center"
                                                            Width="50px">
                                                        </dxwgv:GridViewDataCheckColumn>
                                                    </Columns>
                                                </dxwgv:ASPxGridView>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>



                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>

                    </TabPages>
                </dx:ASPxPageControl>

            </td>
        </tr>
    </table>
</asp:Content>

