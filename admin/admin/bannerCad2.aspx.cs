﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TreeNode = System.Web.UI.WebControls.TreeNode;
using TreeNodeCollection = System.Web.UI.WebControls.TreeNodeCollection;

public partial class admin_bannerCad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
        if (!IsPostBack)
        {
            PopulateRootLevel();

            txtAgendamentoDataAtivacao.Text = DateTime.Now.Date.ToString("d");
            txtAgendamentoHoraAtivacao.Text = DateTime.Now.AddMinutes(20).ToString("t");

            txtAgendamentoDataDesativacao.Text = DateTime.Now.AddDays(1).ToString("d");
            txtAgendamentoHoraDesativacao.Text = "00:00";

            Carrega_ddlEstiloContador();
        }

    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                tbBanner banner = new tbBanner
                {
                    linkProduto = txtLinkProduto.Text,
                    idProduto = txtIdProduto.Text != "" ? Convert.ToInt32(txtIdProduto.Text) : 0,
                    foto = FileUploadBanner.FileName,
                    visualizacoes = 0,
                    clicks = 0,
                    local = Convert.ToInt32(ddlLocal.SelectedValue),
                    posicao = Convert.ToInt32(ddlPosicao.SelectedValue),
                    ativo = false,
                    mobile = ckbBannerMobile.Checked,
                    teste = ckbEmTeste.Checked,
                    filtro = rblFiltro.SelectedItem == null ? 0 : Convert.ToInt32(rblFiltro.SelectedItem.Value)
                };

                if (String.IsNullOrEmpty(txtRelevancia.Text))
                    banner.relevancia = 1;
                else
                    banner.relevancia = Convert.ToDecimal(txtRelevancia.Text);

                data.tbBanners.InsertOnSubmit(banner);
                data.SubmitChanges();

                var log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                log.descricoes.Add("Cadastro de Banner id: " + banner.Id);
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                log.InsereLog();

                var bannerRenomeia = (from b in data.tbBanners where b.Id == banner.Id select b).FirstOrDefault();

                if (bannerRenomeia != null)
                {
                    bannerRenomeia.foto = banner.Id + FileUploadBanner.FileName;
                    data.SubmitChanges();
                }

                var deletaRelacaoFiltro = (from c in data.tbBannersFiltros where c.bannerId == banner.Id select c);
                foreach (var bannerFiltro in deletaRelacaoFiltro)
                {
                    data.tbBannersFiltros.DeleteOnSubmit(bannerFiltro);
                    data.SubmitChanges();
                }

                foreach (var bannerFiltro in from TreeNode node in treeCategorias.CheckedNodes select new tbBannersFiltro { bannerId = banner.Id, filtro = Convert.ToInt32(node.Value) })
                {
                    data.tbBannersFiltros.InsertOnSubmit(bannerFiltro);
                    data.SubmitChanges();
                }


                string caminhoBannerLocalParcial = (banner.local == 1 ? "banners\\home\\" : "banners\\");
                string caminhoBannerRemotoParcial = (banner.local == 1 ? "banners/home/" : "banners/");
                string caminhoBanner = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoBannerLocalParcial;
                string nomeArquivoOriginal = banner.Id + FileUploadBanner.FileName;
                string nomeArquivoWebp = banner.Id + FileUploadBanner.FileName.Split('.')[0] + ".webp";

                FileUploadBanner.PostedFile.SaveAs(caminhoBanner + nomeArquivoOriginal);
                //uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoOriginal);

                bool webpCarregado = true;
                try
                {
                    Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
                }
                catch(Exception ex)
                {
                    webpCarregado = false;
                }
                
                if (webpCarregado)
                {
                    System.Drawing.Bitmap mBitmap;
                    FileStream outStream = new FileStream(caminhoBanner + nomeArquivoWebp, FileMode.Create);
                    using (Stream BitmapStream = System.IO.File.Open(caminhoBanner + nomeArquivoOriginal, System.IO.FileMode.Open))
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                        mBitmap = new System.Drawing.Bitmap(img);
                        var encoder = new Imazen.WebP.SimpleEncoder();
                        encoder.Encode(mBitmap, outStream, 70);
                        outStream.Close();
                        uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, nomeArquivoWebp);
                    }
                }

                #region compactar Imagem Kraken
                 KrakenConfig _kConfig = new KrakenConfig();
                 string key = _kConfig.APIKey;
                 string secret = _kConfig.APISecret;

                 Kraken k = new Kraken(key, secret);

                 KrakenRequest kr = new KrakenRequest { Lossy = false };


                 if (_kConfig.UseCallbacks)
                 {
                     kr.CallbackUrl = _kConfig.CallbackURL;
                 }
                 else
                 {
                     kr.Wait = true;
                 }

                 // Can't trust the length of Stream. Converting to a MemoryStream
                 using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                 {
                     FileUploadBanner.FileContent.CopyTo(ms);

                     kr.File = ms.ToArray();
                 }

                 kr.s3_store = new KrakenRequestS3Amazon
                 {
                     S3Key = "AKIAIP7IE6WVNK2K6TIQ",
                     S3Secret = "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",
                     S3Bucket = "cdn2.graodegente.com.br",
                     S3Region = "sa-east-1",
                     S3Path =
                         Convert.ToInt32(ddlLocal.SelectedValue) == 1
                             ? ("banners/home/" + banner.Id + FileUploadBanner.FileName)
                             : ("banners/" + banner.Id + FileUploadBanner.FileName)
                 };

                 var response = k.Upload(kr, FileUploadBanner.PostedFile.FileName, FileUploadBanner.PostedFile.ContentType);

                 if (response.Success == false || response.Error != null)
                 {

                 }

                 using (var webClient = new System.Net.WebClient())
                 using (var stream = webClient.OpenRead(response.KrakedUrl))
                 {
                     webClient.DownloadFile(response.KrakedUrl, (banner.local == 1 ? ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\home\\" : ConfigurationManager.AppSettings["caminhoFisico"] + "banners\\") + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                 }

                 #region Tamanho metade

                 if (File.Exists(ConfigurationManager.AppSettings["caminhoFisico"] + (banner.local == 1 ? "banners\\home\\" : "banners\\") + bannerRenomeia.foto))
                 {
                     #region Formato Metade

                     int width = 0;
                     int heigth = 0;

                     System.Drawing.Image img = System.Drawing.Image.FromFile(ConfigurationManager.AppSettings["caminhoFisico"] + (banner.local == 1 ? "banners\\home\\" : "banners\\") + bannerRenomeia.foto);

                     width = img.Width / 2;
                     heigth = img.Height / 2;
                     img.Dispose();

                     #endregion Formato Metade

                     FileStream imageData = File.Open((ConfigurationManager.AppSettings["caminhoFisico"] + (banner.local == 1 ? "banners\\home\\" : "banners\\") + bannerRenomeia.foto), FileMode.Open);

                     // Can't trust the length of Stream. Converting to a MemoryStream
                     using (MemoryStream ms = new MemoryStream())
                     {
                         imageData.CopyTo(ms);
                         kr.File = ms.ToArray();
                     }

                     kr.s3_store = new KrakenRequestS3Amazon
                     {
                         S3Key = "AKIAIP7IE6WVNK2K6TIQ",
                         S3Secret = "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",
                         S3Bucket = "cdn2.graodegente.com.br",
                         S3Region = "sa-east-1",
                         S3Path = (banner.local == 1 ? "banners/home/m" : "banners/m") + bannerRenomeia.foto
                     };

                     kr.Resize = new KrakenResizeImage { Width = width, Height = heigth, Strategy = "auto" };

                     response = k.Upload(kr, (ConfigurationManager.AppSettings["caminhoFisico"] + (banner.local == 1 ? "banners\\home\\m" : "banners\\m") + bannerRenomeia.foto + ""), ".jpg");

                     if (response.Success == false || response.Error != null)
                     {

                     }

                     using (var webClient = new System.Net.WebClient())
                     using (var stream = webClient.OpenRead(response.KrakedUrl))
                     {

                         imageData.Dispose();

                         webClient.DownloadFile(response.KrakedUrl, ConfigurationManager.AppSettings["caminhoFisico"] + (banner.local == 1 ? "banners\\home\\" : "banners\\") + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                     }

                    if (webpCarregado)
                    {
                        System.Drawing.Bitmap mBitmap;
                        FileStream outStream = new FileStream(caminhoBanner + "m" + nomeArquivoWebp, FileMode.Create);
                        using (Stream BitmapStream = System.IO.File.Open(caminhoBanner + "m" + nomeArquivoOriginal, System.IO.FileMode.Open))
                        {
                            System.Drawing.Image imgGerada = System.Drawing.Image.FromStream(BitmapStream);

                            mBitmap = new System.Drawing.Bitmap(imgGerada);
                            var encoder = new Imazen.WebP.SimpleEncoder();
                            encoder.Encode(mBitmap, outStream, 70);
                            outStream.Close();
                            uploadImagemAmazonS3(caminhoBannerLocalParcial, caminhoBannerRemotoParcial, "m" + nomeArquivoWebp);
                        }
                    }
                }

                 #endregion Tamanho metade

                 #endregion compactar Imagem Kraken

                #region Agendamento do Banner

                if (ckbAgendamentoAtivacao.Checked)
                {
                    var validarAgendamento = Convert.ToDateTime(txtAgendamentoHoraAtivacao.Text).TimeOfDay - DateTime.Now.TimeOfDay;
                    if (Convert.ToDateTime(txtAgendamentoDataAtivacao.Text).Date <= DateTime.Now.Date && validarAgendamento.Minutes < 10)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('O agendamento não foi realizado pois o tempo estipulado é muito curto!');", true);
                    }
                    else
                    {
                        DateTime dataAgendamentoAtivacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataAtivacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraAtivacao.Text).TimeOfDay);
                        var agendamentoAtivacao = new tbQueue
                        {
                            andamento = false,
                            concluido = false,
                            tipoQueue = 29,
                            idRelacionado = banner.Id,
                            mensagem = "",
                            agendamento = dataAgendamentoAtivacao
                        };

                        data.tbQueues.InsertOnSubmit(agendamentoAtivacao);
                        data.SubmitChanges();

                        log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                        log.descricoes.Add("Ativação agendada - " + dataAgendamentoAtivacao.ToString("G") + " - do banner id: " + banner.Id);
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = agendamentoAtivacao.idQueue, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                        log.InsereLog();
                    }

                }

                if (ckbAgendamentoDesativacao.Checked)
                {
                    DateTime dataAgendamentoDesativacao = Convert.ToDateTime(Convert.ToDateTime(txtAgendamentoDataDesativacao.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtAgendamentoHoraDesativacao.Text).TimeOfDay);
                    var agendamentoDesativacao = new tbQueue
                    {
                        andamento = false,
                        concluido = false,
                        tipoQueue = 28,
                        idRelacionado = banner.Id,
                        mensagem = "",
                        agendamento = dataAgendamentoDesativacao
                    };

                    data.tbQueues.InsertOnSubmit(agendamentoDesativacao);
                    data.SubmitChanges();

                    log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                    log.descricoes.Add("Desativação agendada - " + dataAgendamentoDesativacao.ToString("G") + " - do banner id: " + banner.Id);
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = agendamentoDesativacao.idQueue, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Banner });
                    log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                    log.InsereLog();
                }





                #endregion Agendamento do Banner

                #region Contador no Banner

                if (ckbContador.Checked)
                {
                    var validarContador = Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay - DateTime.Now.TimeOfDay;
                    var dataInicioContador = Convert.ToDateTime(Convert.ToDateTime(txtInicioContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtInicioContadorHora.Text).TimeOfDay);
                    var dataFimContador = Convert.ToDateTime(Convert.ToDateTime(txtFimContadorData.Text).ToString("yyyy-MM-dd") + " " + Convert.ToDateTime(txtFimContadorHora.Text).TimeOfDay);

                    if ((Convert.ToDateTime(txtInicioContadorData.Text).Date <= DateTime.Now.Date && validarContador.Minutes < 10) || (dataFimContador < dataInicioContador))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('O contador não foi informado pois o tempo estipulado está incorreto!');", true);
                    }
                    else
                    {
                        banner.inicioContador = dataInicioContador;
                        banner.fimContador = dataFimContador;
                        banner.estilo = Convert.ToInt32(ddlLayoutContador.SelectedValue);

                        data.SubmitChanges();

                        log = new rnLog { usuario = rnUsuarios.retornaNomeUsuarioLogado() };
                        log.descricoes.Add("Contador inicio - " + dataInicioContador.ToString("G") + " fim - " + dataFimContador.ToString("G") + " - no banner id: " + banner.Id);
                        log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = banner.Id, tipoRelacionado = rnEnums.TipoRegistroRelacionado.Contador });
                        log.tiposOperacao.Add(rnEnums.TipoOperacao.Banner);
                        log.InsereLog();

                        ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('O contador informado com sucesso!');", true);
                    }
                }

                #endregion Contador no Banner

                var ativarBanner = (from c in data.tbBanners where c.Id == banner.Id select c).First();
                ativarBanner.ativo = ckbAtivo.Checked;
                data.SubmitChanges();
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Banner cadastrado com sucesso!');", true);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Erro!\\nnão foi possivél cadastrar o banner, tente novamente!');", true);
        }

    }

    #region categorias
    private void PopulateRootLevel()
    {
        treeCategorias.Nodes.Clear();
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPaiPorSiteId(1).Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        int bannerId = Convert.ToInt32(Request.QueryString["bannerId"]);

        using (var data = new dbCommerceDataContext())
        {
            var categoriasSelecionadas = (from c in data.tbBannersFiltros where c.Id == bannerId select new { c.filtro }).ToList();

            foreach (DataRow dr in dt.Rows)
            {
                TreeNode tn = new TreeNode();
                tn.Text = dr["categoriaNome"].ToString();
                tn.Value = dr["categoriaId"].ToString();

                //desabilita os links do treeview
                tn.SelectAction = TreeNodeSelectAction.None;

                //checo as categorias que o banner esta relacionado

                foreach (var categoria in categoriasSelecionadas)
                {
                    if (tn.Value == categoria.filtro.ToString())
                        tn.Checked = true;
                }
                nodes.Add(tn);

                //If node has child nodes, then enable on-demand populating
                tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
            }
        }

    }
    #endregion

    protected void ddlEstiloContador_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                int idTipoEstilo = Convert.ToInt32(ddlEstiloContador.SelectedValue);

                var dados = (from c in data.tbEstilos where c.idTipoEstilo == idTipoEstilo orderby c.nome select c).ToList();
                ddlLayoutContador.DataSource = dados;
                ddlLayoutContador.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    private void Carrega_ddlEstiloContador()
    {
        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var dados = (from c in data.tbTipoEstilos orderby c.nome select c).ToList();
                ddlEstiloContador.DataSource = dados;
                ddlEstiloContador.DataBind();

                int idTipoEstilo = Convert.ToInt32(ddlEstiloContador.SelectedValue);

                var estilos = (from c in data.tbEstilos where c.idTipoEstilo == idTipoEstilo orderby c.nome select c).ToList();
                ddlLayoutContador.DataSource = estilos;
                ddlLayoutContador.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }




    private void uploadImagemAmazonS3(string caminhoLocal, string caminhoRemoto, string foto)
    {

        string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
        string bucketName = "cdn2.graodegente.com.br";
        string filePath = ConfigurationManager.AppSettings["caminhoFisico"] + caminhoLocal + foto;
        string fileContentType = "image/jpg";
        S3CannedACL fileCannedACL = S3CannedACL.PublicReadWrite;

        string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
        string secretAccessKey = "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";

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.ServiceURL = s3ServiceUrl;
            config.CommunicationProtocol = Protocol.HTTP;

            using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
            {
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = bucketName,
                    CannedACL = fileCannedACL,
                    Key = caminhoRemoto + foto,
                    FilePath = filePath,
                    ContentType = fileContentType
                };

                PutObjectResponse response = s3Client.PutObject(request);

            }

        }
        catch (AmazonS3Exception s3Exception)
        {
            Console.WriteLine(s3Exception.Message, s3Exception.InnerException);

            Console.ReadKey();
        }
    }

}