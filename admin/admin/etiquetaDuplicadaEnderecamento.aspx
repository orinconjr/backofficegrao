﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="etiquetaDuplicadaEnderecamento.aspx.cs" Inherits="admin_etiquetaDuplicadaEnderecamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Etiqueta Duplicada/Enviada/Localizar - Endereçamento</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <table style="float: left; padding-left: 33px;">
                            <tr>
                                <td>
                                    <asp:RadioButtonList runat="server" ID="rbOpcoes" RepeatDirection="Horizontal" Style="float: left;">
                                        <asp:ListItem Value="1" Selected="True">Etiqueta Duplicada/Enviada</asp:ListItem>
                                        <asp:ListItem Value="2">Localizar Etiqueta</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div>Informe o numero da etiqueta:</div>
                                    <div style="float: left;">
                                        <asp:TextBox ID="txtNumeroEtiqueta" runat="server" MaxLength="10"></asp:TextBox>
                                    </div>
                                    <div style="float: left;">
                                        <asp:ImageButton ID="imbPesquisar" runat="server" ImageUrl="~/admin/images/btPesquisar.jpg" OnClick="imbPesquisar_Click" />
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RequiredFieldValidator ID="rqvEtiqueta" runat="server"
                                        ControlToValidate="txtNumeroEtiqueta" Display="None"
                                        ErrorMessage="Preencha o numero da etiqueta."
                                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                                    <asp:ValidationSummary ID="llds" runat="server" DisplayMode="List"
                                        ShowMessageBox="True" ShowSummary="False" />
                                    <br />
                                    <br />

                                    <asp:Panel ID="pnlEtiquetas" runat="server" Visible="false">
                                        <table>
                                            <tr style="background-color: #808080; color: white; font-weight: bold;">
                                                <td>Etiqueta
                                                </td>
                                                <td>Expedição
                                                </td>
                                                <td>Produto
                                                </td>
                                                <td style="width: 170px;">Data Endereçamento
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptEtiquetasDuplicadas" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("etiqueta") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("usuarioexpedicao") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("produtoNome") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("dataEnderecamento") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">
                                    <asp:Button ID="btnLiberarSenha" runat="server" Text="Liberar Senha" Visible="false" ToolTip="irá marcar o endereçamento da etiqueta como concluído" OnClick="btnLiberarSenha_Click" OnClientClick="return confirm('Confirma a liberação da senha?')" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="pnlLocalizarEtiqueta" runat="server" Visible="False">
                                        <table>
                                            <tr style="background-color: #808080; color: white; font-weight: bold;">
                                                <td>Etiqueta
                                                </td>
                                                <td>Expedição
                                                </td>
                                                <td>Produto
                                                </td>
                                                <td style="width: 170px;">Carrinho/Caixa/Local
                                                </td>
                                                <td>Data Endereçamento
                                                </td>
                                            </tr>
                                            <asp:Repeater ID="rptLocalizarEtiqueta" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <%#Eval("etiqueta") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("usuarioexpedicao") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("produtoNome") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("enderecamentoTransferencia") %>
                                                        </td>
                                                        <td>
                                                            <%#Eval("dataEnderecamento") %>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

</asp:Content>

