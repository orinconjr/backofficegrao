﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class admin_popCartao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpCookie token = new HttpCookie("token");
        token = Request.Cookies["token"];
        HttpCookie usuarioLogadoId = new HttpCookie("usuarioLogadoId");
        usuarioLogadoId = Request.Cookies["usuarioLogadoId"];
        if (token != null && usuarioLogadoId != null)
        {
            int idUsuario = int.Parse(usuarioLogadoId.Value.ToString());
            var usuario = rnUsuarios.validaToken(idUsuario, token.Value.ToString());
            if (!usuario.valido)
            {
                var log = new rnLog();
                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                log.descricoes = new List<string>();
                log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(Request.QueryString["idPedidoPagamento"]), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                log.tiposOperacao = new List<rnEnums.TipoOperacao>();
                log.descricoes.Add("Tentativa de acesso à dados do cartão");
                log.descricoes.Add(Request.UserHostAddress.ToString());

                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.close();</script>");
            }
        }
        else
        {
            var log = new rnLog();
            log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
            log.descricoes = new List<string>();
            log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
            log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(Request.QueryString["idPedidoPagamento"]), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
            log.tiposOperacao = new List<rnEnums.TipoOperacao>();
            log.descricoes.Add("Tentativa de acesso à dados do cartão");
            log.descricoes.Add(Request.UserHostAddress.ToString());

            Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
            Response.Write("<script>window.close();</script>");
        }

        if (!Page.IsPostBack)
        {
            if (usuarioLogadoId != null)
            {
                string paginaSolicitada = Request.Path.Substring(Request.Path.LastIndexOf("/") + 1);
                if (rnUsuarios.usuarioPaginasPermitidaSeleciona_PorUsuarioIdPaginaPermitidaNome(int.Parse(usuarioLogadoId.Value.ToString()), paginaSolicitada).Tables[0].Rows.Count == 0)
                {
                    Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                    Response.Write("<script>window.close();</script>");
                    var log = new rnLog();
                    log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    log.descricoes = new List<string>();
                    log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(Request.QueryString["idPedidoPagamento"]), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                    log.tiposOperacao = new List<rnEnums.TipoOperacao>();
                    log.descricoes.Add("Tentativa de acesso à dados do cartão");
                    log.descricoes.Add(Request.UserHostAddress.ToString());

                    
                    log.InsereLog();
                }
                else
                {
                    montaCartao();
                    var log = new rnLog();
                    log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                    log.descricoes = new List<string>();
                    log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
                    log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(Request.QueryString["idPedidoPagamento"]), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                    log.tiposOperacao = new List<rnEnums.TipoOperacao>();
                    log.descricoes.Add("Dados do cartão");
                    log.descricoes.Add(Request.UserHostAddress.ToString());
                    log.InsereLog();
                }
            }
            else
            {
                Response.Write("<script>alert('Você não tem acesso a essa área.');</script>");
                Response.Write("<script>window.close();</script>");
                var log = new rnLog();
                log.usuario = rnUsuarios.retornaNomeUsuarioLogado();
                log.descricoes = new List<string>();
                log.tiposRelacionados = new List<rnEnums.TipoRelacionadoIds>();
                log.tiposRelacionados.Add(new rnEnums.TipoRelacionadoIds() { idRegistroRelacionado = Convert.ToInt32(Request.QueryString["idPedidoPagamento"]), tipoRelacionado = rnEnums.TipoRegistroRelacionado.Pedido });
                log.tiposOperacao = new List<rnEnums.TipoOperacao>();
                log.descricoes.Add("Tentativa de acesso à dados do cartão");
                log.descricoes.Add(Request.UserHostAddress.ToString());
                log.InsereLog();
            }
        }
    }

    public void montaCartao()
    {
        int idPedidoPagamento = 0;
        int.TryParse(Request.QueryString["idPedidoPagamento"], out idPedidoPagamento);

        var data = new dbCommerceDataContext();
        var pagamento = (from c in data.tbPedidoPagamentos where c.idPedidoPagamento == idPedidoPagamento select c).FirstOrDefault();
        if (pagamento != null && pagamento.condicaoDePagamentoId == 22)
        {
            string numeroCartao = pagamento.numeroCartao.Replace(" ", "");
            lblNumeroDoCartao.Text = numeroCartao;

            lblNomeDoCartao.Text = pagamento.nomeCartao;

            lblValidade.Text = pagamento.validadeCartao;
            lblCodDeSeguranca.Text = pagamento.codSegurancaCartao;
            /*string numeroFormatado = "";
            long numeroFormatadoLong = 0;
            Int64.TryParse(numeroCartao, out numeroFormatadoLong);

            if(numeroFormatadoLong > 0) numeroFormatado = String.Format("{0:0000 0000 0000 0000}", Convert.ToInt64(numeroCartao));
            if (!string.IsNullOrEmpty(numeroFormatado)) numeroCartao = numeroFormatado;
            lblNumeroDoCartao.Text = numeroCartao;*/
        }

    }
}
