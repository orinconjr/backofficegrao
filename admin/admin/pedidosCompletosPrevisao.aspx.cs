﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHtmlEditor.Internal;

public partial class admin_pedidosCompletosPrevisao : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        fillGrid();
    }

    private void fillGrid()
    {
        var hoje = DateTime.Now;
        var ontem = DateTime.Now.AddDays(-1);
        var pedidosDc = new dbCommerceDataContext();
        var itensPedidosDc = new dbCommerceDataContext();

        List<int> pedidosSeparacao = new List<int>();
        pedidosSeparacao.AddRange((from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 select c.pedidoId).ToList());
        var pedidosSeparacaoLista = (from c in pedidosDc.tbPedidos where c.statusDoPedido == 11 select new { c.pedidoId, c.prazoFinalPedido }).ToList();
        var pedidosItensFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems
                                      where c.tbPedido.statusDoPedido == 11 select c).ToList();
        var pedidosItens = (from c in itensPedidosDc.tbItensPedidos where c.tbPedido.statusDoPedido == 11 select c).ToList();
        var pedidosSepacaracaoCompletos = pedidosDc.admin_pedidosCompletosSeparacaoEstoque().Select(x => x.pedidoId);
        var pedidosSepacaracaoCompletos2 = rnPedidos.verificaPedidosCompletos().Select(x => x.pedidoId);

        var pedidos = (from c in pedidosSeparacaoLista
                       select
                           new
                           {
                               c.pedidoId,
                               dataFornecedor = pedidosItensFornecedor.Where(x => x.idPedido == c.pedidoId).OrderByDescending(x => x.tbPedidoFornecedor.dataLimite).FirstOrDefault() == null ?
                               hoje :
                               pedidosItensFornecedor.Where(x => x.idPedido == c.pedidoId).OrderByDescending(x => x.tbPedidoFornecedor.dataLimite).FirstOrDefault().tbPedidoFornecedor.dataLimite,
                               dataItem = pedidosSeparacaoLista.Where(x => x.pedidoId == c.pedidoId && x.prazoFinalPedido != null).OrderByDescending(x => x.prazoFinalPedido).FirstOrDefault() == null ?
                               hoje : (DateTime)pedidosSeparacaoLista.Where(x => x.pedidoId == c.pedidoId && x.prazoFinalPedido != null).OrderByDescending(x => x.prazoFinalPedido).FirstOrDefault().prazoFinalPedido,
                               produtosEntregues = pedidosItensFornecedor.Count(x => x.idPedido == c.pedidoId && x.entregue)
                           }).ToList();
        var pedidosDatas = (from c in pedidos select new { c.pedidoId, 
            data = c.dataFornecedor.Date > c.dataItem.Date ?
            c.dataFornecedor :
            (int)c.dataItem.DayOfWeek == 0 ? c.dataItem.AddDays(1) :
            (int)c.dataItem.DayOfWeek == 6 ? c.dataItem.AddDays(2) :
            c.dataItem,
            c.produtosEntregues
        }).ToList();
        pedidosDatas = (from c in pedidosDatas select new { c.pedidoId, data = c.data.Date < hoje.Date ? ontem : c.data, c.produtosEntregues }).ToList();
        pedidosDatas = pedidosDatas.Distinct().ToList();


        var pedidosItensNaoEntregues = (from c in pedidosDc.tbPedidoFornecedorItems where c.entregue == false select new
        {
            c.idPedidoFornecedorItem,
            c.tbPedidoFornecedor.dataLimite
        }).ToList();
        pedidosItensNaoEntregues = (from c in pedidosItensNaoEntregues select new { c.idPedidoFornecedorItem, dataLimite = c.dataLimite.Date < hoje.Date ? ontem : c.dataLimite}).ToList();


        var pedidosDatasSeparacao = (from c in pedidosDatas join d in pedidosSepacaracaoCompletos2 on c.pedidoId equals d select c).Distinct().ToList();
        var datas = (from c in pedidosDatas select new { data = c.data.Date }).Distinct().ToList();
        var datasPedidos = (from c in datas
                            select new
                            {
                                data = c.data,
                                pedidosCompletos = (from d in pedidosDatasSeparacao where d.data.Date == c.data.Date select d).Count(),
                                pedidos = (from d in pedidosDatas where d.data.Date == c.data.Date select d).Count(),
                                produtosEntregues = (from d in pedidosDatas where d.data.Date == c.data.Date select d).Any() ?
                                (from d in pedidosDatas where d.data.Date == c.data.Date select d).Sum(x => x.produtosEntregues) : 0,
                                produtosNaoEntregues = (from d in pedidosItensNaoEntregues where d.dataLimite.Date == c.data.Date select d).Any() ?
                                (from d in pedidosItensNaoEntregues where d.dataLimite.Date == c.data.Date select d).Count() : 0
                            }).ToList();
        var datasPedidosFinal = (from c in datasPedidos
                            select new
                            {
                                c.data,
                                c.pedidos,
                                c.pedidosCompletos,
                                pedidosNaoEntregues = c.pedidos - c.pedidosCompletos,
                                c.produtosEntregues,
                                c.produtosNaoEntregues
                            }).ToList();
        grd.DataSource = datasPedidosFinal.OrderBy(x => x.data);
        if (!Page.IsPostBack && !Page.IsCallback) grd.DataBind();
    }
    protected void lstDatas_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {
        var hdfPedidoId = (HiddenField) e.Item.FindControl("hdfPedidoId");
        int idPedido = Convert.ToInt32(hdfPedidoId.Value);
        var litRomaneios = (Literal)e.Item.FindControl("litRomaneios");
        var data = new dbCommerceDataContext();
        var pedidosFornecedor = (from c in data.tbPedidoFornecedorItems where c.idPedido == idPedido select new {c.idPedidoFornecedor, c.tbPedidoFornecedor.dataLimite}).Distinct();
        foreach (var pedidoFornecedor in pedidosFornecedor)
        {
            litRomaneios.Text = litRomaneios.Text + "Romaneio: " + pedidoFornecedor.idPedidoFornecedor + " - Data: " + pedidoFornecedor.dataLimite.ToShortDateString() + "<br>";
        }
    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        if (e.Column.FieldName == "pedidoIdCliente")
        {
            int pedidoId = Convert.ToInt32(e.GetListSourceFieldValue("pedidoId"));
            e.Value = rnFuncoes.retornaIdCliente(pedidoId);
        }
    }
}