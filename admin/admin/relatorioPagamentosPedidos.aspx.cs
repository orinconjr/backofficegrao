﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_relatorioPagamentosPedidos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            txtDataInicial.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataInicial.ClientID + "');");
            txtDataFinal.Attributes.Add("onkeypress", "Mascaras('DATA', '" + txtDataFinal.ClientID + "');");
        }
        fillGrid(false);
    }

    private void fillGrid(bool rebind)
    {
        var dataInicial = DateTime.Now;
        var dataFinal = DateTime.Now;
        try
        {
            dataInicial = Convert.ToDateTime(txtDataInicial.Text);
            dataFinal = Convert.ToDateTime(txtDataFinal.Text);
            dataFinal = dataFinal.AddDays(1);
        }
        catch(Exception ex) { }

        using (var txn = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
        {
            var data = new dbCommerceDataContext();


            var pedidosGeral = (from c in data.tbPedidos
                              where c.dataHoraDoPedido > dataInicial
                              select new
                              {
                                  c.pedidoId,
                                  c.dataHoraDoPedido,
                                  c.clienteId
                              }
                              ).ToList();

            var pagamentosGeral = (from c in data.tbPedidoPagamentos
                              join e in data.tbCondicoesDePagamentos on c.condicaoDePagamentoId equals e.condicaoId
                              join gateway in data.tbPedidoPagamentoGateways on c.idPedidoPagamento equals gateway.idPedidoPagamento into retornoGateway
                              join risco in data.tbPedidoPagamentoRiscos on c.idPedidoPagamento equals risco.idPedidoPagamento into retornoRisco
                              where c.tbPedido.dataHoraDoPedido > dataInicial && c.tbPedido.dataHoraDoPedido < dataFinal && c.pagamentoPrincipal == true
                              orderby c.pedidoId
                              select new
                              {
                                  c.pedidoId,
                                  c.tbPedido.dataHoraDoPedido,
                                  c.tbPedido.tbPedidoSituacao.situacao,
                                  c.tbPedido.valorCobrado,
                                  c.tbPedido.clienteId,
                                  c.pago,
                                  c.valor,
                                  e.condicaoNome,
                                  statusGateway = retornoGateway.Count() == 0 ? "" : retornoGateway.FirstOrDefault().responseMessage,
                                  statusClearsale = retornoRisco.Count() == 0 ? "" : retornoRisco.FirstOrDefault().statusGateway
                                  //pedidosApos = pedidosCliente.Count(x => x.dataHoraDoPedido > c.tbPedido.dataHoraDoPedido)
                              }
                              ).ToList();
            var pagamentos = (from c in pagamentosGeral select new
            {
                c.pedidoId,
                c.dataHoraDoPedido,
                c.situacao,
                c.valorCobrado,
                c.pago,
                c.valor,
                c.condicaoNome,
                c.statusGateway,
                c.statusClearsale,
                pedidosApos = (from ped in pedidosGeral where ped.clienteId == c.clienteId && ped.dataHoraDoPedido > c.dataHoraDoPedido select c).Count()
            }
            ).ToList();

            
            grd.DataSource = pagamentos;
            if (!Page.IsPostBack | rebind)
            {
                grd.DataBind();
            }
        }

    }
    protected void imbInsert_Click(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
}