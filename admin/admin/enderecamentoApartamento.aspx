﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="enderecamentoApartamento.aspx.cs" Inherits="admin_enderecamentoApartamento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <h1>Apartamentos</h1>
   
    Área<br />
    <asp:DropDownList ID="ddlarea" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlarea_SelectedIndexChanged"></asp:DropDownList><br />
     
     Rua:<br>
    <asp:DropDownList ID="ddlrua" runat="server" OnSelectedIndexChanged="ddlrua_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><br />

    Predio <br />
     <asp:DropDownList ID="ddlpredio" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlpredio_SelectedIndexChanged1" ></asp:DropDownList><br />

    Andar <br />
     <asp:DropDownList ID="ddlandar" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlandar_SelectedIndexChanged" ></asp:DropDownList><br />
   

   
       Apartamento:<br>
    <asp:TextBox ID="txtApartamento" runat="server"></asp:TextBox>
    <br /> <br />
    <asp:Button ID="btnnovoAndar" runat="server" Text="Novo Apartamento" OnClick="btnnovoAndar_Click" />
    <br /> <br />

      <asp:GridView ID="GridView2" runat="server"></asp:GridView>

    <asp:GridView ID="GridView1" DataKeyNames="idenderecamentoApartamento"  BorderWidth="0px"
            runat="server" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"
             OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound"
            OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowEditing="GridView1_RowEditing"
            OnRowUpdating="GridView1_RowUpdating" PageSize="50">
            <Columns>
                <asp:BoundField DataField="idenderecamentoApartamento" HeaderText="IdAndar" ReadOnly="True"></asp:BoundField>
                <asp:BoundField DataField="apartamento" HeaderText="Apartamento" ReadOnly="True"></asp:BoundField>
                <asp:BoundField DataField="andar" HeaderText="Andar" ReadOnly="True"></asp:BoundField>
                <asp:BoundField DataField="predio" HeaderText="Predio" ReadOnly="True"></asp:BoundField>
                 <asp:BoundField DataField="rua" HeaderText="rua" ReadOnly="True"></asp:BoundField>
                <asp:BoundField DataField="lado" HeaderText="lado" ReadOnly="True"></asp:BoundField>
                 
                <asp:CommandField CancelText="Cancela" EditText="Alterar" ControlStyle-Width="22px"
                    ControlStyle-Height="24px" HeaderText="Editar" ShowEditButton="True" UpdateText="Confirma"
                    ButtonType="Image" EditImageUrl="imagens/lapis.png" HeaderStyle-HorizontalAlign="Center">
                    <ItemStyle></ItemStyle>
                    <ControlStyle Height="24px" Width="22px"></ControlStyle>
                    <HeaderStyle />
                </asp:CommandField>
                <asp:TemplateField HeaderText="Excluir" ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="cmdDelete" runat="server" CausesValidation="False" CommandName="Delete"
                            Text="Delete">
                                <img  src="imagens/lixeira.png" alt="Excluir a área" style="border-style:none;"/>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <AlternatingRowStyle BackColor="#E6E6E6" />
            <PagerSettings Mode="NumericFirstLast" />
        </asp:GridView>

  

</asp:Content>

