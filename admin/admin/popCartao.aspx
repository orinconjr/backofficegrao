﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="popCartao.aspx.cs" Inherits="admin_popCartao" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="estilos/estilos.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color:White;">
    <form id="formulario" runat="server">

    <table cellpadding="0" cellspacing="0" style="width: 700px">
        <tr>
            <td style="height: 78px; background-image: url('images/cabecalhoPop.jpg'); background-repeat: no-repeat;" 
                class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Dados do Cartão</asp:Label>
            </td>
        </tr>
        <tr>
            <td bgcolor="White">
                <table align="center" cellpadding="0" cellspacing="0" style="width: 700px">
                    <tr>
                        <td align="right" class="rotulos" style="width: 125px">
                            Nome no Cartão:</td>
                        <td>
                            <asp:Label ID="lblNomeDoCartao" runat="server" CssClass="rotulos" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="rotulos" style="width: 125px">
                            Número do Cartão:</td>
                        <td>
                            <asp:Label ID="lblNumeroDoCartao" runat="server" CssClass="rotulos" 
                                Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="rotulos" style="width: 125px">
                            Data de Validade:</td>
                        <td>
                            <asp:Label ID="lblValidade" runat="server" CssClass="rotulos" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="rotulos" style="width: 125px">
                            Código de Segurança:</td>
                        <td>
                            <asp:Label ID="lblCodDeSeguranca" runat="server" CssClass="rotulos" 
                                Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 78px; background-image: url('images/rodapePop.jpg'); background-repeat: no-repeat;">
                &nbsp;</td>
        </tr>
    </table>

    </form>
</body>
</html>
