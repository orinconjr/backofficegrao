﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using DevExpress.Web.ASPxHtmlEditor;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

public partial class admin_produtoCad : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            exibeOcultaObjeto();
            PopulateRootLevel();
            atribuiJs();
        }

        montaQuantidadeDeEspecificacoes();
        montaInformacoesAdcionais(pegaQuantidadeDeInformacoesAdcionais());
    }

    protected int verificaSeExisteProdutoPaiIdNaQueryString()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["produtoPaiId"]))
        {
            return int.Parse(Request.QueryString["produtoPaiId"].ToString());
        }
        else
        {
            return 0;
        }
    }

    #region Exibe e oculta
    protected void exibeOcultaObjeto()
    {
        if (ConfigurationManager.AppSettings["tipoDeLoja"] == "full")
        {
            trEspecificacoes.Visible = true;
            trGarantiaEstendida.Visible = true;
            txtQuantidadeDeProdutos.Visible = true;
        }
        else
        {
            trEspecificacoes.Visible = false;
            trGarantiaEstendida.Visible = false;
            txtQuantidadeDeProdutos.Visible = false;
        }
    }
    #endregion

    #region categorias
    private void PopulateRootLevel()
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelPai().Tables[0], treeCategorias.Nodes);
    }

    private void PopulateSubLevel(int categoriaPaiId, TreeNode parentNode)
    {
        PopulateNodes(rnCategorias.categoriasSelecionaNivelFilho(categoriaPaiId).Tables[0], parentNode.ChildNodes);
    }

    protected void TreeView1_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        PopulateSubLevel(Int32.Parse(e.Node.Value), e.Node);
    }

    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Text = dr["categoriaNome"].ToString();
            tn.Value = dr["categoriaId"].ToString();

            //desabilita os links do treeview
            tn.SelectAction = TreeNodeSelectAction.None;
            nodes.Add(tn);

            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["childnodecount"]) > 0);
        }
    }
    #endregion

    protected void atribuiJs()
    {
        txtProdutoEstoqueAtual.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoEstoqueMinimo.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoPeso.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProdutoPrecoDeCusto.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPreco.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPrecoPromocional.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoPrecoAtacado.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtProdutoQuantidadeMininaParaAtacado.Attributes.Add("onkeypress", "return soNumero(event);");
        txtValor1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtValor2.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtValor3.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtValor4.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
        txtQuantidadeDeProdutos.Attributes.Add("onkeypress", "return soNumero(event);");
        txtFios.Attributes.Add("onkeypress", "return soNumero(event);");
        txtPecas.Attributes.Add("onkeypress", "return soNumero(event);");
        txtBrindes.Attributes.Add("onkeypress", "return soNumero(event);");
        txtAltura.Attributes.Add("onkeypress", "return soNumero(event);");
        txtLargura.Attributes.Add("onkeypress", "return soNumero(event);");
        txtProfundidade.Attributes.Add("onkeypress", "return soNumero(event);");

        txtMargem.Attributes.Add("onkeyup", "javascript:return CalculoMargemVenda('" + txtMargem.ClientID + "', '" + lblPrecoPromocional.ClientID + "'," + "'" + lblPrecoVenda.ClientID + "', '" + txtProdutoPrecoDeCusto.ClientID + "');");
    }

    #region Especificaçoes
    protected void montaQuantidadeDeEspecificacoes()
    {
        if (wzProdCad.ActiveStep.Name.ToString() == "passo1")
        {
            if (txtQuantidadeDeProdutos.Text != string.Empty)
            {
                int[] arrQuantidadeDeEspecificacoes = new int[int.Parse(txtQuantidadeDeProdutos.Text)];

                dtlQuantidadeDeEspecificacoes.DataSource = arrQuantidadeDeEspecificacoes;
                dtlQuantidadeDeEspecificacoes.DataBind();
            }
        }
    }

    protected void dtlQuantidadeDeEspecificacoes_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            #region Atribui valor a os campos
            TextBox txtProdutoNome1 = (TextBox)e.Item.FindControl("txtProdutoNome1");
            TextBox txtProdutoDescricao1 = (TextBox)e.Item.FindControl("txtProdutoDescricao1");
            TextBox txtProdutoIdDaEmpresa1 = (TextBox)e.Item.FindControl("txtProdutoIdDaEmpresa1");
            TextBox txtProdutoCodDeBarras1 = (TextBox)e.Item.FindControl("txtProdutoCodDeBarras1");
            TextBox txtProdutoPreco1 = (TextBox)e.Item.FindControl("txtProdutoPreco1");
            TextBox txtProdutoPrecoDeCusto1 = (TextBox)e.Item.FindControl("txtProdutoPrecoDeCusto1");
            TextBox txtProdutoPrecoPromocional1 = (TextBox)e.Item.FindControl("txtProdutoPrecoPromocional1");
            TextBox txtProdutoPrecoAtacado1 = (TextBox)e.Item.FindControl("txtProdutoPrecoAtacado1");
            TextBox txtProdutoQuantidadeMininaParaAtacado1 = (TextBox)e.Item.FindControl("txtProdutoQuantidadeMininaParaAtacado1");
            TextBox txtProdutoEstoqueAtual1 = (TextBox)e.Item.FindControl("txtProdutoEstoqueAtual1");

            txtProdutoPreco1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPrecoDeCusto1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPrecoPromocional1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoPrecoAtacado1.Attributes.Add("onkeyPress", "return moeda(this,'.',',',event);");
            txtProdutoQuantidadeMininaParaAtacado1.Attributes.Add("onkeypress", "return soNumero(event);");
            txtProdutoEstoqueAtual1.Attributes.Add("onkeypress", "return soNumero(event);");

            txtProdutoNome1.Text = txtProdutoNome.Text;
            txtProdutoDescricao1.Text = txtProdutoDescricao.Text;
            txtProdutoIdDaEmpresa1.Text = txtProdutoIdDaEmpresa.Text;
            txtProdutoCodDeBarras1.Text = txtProdutoCodDeBarras.Text;
            txtProdutoPreco1.Text = txtProdutoPreco.Text;
            txtProdutoPrecoDeCusto1.Text = txtProdutoPrecoDeCusto.Text;
            txtProdutoPrecoPromocional1.Text = txtProdutoPrecoPromocional.Text;
            txtProdutoPrecoAtacado1.Text = txtProdutoPrecoAtacado.Text;
            txtProdutoQuantidadeMininaParaAtacado1.Text = txtProdutoQuantidadeMininaParaAtacado.Text;
            txtProdutoEstoqueAtual1.Text = txtProdutoEstoqueAtual.Text;
            #endregion

            montaEspecificacoesSelecionadas(e.Item);
        }
    }

    protected void montaEspecificacoesSelecionadas(DataListItem especificacaoId)
    {
        DataTable dtEspecificacoesSelecionadas = new DataTable();

        DataColumn colEspecificacaoId = new DataColumn();
        DataColumn colEspecificacaoNome = new DataColumn();

        colEspecificacaoId.ColumnName = "especificacaoId";
        colEspecificacaoId.DataType = Type.GetType("System.Int32");

        colEspecificacaoNome.ColumnName = "especificacaoNome";
        colEspecificacaoNome.DataType = Type.GetType("System.String");

        dtEspecificacoesSelecionadas.Columns.Add(colEspecificacaoId);
        dtEspecificacoesSelecionadas.Columns.Add(colEspecificacaoNome);

        foreach (ListItem li in ckbEspecificacoes.Items)
        {
            if (li.Selected)
            {
                DataRow row = dtEspecificacoesSelecionadas.NewRow();
                row["especificacaoId"] = li.Value;
                row["especificacaoNome"] = li.Text;
                dtEspecificacoesSelecionadas.Rows.Add(row);
            }
        }
        if (dtEspecificacoesSelecionadas.Rows.Count > 0)
        {
            DataList dtlEspecificacoes = (DataList)especificacaoId.FindControl("dtlEspecificacoes");
            dtlEspecificacoes.DataSource = dtEspecificacoesSelecionadas;
            dtlEspecificacoes.DataBind();
        }
    }
    #endregion

    #region Informações adcionais
    protected int pegaQuantidadeDeInformacoesAdcionais()
    {
        if (string.IsNullOrEmpty(txtQuantidadeDeInformacoesAdcionais.Text.ToString()))
            return 0;
        else
            return int.Parse(txtQuantidadeDeInformacoesAdcionais.Text.ToString());
    }

    protected void montaInformacoesAdcionais(int quantidadeDeInformacoesAdcionais)
    {
        if (quantidadeDeInformacoesAdcionais > 0)
        {
            int i;
            for (i = 1; i <= quantidadeDeInformacoesAdcionais; i++)
            {
                Label lblInformacaoAdcionalNome = new Label();
                lblInformacaoAdcionalNome.ID = "lblInformacaoAdcionalNome" + i;
                lblInformacaoAdcionalNome.CssClass = "rotulos";
                lblInformacaoAdcionalNome.Text = "Nome da informação adcional<br>";
                pnlInformacoesAdcionais.Controls.Add(lblInformacaoAdcionalNome);

                TextBox txtInformacaoAdcionalNome = new TextBox();
                txtInformacaoAdcionalNome.ID = "txtInformacaoAdcionalNome" + i;
                txtInformacaoAdcionalNome.CssClass = "campos";
                txtInformacaoAdcionalNome.Width = 300;
                pnlInformacoesAdcionais.Controls.Add(txtInformacaoAdcionalNome);

                Label lblInformacaoAdcional = new Label();
                lblInformacaoAdcional.ID = "lblInformacaoAdcional" + i;
                lblInformacaoAdcional.CssClass = "rotulos";
                lblInformacaoAdcional.Text = "<br>Informação Adcional<br>";
                pnlInformacoesAdcionais.Controls.Add(lblInformacaoAdcional);

                CuteEditor.Editor txtInformacaoAdcional = new CuteEditor.Editor();
                txtInformacaoAdcional.AutoConfigure = CuteEditor.AutoConfigure.Full_noform;
                txtInformacaoAdcional.ID = "txtInformacaoAdcional" + i;
                txtInformacaoAdcional.Width = 834;
                pnlInformacoesAdcionais.Controls.Add(txtInformacaoAdcional);
            }
        }
    }
    #endregion

    protected void wzProdCad_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        //var idExiste = rnProdutos.produtoSelecionaIdDoProdutoPrincipal_PorProdutoIdDaEmpresa(txtProdutoIdDaEmpresa.Text);
        //if(idExiste.Tables[0].Rows.Count > 0)
        //{
        //    Response.Write("<script>alert('ID da Empresa já cadastrado.');</script>");
        //    return;
        //}
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            if (incluiProdutos())
            {
                Response.Write("<script>alert('Produto cadastrado com sucesso.');</script>");
                Response.Write("<script>window.location=('produtoCad.aspx');</script>");
            }
            else
                Response.Write("<script>alert('Ocorreu um erro ao cadastrar os produtos.');</script>");
        }
        else
        {
            Response.Write("<script>alert('Selecione pelo menos uma categoria.');</script>");
        }
    }

    protected bool incluiProdutos()
    {
        try
        {
            int contador = 1;
            int produtoId;
            int produtoPaiId = verificaSeExisteProdutoPaiIdNaQueryString();
            int i = 1;
            string produtoMetaDescription = txtProdutoNome.Text + " " + txtProdutoDescricao.Text;
            string produtoMetakeywords = txtProdutoNome.Text + " " + txtProdutoDescricao.Text; ;
            string produtoUrl = rnProdutos.GerarPermalinkProduto(txtProdutoNome.Text).Trim().ToLower();

            foreach (DataListItem item in dtlQuantidadeDeEspecificacoes.Items)
            {
                if (item.ItemType == ListItemType.Item || item.ItemType == ListItemType.AlternatingItem)
                {
                    #region Localiza campos no dtlQuantidadeDeEspecificacoes
                    TextBox txtProdutoNome1 = (TextBox)item.FindControl("txtProdutoNome1");
                    TextBox txtProdutoDescricao1 = (TextBox)item.FindControl("txtProdutoDescricao1");
                    TextBox txtProdutoIdDaEmpresa1 = (TextBox)item.FindControl("txtProdutoIdDaEmpresa1");
                    TextBox txtProdutoCodDeBarras1 = (TextBox)item.FindControl("txtProdutoCodDeBarras1");
                    TextBox txtProdutoPreco1 = (TextBox)item.FindControl("txtProdutoPreco1");
                    TextBox txtProdutoPrecoDeCusto1 = (TextBox)item.FindControl("txtProdutoPrecoDeCusto1");
                    TextBox txtProdutoPrecoPromocional1 = (TextBox)item.FindControl("txtProdutoPrecoPromocional1");
                    TextBox txtProdutoPrecoAtacado1 = (TextBox)item.FindControl("txtProdutoPrecoAtacado1");
                    TextBox txtProdutoQuantidadeMininaParaAtacado1 = (TextBox)item.FindControl("txtProdutoQuantidadeMininaParaAtacado1");
                    TextBox txtProdutoEstoqueAtual1 = (TextBox)item.FindControl("txtProdutoEstoqueAtual1");
                    #endregion

                    #region Define valores dos checkboxs
                    string produtoAtivo;
                    if(ckbAtivo.Checked == true)
                        produtoAtivo = "True";
                    else
                        produtoAtivo = "False";

                    string produtoPrincipal;
                    if (contador != 1 && ckbPrincipal.Checked == true)
                        produtoPrincipal = "False";
                    else
                        produtoPrincipal = "True";

                    string produtoFreteGratis;
                    if (ckbFreteGratis.Checked == true)
                        produtoFreteGratis = "True";
                    else
                        produtoFreteGratis = "False";

                    string produtoLancamento;
                    if (ckbLancamento.Checked == true)
                        produtoLancamento = "True";
                    else
                        produtoLancamento = "False";

                    string preVenda;
                    if (ckbPreVenda.Checked == true)
                        preVenda = "True";
                    else
                        preVenda = "False";

                    string produtoPromocao;
                    if (ckbPromocao.Checked == true)
                        produtoPromocao = "True";
                    else
                        produtoPromocao = "False";

                    string destaque1;
                    if (ckbDestaque1.Checked == true)
                        destaque1 = "True";
                    else
                        destaque1 = "False";

                    string destaque2;
                    if (ckbDestaque2.Checked == true)
                        destaque2 = "True";
                    else
                        destaque2 = "False";

                    string destaque3;
                    if (ckbDestaque3.Checked == true)
                        destaque3 = "True";
                    else
                        destaque3 = "False";

                    string exclusivo;
                    if (chkExclusivo.Checked == true)
                        exclusivo = "True";
                    else
                        exclusivo = "False";

                    string marketplaceCadastrar;
                    if (ckbMarketplaceCadastrar.Checked == true)
                        marketplaceCadastrar = "True";
                    else
                        marketplaceCadastrar = "False";

                    string marketplaceEnviarMarca;
                    if (ckbMarketplaceEnviarMarca.Checked == true)
                        marketplaceEnviarMarca = "True";
                    else
                        marketplaceEnviarMarca = "False";

                    string exibirDiferencaCombo = ckbExibirDiferencaCombo.Checked == true ? "True" : "False";

                    #endregion

                    #region Incluo os produtos herdando as informacoes do form
                    if (rnProdutos.produtoInclui(produtoPaiId, produtoPrincipal, txtProdutoIdDaEmpresa1.Text, txtProdutoCodDeBarras1.Text, txtProdutoNome1.Text, produtoUrl,
                                                txtProdutoDescricao1.Text, int.Parse(drpFornecedor.SelectedItem.Value), int.Parse(drpMarca.SelectedItem.Value),
                                                decimal.Parse(txtProdutoPreco1.Text), decimal.Parse(txtProdutoPrecoDeCusto1.Text),
                                                decimal.Parse(txtProdutoPrecoPromocional1.Text), decimal.Parse(txtProdutoPrecoAtacado1.Text),
                                                int.Parse(txtProdutoQuantidadeMininaParaAtacado1.Text), txtProdutoLegendaAtacado.Text,
                                                int.Parse(txtProdutoEstoqueAtual1.Text), int.Parse(txtProdutoEstoqueMinimo.Text),
                                                decimal.Parse(txtProdutoPeso.Text), produtoAtivo, produtoFreteGratis, produtoLancamento,
                                                produtoPromocao, produtoMetaDescription, produtoMetakeywords, destaque1, destaque2, destaque3, txtDisponibilidadeEmEstoque.Text, preVenda,
                                                drpComposicao.SelectedValue, txtFios.Text, txtPecas.Text, txtBrindes.Text, txtProdutoTags.Text, "", marketplaceCadastrar, marketplaceEnviarMarca,
                                                exclusivo, int.Parse(txtAltura.Text), int.Parse(txtLargura.Text), int.Parse(txtProfundidade.Text), "", "False", "False", "", exibirDiferencaCombo,
                                                Convert.ToInt32(ddlSite.SelectedValue), 0, false, 0, false, false, 0, "", "", "", "", false, false, 0) > 0)
                    {
                        #region Recupero o produtoId
                        produtoId = int.Parse(rnProdutos.produtoSelecionaUltimoId().Tables[0].Rows[0]["produtoId"].ToString());
                        #endregion

                        if (contador == 1 && ckbPrincipal.Checked == true)// Se for o primeiro produto tenho que alterar o produtoPaId para o produtoId. Assim o proprio produto é pai dele
                            rnProdutos.produtoPaiIdAltera(produtoId, produtoId); //tenho q ver isso aqui. o primeiro produto o produtopai tem q ser o produto id em todas as tabelas q for incluido

                        #region Recupero o produtoPaiId para usar nos produtos filhos
                        if (ckbPrincipal.Checked == true)
                        {
                            if (produtoPaiId != 0)
                            {
                                produtoPaiId = produtoPaiId;
                            }
                            else
                            {
                                produtoPaiId = produtoId;
                            }
                        }
                        else
                            produtoPaiId = 0;
                        #endregion

                        #region incluo as categorias
                        recuperaEincluiCategoria(produtoId, produtoPaiId);
                        #endregion

                        #region incluo as garantias estendidas
                        string tempo;
                        string valor;
                        for (i = 1; i <= 4; i++)
                        {
                            tempo = ((TextBox)pnlGarantiaEstendida.FindControl("txtTempo" + i.ToString())).Text;
                            valor = ((TextBox)pnlGarantiaEstendida.FindControl("txtValor" + i.ToString())).Text;

                            if (tempo != string.Empty && valor != string.Empty)
                                rnGarantiaEstendida.garantiaEstendidaInclui(produtoId, tempo, Convert.ToDecimal(valor));
                        }
                        #endregion

                        DataList dtlEspecificacoes = (DataList)item.FindControl("dtlEspecificacoes");

                        #region incluo as especicifacoes
                        foreach (DataListItem item1 in dtlEspecificacoes.Items)
                        {
                            if (item1.ItemType == ListItemType.Item || item1.ItemType == ListItemType.AlternatingItem)
                            {
                                Label lblEspecificacaoId = (Label)item1.FindControl("lblEspecificacaoId");
                                DropDownList drpEspecificacoes = (DropDownList)item1.FindControl("drpEspecificacoes");

                                int especificacaoId = int.Parse(lblEspecificacaoId.Text);

                                #region inclui produtoId e especificacao na tabela tbJuncaoProdutoEspecificacao. antes de incluir verifico se já existe na procedure
                                rnEspecificacao.juncaoProdutoEspecificacaoInclui(produtoId, especificacaoId);
                                #endregion

                                int produtoItemEspecificacaoId = int.Parse(drpEspecificacoes.SelectedItem.Value);

                                #region incluo as especificacoes selecionadas na tabela tbProdutoItemEspecificacao
                                if (contador == 1)// Se for o primeiro produto digo que o produtoPaiId é o produtoId. Assim o proprio produto é pai dele
                                    rnEspecificacao.produtoItensEspecificacaoInclui(produtoId, produtoId, especificacaoId, produtoItemEspecificacaoId);
                                else
                                    rnEspecificacao.produtoItensEspecificacaoInclui(produtoId, produtoPaiId, especificacaoId, produtoItemEspecificacaoId);
                                #endregion
                            }
                        }
                        #endregion

                        #region Crio o diretório de fotos
                        Directory.CreateDirectory(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId);


                        CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["discoAzure"]);
                        var blobClient = storageAccount.CreateCloudBlobClient();

                        CloudBlobContainer container = blobClient.GetContainerReference("fotos");
                        container.CreateIfNotExists();

                        container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                        #endregion

                        #region incluo as fotos
                        string Location;
                        string Target;
                        for (i = 1; i <= 3; i++)
                        {
                            FileUpload upl = (FileUpload)item.FindControl("upl" + i.ToString());
                            TextBox txtFotoDescricao = (TextBox)item.FindControl("txtFotoDescricao" + i.ToString());

                            if (upl.HasFile == true)
                            {
                                FileInfo file = new FileInfo(upl.PostedFile.FileName);

                                #region Recupero o produtoFotoId
                                int fotoId;
                                if (rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows.Count > 0)
                                    fotoId = int.Parse(rnFotos.produtoFotoSelecionaUltimoId().Tables[0].Rows[0]["produtoFotoId"].ToString()) + 1;
                                else
                                    fotoId = 0;
                                #endregion

                                string fotoRenomeada = fotoId + ".jpg";

                                //Salva foto no banco
                                rnFotos.fotosInclui(produtoId, fotoId.ToString(), txtFotoDescricao.Text);

                                //Salva foto no disco
                                upl.PostedFile.SaveAs(ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoRenomeada);
                                Location = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + fotoRenomeada;
                                //Cria a foto média
                                Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "media_" + fotoRenomeada;
                                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), Location, Target, ImageFormat.Jpeg);
                                //Cria a foto pequena
                                Target = ConfigurationManager.AppSettings["caminhoFisico"] + "fotos\\" + produtoId + "\\" + "pequena_" + fotoRenomeada;
                                rnFotos.ResizeImageFile(int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), Location, Target, ImageFormat.Jpeg);



                                CloudBlockBlob blob = container.GetBlockBlobReference(produtoId + "/" + fotoRenomeada);
                                blob.UploadFromStream(upl.FileContent);
                                blob.Properties.ContentType = upl.PostedFile.ContentType;

                                //Cria a foto média
                                string fotoMedia = produtoId + "/media_" + fotoId + ".jpg";
                                rnFotos.ResizeImageFileAzure(int.Parse(ConfigurationManager.AppSettings["fotoMedia"]), produtoId + "/" + fotoRenomeada, fotoMedia, ImageFormat.Jpeg);

                                //Cria a foto pequena
                                string fotoPequena = produtoId + "/pequena_" + fotoId + ".jpg";
                                rnFotos.ResizeImageFileAzure(int.Parse(ConfigurationManager.AppSettings["fotoPequena"]), produtoId + "/" + fotoRenomeada, fotoPequena, ImageFormat.Jpeg);

                                if(i == 1)
                                {
                                    var produtoData = new dbCommerceDataContext();
                                    var produto = (from c in produtoData.tbProdutos where c.produtoId == produtoId select c).FirstOrDefault();
                                    produto.fotoDestaque = fotoId.ToString();
                                    produtoData.SubmitChanges();
                                }
                            }
                        }
                        #endregion

                        #region incluo as informacoes adcionais do produtoId que acabo de incluir
                        if (!string.IsNullOrEmpty(txtQuantidadeDeInformacoesAdcionais.Text))
                            recuperaEIncluiInformacoesAdcionais(int.Parse(txtQuantidadeDeInformacoesAdcionais.Text), produtoId);
                        #endregion


                        rnProdutos.AtualizaDescontoCombo(produtoId);

                        rnBuscaCloudSearch.AtualizarProduto(produtoId);

                    }
                    #endregion
                }
                contador++;
            }
            return true;
        }
        catch
        {
            return false;
        }
    }

    private void recuperaEincluiCategoria(int produtoId, int produtoPaiId)
    {
        if (treeCategorias.CheckedNodes.Count > 0)
        {
            foreach (TreeNode node in treeCategorias.CheckedNodes)
            {
                rnCategorias.juncaoProdutoCategoriaInclui(produtoId, Convert.ToInt32(node.Value), produtoPaiId);
            }
        }
    }

    protected void recuperaEIncluiInformacoesAdcionais(int quantidadeDeInformacoesAdcionais, int produtoId)
    {
        int i;
        for (i = 1; i <= quantidadeDeInformacoesAdcionais; i++)
        {
            TextBox txtInformacaoAdcionalNome = (TextBox)pnlInformacoesAdcionais.FindControl("txtInformacaoAdcionalNome" + i.ToString());
            CuteEditor.Editor txtInformacaoAdcional = (CuteEditor.Editor)pnlInformacoesAdcionais.FindControl("txtInformacaoAdcional" + i.ToString());

            rnInformacoesAdcionais.produtoInformacoesAdcionaisInclui(produtoId, txtInformacaoAdcionalNome.Text, txtInformacaoAdcional.Text);
        }
    }
}
