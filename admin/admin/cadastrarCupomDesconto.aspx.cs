﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_cadastrarCupomDesconto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(validarCampos())
        {

            var data = new dbCommerceDataContext();
            CuponsDeDesconto cupom = new CuponsDeDesconto();
            cupom.ID = txtID.Text;
            cupom.DescontoPercentual = Convert.ToDecimal(txtDescontoPercentual.Text);
            cupom.Usado = false;
            cupom.IDCampanha = Convert.ToInt32(txtIDCampanha.Text);
            data.CuponsDeDescontos.InsertOnSubmit(cupom);
            try
            {
                data.SubmitChanges();
                Page.RegisterStartupScript("meuscrit", "<script>alert('Cupom Cadastrado com sucesso');</script>");
            }
            catch (Exception ex)
            {
                string erro = "";
                if (ex.Message.IndexOf("PK__CuponsDe__") > -1)
                    erro = "Este cupom já está cadastrado";
                else
                    erro = ex.Message.Replace("'", " ");
                Page.RegisterStartupScript("meuscrit", "<script>alert('"+erro+"');</script>");
            }
        }
     
    }
    bool validarCampos()
    {
        bool ok = true;
        string erro = "";
        if(String.IsNullOrEmpty(txtID.Text))
        {
            erro += "Informe código do cupom \\n";
            ok = false;
        }
        if (String.IsNullOrEmpty(txtIDCampanha.Text))
        {
            erro += "Informe o ID da Campanha \\n";
            ok = false;
        }
        else
        {
            try
            {
                int campanha = Convert.ToInt32(txtIDCampanha.Text);
            }
            catch
            {
                erro += "ID da campanha deve ser um número \\n";
                ok = false;
            }
        }

        if (String.IsNullOrEmpty(txtDescontoPercentual.Text))
        {
            erro += "Informe a porcentagem do desconto \\n";
            ok = false;
        }
        else
        {
            try
            {
                decimal DescontoPercentual = Convert.ToDecimal(txtDescontoPercentual.Text);
            }
            catch
            {
                erro += "Informe a porcentagem de desconto corretamente \\n";
                ok = false;
            }
        }
       if(!ok)
        {
            Page.RegisterStartupScript("meuscrit", "<script>alert('" + erro + "');</script>");
            
        }
        return ok;
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        var listCupons = new List<string>();
        string[] cupons = txtIDBusca.Text.Split(',');
        listCupons.AddRange(cupons.Select(t => t));
        using (var data = new dbCommerceDataContext())
        {
            var pesquisaCupons = (from c in data.CuponsDeDescontos where listCupons.Contains(c.ID) select c).ToList();

            if(pesquisaCupons.Count > 0 )
            { 

                GridView1.DataSource = pesquisaCupons;
                GridView1.DataBind();
            }
            else
            {
                lblMensagem.Text = "Não foram encontrados cupons para a busca";
                GridView1.DataSource = null;
                GridView1.DataBind();

            }

        }
    }
}