﻿<%@ Page Title="" Theme="Glass" Language="C#" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="categoriacad.aspx.cs" Inherits="admin_categoriacad" %>

<%@ Register Assembly="DevExpress.Web.v11.1" Namespace="DevExpress.Web.ASPxTreeView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
        <tr>
            <td class="tituloPaginas" valign="top">
                <asp:Label ID="lblAcao" runat="server">Categorias</asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMensagem" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="height: 400px">
                    <tr>
                        <td style="text-align: left; vertical-align: top">
                            <asp:TreeView ID="tvCategorias" ShowLines="True" Font-Names="Tahoma"
                                Font-Size="12px" ForeColor="Black" ExpandDepth="30"
                                runat="server" ShowCheckBoxes="None" OnSelectedNodeChanged="tvCategorias_SelectedNodeChanged">
                            </asp:TreeView>
                        </td>
                        <td valign="top" class="rotulos">
                            <table width="98%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td align="right" style="padding-right: 8px; padding-bottom: 5px">
                                        <dx:ASPxButton ID="btnNovo" runat="server" Text="Novo" OnClick="btnNovo_Click"></dx:ASPxButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">

                                        <asp:Panel ID="pnlGridInicial" runat="server">
                                            
                                        </asp:Panel><dx:ASPxGridView ID="gvInicial" ClientInstanceName="gvInicial" runat="server" AutoGenerateColumns="False"
                                                KeyFieldName="categoriaId" Width="98%" Settings-ShowFilterRow="True" Settings-ShowFilterBar="Visible"
                                                Cursor="auto" OnHtmlRowCreated="gvInicial_HtmlRowCreated" EnableViewState="False">
                                                <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                <Styles>
                                                    <Footer Font-Bold="True">
                                                    </Footer>
                                                </Styles>
                                                <SettingsPager Position="TopAndBottom" PageSize="500"
                                                    ShowDisabledButtons="False" AlwaysShowPager="True">
                                                    <Summary AllPagesText="Páginas: {0} - {1} ({2} items)"
                                                        Text="Página {0} de {1} ({2} registros encontrados)" />
                                                </SettingsPager>
                                                <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowFooter="True" />
                                                <SettingsEditing EditFormColumnCount="4"
                                                    PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1"
                                                    PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter"
                                                    PopupEditFormWidth="700px" />

                                                <Columns>
                                                    <dx:GridViewDataTextColumn Caption="ID" Width="50px" FieldName="categoriaId" VisibleIndex="0"></dx:GridViewDataTextColumn>
                                                    <%--<dx:GridViewDataTextColumn Caption="Categoria Pai" Width="300px" FieldName="categoriaPaiId"  Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains">
                                            <DataItemTemplate>
                                                <asp:Label ID="lblCategoriaPaiInicial" runat="server"></asp:Label>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>--%>
                                                    <dx:GridViewDataTextColumn Caption="Nome exibição" Width="280px" FieldName="nomeExibicao" Settings-AutoFilterCondition="Contains" VisibleIndex="0">
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Descricao" Width="280px" FieldName="descricao" VisibleIndex="0">
                                                        <Settings AutoFilterCondition="Contains" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Posicao" FieldName="posicao" VisibleIndex="0">
                                                        <Settings AutoFilterCondition="Contains" />
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Exibir" Width="50px" FieldName="exibir" Settings-FilterMode="DisplayText"
                                                        Settings-AutoFilterCondition="Contains" VisibleIndex="0">
                                                        <DataItemTemplate>
                                                            <dx:ASPxLabel ID="lblExibir" runat="server"></dx:ASPxLabel>
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                    <dx:GridViewDataTextColumn Caption="Alterar" Name="alterar" VisibleIndex="0">
                                                        <DataItemTemplate>
                                                            <asp:ImageButton ID="btnAlterar" CommandArgument='<%# Eval("categoriaId") %>' CommandName="Editar"
                                                                OnCommand="btnAlterar_Command" runat="server" ImageAlign="AbsMiddle" ImageUrl="images/btAlterar.jpg" />

                                                            <asp:ImageButton ID="btnApagar" CommandArgument='<%# Eval("categoriaId") %>' CommandName="Apagar"
                                                                OnCommand="btnApagar_Command" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/admin/images/btExcluir.jpg" />
                                                        </DataItemTemplate>
                                                    </dx:GridViewDataTextColumn>
                                                </Columns>
                                            </dx:ASPxGridView>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Panel ID="pnlFormulario" runat="server">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td valign="top">
                                            <b>Categoria Pai</b><br />
                                            <asp:HiddenField ID="hdnCategoriaPaiId" runat="server" />
                                            <asp:HiddenField ID="hdnCategoriaId" runat="server" />
                                            <asp:HiddenField ID="hdnAcao" runat="server" />
                                            <asp:DropDownList ID="ddlCategoriaPai" runat="server"></asp:DropDownList>
                                        </td>
                                        <td valign="top">
                                            <b>Nome exibição</b><br />
                                            <asp:TextBox ID="txtNomeExibicao" runat="server"></asp:TextBox>
                                        </td>
                                        <td valign="top">
                                            <b>Descrição</b><br />
                                            <asp:TextBox ID="txtDescricao" runat="server" MaxLength="500" TextMode="MultiLine" Rows="5"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <b>Posição</b><br />
                                            <asp:TextBox ID="txtPosicao" runat="server"></asp:TextBox>
                                        </td>
                                        <td colspan="2" valign="top">
                                            <b>Exibir</b><br />
                                            <asp:CheckBox ID="chkExibir" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" style="text-align: right; padding-right: 15px" valign="top">
                                            <asp:Button ID="btnSalvar" runat="server" Text="Gravar" OnClick="btnSalvar_Click" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlGrid" runat="server">
                                <asp:GridView ID="gvCategoria" runat="server" OnRowDataBound="gvCategoria_DataBound" AutoGenerateColumns="false" Width="98%">
                                    <Columns>
                                        <asp:BoundField DataField="categoriaId" HeaderText="ID" ReadOnly="true" />
                                        <asp:TemplateField HeaderText="Categoria Pai">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCategoriaPai" runat="server" Text='<%# Eval("categoriaPaiId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="nomeExibicao" HeaderText="Nome Exibição" ReadOnly="true" />
                                        <asp:BoundField DataField="posicao" HeaderText="Posicao" ReadOnly="true" />
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>

