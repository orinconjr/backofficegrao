﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class admin_logPrazoPedido : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
 
    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
  
        if(String.IsNullOrEmpty(txtiditempedido.Text))
        {
            lblerrofiltro.Text = "Informe o IdItemPedido";
            
        }
        else
        {
            lblerrofiltro.Text = "";
            carregaGrid();
        }

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        carregaGrid();
    }
    void carregaGrid()
    {
        var data = new dbCommerceDataContext();
        int idItemPedido = Convert.ToInt32(txtiditempedido.Text);
        var logs = (from lrr in data.tbLogRegistroRelacionados
                    join ld in data.tbLogDescricaos on lrr.tbLog.idLog equals ld.idLog
                    where lrr.idRegistroRelacionado == idItemPedido &&
                    lrr.tipoRegistroRelacionado == 3
                    select new
                    {
                        lrr.idLogRegistroRelacionado,
                        lrr.tbLog.data,
                        ld.descricao,
                        lrr.tbLog.idLog

                    }).OrderBy(x => x.idLog).ToList();
        GridView1.DataSource = logs;
        GridView1.DataBind();
    }
}