﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using Specifications.Extensions;
using System.IO;
using System.Xml;
public partial class admin_empresanfe2 : System.Web.UI.Page
{
    public class Nota
    {
        public int idNotaFiscal { get; set; }
        public int idPedido { get; set; }
        public int numeroNota { get; set; }
        public decimal? valor { get; set; }
        public string naturezaOperacao { get; set; }
        public string erro { get; set; }
        public string caminho { get; set; }
        public string natOp { get; set; }
    }
    //public class Nota2
    //{
    //    public decimal? vlNota { get; set; }
    //    public string natOperacao { get; set; }
    //    public string caminho { get; set; }
    //    public string erro { get; set; }
    //}
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            for (int i = 0; i < 3; i++)
            {
                ddlMes.Items.Add(new ListItem(DateTime.Now.AddMonths(-i).ToString("MMMM"), DateTime.Now.AddMonths(-i).Month + "#" + DateTime.Now.AddMonths(-i).Year));
            }

        }
    }

    protected void sqlEmpresa_Updating(object sender, LinqDataSourceUpdateEventArgs e)
    {
        try
        {
            tbEmpresa especificacao = (tbEmpresa)e.NewObject;
            ASPxCheckBox chkSimples = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["simples"], "chkSimples");
            ASPxCheckBox chkAtivo = (ASPxCheckBox)grd.FindEditRowCellTemplateControl((GridViewDataColumn)grd.Columns["ativo"], "chkAtivo");
            try
            {
                using (var data = new dbCommerceDataContext())
                {
                    var empresa = (from c in data.tbEmpresas where c.idEmpresa == especificacao.idEmpresa select c).First();
                    empresa.simples = chkSimples.Checked;
                    empresa.ativo = chkAtivo.Checked;
                    data.SubmitChanges();
                }
            }
            catch (Exception ex)
            {

            }
        }
        catch (Exception ex)
        {

        }

    }

    protected void grd_OnCustomUnboundColumnData(object sender, ASPxGridViewColumnDataEventArgs e)
    {
        int idEmpresa = Convert.ToInt32(e.GetListSourceFieldValue("idEmpresa"));
        List<Nota> notas = new List<Nota>();
        using (var data = new dbCommerceDataContext())
        {

            using (
                var txn =
                    new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.Required,
                        new System.Transactions.TransactionOptions
                        {
                            IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                        }))
            {
                string caminho = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\";
                //string caminho = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + notaPedido.numeroNota + ".xml";
                if (ddlMes.SelectedValue != DateTime.Now.Month.ToString())
                {
                    int mesSelecionado = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[0]);
                    int ano = Convert.ToInt32(ddlMes.SelectedValue.Split('#')[1]);

                    notas = (from c in data.tbNotaFiscals
                             where
                                 c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                 (c.dataHora.Date.Month == mesSelecionado &&
                                  c.dataHora.Date.Year == ano)
                             select new Nota()
                             {
                                 idNotaFiscal = c.idNotaFiscal,
                                 idPedido = c.idPedido,
                                 numeroNota = c.numeroNota,
                                 valor = c.valor,
                                 naturezaOperacao = getNaturezaOperacao(caminho + c.idPedido + "_" + c.numeroNota + ".xml")
                             }).Take(100).ToList<Nota>();

                }
                else
                {
                    notas = (from c in data.tbNotaFiscals
                             where
                                 c.linkDanfe != null && c.idCNPJ == idEmpresa &&
                                       (c.dataHora.Date.Month == DateTime.Now.Month &&
                                        c.dataHora.Date.Year == DateTime.Now.Year)
                             select new Nota()
                             {
                                 idNotaFiscal = c.idNotaFiscal,
                                 idPedido = c.idPedido,
                                 numeroNota = c.numeroNota,
                                 valor = c.valor,
                                 naturezaOperacao =  getNaturezaOperacao(caminho + c.idPedido + "_" + c.numeroNota + ".xml")
                             }).Take(100).ToList<Nota>();

                }
            }
        }

        if (e.Column.FieldName == "emissoesMes")
        {
 
            int emissoesMes = 0;
            emissoesMes = (from n in notas
                           select new { n.idNotaFiscal }).Count();
            e.Value = emissoesMes;
        }

        if (e.Column.FieldName == "totalEmitido")
        {
            decimal totalEmitido = 0;
            totalEmitido = notas.Sum(x => x.valor ?? 0);
            e.Value = totalEmitido.ToString("N");
        }


        if (e.Column.FieldName == "qtdvenda")
        {
             
            int qtdvenda = 0;
            qtdvenda = (from n in notas
                        where n.naturezaOperacao.Substring(0, 5) == "VENDA"
                        select new { n.idNotaFiscal }).Count();

            e.Value = qtdvenda;
        }

        if (e.Column.FieldName == "vlvenda")
        {

            decimal vlvenda = 0;

            vlvenda = (from n in notas
                       where n.naturezaOperacao.Substring(0,5) == "VENDA"
                        select new { n.valor }).Sum(x => x.valor ?? 0);

            e.Value = vlvenda;
        }


        if (e.Column.FieldName == "qtddevolucao")
        {
            int qtddevolucao = 0;
            qtddevolucao = (from n in notas
                        where n.naturezaOperacao.ToUpper().Replace("Ç","C").Replace("Ã","A").Substring(0,9) == "DEVOLUCAO"
                        select new { n.idNotaFiscal }).Count();
            e.Value = qtddevolucao;
        }

        if (e.Column.FieldName == "vldevolucao")
        {
            decimal vldevolucao = 0;
            vldevolucao = (from n in notas
                           where n.naturezaOperacao.ToUpper().Replace("Ç", "C").Replace("Ã", "A").Substring(0, 9) == "DEVOLUCAO"
                       select new { n.valor }).Sum(x => x.valor ?? 0);
            e.Value = vldevolucao;
        }
    }

    protected void grd_OnStartRowEditing(object sender, ASPxStartRowEditingEventArgs e)
    {
        var usuarioLogadoId = Request.Cookies["usuarioLogadoId"];

        if (usuarioLogadoId == null)
            Response.Redirect("default.aspx");

        int usuarioId = Convert.ToInt32(usuarioLogadoId.Value);

        List<int> idUsuarioPermitido = new List<int> { 105, 75, 167 };

        if (!idUsuarioPermitido.Contains(usuarioId))
            e.Cancel = true;
    }

    protected void sqlEmpresa_OnUpdated(object sender, LinqDataSourceStatusEventArgs e)
    {
        try
        {
            //string t = e.Exception.ToString();
            grd.DataBind();
        }
        catch (Exception ex)
        {

        }
    }

    protected void ddlMes_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        grd.DataBind();
    }

    public string getNaturezaOperacao(string urlXML)
    {
        string natOp = "NÃO DEFINIDO";
        try
        {
            ////XmlDocument doc = new XmlDocument();
            ////doc.Load(urlXML);

            //////Display all the book titles.
            ////XmlNodeList elemList = doc.GetElementsByTagName("natOp");
            ////natOp = elemList[0].InnerXml;

            ///////////////////////////
            XmlDocument doc = new XmlDocument();

            XmlElement rootNode = doc.CreateElement("NFe");

            doc.AppendChild(rootNode);
            string conteudoArquivoXML = File.ReadAllText(urlXML).Replace("\r\n", "").Trim().Replace(">        <", "><");//.Replace("\"", "").Trim();
            // Regex.Replace(conteudoArquivoXML, @"(?:(?<=^\<mattext\>)[^\<]*)|(?:[^\>]*(?=\</mattext\>$))", string.Empty, RegexOptions.Multiline);

            XmlNodeList NFeRaiz = doc.GetElementsByTagName("NFe");
            NFeRaiz[0].InnerXml = conteudoArquivoXML;

            XmlNodeList elemList = doc.GetElementsByTagName("natOp");
            natOp = elemList[0].InnerXml;
            ////////////////////////////
        }
        catch (Exception ex) { }

        return natOp;
    }

    //public List<Nota2> getValoresNota(int pedidoId)
    //{
    //    List<Nota2> notas = new List<Nota2>();

    //    var data = new dbCommerceDataContext();
    //    var notasPedido = (from nf in data.tbNotaFiscals
    //                       where nf.idPedido == pedidoId
    //                       select new { nf.numeroNota, nf.dataHora }).OrderBy(x => x.dataHora).ToList();
    //    foreach (var notaPedido in notasPedido)
    //    {
    //        Nota2 nota = new Nota2();
    //        string caminho = System.Configuration.ConfigurationManager.AppSettings["caminhoFisico"] + "\\admin\\notas\\" + pedidoId + "_" + notaPedido.numeroNota + ".xml";
    //        nota.caminho = caminho;
    //        try
    //        {

    //            XmlDocument doc = new XmlDocument();

    //            XmlElement rootNode = doc.CreateElement("NFe");

    //            doc.AppendChild(rootNode);
    //            string conteudoArquivoXML = File.ReadAllText(caminho).Replace("\r\n", "").Trim().Replace(">        <", "><");//.Replace("\"", "").Trim();
    //            // Regex.Replace(conteudoArquivoXML, @"(?:(?<=^\<mattext\>)[^\<]*)|(?:[^\>]*(?=\</mattext\>$))", string.Empty, RegexOptions.Multiline);

    //            XmlNodeList NFeRaiz = doc.GetElementsByTagName("NFe");
    //            NFeRaiz[0].InnerXml = conteudoArquivoXML;

    //            XmlNodeList elemList = doc.GetElementsByTagName("natOp");
    //            nota.natOperacao = elemList[0].InnerXml;

    //            XmlNodeList vNF = doc.GetElementsByTagName("vNF");
    //            nota.vlNota = Convert.ToDecimal(vNF[0].InnerXml.Replace(".", ","));


    //        }
    //        catch (Exception ex)
    //        {
    //            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
    //            //Console.WriteLine("Line: " + trace.GetFrame(0).GetFileLineNumber());
    //            nota.erro = ex.Message + "Linha: " + trace.GetFrame(0).GetFileLineNumber();
    //            string erro = ex.Message;
    //        }
    //        notas.Add(nota);
    //    }




    //    return notas;
    //}
}