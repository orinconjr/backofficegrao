﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dashboardExpedicao1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["chave"] != "gdg123")
        {
            //Response.Redirect("https://www.graodegente.com.br");
        }
    }

    [WebMethod]
    public static int qtdPedidosBalanca()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now;

        var pedidos = (from pac in data.tbPedidoPacotes
                       where
                         pac.peso == 0 && pac.tbPedidoEnvio.idCentroDeDistribuicao > 3 &&
                         (pac.despachado == false ||
                         pac.despachado == null) && pac.tbPedidoEnvio.dataFimEmbalagem != null
                       select new
                       {
                           pac.tbPedido.pedidoId
                       }).Distinct().Count();



        return pedidos;
    }

    [WebMethod]
    public static string Top10ListaPedidosBalanca()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now;
        var pedidos = (from c in data.tbPedidos
                       join pac in data.tbPedidoPacotes on c.pedidoId equals pac.idPedido
                       where
                         pac.peso == 0 && pac.tbPedidoEnvio.idCentroDeDistribuicao > 3 &&
                         (pac.despachado == false ||
                         pac.despachado == null) && pac.tbPedidoEnvio.dataFimEmbalagem != null 
                       select new
                       {
                           pac.tbPedido.pedidoId,
                           cor = (hoje - pac.tbPedidoEnvio.dataFimEmbalagem.Value).TotalMinutes < 1 ? "#7fffd4" : (hoje - pac.tbPedidoEnvio.dataFimEmbalagem.Value).TotalMinutes < 2 ? "#ffec8b" : "#ff4500",
                           dif = (hoje - pac.tbPedidoEnvio.dataFimEmbalagem.Value).TotalMinutes,
                       }).Distinct().ToList().OrderByDescending(x => x.dif);

        var pedidosFormatados = (from c in pedidos
                                       select new
                                       {
                                           c.pedidoId,
                                           c.cor,
                                           dif = c.dif < 60 ? c.dif.ToString("0.##") + " min" : c.dif < (60 * 24) ? (c.dif / 60).ToString("0.##") + " hr" : (c.dif / 60 / 24).ToString("0.##") + " dias",
                                       }).ToList();

        JavaScriptSerializer js = new JavaScriptSerializer();
        var pedidosJson = js.Serialize(pedidosFormatados.Take(10));
        return pedidosJson;


    }

    [WebMethod]
    public static int qtdPedidosTransportadora()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;

        var pedidos = (from pac in data.tbPedidoPacotes
                       join env in data.tbPedidoEnvios on pac.idPedido equals env.idPedido
                       where
                        (pac.rastreio ?? "") == "" &&
                        (env.nfeImpressa ?? false) == false && (pac.rastreamentoConcluido ?? false) == false
                        && env.idCentroDeDistribuicao > 3 && env.dataFimEmbalagem != null && env.formaDeEnvio != ""
                       select new
                       {
                           pedidoid = pac.idPedido
                       }).Distinct().Count();

        return pedidos;
    }

    [WebMethod]
    public static string Top10ListaPedidosTransportadora()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from pac in data.tbPedidoPacotes
                       join env in data.tbPedidoEnvios on pac.idPedido equals env.idPedido
                       where
                        (pac.rastreio ?? "") == "" &&
                        (env.nfeImpressa ?? false) == false && (pac.rastreamentoConcluido ?? false) == false
                        && env.idCentroDeDistribuicao > 3 && env.dataFimEmbalagem != null && env.formaDeEnvio != ""
                       select new
                       {
                           pedidoid = pac.idPedido,
                           transportadora = env.formaDeEnvio,
                           cor = (hoje - env.dataFimEmbalagem.Value).TotalHours < 2 ? "#7fffd4" : (hoje - env.dataFimEmbalagem.Value).TotalHours < 3 ? "#ffec8b" : "#ff4500",
                           dif = (hoje - env.dataFimEmbalagem.Value).TotalMinutes
                       }).Distinct().ToList().OrderByDescending(x => x.dif);

        var pedidosFormatados = (from c in pedidos
                                 select new
                                 {
                                     c.pedidoid,
                                     c.transportadora,
                                     c.cor,
                                     dif = c.dif < 60 ? c.dif.ToString("0.##") + " min" : c.dif < (60 * 24) ? (c.dif / 60).ToString("0.##") + " hr" : (c.dif / 60 / 24).ToString("0.##") + " dias",
                                 }).ToList();

        JavaScriptSerializer js = new JavaScriptSerializer();
        var pedidosJson = js.Serialize(pedidosFormatados.Take(10));
        return pedidosJson;
    }


    [WebMethod]
    public static int pedidosAtrasadosEnxoval()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true && (c.prazoMaximoPostagemAtualizado ?? DateTime.Now).Date <= hoje
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosSepararEnxoval()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true && c.separadoCd4 == false
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosEmbalarEnxoval()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd4 > 0 && c.envioLiberadoCd4 == true && c.separadoCd4 == true
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosAtrasadosMovel()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd5 > 0 && c.envioLiberadoCd5 == true && (c.prazoMaximoPostagemAtualizado ?? DateTime.Now).Date <= hoje
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosSepararMovel()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd5 > 0 && c.envioLiberadoCd5 == true && c.separadoCd5 == false
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosEmbalarMovel()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidos
                       where c.statusDoPedido == 11 && c.itensPendentesEnvioCd5 > 0 && c.envioLiberadoCd5 == true && c.separadoCd5 == true
                       select new
                       {
                           c.pedidoId
                       }).Count();
        return pedidos;
    }



    [WebMethod]
    public static int pedidosSeparadosEnxoval()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidoEnvios
                       where c.dataFimSeparacao != null && c.idCentroDeDistribuicao == 4 && c.dataInicioEmbalagem == null && c.tbPedido.statusDoPedido == 11 && c.tbPedido.itensPendentesEnvioCd4 > 0
                       select new
                       {
                           c.idPedido
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosSeparadosMovel()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidoEnvios
                       where c.dataFimSeparacaoCd2 != null && c.dataInicioEmbalagem == null && c.dataFimEmbalagem == null && c.tbPedido.statusDoPedido == 11 && c.tbPedido.itensPendentesEnvioCd5 > 0
                       select new
                       {
                           c.idPedido
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosEmbaladosEnxoval()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidoEnvios
                       where (c.dataFimEmbalagem ?? DateTime.Now.AddDays(-1)).Date >= hoje && c.dataFimEmbalagem != null && c.idCentroDeDistribuicao == 4
                       select new
                       {
                           c.idPedido
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static int pedidosEmbaladosMovel()
    {
        var data = new dbCommerceDataContext();
        var hoje = DateTime.Now.Date;
        var pedidos = (from c in data.tbPedidoEnvios
                       where (c.dataFimSeparacaoCd2 ?? DateTime.Now.AddDays(-1)).Date >= hoje && c.dataFimEmbalagem != null && c.dataFimSeparacaoCd2 != null
                       select new
                       {
                           c.idPedido
                       }).Count();
        return pedidos;
    }

    [WebMethod]
    public static string SendoSeparadosEnxoval()
    {
        var data = new dbCommerceDataContext();
        var agora = DateTime.Now;
        var sendoSeparados = (from c in data.tbPedidoEnvios
                              join d in data.tbUsuarioExpedicaos on c.idUsuarioSeparacao equals d.idUsuarioExpedicao
                              where c.dataInicioSeparacao != null && c.dataFimSeparacao == null && c.idCentroDeDistribuicao == 4
                              select new
                              {
                                  data = c.dataInicioSeparacao.ToShortDateString(),
                                  d.nome,
                                  c.idPedido,
                                  tempoTotal = (agora - c.dataInicioSeparacao).TotalMinutes,
                                  cor = (agora - c.dataInicioSeparacao).TotalMinutes < 5 ? "#7fffd4" : (agora - c.dataInicioSeparacao).TotalMinutes < 10 ? "#ffec8b" : "#ff4500"
                              }).ToList();
        var sendoSeparadosFormatado = (from c in sendoSeparados
                                       select new
                                       {
                                           c.data,
                                           c.nome,
                                           c.idPedido,
                                           tempoTotal = c.tempoTotal,
                                           tempo = c.tempoTotal < 60 ? c.tempoTotal.ToString("0.##") + " min" : c.tempoTotal < (60 * 24) ? (c.tempoTotal / 60).ToString("0.##") + " hr" : (c.tempoTotal / 60 / 24).ToString("0.##") + " dias",
                                           c.cor
                                       }).ToList();

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(sendoSeparadosFormatado.OrderBy(x => x.tempoTotal));
        return produtosJson;
    }


    [WebMethod]
    public static string SendoEmbaladosEnxoval()
    {
        var data = new dbCommerceDataContext();
        var agora = DateTime.Now;
        var sendoSeparados = (from c in data.tbPedidoEnvios
                              join d in data.tbUsuarioExpedicaos on c.idUsuarioEmbalagem equals d.idUsuarioExpedicao
                              where c.dataInicioEmbalagem != null && c.dataFimEmbalagem == null && c.idCentroDeDistribuicao == 4
                              select new
                              {
                                  data = (c.dataInicioEmbalagem ?? DateTime.Now).ToShortDateString(),
                                  d.nome,
                                  c.idPedido,
                                  tempoTotal = (agora - (c.dataInicioEmbalagem ?? DateTime.Now)).TotalMinutes,
                                  cor = (agora - (c.dataInicioEmbalagem ?? DateTime.Now)).TotalMinutes < 5 ? "#7fffd4" : (agora - (c.dataInicioEmbalagem ?? DateTime.Now)).TotalMinutes < 10 ? "#ffec8b" : "#ff4500"
                              }).ToList();
        var sendoSeparadosFormatado = (from c in sendoSeparados
                                       select new
                                       {
                                           c.data,
                                           c.nome,
                                           c.idPedido,
                                           tempoTotal = c.tempoTotal,
                                           tempo = c.tempoTotal < 60 ? c.tempoTotal.ToString("0.##") + " min" : c.tempoTotal < (60 * 24) ? (c.tempoTotal / 60).ToString("0.##") + " hr" : (c.tempoTotal / 60 / 24).ToString("0.##") + " dias",
                                           c.cor
                                       }).ToList();

        JavaScriptSerializer js = new JavaScriptSerializer();
        var produtosJson = js.Serialize(sendoSeparadosFormatado.OrderBy(x => x.tempoTotal));
        return produtosJson;
    }
}