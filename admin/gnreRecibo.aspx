﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="gnreRecibo.aspx.cs" Inherits="gnreRecibo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
	    @page { 
	        margin: 5px;
	        padding:0px;
	    }

	    body{
	        padding:0px;
	        margin: 15px;
	        font-size: 0.54rem;
	    }

		.title {
			color: #303030;
			font-size: 14px;
			font-weight: bold;
			line-height: 100%;
			padding-top: 10px;
			font-family: "Arial";
		}

		tr td:first-child {
			padding-right: 0;
		}

		td {
			padding: 2px 5px;
			color: #303030;
			font-size: 12px;
			line-height: 100%;
			font-family: "Arial";
		}

		.columnLeft {
			width: 176px;
		}

		.bt {
			border-top: 3px solid #000000;
		}

		.bb {
			border-bottom: 3px solid #000000;
		}

		.pt-0 {
			padding-top: 0;
		}

		.pt-5 {
			padding-top: 5px;
		}

		.pb-0 {
			padding-bottom: 0;
		}

		.pb-8 {
			padding-bottom: 8px;
		}

		.pl-45 {
			padding-left: 45px;
		}
	</style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ListView runat="server" id="lstBoleto">
        <ItemTemplate>
            <table cellspacing="0" cellpadding="1" style="width:100%">
			<tr>
				<td colspan="2">
					<img src="itau-logo.jpg" alt="Itaú" />
				</td>
			</tr>
			<tr>
				<td colspan="2" class="title" align="center">
					Banco Itaú - Comprovante de Pagamento <br>
					Tributos Estaduais com código de barras
				</td>
			</tr>
			<tr>
				<td class="columnLeft bt bb pb-0" align="right">
					<strong>Identificação no extrato:</strong>
				</td>
				<td class="columnRight bt bb">
					SISPAG TRIBUTOS
				</td>
			</tr>
			<tr>
				<td colspan="2" class="pt-5">
					<strong>Dados da conta debitada:</strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft" align="right">
					Nome:
				</td>
				<td class="columnRight">
					<strong style="font-size: 13px;">LINDSAY FERRANDO ME</strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft pb-0" align="right">
					Agência:
				</td>
				<td class="columnRight pb-0">
					<strong>2973</strong>
					<span class="pl-45">
						Conta: <strong>15805 - 4</strong>
					</span>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="bt pb-8">
					<strong>Dados do pagamento:</strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft pb-8 pt-0" align="right">
					Código de barras:
				</td>
				<td class="columnRight pb-8 pt-0">
					<strong><%# Eval("linhaDigitavelCodigoDeBarras") %></strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft pb-8 pt-0" align="right">
					Controle:
				</td>
				<td class="columnRight pb-8 pt-0">
					<strong><%# Eval("autenticacaoPagamentoGnre") %></strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft pb-8 pt-0" align="right">
					Valor do documento:
				</td>
				<td class="columnRight pb-8 pt-0">
					<strong><%# Convert.ToDecimal(Eval("valorPartilha")).ToString("C") %></strong>
				</td>
			</tr>
			<tr>
				<td class="columnLeft pb-0 pt-0" align="right">
					Informações fornecidas pelo <br>
					pagador:
				</td>
				<td class="columnRight">
					
				</td>
			</tr>
			<tr>
				<td colspan="2" class="bt bb pb-0">
					<strong>Operação efetuada em <%# Convert.ToDateTime(Eval("dataEfetivacaoPagamentoGnre")).ToShortDateString() %> via Sispag, CTRL <%# Eval("autenticacaoPagamentoGnre") %>.</strong>
				</td>
			</tr>
			<%--<tr>
				<td colspan="2" class="pt-5">
					<strong>Autenticação:</strong>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					3A8BE82147350AEF9307B9E665F0545E531C2D3C
				</td>
			</tr>--%>
		</table>
        </ItemTemplate>
    </asp:ListView>
    </form>
</body>
</html>
