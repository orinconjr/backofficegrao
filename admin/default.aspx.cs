﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        /*var separar = new SepararWs();
        separar.ListaProdutosSeparacao("1160546", "318", 0);*/
        /*var data = new dbCommerceDataContext();
        var envios = (from c in data.tbPedidoEnvios where c.formaDeEnvio == "transfolha" select c).ToList();
        foreach(var envio in envios)
        {
            var nota = (from c in data.tbNotaFiscals where c.idPedidoEnvio == envio.idPedidoEnvio select c).First();
            string xmlNota = "";
            if (string.IsNullOrEmpty(nota.xmlBase64))
            {
                xmlNota = rnNotaFiscal.retornaXmlNota(nota.numeroNota, nota.idNotaFiscal);
            }
            else
            {
                xmlNota = rnNotaFiscal.retornaXmlNotaFromBase64(nota.xmlBase64);
            }

            var queueNota = new tbQueue();
            queueNota.agendamento = DateTime.Now;
            queueNota.idRelacionado = nota.numeroNota;
            queueNota.tipoQueue = 44;
            queueNota.concluido = false;
            queueNota.andamento = false;
            queueNota.mensagem = xmlNota;
            data.tbQueues.InsertOnSubmit(queueNota);
            data.SubmitChanges();
        }
        /*var dataInicio = Convert.ToDateTime("01/11/2017");
        var dataFim = Convert.ToDateTime("01/12/2017");
        var pacotes = (from c in data.tbPedidoPacotes
                      where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim
                      select new
                      {
                          c.idPedidoEnvio,
                          c.idPedido,
                          c.peso,
                          c.altura,
                          c.largura,
                          c.profundidade
                      }).ToList();
        var envios = (from c in data.tbPedidoEnvios
                      where c.dataFimEmbalagem >= dataInicio && c.dataFimEmbalagem < dataFim
                      select new
                      {
                          c.idPedidoEnvio,
                          c.idPedido,
                          c.tbPedido.endCep,
                          c.nfeValor
                      }).ToList();

        int contagem = 0;
        int atual = 0;
        int total = envios.Count();
        foreach (var envio in envios)
        {
            atual++;
            contagem++;
            var volumes = pacotes.Where(x => x.idPedidoEnvio == envio.idPedidoEnvio).ToList();
            int prazoCorreios = 0;
            decimal valorTotalCorreios = 0;
            bool utilizarCorreios = true;
            foreach (var pacote in volumes)
            {
                decimal valorDoPacote = (envio.nfeValor ?? 1) / volumes.Count();
                var consultaCorreios = rnFrete.CalculaFrete(24, Convert.ToInt32(pacote.peso),
                    envio.endCep.Replace("-", "").Trim(), valorDoPacote,
                    Convert.ToDecimal((pacote.largura ?? 0)), Convert.ToDecimal((pacote.altura ?? 0)),
                    Convert.ToDecimal((pacote.profundidade ?? 0)));
                valorTotalCorreios += consultaCorreios.valor;
                if (consultaCorreios.prazo > prazoCorreios) prazoCorreios = consultaCorreios.prazo;
                if (consultaCorreios.valor == 0) utilizarCorreios = false;
            }


            var valorTransporteCorreios = new tbPedidoEnvioValorTransporte();
            valorTransporteCorreios.idPedidoEnvio = envio.idPedidoEnvio;
            valorTransporteCorreios.prazo = prazoCorreios;
            valorTransporteCorreios.servico = "pacgrandesformatos";
            valorTransporteCorreios.valor = utilizarCorreios ? Convert.ToDecimal(valorTotalCorreios.ToString("0.00")) : 0;
            data.tbPedidoEnvioValorTransportes.InsertOnSubmit(valorTransporteCorreios);
            data.SubmitChanges();
        }*/


        /*var dataInicio = Convert.ToDateTime("01/09/2017");
        var dataFim = Convert.ToDateTime("01/12/2017");
        var envios = (from c in data.tbPedidoPacotes where c.tbPedidoEnvio.dataFimEmbalagem >= dataInicio && c.tbPedidoEnvio.dataFimEmbalagem < dataFim select new {
            c.idPedidoEnvio,
            c.idPedido,
            c.tbPedido.endCep,
            c.tbPedidoEnvio.nfeValor,
            c.peso,
            c.altura,
            c.largura,
            c.profundidade,
            c.tbPedidoEnvio.dataFimEmbalagem
        }).ToList();

        string retorno = "data;cep;pesoreal;pesocubado;volumes;nota";
        var idsEnvios = (from c in envios select c.idPedidoEnvio).Distinct();
        foreach(var idEnvio in idsEnvios)
        {
            retorno += Environment.NewLine;
            var pacotes = (from c in envios where c.idPedidoEnvio == idEnvio select c).ToList();
            retorno += (pacotes.First().dataFimEmbalagem ?? DateTime.Now).ToShortDateString() + ";";
            retorno += (pacotes.First().endCep) + ";";
            retorno += (pacotes.Sum(x => x.peso)) + ";";
            decimal pesoTotalCubagem = 0;
            foreach (var volume in pacotes)
            {
                var cubagem = (Convert.ToDecimal(volume.altura * volume.largura * volume.profundidade / 6000));
                if (cubagem > 10 && cubagem > (Convert.ToDecimal(volume.peso) / 1000))
                {
                    pesoTotalCubagem += cubagem;
                }
                else
                {
                    pesoTotalCubagem += (Convert.ToDecimal(volume.peso) / 1000);
                }
            }
            var pesoCubagem = Convert.ToInt32(pesoTotalCubagem * 1000);
            retorno += (pesoCubagem) + ";";
            retorno += (pacotes.Count()) + ";";
            retorno += (pacotes.First().nfeValor ?? 0).ToString("0.00") + ";";
            retorno += Environment.NewLine;

        }
        Response.Write(retorno);*/


        /*var faixasDeCep = (from c in data.tbFaixaDeCeps where c.tipodeEntregaId == 24 select c).ToList();
        foreach(var faixaDeCep in faixasDeCep)
        {
            var faixaAnterior = data.site_CepFaixaPorTipoDeEntrega(Convert.ToInt64(faixaDeCep.faixaInicial), 20).FirstOrDefault();
            if(faixaAnterior != null)
            {
                var faixaDetalhe = (from c in data.tbFaixaDeCeps where c.faixaDeCepId == faixaAnterior.faixaDeCepId select c).First();
                faixaDeCep.prazo = faixaDetalhe.prazo;
                faixaDeCep.porcentagemDeDesconto = faixaDetalhe.porcentagemDeDesconto;
                faixaDeCep.porcentagemDoSeguro = faixaDetalhe.porcentagemDoSeguro;
                faixaDeCep.ativo = faixaDetalhe.ativo;
                faixaDeCep.interiorizacao = faixaDetalhe.interiorizacao;
                string tabela = faixaDetalhe.idTipoDeEntregaTabelaPreco.ToString();
                tabela = tabela.Replace("27934", "28071");
                tabela = tabela.Replace("27935", "28072");
                tabela = tabela.Replace("27936", "28073");
                tabela = tabela.Replace("27937", "28074");
                tabela = tabela.Replace("27938", "28075");
                tabela = tabela.Replace("27939", "28076");
                tabela = tabela.Replace("27940", "28077");
                tabela = tabela.Replace("27942", "28078");
                tabela = tabela.Replace("27943", "28078");
                tabela = tabela.Replace("27944", "28080");
                tabela = tabela.Replace("27945", "28081");
                tabela = tabela.Replace("27946", "28082");
                tabela = tabela.Replace("27947", "28083");
                tabela = tabela.Replace("27948", "28084");

                faixaDeCep.idTipoDeEntregaTabelaPreco = Convert.ToInt32(tabela);
                data.SubmitChanges();
            }
        }
        */


        /*var idInterno = 1027506;
        //var envio = (from c in data.tbPedidoEnvios where c.idPedido == 1027506  select c).First();
        var notaAlterar = (from c in data.tbNotaFiscals where  c.idPedidoEnvio == 685301 select c).First();
        var nota = rnNotaFiscal.gerarNotaFiscal(idInterno, "1", new List<int>(), true, 685301);
        notaAlterar.idPedidoEnvio = null;
        data.SubmitChanges();






        //rnNotaFiscal.autorizarNota(nota.xml, 685634, nota.idNotaFiscal);
        //var embalagem = new EmbalagemWs();
        //embalagem.SelecionarPedidoCd5(44);
        //embalagem.EmitirCorreiosInterno(629858, 934878, 1,1);
        //rnNotaFiscal.emiteNotasCompletas(604246);
        // rnNotaFiscal.gerarNotaFiscal(907373, "", new List<int>(), true, 603981);
        /*
        embalagem.RetornaProximaEmissao("glmp152029", 4, 0, 911376);*/


        //
        // embalagem.SelecionarPedidoCd2(181);
        /*var pedidos = rnPedidos.verificaIdsPedidosCompletos(4, false, null);
        foreach(var pedido in pedidos)
        {
            Response.Write(pedido + "<br>");

        }
       // embalagem.GravaPacotesSemPesoAguardaConferenciaPacote(206, 928319, 1, false);
        /*var pedidoFornecedorWs = new PedidosFornecedorWs();
        var data = new dbCommerceDataContext();
        var itensPedido = (from c in data.tbPedidoFornecedorItems where c.idPedidoFornecedor == 47366 && c.entregue == false select c.idPedidoFornecedorItem).ToList();
        foreach (var itemPedido in itensPedido)
        {
            pedidoFornecedorWs.RegistrarEntradaRomaneioComEndereco(itemPedido, 75, 88, "glmp152029");
        }*/

        /*var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbPedidos where c.prazoDeEntrega == "0" && c.statusDoPedido == 5 orderby c.pedidoId descending select c).ToList();
        foreach(var pedido in pedidos)
        {
            var cep = pedido.endCep;
            if (!string.IsNullOrEmpty(pedido.endCep))
            {
                var valoresPackage = rnFrete.CalculaFrete(13, Convert.ToInt32(500), cep.Replace("-", "").Trim(), 100, 0, 0, 0);
                var valoresTnt = rnFrete.CalculaFrete(10, Convert.ToInt32(500), cep.Replace("-", "").Trim(), 100, 0, 0, 0);

                int prazo = valoresPackage.prazo;
                if (valoresTnt.prazo > prazo) prazo = valoresTnt.prazo;

                pedido.prazoDeEntrega = prazo.ToString();
                data.SubmitChanges();

            }
        }
        string teste = "";*/
        /*var emissao = new EmbalagemWs();
        emissao.SelecionarPedidoCd2(44);*/
        //emissao.NotaImpressaTnt("22009513000104", 188612, 75);

        /*var separar = new EmbalagemWs();
        var lista = separar.SelecionarPedidoCd2(181);*/
        // Response.Write(lista.pedidoId);
        //return;
        //int pedidoId = 301304;
        //var clearSaleRepo = new BarkCommerce.Core.Repositories.AnaliseRisco.ClearSaleRepository();
        //var maxiRepo = new BarkCommerce.Core.Repositories.Gateway.MaxipagoRepository();
        //var pagamentoRepo = new BarkCommerce.Core.Repositories.Pagamento.PagamentoRepository(clearSaleRepo, maxiRepo);
        //var resultados = clearSaleRepo.ObterResultadosAnaliseRisco();

        //foreach (var resultado in resultados)
        //{
        //    rnInteracoes.interacaoInclui(resultado.PedidoId,
        //        "Score: " + resultado.Score + " - Status: " + resultado.Status, "Andre", "False");
        //    clearSaleRepo.BaixarAnaliseProcessada(resultado.PedidoId);
        //}
        //var detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343096);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343097);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343098);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343099);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343100);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343101);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343102);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343103);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343104);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);
        //detalhePagamento = pagamentoRepo.RetornaDetalhesPagamento(343105);
        //clearSaleRepo.EnviarAnaliseRisco(detalhePagamento);















        //var data = new dbCommerceDataContext();
        //var produtosAguardandoEstoque = (from c in data.tbItemPedidoEstoques
        //                                 join d in data.tbItensPedidos on c.itemPedidoId equals d.itemPedidoId
        //                                 where
        //                                     c.dataLimite != null && c.cancelado == false && c.reservado == false &&
        //                                     d.pedidoId == pedidoId
        //                                 select c).ToList();

        //var aguardandoGeral = (from c in data.tbItemPedidoEstoques
        //                       where produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.produtoId) && c.reservado == false && c.cancelado == false && c.dataLimite != null
        //                       orderby c.dataLimite
        //                       select new
        //                       {
        //                           c.idItemPedidoEstoque,
        //                           c.tbItensPedido.pedidoId,
        //                           c.dataCriacao,
        //                           dataLimiteReserva = c.dataLimite,
        //                           c.produtoId,
        //                           c.itemPedidoId
        //                       }).ToList();
        //var pedidosFornecedorGeral = (from c in data.tbPedidoFornecedorItems
        //                              join d in data.tbProdutoFornecedors on c.tbPedidoFornecedor.idFornecedor equals d.fornecedorId
        //                              where (c.entregue == false | (c.entregue == true && (c.liberado ?? false) == false)) && produtosAguardandoEstoque.Select(x => x.produtoId).Contains(c.idProduto)
        //                              orderby c.tbPedidoFornecedor.dataLimite, c.tbPedidoFornecedor.idPedidoFornecedor
        //                              select new
        //                              {
        //                                  c.idPedidoFornecedor,
        //                                  c.idPedidoFornecedorItem,
        //                                  dataLimiteFornecedor = c.tbPedidoFornecedor.dataLimite,
        //                                  c.idProduto,
        //                                  d.fornecedorNome,
        //                                  c.entregue
        //                              }).ToList();


        //var embalagem = new EmbalagemWs();
        //var pedidos = embalagem.SelecionarPedido(107);

        //Response.Redirect("admin/default.aspx");


        /* Dictionary<int, List<string>> tags = new Dictionary<int, List<string>>();
         tags.Add(1, new List<string>() { "Az", "Ver", "Rox", "Amarelo" });
         tags.Add(2, new List<string>() { "P", "M", "G" });
         tags.Add(3, new List<string>() { "100f", "200f", "300f" });
         tags.Add(4, new List<string>() { "Uva", "Morango"});
         var retorno = GetCombos(tags);
         foreach(var ret in retorno)
         {
             Response.Write(ret + "<br>");
         }*/
    }


    List<string> GetCombos(IEnumerable<KeyValuePair<int, List<string>>> remainingTags)
    {
        if (remainingTags.Count() == 1)
        {
            return remainingTags.First().Value;
        }
        else
        {
            var current = remainingTags.First();
            List<string> outputs = new List<string>();
            List<string> combos = GetCombos(remainingTags.Where(tag => tag.Key != current.Key));

            foreach (var tagPart in current.Value)
            {
                foreach (var combo in combos)
                {
                    outputs.Add(tagPart + ";" + combo);
                }
            }

            return outputs;
        }


    }
}






