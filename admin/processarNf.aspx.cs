﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class processarNf : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidos = (from c in data.tbNotaFiscals
                       join d in data.tbPedidos on c.idPedido equals d.pedidoId
                       where c.idCNPJ == 6 && c.statusNfe == 1
                       select d).ToList();
        foreach (var pedido in pedidos)
        {
            string codIbge = "";
            var cepsDc = new dbCommerceDataContext();
            try
            {
                string cepOut = pedido.endCep.Replace("-", "");
                var endereco =
                    (from c in cepsDc.tbCepEnderecos where c.cep == cepOut.Replace("-", "") select c).FirstOrDefault
                        ();
                if (endereco != null)
                {
                    var cidade =
                        (from c in cepsDc.tbCepCidades where c.id_cidade == endereco.id_cidade select c)
                            .FirstOrDefault();
                    if (cidade != null)
                    {
                        codIbge = cidade.cod_ibge;
                    }
                }

                if (codIbge == "0") codIbge = "";
                if (string.IsNullOrEmpty(codIbge))
                {
                    string nomeCidade = pedido.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                    var cidade = (from c in cepsDc.tbCepCidades
                                  where
                                      c.cidade.ToLower().Trim() == nomeCidade &&
                                      c.uf.ToLower() == pedido.endEstado.ToLower()
                                  select c).FirstOrDefault();
                    if (cidade != null)
                    {
                        codIbge = cidade.cod_ibge;
                    }
                }

                if (codIbge == "0") codIbge = "";
                if (string.IsNullOrEmpty(codIbge))
                {
                    string nomeCidade = pedido.endCidade.Replace("´", "'").Replace("`", "'").ToLower().Trim();
                    var nomeCidadeSplit = nomeCidade.Split('(');
                    if (nomeCidadeSplit.Count() > 1)
                    {
                        string nomeParte2 = nomeCidadeSplit[1];
                        var nomeCidadeSplit2 = nomeParte2.Split(')')[0];
                        var cidade = (from c in cepsDc.tbCepCidades
                                      where
                                          c.cidade.ToLower().Trim() == nomeCidadeSplit2 &&
                                          c.uf.ToLower() == pedido.endEstado.ToLower()
                                      select c).FirstOrDefault();
                        if (cidade != null)
                        {
                            codIbge = cidade.cod_ibge;
                        }
                    }
                }



                var xmlNotaFiscal = rnNotaFiscal.gerarNotaFiscal(pedido.pedidoId, codIbge, new List<int>(), false);
                rnNotaFiscal.autorizarNota(xmlNotaFiscal.xml, 0, xmlNotaFiscal.idNotaFiscal);
            }
            catch (Exception)
            {

            }
        }

    }
}