﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pesquisarCobranca : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        litDadosCobranca.Text = String.Empty;
        string ultimosDigitosCartao = txtFinalNumeroCartao.Text;

        if (ultimosDigitosCartao.Length < 4)
        {
             litDadosCobranca.Text = "Nenhuma informação encontrada!";
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Nenhuma informação encontrada!')", true);
            return;
        }

        if (String.IsNullOrEmpty(ultimosDigitosCartao))
        {
            litDadosCobranca.Text = "Nenhuma informação encontrada!";
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Nenhuma informação encontrada!')", true);
            return;
        }

        int numero = Int32.TryParse(txtFinalNumeroCartao.Text, out numero) ? numero : 0;

        if (numero == 0)
        {
            litDadosCobranca.Text = "Nenhuma informação encontrada!";
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Digite apesnas numeros!\\nNenhuma informação encontrada!')", true);
            return;
        }

        try
        {
            using (var data = new dbCommerceDataContext())
            {
                var resultado = (from c in data.tbPedidoPagamentos
                                 join p in data.tbPedidos on c.pedidoId equals p.pedidoId
                                 where c.numeroCartao.EndsWith(ultimosDigitosCartao) && c.cancelado == false
                                 select new { c.pedidoId, c.dataPagamento, c.numeroCartao, c.valorTotal, p.dataHoraDoPedido });

                if (resultado != null)
                {
                    foreach (var item in resultado)
                    {
                        litDadosCobranca.Text += "<div style=\"float:left;margin-left:25px;width:240px;\">pedidoId: " + item.pedidoId.ToString() + "<br>dataDoPedido:" + String.Format("{0}", item.dataHoraDoPedido) + "<br>dataPagamento: " + String.Format("{0}", item.dataPagamento) + "<br>numeroCartao: " + item.numeroCartao + "<br>valor: " + String.Format("{0:c}", item.valorTotal) + "<br><br></div>";
                    }
                }
                else
                {
                    litDadosCobranca.Text = "Nenhuma informação encontrada!";
                    ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Nenhuma informação encontrada!')", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "')", true);
        }


    }
}