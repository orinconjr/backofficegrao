﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dashboardExpedicao1.aspx.cs" Inherits="dashboardExpedicao1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard</title>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            var painelatual = 0;
            var contagemTotal = 0;

            var carregarPainel = function(){
                painelatual++;
                contagemTotal++;
                if (painelatual == 1) carregarPainel1();
                else if (painelatual == 2) carregarPainel2();
                else if (painelatual == 3) carregarPainel3();
                else if (painelatual == 4) {
                    carregarPainel5();
                    painelatual = 0;
                }
                else {
                    carregarPainel5();
                    painelatual = 0;
                }

                if (contagemTotal == 31) {
                    location.reload();
                }
                else {
                    setTimeout(carregarPainel, 8000);
                }
            }

            var hidePainels = function ()
            {
                $("#painel1").hide();
                $("#painel2").hide();
                $("#painel3").hide();
                $("#painel4").hide();
                $('#painel5').hide();
            }

            var carregaInformacaoDivCores = function (url, id, amarelo, vermelho, divcor, inverso) {
                $.ajax({
                    url: "dashboardExpedicao1.aspx/" + url,
                    //data: dados,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var retorno = data.d;
                        var valor = parseInt(retorno);
                        if (!inverso){
                            if (valor > vermelho) {
                                $(divcor).css("background-color", "red");
                                $(divcor).css("color", "black");
                            }
                            else if (valor > amarelo) {
                                $(divcor).css("background-color", "yellow");
                                $(divcor).css("color", "black");
                            }
                            else {
                                $(divcor).css("background-color", "green");
                                $(divcor).css("color", "black");
                            }
                        }
                        else {
                            if (valor < vermelho) {
                                $(divcor).css("background-color", "red");
                                $(divcor).css("color", "black");
                            }
                            else if (valor < amarelo) {
                                $(divcor).css("background-color", "yellow");
                                $(divcor).css("color", "black");
                            }
                            else {
                                $(divcor).css("background-color", "green");
                                $(divcor).css("color", "black");
                            }
                        }
                        $(id).html(retorno);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    }
                });
            }

            var carregarPainel1 = function () {
                hidePainels();

                carregaInformacaoDivCores("pedidosAtrasadosEnxoval", "#pedidosAtrasadosEnxoval", 0, 10, "#divPedidosAtrasadosEnxoval", false);
                carregaInformacaoDivCores("pedidosSepararEnxoval", "#pedidosSepararEnxoval", 10, 100, "#divPedidosSepararEnxoval", false);
                carregaInformacaoDivCores("pedidosEmbalarEnxoval", "#pedidosEmbalarEnxoval", 50, 100, "#divPedidosSeparadosEnxoval", false);

                carregaInformacaoDivCores("pedidosAtrasadosMovel", "#pedidosAtrasadosMovel", 0, 10, "#divPedidosAtrasadosMovel", false);
                carregaInformacaoDivCores("pedidosSepararMovel", "#pedidosSepararMovel", 50, 100, "#divPedidosSepararMovel", false);
                carregaInformacaoDivCores("pedidosEmbalarMovel", "#pedidosEmbalarMovel", 50, 100, "#divPedidosSeparadosMovel", false);

                var height = $(window).height();
                $("#painel1").css("height", height);

                $("#tituloEnxoval").css("height", (height / 100) * 10);
                $("#tituloMoveis").css("height", (height / 100) * 10);

                $("#divPedidosAtrasadosEnxoval").css("height", (height / 100) * 40);
                $("#divPedidosSepararEnxoval").css("height", (height / 100) * 40);
                $("#divPedidosSeparadosEnxoval").css("height", (height / 100) * 40);
                $("#divPedidosAtrasadosMovel").css("height", (height / 100) * 40);
                $("#divPedidosSepararMovel").css("height", (height / 100) * 40);
                $("#divPedidosSeparadosMovel").css("height", (height / 100) * 40);
                $("#painel1").show();
            }

            var carregarPainel2 = function () {
                hidePainels();

                carregaInformacaoDivCores("pedidosSeparadosEnxoval", "#pedidosSepadadosEnxoval", 400, 300, "#divPedidosSepadadosEnxoval", true);
                carregaInformacaoDivCores("pedidosEmbaladosEnxoval", "#pedidosEmbaladosEnxoval", 2000, 1500, "#divPedidosEmbaladosEnxoval", true);

                carregaInformacaoDivCores("pedidosSeparadosMovel", "#pedidosSepadadosMovel", 1800, 400, "#divPedidosSepadadosMovel", true);
                carregaInformacaoDivCores("pedidosEmbaladosMovel", "#pedidosEmbaladosMovel", 1800, 400, "#divPedidosEmbaladosMovel", true);

                var height = $(window).height();
                $("#painel2").css("height", height);

                $("#tituloEnxoval2").css("height", (height / 100) * 10);
                $("#tituloMoveis2").css("height", (height / 100) * 10);

                $("#divPedidosSepadadosEnxoval").css("height", (height / 100) * 40);
                $("#divPedidosEmbaladosEnxoval").css("height", (height / 100) * 40);
                $("#divPedidosSepadadosMovel").css("height", (height / 100) * 40);
                $("#divPedidosEmbaladosMovel").css("height", (height / 100) * 40);

                $("#painel2").show();

            }

            var carregarPainel3 = function () {
                hidePainels();
                var height = $(window).height();
                $("#painel3").css("height", height);
                $("#titulosSeparandoEmbaladando").css("height", (height / 100) * 10);
                $("#listaSeparandoEmbalando").css("height", (height / 100) * 90);

                $.ajax({
                    url: "dashboardExpedicao1.aspx/SendoSeparadosEnxoval",
                    //data: dados,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var retorno = data.d;
                        console.log(retorno)
                        var pedidos = JSON.parse(retorno);

                        var listaSeparados = "";

                        for (var i = 0; i < pedidos.length; i++) {
                            var pedido = pedidos[i];
                            console.log(pedido);
                            listaSeparados += "<tr style='background: " + pedido.cor + "'>";
                            listaSeparados += "<td>" + pedido.nome + "</td>";
                            listaSeparados += "<td>" + pedido.tempo + "</td>";
                            listaSeparados += "</tr>";
                        }

                        $("#conteudoSeparandoEnxoval").html(listaSeparados);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    }
                });


                $.ajax({
                    url: "dashboardExpedicao1.aspx/SendoEmbaladosEnxoval",
                    //data: dados,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var retorno = data.d;
                        console.log(retorno)
                        var pedidos = JSON.parse(retorno);

                        var listaSeparados = "";

                        for (var i = 0; i < pedidos.length; i++) {
                            var pedido = pedidos[i];
                            console.log(pedido);
                            listaSeparados += "<tr style='background: " + pedido.cor + "'>";
                            listaSeparados += "<td>" + pedido.nome + "</td>";
                            listaSeparados += "<td>" + pedido.tempo + "</td>";
                            listaSeparados += "</tr>";
                        }

                        $("#conteudoEmbalandoEnxoval").html(listaSeparados);

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    }
                });


                $("#painel3").show();
            }

            var carregaListaTop10Balanca = function () {
                $.ajax({
                    url: "dashboardExpedicao1.aspx/Top10ListaPedidosBalanca",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var retorno = data.d;

                        console.log("Retorno: " + retorno);

                        var objJson = JSON.parse(retorno);

                        var table = "";
                        table += "<table style='width: 100%;'>";
                        table += "<thead>";
                        table += "<tr style='width: 100%;' >";
                        table += "<td style='width: 50%;'>Nº Pedido</td>";
                        table += "<td style='width: 50%'>Tempo</td>";
                        table += "</tr>";
                        table += "</thead>";

                        var fimTable = "</table>";
                        var eachrow = "";
                        $.each(objJson, function (index) {
                            eachrow += "<tr style='background: " + objJson[index].cor + "'>"
                                + "<td style='width: 50%;'>" + objJson[index].pedidoId + "</td>"
                                + "<td style='width: 50%;'>" + objJson[index].dif + "</td>"
                                + "</tr>";
                        });

                        $('#divTop10ListaBalanca').html(table + eachrow + fimTable);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    }
                });
            }

            var carregaListaTop10Transportadora = function () {
                $.ajax({
                    url: "dashboardExpedicao1.aspx/Top10ListaPedidosTransportadora",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataFilter: function (data) { return data; },
                    success: function (data) {
                        var retorno = data.d;

                        console.log("Retorno: " + retorno);

                        var objJson = JSON.parse(retorno);
                        var table = "";
                        table += "<table style='width: 100%;'>";
                        table += "<thead>";
                        table += "<tr style='width: 100%'>";
                        table += "<td style='width: 33.33%'>Nº Pedido</td>";
                        table += "<td style='width: 33.33%'>Transportadora</td>";
                        table += "<td style='width: 33.33%'>Tempo</td>";
                        table += "</tr>";
                        table += "</thead >";
                        table += "<tbody>"
                        var eachrow = "";
                        $.each(objJson, function (index) {
                            eachrow += "<tr style='background: " + objJson[index].cor + "'>"
                                + "<td style='width: 33.33%;'>" + objJson[index].pedidoid + "</td>"
                                + "<td style='width: 33.33%;'>" + objJson[index].transportadora + "</td>"
                                + "<td style='width: 33.33%;'>" + objJson[index].dif + "</td>"
                                + "</tr>";
                        });
                        var fimTable = "</table>";
                        $('#divListaTransportadoda').html(table + eachrow + fimTable);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {

                    }
                });
            }

            var carregarPainel5 = function () {
                hidePainels();
                carregaInformacaoDivCores("qtdPedidosBalanca", "#qtdPedidosBalanca", 0, 10, "#divTotalBalanca", false);
                carregaInformacaoDivCores("qtdPedidosTransportadora", "#qtdPedidosTransportadora", 0, 10, "#divTotalTransportadora", false);

                carregaListaTop10Balanca();
                carregaListaTop10Transportadora();

                var height = $(window).height();
                $("#painel5").css("height", height);

                $("#divTotalBalanca").css("height", (height / 100) * 40);
                $("#divTotalTransportadora").css("height", (height / 100) * 40);

                $("#divListaBalanca").css("height", (height / 100) * 40);
                $("#divListaTransportadora").css("height", (height / 100) * 40);
                $("#painel5").show();
            }
            carregarPainel();
        });
    </script>

    <style>
        body{
            margin: 0;
            padding: 0;
            font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
        p {
            text-align: center;
            position: relative;
            top: 50%;
            -ms-transform: translateY(-50%);
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
            padding: 0;
            margin: 0;
        }
        table, tr, td {
            border: 0; padding: 0; margin: 0;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="painel1" style="display: none;">
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;" id="tituloEnxoval">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;">
                                PENDENCIAS ENXOVAL:
                           </td>
                    </tr>
                </table>
            </div>
            
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosAtrasadosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%;">
                                    ATRASADOS<br />
                                    <span id="pedidosAtrasadosEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepararEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    SEPARAR<br />
                                    <span id="pedidosSepararEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSeparadosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    EMBALAR<br />
                                    <span id="pedidosEmbalarEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>            
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; clear:both;" id="tituloMoveis">
                <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;">
                                    PENDENCIAS MÓVEIS:
                               </td>
                        </tr>
                    </table>
            </div>
            
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosAtrasadosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    ATRASADOS<br />
                                    <span id="pedidosAtrasadosMovel" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepararMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                SEPARAR<br />
                                <span id="pedidosSepararMovel" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle; text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSeparadosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                EMBALAR<br />
                                <span id="pedidosEmbalarMovel" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="painel2" style="display: none;">
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;" id="tituloEnxoval2">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;">
                            CONCLUÍDOS HOJE ENXOVAL:
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 50%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepadadosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                SEPARADOS<br />
                                <span id="pedidosSepadadosEnxoval" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 50%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosEmbaladosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                EMBALADOS<br />
                                <span id="pedidosEmbaladosEnxoval" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>            
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; clear:both;" id="tituloMoveis2">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;line-height: 190%;">
                            CONCLUÍDOS HOJE MÓVEIS:
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 50%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepadadosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;line-height: 190%;">
                            SEPARADOS<br />
                            <span id="pedidosSepadadosMovel" style="font-size: 200px; ">50</span>
                        </td>
                    </tr>
                </table>
                </div>
            </div>
            <div style="width: 50%; float: left;vertical-align: middle; text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosEmbaladosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%; line-height: 190%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                            EMBALADOS<br />
                            <span id="pedidosEmbaladosMovel" style="font-size: 200px;">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="painel3" style="display: none;">            
            <div style="width: 100%; font-size: 40px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;" id="titulosSeparandoEmbaladando">
                <table style="width: 100%; height: 100%; vertical-align: middle;">
                    <tr>
                        <td>
                            PEDIDOS EM ABERTO
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 100%; font-size: 40px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;" id="listaSeparandoEmbalando">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td id="separandoEnxoval" style="width: 50%;">
                            <table style="width: 100%; color: #000000; font-size: 45px;" id="conteudoSeparandoEnxoval">

                            </table>
                        </td>
                        <td id="embalandoEnxoval">                            
                            <table style="width: 100%; color: #000000; font-size: 45px;" id="conteudoEmbalandoEnxoval">

                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="painel4" style="display: none;">                     
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;" id="tituloEnxoval">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;">
                                PENDENCIAS ENXOVAL:
                           </td>
                    </tr>
                </table>
            </div>
            
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosAtrasadosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%;">
                                    ATRASADOS<br />
                                    <span id="pedidosAtrasadosEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepararEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    SEPARAR<br />
                                    <span id="pedidosSepararEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSeparadosEnxoval">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    EMBALAR<br />
                                    <span id="pedidosEmbalarEnxoval" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>            
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; clear:both;" id="tituloMoveis">
                <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;">
                                    PENDENCIAS MÓVEIS:
                               </td>
                        </tr>
                    </table>
            </div>
            
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosAtrasadosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                    ATRASADOS<br />
                                    <span id="pedidosAtrasadosMovel" style="font-size: 200px; ">50</span>
                               </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle;text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSepararMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                SEPARAR<br />
                                <span id="pedidosSepararMovel" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 33.33%; float: left;vertical-align: middle; text-align: center; font-size: 75px;font-weight: bold;" id="divPedidosSeparadosMovel">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle;line-height: 190%;">
                                EMBALAR<br />
                                <span id="pedidosEmbalarMovel" style="font-size: 200px; ">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div id="painel5" style="display: none;">
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; vertical-align: middle;">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle; width: 50%">BALANÇA<br />
                        </td>
                        <td style="vertical-align: middle; width: 50%">TRANSPORTADORA<br />
                        </td>
                    </tr>
                </table>
            </div>

            <div style="width: 50%; float: left; vertical-align: middle; text-align: center; font-size: 75px; font-weight: bold;" id="divTotalBalanca">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%;">
                                <span id="qtdPedidosBalanca" style="font-size: 200px;">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 50%; float: left; vertical-align: middle; text-align: center; font-size: 75px; font-weight: bold;" id="divTotalTransportadora">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%;">
                                <span id="qtdPedidosTransportadora" style="font-size: 200px;">50</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 100%; font-size: 50px; font-weight: bold; background: #000000; color: #FFFFFF; text-align: center; clear: both;" id="tituloMoveis">
                <table style="width: 100%; height: 100%;">
                    <tr>
                        <td style="vertical-align: middle;">INFORMAÇÕES ANALÍTICAS                                    
                        </td>
                    </tr>
                </table>
            </div>

            <div style="width: 50%; float: left; vertical-align: middle; text-align: center; font-weight: bold;" id="divListaBalanca">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%; vertical-align: top;">
                                <div id="divTop10ListaBalanca">
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="width: 50%; float: left; vertical-align: middle; text-align: center; font-weight: bold;" id="divListaTransportadora">
                <div style="border: 3px solid #000000; height: 100%;">
                    <table style="width: 100%; height: 100%;">
                        <tr>
                            <td style="vertical-align: middle; line-height: 190%; vertical-align: top;">
                                <div id="divListaTransportadoda"></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
