﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class imprimirCatalogo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        FillGrid();

    }

    private void FillGrid()
    {
        int idMarca = 0;
        int fornecedor = 0;
        if (Request.QueryString["fornecedor"] != null) fornecedor = Convert.ToInt32(Request.QueryString["fornecedor"]);
        if (Request.QueryString["marca"] != null) idMarca = Convert.ToInt32(Request.QueryString["marca"]);
        using (var data = new dbCommerceDataContext())
        {
            //var dados = (from c in data.tbProdutos
            //             join comp in data.tbInformacaoAdcionals on c.produtoId equals comp.produtoId into produtoComposicao
            //             where c.produtoFornecedor == fornecedor && c.produtoAtivo.ToLower().Contains("true")
            //             select new { c.fotoDestaque, c.produtoNome, c.produtoId, referencia = "Ref: " + c.produtoId, c.largura, c.altura, composicao = produtoComposicao.FirstOrDefault(x => x.informacaoAdcionalNome.ToLower().Contains("composi")).informacaoAdcionalConteudo, descricao = produtoComposicao.FirstOrDefault(x => x.informacaoAdcionalNome.ToLower().Contains("descri")).informacaoAdcionalConteudo }).ToList();

            var dados = (from c in data.tbProdutos
                         join comp in data.tbInformacaoAdcionals on c.produtoId equals comp.produtoId into produtoDescricao
                         where (fornecedor > 0 ? c.produtoFornecedor == fornecedor : c.produtoFornecedor > 0) && (idMarca > 0 ? c.produtoMarca == idMarca : c.produtoMarca > 0) && c.produtoAtivo.ToLower().Contains("true")
                         select new { c.fotoDestaque, c.produtoNome, c.produtoId, referencia = "Ref: " + c.produtoId, c.largura, c.altura, composicao = produtoDescricao.FirstOrDefault(x => x.informacaoAdcionalNome.ToLower().Contains("composi")).informacaoAdcionalConteudo, descricao = produtoDescricao.FirstOrDefault(x => x.informacaoAdcionalNome.ToLower().Contains("descri")).informacaoAdcionalConteudo }).ToList().Take(1750).Distinct();

            if (Request.QueryString["filtro"] != null)
            {
                if (Request.QueryString["filtro"].ToLower() == "todos")
                {
                    dados = dados.OrderBy(x => x.produtoNome).ToList().Distinct();
                    GridView1.DataSource = dados.ToList();
                    GridView1.DataBind();
                }
                else
                {
                    int categoriaId = Convert.ToInt32(Request.QueryString["filtro"]);
                    List<int> produtosIdLista = dados.Select(id => id.produtoId).Distinct().ToList();

                    var produtosDaCategoria =
                        (from c in data.tbJuncaoProdutoCategorias
                         where produtosIdLista.Contains(c.produtoId) && c.categoriaId == categoriaId
                         select c.produtoId).Distinct();

                    //string produtoPesquisa = Request.QueryString["filtro"].ToLower();
                    //dados = dados.Where(x => x.produtoNome.ToLower().Contains(produtoPesquisa)).ToList();

                    List<int> produtosFiltro = new List<int>();
                    produtosFiltro.AddRange(produtosDaCategoria);

                    dados = dados.Where(x => produtosFiltro.Contains(x.produtoId)).OrderBy(x => x.produtoNome).ToList().Distinct();
                    GridView1.DataSource = dados.ToList();
                    GridView1.DataBind();
                }
            }
            else
            {
                dados = dados.OrderBy(x => x.produtoNome).Distinct();
                GridView1.DataSource = dados.ToList();
                GridView1.DataBind();
            }


        }
    }

    protected void GridView1_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        FillGrid();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (e.Row.RowIndex % 5 == 0 & e.Row.RowIndex !=0)
            {
                e.Row.CssClass = "page-break";
            }
            
     
        }
    }
}