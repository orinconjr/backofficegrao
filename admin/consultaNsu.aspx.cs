﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class consultaNsu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void FillGrid()
    {

        List<string> nsu = new List<string>(txtNsu.Text.Split(',').Select(n => Convert.ToInt32(n).ToString())).ToList();

        using (var data = new dbCommerceDataContext())
        {
            var pedidos = (from c in data.tbPedidoPagamentos where nsu.Contains(c.numeroCobranca) select c).ToList();

            grd.DataSource = pedidos;
            grd.DataBind();

            if (grd.Rows.Count != 0) return;
            grd.DataSource = (from c in data.tbPedidoPagamentoGateways
                              join p in data.tbPedidoPagamentos on c.idPedidoPagamento equals p.idPedidoPagamento
                              where nsu.Contains(c.processorReferenceNumber)
                              select new { numeroCobranca = c.processorReferenceNumber, p.pedidoId, p.valor }).ToList();
            grd.DataBind();
        }
    }


    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        FillGrid();
    }
}