﻿<%@ Page Title="" Language="C#" Theme="Glass" MasterPageFile="~/admin/admin.master" AutoEventWireup="true" CodeFile="sendoEmbaladosAtual.aspx.cs" Inherits="admin_sendoEmbaladosAtual" %>

<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v11.1" namespace="DevExpress.Web.ASPxPopupControl" tagprefix="dx" %>
<%@ Register namespace="Controls" tagprefix="uc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="conteudoPrincipal" Runat="Server">
    <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Pedidos em Separação e Embalagem</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" OnClick="btPdf_Click" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" OnClick="btXsl_Click" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" OnClick="btRtf_Click" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" OnClick="btCsv_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" KeyFieldName="pedidoId" Width="834px" EnableCallBacks="False"
                    Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"  OnCustomUnboundColumnData="grd_OnCustomUnboundColumnData">
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="50" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" ShowGroupButtons="False" 
                        ShowHeaderFilterButton="True" ShowFooter="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Pedido" FieldName="pedidoIdCliente" VisibleIndex="0" UnboundType="Integer">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Usuário" FieldName="nome" UnboundType="String" VisibleIndex="0">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Inicio Embalagem" FieldName="dataSendoEmbalado" UnboundType="DateTime" VisibleIndex="3" Width="120px">
                            <PropertiesDateEdit DisplayFormatString="" EditFormat="DateTime">
                                <CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            </PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                        <dxwgv:GridViewDataTextColumn VisibleIndex="8" Width="80">
                            <DataItemTemplate>
                                <asp:Button runat="server" ID="btnCancelar" Text="Cancelar Embalagem" OnCommand="btnCancelar_OnCommand" CommandArgument='<%# Eval("pedidoId") %>' />
                            </DataItemTemplate>
                        </dxwgv:GridViewDataTextColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
              <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>
    
    <dx:ASPxPopupControl ID="pcCancelarEmbalagem" runat="server" CloseAction="CloseButton" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="TopSides" ClientInstanceName="pcCancelarEmbalagem"
        HeaderText="Cancelar Embalagem" AllowDragging="True" PopupAnimationType="None" EnableViewState="True">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <asp:HiddenField runat="server" ID="hdfPedidoId" />
                <div style="width: 1000px; height: 500px; overflow: scroll;">
                    <div>
                        <table style="width: 100%">
                            <tr class="rotulos">
                                <td colspan="3">
                                    Motivo para voltar o pedido para separação:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:TextBox CssClass="campos" runat="server" ID="txtMotivo" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="rotulos">
                                <td style="width: 90%; font-weight: bold; padding-top: 30px;">
                                    Produto
                                </td>
                                <td style="padding-left: 20px; font-weight: bold;">
                                    Qtd.
                                </td>
                                <td style="padding-left: 20px; font-weight: bold;">
                                    Faltando
                                </td>
                            </tr>
                        <asp:ListView runat="server" ID="lstItensPedido" OnItemDataBound="lstItensPedido_ItemDataBound">
                            <ItemTemplate>
                                <div style="font-weight: bold;">
                                   <%-- <%# Eval("produtoNome") %> --%>
                                    <asp:HiddenField runat="server" ID="hdfProdutoId" Value='<%# Eval("produtoId") %>'/>
                                    <asp:HiddenField runat="server" ID="hdfItemPedidoId" Value='<%# Eval("itemPedidoId") %>'/>
                                    <asp:HiddenField runat="server" ID="hdfQuantidade" Value='<%# Eval("itemQuantidade") %>'/>
                                        <tr>
                                            <td>
                                                <asp:Literal runat="server" ID="litNome"></asp:Literal>
                                            </td>
                                            <td style="padding-left: 20px;">
                                                <asp:DropDownList runat="server" ID="ddlQuantidade"/>
                                            </td>
                                            <td style="padding-left: 20px;">
                                                <asp:CheckBox runat="server" ID="chkCancelar" />
                                            </td>
                                        </tr>                                    
                                </div>
                                <asp:ListView runat="server" ID="lstItensPedidoCombo" OnItemDataBound="lstItensPedidoCombo_OnItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td style="padding-left: 50px;">
                                                <asp:HiddenField runat="server" ID="hdfQuantidadeCombo" Value='<%# Eval("itemQuantidade") %>'/>
                                                <asp:HiddenField runat="server" ID="hdfProdutoIdFilho" Value='<%# Eval("produtoId") %>'/>
                                                <%# Eval("produtoNome") %>
                                            </td>
                                            <td style="padding-left: 20px;">
                                                <asp:DropDownList runat="server" ID="ddlQuantidadeCombo"/>
                                            </td>
                                            <td style="padding-left: 20px;">
                                                <asp:CheckBox runat="server" ID="chkCancelarCombo" />
                                            </td>
                                        </tr>  
                                    </ItemTemplate>
                                </asp:ListView>
                                <tr>
                                    <td colspan="3">
                                        <hr color="#7EACB1" size="1" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                            <tr class="rotulos">
                                <td style="width: 80%; font-weight: bold;">
                                    &nbsp;
                                </td>
                                <td style="width: 80%; font-weight: bold;">
                                    &nbsp;
                                </td>
                                <td style="padding-left: 20px; font-weight: bold;">
                                    <uc:OneClickButton ID="btnCancelarEmbalagem" runat="server" Text="Cancelar Embalagem" ReplaceTitleTo="Aguarde..." onclick="btnCancelarEmbalagem_OnClick" />
                                </td>
                            </tr>
                            </table>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <ContentStyle>
            <Paddings PaddingBottom="5px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    </asp:Content>