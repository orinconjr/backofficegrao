﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class trocarTransoportadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnPesquisar_OnClick(object sender, EventArgs e)
    {

        int idPedido = Convert.ToInt32(txtNumeroPedido.Text);

        if (idPedido.ToString().Length > 6)
        {
            idPedido = rnFuncoes.retornaIdInterno(idPedido);
        }

        using (var data = new dbCommerceDataContext())
        {
            var pedido = (from p in data.tbPedidos
                          join pp in data.tbPedidoPacotes on p.pedidoId equals pp.idPedido
                          where p.pedidoId == idPedido && (pp.rastreio ?? "") == ""
                          select new
                          {
                              p.endNomeDoDestinatario,
                              p.endRua,
                              p.endNumero,
                              p.endComplemento,
                              p.endBairro,
                              p.endCidade,
                              p.endEstado,
                              p.endCep,
                              p.endReferenciaParaEntrega,
                              pp.formaDeEnvio
                          }).FirstOrDefault();

            if (pedido != null)
            {
                lblTransportadora.Text = pedido.formaDeEnvio;
                lblNome.Text = pedido.endNomeDoDestinatario;
                lblRua.Text = pedido.endRua;
                lblNumero.Text = pedido.endNumero;
                lblComplemento.Text = pedido.endComplemento;
                lblBairro.Text = pedido.endBairro;
                lblCidade.Text = pedido.endCidade;
                lblEstado.Text = pedido.endEstado;
                lblCep.Text = pedido.endCep;
                lblReferencia.Text = pedido.endReferenciaParaEntrega;

                ddlAlterarTranportadora.Items.Clear();
                string formaDeEnvio = pedido.formaDeEnvio.ToLower();
                if (formaDeEnvio == "jadlog" | formaDeEnvio == "jadlogcom")
                {
                    ddlAlterarTranportadora.Items.Add(new ListItem("tnt", "tnt"));
                    ddlAlterarTranportadora.Items.Add(new ListItem("belle", "belle"));
                }
                else if (formaDeEnvio == "tnt")
                {
                    ddlAlterarTranportadora.Items.Add(new ListItem("jadlog", "jadlog"));
                    ddlAlterarTranportadora.Items.Add(new ListItem("belle", "belle"));
                }
                else if (formaDeEnvio == "belle")
                {
                    ddlAlterarTranportadora.Items.Add(new ListItem("jadlog", "jadlog"));
                    ddlAlterarTranportadora.Items.Add(new ListItem("tnt", "tnt"));
                }
                else if (formaDeEnvio == "pac")
                {
                    ddlAlterarTranportadora.Items.Add(new ListItem("jadlog", "jadlog"));
                    ddlAlterarTranportadora.Items.Add(new ListItem("tnt", "tnt"));
                    ddlAlterarTranportadora.Items.Add(new ListItem("belle", "belle"));
                }
                btnConfirmar.Visible = true;
            }
            else
            {
                lblTransportadora.Text = "";
                lblNome.Text = "";
                lblRua.Text = "";
                lblNumero.Text = "";
                lblComplemento.Text = "";
                lblBairro.Text = "";
                lblCidade.Text = "";
                lblEstado.Text = "";
                lblCep.Text = "";
                lblReferencia.Text = "";

                ddlAlterarTranportadora.Items.Clear();
                btnConfirmar.Visible = false;

                ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Pedido não encontrado!')", true);
            }


        }

    }

    protected void btnConfirmar_OnClick(object sender, EventArgs e)
    {
        try
        {
            int idPedido = Convert.ToInt32(txtNumeroPedido.Text);

            if (idPedido.ToString().Length > 6)
            {
                idPedido = rnFuncoes.retornaIdInterno(idPedido);
            }

            using (var data = new dbCommerceDataContext())
            {
                var pedidoPacotes = (from pp in data.tbPedidoPacotes where pp.idPedido == idPedido && (pp.rastreio ?? "") == "" select pp);
                var pedidoEnvios = (from pe in data.tbPedidoEnvios where pe.idPedido == idPedido && pe.idPedidoEnvio == pedidoPacotes.First().idPedidoEnvio select pe);

                foreach (var pedidoPacote in pedidoPacotes)
                {

                    if (ddlAlterarTranportadora.SelectedValue == "tnt")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.codJadlog = "";
                        pedidoPacote.formaDeEnvio = "tnt";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "jadlog" | ddlAlterarTranportadora.SelectedValue == "jadlogcom")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.formaDeEnvio = "jadlog";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "belle")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.formaDeEnvio = "belle";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "pac")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.formaDeEnvio = "pac";
                    }

                    data.SubmitChanges();
                }

                foreach (var pedidoEnvio in pedidoEnvios)
                {
                    if (ddlAlterarTranportadora.SelectedValue == "tnt")
                    {
                        pedidoEnvio.formaDeEnvio = "tnt";
                        pedidoEnvio.nfeObrigatoria = true;
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "jadlog" | ddlAlterarTranportadora.SelectedValue == "jadlogcom")
                    {
                        pedidoEnvio.formaDeEnvio = "jadlog";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "belle")
                    {
                        pedidoEnvio.formaDeEnvio = "belle";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "pac")
                    {
                        pedidoEnvio.formaDeEnvio = "pac";
                    }

                    data.SubmitChanges();
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Alteração realizada com sucesso!');", true);

            txtNumeroPedido.Focus();
            btnConfirmar.Focus();
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "');", true);
        }
    }
}