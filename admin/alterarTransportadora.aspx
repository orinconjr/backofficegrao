﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="alterarTransportadora.aspx.cs" Inherits="alterarTransportadora" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="admin/js/funcoes.js"></script>

    <script type="text/javascript">

        var transportadoraSelecionada = "";

        function PesquisaPedido() {

            var numeroDoPedido = $("#<%=txtNumeroPedido.ClientID%>").val();

            if (numeroDoPedido != "") {

                numeroDoPedido = parseInt(numeroDoPedido);

                $("#<%=lblTransportadora.ClientID%>").html("");
                $("#<%=lblNome.ClientID%>").html("");
                $("#<%=lblRua.ClientID%>").html("");
                $("#<%=lblNumero.ClientID%>").html("");
                $("#<%=lblComplemento.ClientID%>").html("");
                $("#<%=lblBairro.ClientID%>").html("");
                $("#<%=lblCidade.ClientID%>").html("");
                $("#<%=lblEstado.ClientID%>").html("");
                $("#<%=lblCep.ClientID%>").html("");
                $("#<%=lblReferencia.ClientID%>").html("");
                $("#<%=ddlAlterarTranportadora.ClientID%> option").remove();

                $.ajax({
                    type: "POST",
                    url: "alterarTransportadora.aspx/pesquisarPedido",
                    data: '{idPedido:' + numeroDoPedido + '}',
                    contentType: "application/json; charset=utf-8",
                    
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert('XMLHttpRequest: ' + XMLHttpRequest + '\n' + 'textStatus: ' + textStatus + '\n' + errorThrown);
                    }
                }).done( function (data) {
                    alert(data.d);
                    if (data.d != "" && data.d != undefined) {
                        var result = JSON.parse(data.d);

                        $("#<%=lblTransportadora.ClientID%>").html(result[0].formaDeEnvio);
                        $("#<%=lblNome.ClientID%>").html(result[0].endNomeDoDestinatario);
                        $("#<%=lblRua.ClientID%>").html(result[0].endRua);
                        $("#<%=lblNumero.ClientID%>").html(result[0].endNumero);
                        $("#<%=lblComplemento.ClientID%>").html(result[0].endComplemento);
                        $("#<%=lblBairro.ClientID%>").html(result[0].endBairro);
                        $("#<%=lblCidade.ClientID%>").html(result[0].endCidade);
                        $("#<%=lblEstado.ClientID%>").html(result[0].endEstado);
                        $("#<%=lblCep.ClientID%>").html(result[0].endCep);
                        $("#<%=lblReferencia.ClientID%>").html(result[0].endReferenciaParaEntrega);

                        if (result[0].formaDeEnvio == "jadlog") {
                            $("#<%=ddlAlterarTranportadora.ClientID%>").append($('<option>', {
                                value: "tnt",
                                text: "tnt"
                            }));

                            transportadoraSelecionada = "tnt";
                        }
                        else {
                            $("#<%=ddlAlterarTranportadora.ClientID%>").append($('<option>', {
                                value: "jadlog",
                                text: "jadlog"
                            }));

                            transportadoraSelecionada = "jadlog";
                        }

                        $("#<%=btnConfirmar.ClientID%>").show();

                    }
                    else {
                        $("#<%=btnConfirmar.ClientID%>").hide();
                        alert('Pedido não encontrado');
                    }

                });
            }
        }

        function alterarTransportadora() {

            if (confirm('Confirma alteração de transportadora?')) {

                var numeroDoPedido = $("#<%=txtNumeroPedido.ClientID%>").val();

                numeroDoPedido = parseInt(numeroDoPedido);

                $.ajax({
                    type: "POST",
                    url: "alterarTransportadora.aspx/alteraTransportadora",
                    data: "{idPedido:" + numeroDoPedido + ",transportadoraSelecionada:'" + transportadoraSelecionada + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {

                        alert('Alteração realizada com sucesso!');

                    },
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert('XMLHttpRequest: ' + XMLHttpRequest + '\n' + 'textStatus: ' + textStatus + '\n' + errorThrown);
                    }
                });

            } else {
                return false;
            }

        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:HiddenField runat="server" ID="hfFormaDeEnvio" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <div>
                Numero do Pedido:
            <asp:TextBox ID="txtNumeroPedido" runat="server" MaxLength="10" onkeypress="return soNumeroDigito(event)" onblur="PesquisaPedido()"></asp:TextBox>
            </div>

            <div>
                Entregar pela:
                <b>
                    <asp:Label ID="lblTransportadora" runat="server" Text=""></asp:Label></b>
                Alterar para:
           <b>
               <asp:DropDownList ID="ddlAlterarTranportadora" runat="server"></asp:DropDownList></b>
                <asp:Button ID="btnConfirmar" runat="server" Text="Confimar" Style="display: none;" OnClientClick="alterarTransportadora()" />
            </div>

            <br />
            <div>
                Nome para Entrega:
                <asp:Label ID="lblNome" runat="server" Text=""></asp:Label>
            </div>
            <div>
                Rua:
                <asp:Label ID="lblRua" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Numero:
                <asp:Label ID="lblNumero" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Complemento:
                <asp:Label ID="lblComplemento" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Bairro:
                <asp:Label ID="lblBairro" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Cidade:
                <asp:Label ID="lblCidade" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Estado:
                <asp:Label ID="lblEstado" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Cep:
                <asp:Label ID="lblCep" runat="server" Text="Label"></asp:Label>
            </div>
            <div>
                Referência para Entrega:
                <asp:Label ID="lblReferencia" runat="server" Text="Label"></asp:Label>

            </div>
        </div>
    </form>
</body>
</html>
