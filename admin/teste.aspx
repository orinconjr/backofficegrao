﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="teste.aspx.cs" Inherits="teste" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <style type="text/css">
			body {
				margin: 0;
				padding: 0;
			}

			@font-face {
			    font-family: 'robotobold';
			    src: url('fonts/roboto-bold-webfont.eot');
			    src: url('fonts/roboto-bold-webfont.eot?#iefix') format('embedded-opentype'),
			         url('fonts/roboto-bold-webfont.woff') format('woff'),
			         url('fonts/roboto-bold-webfont.ttf') format('truetype'),
			         url('fonts/roboto-bold-webfont.svg#robotobold') format('svg');
			    font-weight: normal;
			    font-style: normal;
			}

			@font-face {
			    font-family: 'robotoregular';
			    src: url('fonts/roboto-regular-webfont.eot');
			    src: url('fonts/roboto-regular-webfont.eot?#iefix') format('embedded-opentype'),
			         url('fonts/roboto-regular-webfont.woff') format('woff'),
			         url('fonts/roboto-regular-webfont.ttf') format('truetype'),
			         url('fonts/roboto-regular-webfont.svg#robotoregular') format('svg');
			    font-weight: normal;
			    font-style: normal;
			}

			@font-face {
			    font-family: 'museo300';
			    src: url('fonts/museo300-regular-webfont.woff2') format('woff2'),
			         url('fonts/museo300-regular-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}

			@font-face {
			    font-family: 'museo700';
			    src: url('fonts/museo700-regular-webfont.woff2') format('woff2'),
			         url('fonts/museo700-regular-webfont.woff') format('woff');
			    font-weight: normal;
			    font-style: normal;
			}	

			.boxInfo {
				left: 0;
				right: 0;
				bottom:0;
				float: left;
				width: 100%;
				height: 150px;
				position: absolute;
				background: #e9e9ec;
				box-sizing: border-box;
				padding: 45px 35px 30px 35px;
			}

			.postPadrao {
				float: left;
				width: 800px;
				height: 800px;
				position: relative;
			}

			p {
				margin: 0;
				padding: 0;
				float: left;
				line-height: 100%;
			}

			.exclusivo {
				color: #818181;
				margin-top: 10px;
				font-size: 33.86px;
				font-family: 'museo300';
			}

			.exclusivo span {
				color: #c9486c;
				font-size: 33.86px;
				font-family: 'museo700';
			}

			.preco {
				float: right;
				display: none;
				
				line-height: 100%;
				text-align: right;
			}

			.showBoleto .Avista {
				display: block;
			}

			.showParcelado .precoParcelado {
				display: block;
			}

			.showDePor .precoDePor {
				display: block;
			}

			.precoDePor span {
				float: right;
			}

			.precoDePor .moeda, .precoDePor .minText, .precoDePor .moedaTopo {
				float: none;
			}

			.preco .moeda {
				font-size: 14px;
				margin-right: 4px;
			}

			.preco .moedaTopo {
				font-size: 16px;
			    vertical-align: top;
			    padding-top: 6px;
			    display: inline-block;
			}

			.preco .minText {
				font-size: 16px;
			}

			.preco .mediumText {
				font-size: 20px;
			}

			.preco.Avista .padrao {
				font-size: 60px;
			}

			.textBoleto {
				font-size: 20px;
				margin-right: 30px;
			}

			.preco span {
				color: #81854b;
				line-height: 100%;
				font-size: 33.6px;
				font-family: 'robotobold';
			}

			.preco span.padrao {
				color: #c9486c;
				font-size: 40.16px;
				font-family: 'robotobold';
			}

			.preco span.padrao span {
				color: #c9486c;
			}

			.preco span.light {
				color: #c9486c;
				font-size: 25px;
				line-height: 50px;
				font-family: 'robotoregular';
			}

			.cutLine {
				text-decoration: line-through;
			}

			.porcentagem {
				right: 15px;
				width: 122px;
				height: 141px;
				bottom: 125px;
				color: #ffffff;
				padding-top: 25px;
				line-height: 100%;
				position: absolute;
				text-align: center;
				padding-right: 10px;
				box-sizing: border-box;
				background: url("imagens/bullet.png") top left no-repeat;
			}
			
			.porcentagem .porcento {
				top: 40px;
				right: 4px;
				font-size: 33px;
				position: absolute;
				font-family: 'robotoregular';
			}
			
			.porcentagem span {
				line-height: 70%;
				font-size: 70.89px;
				font-family: 'robotobold';
			}

			.porcentagem .lower {
				padding-top: 5px;
				font-size: 28.22px;
				padding-left: 12px;
				display: inline-block;
				font-family: 'robotoregular';
			}

			.logoGrao {
				top: 0;
				left: 0;
				padding: 0 10px;
				position: absolute;
				background: #EB476C;

				-webkit-border-bottom-right-radius: 10px;
				-moz-border-radius-bottomright: 10px;
				border-bottom-right-radius: 10px;
			}
		</style>

</head>
<body>
    <form id="form1" runat="server">

    </form>
</body>
</html>
