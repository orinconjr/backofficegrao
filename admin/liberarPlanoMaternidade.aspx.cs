﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class liberarPlanoMaternidade : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        var pedidosPlano = (from c in data.tbPedidos where c.tipoDePagamentoId == 11 && c.statusDoPedido == 2 select c);
        foreach (var pedido in pedidosPlano)
        {
            var ultimaParcela = (from c in data.tbPedidoPagamentos
                where c.pago == false && c.cancelado == false && c.pedidoId == pedido.pedidoId
                orderby c.dataVencimento descending
                select c).FirstOrDefault();
            if (ultimaParcela != null)
            {
                int prazoMaximo = 1;
                var itensPedido = (from c in data.tbItensPedidos where c.pedidoId == pedido.pedidoId select c);
                foreach (var itemPedido in itensPedido)
                {
                    if (itemPedido.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos > prazoMaximo)
                    {
                        prazoMaximo = (int) itemPedido.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos;
                    }
                }
                var itensPedidoCombo = (from c in data.tbItensPedidoCombos where c.tbItensPedido.pedidoId == pedido.pedidoId select c);
                foreach (var itemPedido in itensPedidoCombo)
                {
                    if (itemPedido.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos > prazoMaximo)
                    {
                        prazoMaximo = (int) itemPedido.tbProduto.tbProdutoFornecedor.fornecedorPrazoPedidos;
                    }
                }
                var prazoDias = ultimaParcela.dataVencimento.AddDays(prazoMaximo * (-1));
                var cancelarBool = DateTime.Now.AddDays(prazoMaximo) < ultimaParcela.dataVencimento ? true : false;
                var cancelar = DateTime.Now.AddDays(prazoMaximo) < ultimaParcela.dataVencimento ? "Sim" : "Não";
                Response.Write("Pedido: " + pedido.pedidoId + "<br>");
                Response.Write("Parcela: " + ultimaParcela.dataVencimento.ToShortDateString() + "<br>");
                Response.Write("Data Maxima Pedidos: " + prazoDias.ToShortDateString() + "<br>");
                Response.Write("Data Maxima Pedidos: " + prazoDias.ToShortDateString() + "<br>");
                Response.Write("Cancelar: " + cancelar + "<br><br>");

                if (cancelarBool)
                {
                    var reservas =
                        (from c in data.tbProdutoReservaEstoques where c.idPedido == pedido.pedidoId select c);
                    foreach (var reserva in reservas)
                    {
                        data.tbProdutoReservaEstoques.DeleteOnSubmit(reserva);
                    }
                    data.SubmitChanges();
                }
            }
        }
    }
}