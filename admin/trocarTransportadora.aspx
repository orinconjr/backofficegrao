﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="trocarTransportadora.aspx.cs" Inherits="trocarTransoportadora" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="admin/js/funcoes.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div>
                        Numero do Pedido:
                        <asp:TextBox ID="txtNumeroPedido" runat="server" MaxLength="10" onkeypress="return soNumeroDigito(event)"></asp:TextBox>
                        <asp:Button runat="server" ID="btnPesquisar" Text="Pesquisar" Height="22" OnClick="btnPesquisar_OnClick" />
                    </div>

                    <div style="padding-top: 8px;">
                        Entregar pela:
                <b>
                    <asp:Label ID="lblTransportadora" runat="server" Text=""></asp:Label></b>
                        Alterar para:
           <b>
               <asp:DropDownList ID="ddlAlterarTranportadora" runat="server"></asp:DropDownList></b>
                        <asp:Button ID="btnConfirmar" runat="server" Text="Confimar" Visible="False" OnClientClick="return confirm('Confirma alteração de transportadora?')" OnClick="btnConfirmar_OnClick" />
                    </div>

                    <br />
                    <div>
                        Nome para Entrega:
                <asp:Label ID="lblNome" runat="server" Text=""></asp:Label>
                    </div>
                    <div>
                        Rua:
                <asp:Label ID="lblRua" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Numero:
                <asp:Label ID="lblNumero" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Complemento:
                <asp:Label ID="lblComplemento" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Bairro:
                <asp:Label ID="lblBairro" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Cidade:
                <asp:Label ID="lblCidade" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Estado:
                <asp:Label ID="lblEstado" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Cep:
                <asp:Label ID="lblCep" runat="server" Text="Label"></asp:Label>
                    </div>
                    <div>
                        Referência para Entrega:
                <asp:Label ID="lblReferencia" runat="server" Text="Label"></asp:Label>

                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
