﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class alterarTransportadora : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Marca como paga a fatura ou desmarca paga
    /// </summary>
    /// <param name="idFatura">id da fatura no banco de dados</param>
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod]
    public static string pesquisarPedido(int idPedido)
    {

        string json = "";

        if (idPedido.ToString().Length > 6)
        {
            idPedido = rnFuncoes.retornaIdInterno(idPedido);
        }

        using (var data = new dbCommerceDataContext())
        {
            var pedido = (from p in data.tbPedidos
                          join pp in data.tbPedidoPacotes on p.pedidoId equals pp.idPedido
                          where p.pedidoId == idPedido
                          select new
                          {
                              p.endNomeDoDestinatario,
                              p.endRua,
                              p.endNumero,
                              p.endComplemento,
                              p.endBairro,
                              p.endCidade,
                              p.endEstado,
                              p.endCep,
                              p.endReferenciaParaEntrega,
                              pp.formaDeEnvio
                          });

            if (pedido.Any())
            {
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                json = serializer.Serialize(pedido);
            }
        }

        return json;
    }

    /// <summary>
    /// Realiza alteração de transportadora
    /// </summary>
    /// <param name="idPedido"></param>
    /// <param name="transportadoraSelecionada"></param>
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [WebMethod]
    public static string alteraTransportadora(int idPedido, string transportadoraSelecionada)
    {
        string resultado = "";

        try
        {
            if (idPedido.ToString().Length > 6)
            {
                idPedido = rnFuncoes.retornaIdInterno(idPedido);
            }

            using (var data = new dbCommerceDataContext())
            {
                var pedidoPacotes = (from pp in data.tbPedidoPacotes where pp.idPedido == idPedido select pp);
                var pedidoEnvios = (from pe in data.tbPedidoEnvios where pe.idPedido == idPedido select pe);

                foreach (var pedidoPacote in pedidoPacotes)
                {

                    if (transportadoraSelecionada == "tnt")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.codJadlog = "";
                        pedidoPacote.formaDeEnvio = "tnt";
                    }

                    if (transportadoraSelecionada == "jadlog")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.formaDeEnvio = "jadlog";
                    }

                    data.SubmitChanges();
                }

                foreach (var pedidoEnvio in pedidoEnvios)
                {
                    if (transportadoraSelecionada == "tnt")
                    {
                        pedidoEnvio.formaDeEnvio = "tnt";
                        pedidoEnvio.nfeObrigatoria = true;
                    }

                    if (transportadoraSelecionada == "jadlog")
                    {
                        pedidoEnvio.formaDeEnvio = "jadlog";
                    }

                    data.SubmitChanges();
                }
            }
            resultado = "OK";

            return resultado;

        }
        catch (Exception ex)
        {
            resultado = "ERRO";
            return resultado;
        }
    }

    protected void btnConfirmar_Click(object sender, EventArgs e)
    {
        try
        {
            int idPedido = Convert.ToInt32(txtNumeroPedido.Text);

            if (idPedido.ToString().Length > 6)
            {
                idPedido = rnFuncoes.retornaIdInterno(idPedido);
            }

            using (var data = new dbCommerceDataContext())
            {
                var pedidoPacotes = (from pp in data.tbPedidoPacotes where pp.idPedido == idPedido select pp);
                var pedidoEnvios = (from pe in data.tbPedidoEnvios where pe.idPedido == idPedido select pe);

                foreach (var pedidoPacote in pedidoPacotes)
                {
                    string teste = hfFormaDeEnvio.Value;

                    if (ddlAlterarTranportadora.SelectedValue == "tnt")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.codJadlog = "";
                        pedidoPacote.formaDeEnvio = "tnt";
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "jadlog")
                    {
                        pedidoPacote.rastreio = "";
                        pedidoPacote.formaDeEnvio = "jadlog";
                    }

                    data.SubmitChanges();
                }

                foreach (var pedidoEnvio in pedidoEnvios)
                {
                    if (ddlAlterarTranportadora.SelectedValue == "tnt")
                    {
                        pedidoEnvio.formaDeEnvio = "tnt";
                        pedidoEnvio.nfeObrigatoria = true;
                    }

                    if (ddlAlterarTranportadora.SelectedValue == "jadlog")
                    {
                        pedidoEnvio.formaDeEnvio = "jadlog";
                    }

                    data.SubmitChanges();
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('Alteração realizada com sucesso!');", true);

            txtNumeroPedido.Focus();
            btnConfirmar.Focus();
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterStartupScript(this, GetType(), Guid.NewGuid().ToString(), "alert('ERRO: " + ex.Message + "');", true);
        }


    }
}