﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="etiquetaromaneio.aspx.cs" Inherits="fornecedor_etiquetaromaneio" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
    body {
        height:297mm; 
        width:190mm; 
        margin-left:auto; 
        margin-right:auto;
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ListView runat="server" ID="lstEtiquetas" OnItemDataBound="lstEtiquetas_ItemDataBound">
                <ItemTemplate>
                    <div style="float: left; width: 48%; height: 69mm; padding-left: 0.9%; position: relative; border-right: 1px solid; border-bottom: 1px solid; <%# (Container.DisplayIndex + 1) % 8 == 0 ? "page-break-after: always;" : "" %> ">
                        <div style="position: absolute; top: 10px; left: 10px;">
                            <asp:Image runat="server" ID="imgCodigoDeBarras"/>
                        </div>
                        <div style="position: absolute; top: 15px; left: 165px; width: 210px; font-family: Arial; font-size: 12px">
                            <%# Eval("idPedidoFornecedor") %> - <%# Eval("fornecedorNome") %><br />
                            Et: <%# Eval("idPedidoFornecedorItem") %> - Prod: <%# Eval("numeroItem") %>/<%# Eval("totalItens") %><br />
                            <%# Eval("produtoId") %> - <%# Eval("produtoIdDaEmpresa") %><br />
                            <%# Eval("complementoIdDaEmpresa") %>
                        </div>
                        <div style="position: absolute; top: 92px; left: 10px;width: 335px;">
                            <div style="font-family: Arial; font-size: 14px">
                                <%# Eval("produtoNome") %>
                            </div>                        
                            <div style="font-family: 'Arial Black', Arial; font-weight: 900; font-size: 65px; margin-top: 10px; line-height: 65px; transform: scaleY(1.5);">
                                <%# Eval("codigoEtiqueta") %>
                            </div>
                       </div>
                        <div style="position: absolute; top: 199px; left: 10px;">
                            <asp:PlaceHolder ID="qrCode1" runat="server" />
                       </div>
                        <div style="position: absolute; top: 149px; left: 240px;">
                            <asp:PlaceHolder ID="qrCode2" runat="server" />
                       </div>
                       
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </form>
</body>
</html>
