<%@ Page Language="C#" Theme="Glass" EnableEventValidation="false" AutoEventWireup="true" CodeFile="fornecedor.aspx.cs" Inherits="fornecedor_fornecedor" %>

<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dxwgv" %>

<%@ Register Assembly="DevExpress.Web.ASPxEditors.v11.1" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxe" %>
<%@ Register Assembly="DevExpress.Web.ASPxGridView.v11.1" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dxwgv" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="../admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="../admin/js/funcoes.js"></script>
    <meta http-equiv="Content-Type" content="application/vnd.ms-excel;charset=iso-8859-1">
</head>
<body>
    <form id="formulario" runat="server">
        <script language="javascript" type="text/javascript">
            function OnDropDownDataPedido(s, dateFrom, dateTo) {
                var str = s.GetText();
                if (str == "") {
                    dateFrom.SetDate(new Date(1950, 0, 1));
                    dateTo.SetDate(new Date(1960, 11, 31));
                    return;
                }
                var d = str.split("|");
                dateFrom.SetText(d[0]);
                dateTo.SetText(d[1]);
            }

            function ApplyFilter(dde, dateFrom, dateTo, campo) {
                var colunaSplit = dde.name.split('_');
                var coluna = colunaSplit[colunaSplit.length - 1].replace("DXFREditorcol", "");
                var colunaFiltro = "";
                if (coluna == "1") colunaFiltro = "data";
                if (coluna == "2") colunaFiltro = "dataLimite";

                var d1 = dateFrom.GetText();
                var d2 = dateTo.GetText();
                if (d1 == "" || d2 == "") return;
                dde.SetText(d1 + "|" + d2);
                grd.AutoFilterByColumn(colunaFiltro, d1 + "|" + d2);
            }
            function OnDropDown(s, dateFrom, dateTo) {
                var str = s.GetText();
                if (str == "") {
                    dateFrom.SetDate(new Date(1950, 0, 1));
                    dateTo.SetDate(new Date(1960, 11, 31));
                    return;
                }
                var d = str.split("|");
                dateFrom.SetText(d[0]);
                dateTo.SetText(d[1]);
            }
            function grid_ContextMenu(s, e) {
                if (e.objectType == "header")
                    pmColumnMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
            }
        </script>
        <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal"
            style="width: 100%">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 210px; height: 97px"></td>
                                        <td style="width: 140px; height: 97px"></td>
                                        <td style="width: 189px; height: 97px"></td>
                                        <td style="width: 208px; height: 97px"></td>
                                        <td style="height: 97px"></td>
                                    </tr>
                                    <tr valign="bottom">
                                        <td style="width: 210px">&nbsp;</td>
                                        <td class="textoCabecalho" style="width: 140px"></td>
                                        <td class="textoCabecalho" style="width: 189px"></td>
                                        <td class="textoCabecalho" style="width: 208px"></td>
                                        <td></td>
                                        <td class="textoCabecalho">Seu IP:
                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123"
                                EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="12"></td>
                        </tr>

                        <tr>
                            <td height="30"></td>
                        </tr>
                        <tr>
                            <td bgcolor="White" style="height: 300px" valign="top">

                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td class="tituloPaginas" valign="top">
                                            <asp:Label ID="lblAcao" runat="server">Produtos em Fabrica��o</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
                                                <tr>
                                                    <td align="right">
                                                        <table cellpadding="0" cellspacing="0" style="width: 834px">
                                                            <tr>


                                                                <td>

                                                                    <asp:CheckBox ID="ckEntregue" runat="server" Checked="False" Text="Entregue" />
                                                                    <asp:CheckBox ID="ckPendente" runat="server" Checked="False" Text="Pendente" />
                                                                    <asp:CheckBox ID="CkParcial" runat="server" Checked="False" Text="Pendente"  Visible="False"/>

                                                                    &nbsp;<asp:Button runat="server" ID="btCarregaGrd" Text="Listar Dados" OnClick="btCarregaGrd_Click" Width="100px" />

                                                                </td>

                                                                <td height="38"
                                                                    style="background-image: url('../admin/images/bkExportar.jpg'); padding-bottom: 1px;"
                                                                    valign="bottom" width="231">
                                                                    <table cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td style="width: 97px">&nbsp;</td>
                                                                            <td style="width: 33px">
                                                                                <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg"
                                                                                    Style="margin-left: 5px" OnClick="btPdf_Click" />
                                                                            </td>
                                                                            <td style="width: 31px">
                                                                                <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg"
                                                                                    Style="margin-left: 5px" OnClick="btXsl_Click" />
                                                                            </td>
                                                                            <td style="width: 36px">
                                                                                <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg"
                                                                                    Style="margin-left: 5px" OnClick="btRtf_Click" />
                                                                            </td>
                                                                            <td valign="bottom">
                                                                                <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg"
                                                                                    Style="margin-left: 5px" OnClick="btCsv_Click" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" Width="834px" Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated"
                                                            KeyFieldName="idPedidoFornecedor" OnAutoFilterCellEditorCreate="grid_AutoFilterCellEditorCreate" OnAutoFilterCellEditorInitialize="grid_AutoFilterCellEditorInitialize"
                                                            OnProcessColumnAutoFilter="grid_ProcessColumnAutoFilter">
                                                            <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                                                            <Styles>
                                                                <Footer Font-Bold="True">
                                                                </Footer>
                                                            </Styles>
                                                            <TotalSummary>
                                                                <dxwgv:ASPxSummaryItem FieldName="qtdProdutos" ShowInColumn="qtdProdutos" ShowInGroupFooterColumn="qtdProdutos" SummaryType="Sum" DisplayFormat="{0}" />
                                                                <dxwgv:ASPxSummaryItem FieldName="faltaEntregar" ShowInColumn="faltaEntregar" ShowInGroupFooterColumn="faltaEntregar" SummaryType="Sum" DisplayFormat="{0}" />
                                                            </TotalSummary>
                                                            <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" EmptyDataRow="Nenhum registro encontrado." />
                                                            <SettingsPager Position="TopAndBottom" PageSize="50" ShowDisabledButtons="False" AlwaysShowPager="True">
                                                                <Summary AllPagesText="P�ginas: {0} - {1} ({2} items)" Text="P�gina {0} de {1} ({2} registros encontrados)" />
                                                            </SettingsPager>
                                                            <Settings ShowFilterRow="True" ShowGroupButtons="False" ShowHeaderFilterButton="True" ShowFooter="True" />
                                                            <SettingsEditing EditFormColumnCount="4" PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" PopupEditFormWidth="700px" />
                                                            <Columns>
                                                                <dxwgv:GridViewDataTextColumn Caption="ID" FieldName="idPedidoFornecedor" VisibleIndex="0" Width="50">
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataDateColumn Caption="Data do Pedido" FieldName="data" VisibleIndex="0">
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataDateColumn Caption="Data de Entrega" FieldName="dataLimite" VisibleIndex="0">
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataDateColumn Caption="Total de Itens" FieldName="qtdProdutos" VisibleIndex="0">
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataDateColumn Caption="Total Faltando" FieldName="faltaEntregar" VisibleIndex="0">
                                                                </dxwgv:GridViewDataDateColumn>
                                                                <dxwgv:GridViewDataTextColumn Caption="Etiquetas" Name="etiquetas" VisibleIndex="0" Width="80">
                                                                    <DataItemTemplate>
                                                                        <a target="_blank" href="etiquetaromaneio2.aspx?romaneio=<%# Eval("idPedidoFornecedor") %>">Etiquetas</a>
                                                                    </DataItemTemplate>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn Caption="Pendentes" Name="pendentes" VisibleIndex="0" Width="80">
                                                                    <DataItemTemplate>
                                                                        <asp:LinkButton runat="server" OnCommand="btnExportarPendentes_OnCommand" ID="btnExportarPendentes" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Imprimir Pendentes</asp:LinkButton>
                                                                    </DataItemTemplate>
                                                                </dxwgv:GridViewDataTextColumn>
                                                                <dxwgv:GridViewDataTextColumn Caption="Exportar" Name="exportar" VisibleIndex="0" Width="80">
                                                                    <DataItemTemplate>
                                                                        <asp:LinkButton runat="server" OnCommand="btnExportar_OnCommand" ID="btnExportar" CommandArgument='<%# Eval("idPedidoFornecedor") %>' EnableViewState="False">Imprimir</asp:LinkButton>
                                                                    </DataItemTemplate>
                                                                </dxwgv:GridViewDataTextColumn>
                                                            </Columns>
                                                            <StylesEditors>
                                                                <Label Font-Bold="True">
                                                                </Label>
                                                            </StylesEditors>
                                                        </dxwgv:ASPxGridView>
                                                        <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores"
                                                            GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                                                        </dxwgv:ASPxGridViewExporter>

                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td style="padding-top: 10px;">
                                                        <asp:Button runat="server" ID="btnExportarTodosPendentes" OnClick="btnExportarTodosPendentes_OnClick" Text="Exportar todos os produtos pendentes" />
                                                    </td>
                                                </tr>

                                            </table>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                        <tr>
                            <td style="height: 176px; background-image: url('../admin/images/rodape.jpg');">
                                <table cellpadding="0" cellspacing="0" style="width: 920px">
                                    <tr>
                                        <td style="width: 705px; height: 63px"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 705px">&nbsp;</td>
                                        <td>
                                            <asp:HyperLink ID="btEmail" runat="server"
                                                ImageUrl="../admin/images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
