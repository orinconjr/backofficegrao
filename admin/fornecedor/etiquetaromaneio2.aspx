﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="etiquetaromaneio2.aspx.cs" Inherits="fornecedor_etiquetaromaneio2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
    body {
        /*height:297mm; 
        width:190mm; 
        margin-left:auto; 
        margin-right:auto;
            */

        margin:0px;padding:0px;
    }
    .etiqueta
    {
        float: left; width: 48%; height: 69mm; padding-left: 0.9%; position: relative; border-right: 1px solid; border-bottom: 1px solid; 
    }
    .imgCodigoDeBarras{
        position: absolute; top: 10px; left: 10px;
    }
    .dadosPrincipais{
        position: absolute; top: 15px; left: 165px; width: 210px; font-family: Arial; font-size: 12px
    }
    .nomeCodEtiqueta{
        position: absolute; top: 92px; left: 10px;width: 335px;
    }
    .nomeCodEtiqueta .produtoNome
    {
        font-family: Arial; font-size: 14px
    }
    
    .nomeCodEtiqueta .codigoEtiqueta
    {
        font-family: 'Arial Black', Arial; font-weight: 900; font-size: 65px; margin-top: 10px; line-height: 65px; transform: scaleY(1.5);
    }
    .qrCode1
    {
        position: absolute; top: 199px; left: 10px;
    }
    .qrCode2
    {
        position: absolute; top: 149px; left: 240px;
    }
    .pagina
    {
        height:287mm;
        padding:5mm;
         width:190mm; 
        /* margin-top:50px;*/
    }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
          
            <asp:ListView runat="server" ID="lstEtiquetas" OnItemDataBound="lstEtiquetas_ItemDataBound">
                <ItemTemplate>

                      <asp:Literal ID="litAbreDiv"  runat="server"></asp:Literal>

                    <div class="etiqueta" >
                        <div class="imgCodigoDeBarras" >
                            <asp:Image runat="server" ID="imgCodigoDeBarras"/>
                        </div>
                        <div class="dadosPrincipais" >
                            <%# Eval("idPedidoFornecedor") %> - <%# Eval("fornecedorNome") %><br />
                            Et: <%# Eval("idPedidoFornecedorItem") %> - Prod: <%# Eval("numeroItem") %>/<%# Eval("totalItens") %><br />
                            <%# Eval("produtoId") %> - <%# Eval("produtoIdDaEmpresa") %><br />
                            <%# Eval("complementoIdDaEmpresa") %>
                        </div>
                        <div class="nomeCodEtiqueta" >
                            <div class="produtoNome" >
                                <%# Eval("produtoNome") %>
                            </div>                        
                            <div class="codigoEtiqueta" >
                                <%# Eval("codigoEtiqueta") %>
                            </div>
                       </div>
                        <div class="qrCode1" >
                            <asp:PlaceHolder ID="qrCode1" runat="server" />
                       </div>
                        <div class="qrCode2" >
                            <asp:PlaceHolder ID="qrCode2" runat="server" />
                       </div>
                       
                    </div>
                  <asp:Literal ID="litFechaDiv" runat="server"></asp:Literal>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </form>
</body>
</html>
