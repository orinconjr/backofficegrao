﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarlosAg.ExcelXmlWriter;
using DevExpress.Data.Filtering;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using SpreadsheetLight;

public partial class fornecedor_fornecedor : System.Web.UI.Page
{
    DateTemplate dateTemplate = null;

    public class item
    {
        public int idPedidoFornecedor { get; set; }
        public DateTime data { get; set; }
        public DateTime dataLimite { get; set; }
        public int? qtdProdutos { get; set; }
        public int? faltaEntregar { get; set; }

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ckPendente.Checked = true;


        }

        fillGrid(false);


    }


    private void fillGrid(bool rebind)
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        if (usuarioFornecedorLogadoId == null) Response.Redirect("default.aspx", true);
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario =
            (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c)
                .FirstOrDefault();
        if (usuario != null)
        {
            var pedidoDc = new dbCommerceDataContext();

            List<item> pedidosFornecedorParcial = (from c in pedidoDc.tbPedidoFornecedors
                                                   join d in pedidoDc.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into
                                                       itens
                                                   where c.idFornecedor == usuario.idFornecedor && itens.Count(x => x.entregue) < itens.Count() && c.pendente != true && c.idPedidoFornecedor != 278
                                                   orderby c.data descending
                                                   select new item
                                                   {
                                                       idPedidoFornecedor = c.idPedidoFornecedor,
                                                       data = c.data,
                                                       dataLimite = c.dataLimite,
                                                       qtdProdutos = (int?)itens.Sum(x => x.quantidade) ?? 0,
                                                       faltaEntregar = itens.Where(x => x.entregue == false).Sum(x => x.quantidade)//,
                                                   }).ToList<item>();

            List<item> pedidosFornecedorEntregue = (from c in pedidoDc.tbPedidoFornecedors
                                                    join d in pedidoDc.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into
                                                        itens
                                                    where c.idFornecedor == usuario.idFornecedor && itens.Count(x => x.entregue) == itens.Count() && c.pendente != true
                                                    orderby c.data descending
                                                    select new item
                                                    {
                                                        idPedidoFornecedor = c.idPedidoFornecedor,
                                                        data = c.data,
                                                        dataLimite = c.dataLimite,
                                                        qtdProdutos = (int?)itens.Count(x => x.entregue) ?? 0,
                                                        faltaEntregar = 0
                                                        ///status = "Entregue"
                                                    }).Take(30).ToList<item>();
            List<item> pedidosFornecedorPendente = (from c in pedidoDc.tbPedidoFornecedors
                                                    join d in pedidoDc.tbPedidoFornecedorItems on c.idPedidoFornecedor equals d.idPedidoFornecedor into
                                                        itens
                                                    where c.idFornecedor == usuario.idFornecedor && c.pendente == true && c.idPedidoFornecedor != 278
                                                    select new item
                                                    {
                                                        idPedidoFornecedor = c.idPedidoFornecedor,
                                                        data = c.data,
                                                        dataLimite = c.dataLimite,
                                                        qtdProdutos = (int?)itens.Sum(x => x.quantidade) ?? 0,
                                                        faltaEntregar = itens.Count(x => !x.entregue)  //itens.Sum(x => (x.quantidade  == null ? x.quantidade : 0))
                                                                                                       // status = "Pendente"
                                                    }).ToList<item>();
            List<item> ResultQuery = new List<item>();
            ResultQuery = pedidosFornecedorEntregue.Union(pedidosFornecedorParcial).OrderBy(z => z.idPedidoFornecedor).ToList<item>();

            List<item> grdDataSource = new List<item>();
            switch (ckEntregue.Checked)
            {
                case true:
                    // grd.DataSource = CkParcial.Checked ? (Request.QueryString["pendentes"] != null ? ResultQuery.Union(pedidosFornecedorPendente) : ResultQuery) : (Request.QueryString["pendentes"] != null ? pedidosFornecedorEntregue.Union(pedidosFornecedorPendente) : pedidosFornecedorEntregue);
                    //grd.DataSource = ckPendente.Checked 
                    //      ? 
                    //     (Request.QueryString["pendentes"] != null 
                    //        ? ResultQuery.Union(pedidosFornecedorPendente) 
                    //        : ResultQuery) 
                    //      : 
                    //      (Request.QueryString["pendentes"] != null 
                    //        ? pedidosFornecedorEntregue.Union(pedidosFornecedorPendente) 
                    //        : pedidosFornecedorEntregue);
                    grdDataSource = ckPendente.Checked
                    ? ResultQuery.Union(pedidosFornecedorPendente).ToList<item>()
                    : ResultQuery; // ResultQuery = pedidosFornecedorEntregue.Union(pedidosFornecedorParcial)
                    break;
                case false:
                    //grd.DataSource = ckPendente.Checked 
                    //      ? 
                    //      (Request.QueryString["pendentes"] != null 
                    //        ? pedidosFornecedorParcial.Union(pedidosFornecedorPendente) 
                    //        : pedidosFornecedorParcial) 
                    //       : null;
                    grdDataSource = ckPendente.Checked
                          ? pedidosFornecedorParcial.Union(pedidosFornecedorPendente).ToList<item>()
                          : pedidosFornecedorParcial;

                    break;
            }

            //}

            //grd.DataBind();


            if ((!Page.IsPostBack && !Page.IsCallback) | rebind)
            {
                try
                {
                    grd.DataSource = grdDataSource;
                    grd.DataBind();

                }
                catch (Exception ex)
                {
                    string erro = ex.Message;
                }
            }

        }
    }

    protected void gridClientes_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
    {
        if (e.RowType != GridViewRowType.Data)
        {
            return;
        }

        int idPedidoFornecedor = 0;
        int.TryParse(grd.GetRowValues(e.VisibleIndex, new string[] { "idPedidoFornecedor" }).ToString(), out idPedidoFornecedor);
        LinkButton btnExportar = (LinkButton)grd.FindRowCellTemplateControl(e.VisibleIndex, grd.Columns["exportar"] as GridViewDataColumn, "btnExportar");


        var pedidoDc = new dbCommerceDataContext();
        var pedido = (from c in pedidoDc.tbPedidoFornecedors where c.idPedidoFornecedor == idPedidoFornecedor select c).First();

        var itensDoPedido = (from c in pedidoDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == idPedidoFornecedor select c);
        int totalItens = itensDoPedido.Count();
        if (totalItens > itensDoPedido.Where(x => x.entregue == true).Count() && (pedido.dataLimite.Date <= DateTime.Now.Date))
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                i++;
            }
        }
        else if (totalItens == itensDoPedido.Where(x => x.entregue == false).Count())
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFFFBB");
                i++;
            }
            //litEntregue.Text = "Não";
        }
        else if (totalItens > itensDoPedido.Where(x => x.entregue == true).Count())
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#FFC6C7");
                i++;
            }
            //litEntregue.Text = "Parcialmente";
        }
        else
        {
            int i = 0;
            while ((i < e.Row.Cells.Count))
            {
                e.Row.Cells[i].Style.Add("background", "#D2E9FF");
                i++;
            }
            //litEntregue.Text = "Sim";
        }


    }
    protected void btPdf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WritePdfToResponse();
    }
    protected void btXsl_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteXlsToResponse();
    }
    protected void btRtf_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteRtfToResponse();
    }
    protected void btCsv_Click(object sender, ImageClickEventArgs e)
    {
        this.grdEx.WriteCsvToResponse();
    }
    private void exportaExcelPedidoFornecedor(int pedidoFornecedorId)
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario =
            (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c)
                .FirstOrDefault();
        if (usuario != null)
        {
            var exportacao = new tbFornecedorExportacao();
            exportacao.dataHora = DateTime.Now;
            exportacao.idFornecedorUsuario = usuario.idFornecedorUsuario;
            exportacao.idPedidoFornecedor = pedidoFornecedorId;
            fornecedorDc.tbFornecedorExportacaos.InsertOnSubmit(exportacao);
            fornecedorDc.SubmitChanges();
        }

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();
        pedido.confirmado = true;
        pedidosDc.SubmitChanges();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");



        WorksheetRow cabecalho = sheet.Table.Rows.Add();
        cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
        cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
        cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("Referencia", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");
        if (pedido.idFornecedor == 72)
        {
            titulos.Cells.Add("Peso Un", DataType.String, "normal");
            titulos.Cells.Add("Peso Tot", DataType.String, "normal");
        }

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                /*WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");*/

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtoId.produtoNome = produto.produtoNome;
                produtosIds.Add(produtoId);
            }
        }

        int totalQuantidades = 0;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            totalQuantidades += quantidade;
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            WorksheetRow linha = sheet.Table.Rows.Add();

            linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.produtoId.ToString(), DataType.String, "normal");
            linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
            linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
            linha.Cells.Add(produtosId.custo.ToString("C"), DataType.String, "normal");
            linha.Cells.Add((produtosId.custo * quantidade).ToString("C"), DataType.String, "normal");
            if (pedido.idFornecedor == 72)
            {
                linha.Cells.Add((produto.produtoPeso).ToString(), DataType.String, "normal");
                linha.Cells.Add((produto.produtoPeso * quantidade).ToString(), DataType.String, "normal");
            }
        }
        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total de Produtos: " + totalQuantidades, DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");

        using (MemoryStream memoryStream = new MemoryStream())
        {
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();
            string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";


            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    private void exportaExcelPedidoFornecedor_v2(int pedidoFornecedorId)
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario =
            (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c)
                .FirstOrDefault();
        if (usuario != null)
        {
            var exportacao = new tbFornecedorExportacao
            {
                dataHora = DateTime.Now,
                idFornecedorUsuario = usuario.idFornecedorUsuario,
                idPedidoFornecedor = pedidoFornecedorId
            };
            fornecedorDc.tbFornecedorExportacaos.InsertOnSubmit(exportacao);
            fornecedorDc.SubmitChanges();
        }

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();
        pedido.confirmado = true;
        pedidosDc.SubmitChanges();

        SLDocument sl = new SLDocument();
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);
        sl.SetRowStyle(2, style1);

        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo + ".xlsx");

        sl.SetCellValue("A1", "Pedido: " + pedidoFornecedorId);
        sl.SetCellValue("B1", "Data de Solicitação: " + pedido.data.ToShortDateString());
        sl.SetCellValue("C1", "Data limite de entrega: " + pedido.dataLimite.ToShortDateString());


        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c);
        var produtosIds = new List<fornecedorCusto>();

        sl.SetCellValue(2, 1, "ID do Produto");
        sl.SetCellValue(2, 2, "Referencia");
        sl.SetCellValue(2, 3, "Complemento");
        sl.SetCellValue(2, 4, "Nome");
        sl.SetCellValue(2, 5, "Quantidade");
        sl.SetCellValue(2, 6, "Valor");
        sl.SetCellValue(2, 7, "Total");

        if (pedido.idFornecedor == 72)
        {
            sl.SetCellValue(2, 8, "Peso Un");
            sl.SetCellValue(2, 9, "Peso Tot");
        }

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Any(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo);
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto
                {
                    produtoId = fornecedorItem.idProduto,
                    custo = fornecedorItem.custo,
                    produtoNome = produto.produtoNome
                };
                produtosIds.Add(produtoId);
            }
        }

        int totalQuantidades = 0;
        int linha = 3;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            totalQuantidades += quantidade;
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            sl.SetCellValue(linha, 1, produto.produtoIdDaEmpresa);
            sl.SetCellValue(linha, 2, produto.produtoId);
            sl.SetCellValue(linha, 3, produto.complementoIdDaEmpresa);
            sl.SetCellValue(linha, 4, produto.produtoNome);
            sl.SetCellValue(linha, 5, quantidade.ToString());
            sl.SetCellValue(linha, 6, produtosId.custo.ToString("C"));
            sl.SetCellValue(linha, 7, (produtosId.custo * quantidade).ToString("C"));

            if (pedido.idFornecedor == 72)
            {
                sl.SetCellValue(linha, 8, (produto.produtoPeso));
                sl.SetCellValue(linha, 9, (produto.produtoPeso * quantidade));
            }
            linha++;
        }

        linha++;

        sl.SetCellValue(linha, 1, "");
        sl.SetCellValue(linha, 2, "");
        sl.SetCellValue(linha, 3, "");
        sl.SetCellValue(linha, 4, "");
        sl.SetCellValue(linha, 5, "Total de Produtos: " + totalQuantidades);
        sl.SetCellValue(linha, 6, "");
        sl.SetCellValue(linha, 7, "Total do Pedido: " + totalDoPedido.ToString("C"));
        sl.SetCellStyle(linha, 7, style1);

        //Exporta 
        sl.SaveAs(Response.OutputStream);
        Response.End();
    }

    private void exportaExcelPedidoFornecedorPendentes(int pedidoFornecedorId)
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario = (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c).FirstOrDefault();
        if (usuario != null)
        {
            var exportacao = new tbFornecedorExportacao();
            exportacao.dataHora = DateTime.Now;
            exportacao.idFornecedorUsuario = usuario.idFornecedorUsuario;
            exportacao.idPedidoFornecedor = pedidoFornecedorId;
            fornecedorDc.tbFornecedorExportacaos.InsertOnSubmit(exportacao);
            fornecedorDc.SubmitChanges();
        }

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();
        pedido.confirmado = true;
        pedidosDc.SubmitChanges();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");



        WorksheetRow cabecalho = sheet.Table.Rows.Add();
        cabecalho.Cells.Add("Pedido: " + pedidoFornecedorId, DataType.String, "negrito");
        cabecalho.Cells.Add("Data de Solicitação: " + pedido.data.ToShortDateString(), DataType.String, "negrito");
        cabecalho.Cells.Add("Data limite de entrega: " + pedido.dataLimite.ToShortDateString(), DataType.String, "negrito");

        decimal totalDoPedido = 0;
        var pedidosFornecedor = ckEntregue.Checked && ckPendente.Checked ? (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c) :
            ckEntregue.Checked ? (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId && c.entregue == true select c) :
            (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId && c.entregue == false select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");
        if (pedido.idFornecedor == 72)
        {
            titulos.Cells.Add("Peso Un", DataType.String, "normal");
            titulos.Cells.Add("Peso Tot", DataType.String, "normal");
        }

        int totalProdutos = 0;

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                /*WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");*/

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtoId.produtoNome = produto.produtoNome;
                produtosIds.Add(produtoId);
            }
        }

        int totalQuantidades = 0;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            totalQuantidades += quantidade;
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            WorksheetRow linha = sheet.Table.Rows.Add();

            linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
            linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
            linha.Cells.Add(produtosId.custo.ToString("C"), DataType.String, "normal");
            linha.Cells.Add((produtosId.custo * quantidade).ToString("C"), DataType.String, "normal");
            if (pedido.idFornecedor == 72)
            {
                linha.Cells.Add((produto.produtoPeso).ToString(), DataType.String, "normal");
                linha.Cells.Add((produto.produtoPeso * quantidade).ToString(), DataType.String, "normal");
            }
        }
        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total de Produtos: " + totalQuantidades, DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");

        using (MemoryStream memoryStream = new MemoryStream())
        {
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == pedido.idFornecedor select c).FirstOrDefault();
            string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";


            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    private void exportaExcelPedidoFornecedorPendentes_v2(int pedidoFornecedorId)
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario = (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c).FirstOrDefault();
        if (usuario != null)
        {
            var exportacao = new tbFornecedorExportacao
            {
                dataHora = DateTime.Now,
                idFornecedorUsuario = usuario.idFornecedorUsuario,
                idPedidoFornecedor = pedidoFornecedorId
            };
            fornecedorDc.tbFornecedorExportacaos.InsertOnSubmit(exportacao);
            fornecedorDc.SubmitChanges();
        }

        var pedidosDc = new dbCommerceDataContext();
        var pedido = (from c in pedidosDc.tbPedidoFornecedors where c.idPedidoFornecedor == pedidoFornecedorId select c).First();
        pedido.confirmado = true;
        pedidosDc.SubmitChanges();

        SLDocument sl = new SLDocument();
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);
        sl.AddWorksheet("Pedido");

        string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + "_Pedido_" + pedido.idPedidoFornecedor + "_entrega_ate_" + pedido.dataLimite.ToShortDateString().Replace("/", "-") + ".xls";

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo + ".xlsx");

        sl.SetCellValue("A1", "Pedido: " + pedidoFornecedorId);
        sl.SetCellValue("B1", "Data de Solicitação: " + pedido.data.ToShortDateString());
        sl.SetCellValue("C1", "Data limite de entrega: " + pedido.dataLimite.ToShortDateString());

        decimal totalDoPedido = 0;
        var pedidosFornecedor = ckEntregue.Checked && ckPendente.Checked ? (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId select c) :
            ckEntregue.Checked ? (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId && c.entregue == true select c) :
            (from c in pedidosDc.tbPedidoFornecedorItems where c.idPedidoFornecedor == pedidoFornecedorId && c.entregue == false select c);
        var produtosIds = new List<fornecedorCusto>();

        sl.SetCellValue(2, 1, "ID do Produto");
        sl.SetCellValue(2, 2, "Referencia");
        sl.SetCellValue(2, 3, "Complemento");
        sl.SetCellValue(2, 4, "Nome");
        sl.SetCellValue(2, 5, "Quantidade");
        sl.SetCellValue(2, 6, "Valor");
        sl.SetCellValue(2, 7, "Total");

        if (pedido.idFornecedor == 72)
        {
            sl.SetCellValue(2, 8, "Peso Un");
            sl.SetCellValue(2, 9, "Peso Tot");
        }

        int totalProdutos = 0;

        foreach (var fornecedorItem in pedidosFornecedor)
        {
            bool jaAdicionado = produtosIds.Any(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo);
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto
                {
                    produtoId = fornecedorItem.idProduto,
                    custo = fornecedorItem.custo,
                    produtoNome = produto.produtoNome
                };
                produtosIds.Add(produtoId);
            }
        }

        int totalQuantidades = 0;
        int linha = 3;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            totalQuantidades += quantidade;
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            sl.SetCellValue(linha, 1, produto.produtoIdDaEmpresa);
            sl.SetCellValue(linha, 2, produto.produtoId);
            sl.SetCellValue(linha, 3, produto.complementoIdDaEmpresa);
            sl.SetCellValue(linha, 4, produto.produtoNome);
            sl.SetCellValue(linha, 5, quantidade.ToString());
            sl.SetCellValue(linha, 6, produtosId.custo.ToString("C"));
            sl.SetCellValue(linha, 7, (produtosId.custo * quantidade).ToString("C"));

            if (pedido.idFornecedor == 72)
            {
                sl.SetCellValue(linha, 8, (produto.produtoPeso));
                sl.SetCellValue(linha, 9, (produto.produtoPeso * quantidade));
            }
            linha++;
        }

        linha++;

        sl.SetCellValue(linha, 1, "");
        sl.SetCellValue(linha, 2, "");
        sl.SetCellValue(linha, 3, "");
        sl.SetCellValue(linha, 4, "");
        sl.SetCellValue(linha, 5, "Total de Produtos: " + totalQuantidades);
        sl.SetCellValue(linha, 6, "");
        sl.SetCellValue(linha, 7, "Total do Pedido: " + totalDoPedido.ToString("C"));
        sl.SetCellStyle(linha, 7, style1);

        //Exporta 
        sl.SaveAs(Response.OutputStream);
        Response.End();
    }

    private void exportaExcelPedidoFornecedorPendentesTodos()
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario = (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c).FirstOrDefault();

        var pedidosDc = new dbCommerceDataContext();

        Workbook book = new Workbook();
        WorksheetStyle negrito = book.Styles.Add("negrito");
        WorksheetStyle normal = book.Styles.Add("normal");
        negrito.Font.Bold = true;
        normal.Font.Bold = false;
        Worksheet sheet = book.Worksheets.Add("Pedido");

        WorksheetRow cabecalho = sheet.Table.Rows.Add();

        decimal totalDoPedido = 0;
        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.tbPedidoFornecedor.idFornecedor == usuario.idFornecedor && c.entregue == false select c);
        var produtosIds = new List<fornecedorCusto>();

        WorksheetRow separacao = sheet.Table.Rows.Add();
        WorksheetRow titulos = sheet.Table.Rows.Add();

        titulos.Cells.Add("Data Limite", DataType.String, "normal");
        titulos.Cells.Add("Romaneio", DataType.String, "normal");
        titulos.Cells.Add("Etiqueta", DataType.String, "normal");
        titulos.Cells.Add("ID do Produto", DataType.String, "normal");
        titulos.Cells.Add("ID da Empresa", DataType.String, "normal");
        titulos.Cells.Add("Complemento", DataType.String, "normal");
        titulos.Cells.Add("Nome", DataType.String, "normal");
        titulos.Cells.Add("Quantidade", DataType.String, "normal");
        titulos.Cells.Add("Valor", DataType.String, "normal");
        titulos.Cells.Add("Total", DataType.String, "normal");
        if (usuario.idFornecedor == 72)
        {
            titulos.Cells.Add("Peso Un", DataType.String, "normal");
            titulos.Cells.Add("Peso Tot", DataType.String, "normal");
        }


        foreach (var fornecedorItem in pedidosFornecedor)
        {

            WorksheetRow linha = sheet.Table.Rows.Add();

            linha.Cells.Add(fornecedorItem.tbPedidoFornecedor.dataLimite.ToShortDateString(), DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.tbPedidoFornecedor.idPedidoFornecedor.ToString(), DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.idPedidoFornecedorItem.ToString(), DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.idProduto.ToString(), DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.tbProduto.produtoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.tbProduto.complementoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.tbProduto.produtoNome, DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.quantidade.ToString(), DataType.String, "normal");
            linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
            linha.Cells.Add((fornecedorItem.custo * fornecedorItem.quantidade).ToString("C"), DataType.String, "normal");

            if (fornecedorItem.tbPedidoFornecedor.idFornecedor == 72)
            {
                linha.Cells.Add((fornecedorItem.tbProduto.produtoPeso).ToString(), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.tbProduto.produtoPeso * fornecedorItem.quantidade).ToString(), DataType.String, "normal");
            }
            /*bool jaAdicionado = produtosIds.Where(x => x.produtoId == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Any();
            if (!jaAdicionado)
            {
                int quantidade = pedidosFornecedor.Where(x => x.idProduto == fornecedorItem.idProduto && x.custo == fornecedorItem.custo).Sum(x => x.quantidade);
                var produtoDc = new dbCommerceDataContext();
                var produto = (from c in produtoDc.tbProdutos where c.produtoId == fornecedorItem.idProduto select c).First();

                /*WorksheetRow linha = sheet.Table.Rows.Add();

                linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
                linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
                linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
                linha.Cells.Add(fornecedorItem.custo.ToString("C"), DataType.String, "normal");
                linha.Cells.Add((fornecedorItem.custo * quantidade).ToString("C"), DataType.String, "normal");

                totalDoPedido += (fornecedorItem.custo * quantidade);
                var produtoId = new fornecedorCusto();
                produtoId.produtoId = fornecedorItem.idProduto;
                produtoId.custo = fornecedorItem.custo;
                produtoId.produtoNome = produto.produtoNome;
                produtosIds.Add(produtoId);
            }*/
        }

        /*int totalQuantidades = 0;
        foreach (var produtosId in produtosIds.OrderBy(x => x.produtoNome))
        {
            int quantidade = pedidosFornecedor.Where(x => x.idProduto == produtosId.produtoId && x.custo == produtosId.custo).Sum(x => x.quantidade);
            totalQuantidades += quantidade;
            var produtoDc = new dbCommerceDataContext();
            var produto = (from c in produtoDc.tbProdutos where c.produtoId == produtosId.produtoId select c).First();

            WorksheetRow linha = sheet.Table.Rows.Add();

            linha.Cells.Add(produto.produtoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.complementoIdDaEmpresa, DataType.String, "normal");
            linha.Cells.Add(produto.produtoNome, DataType.String, "normal");
            linha.Cells.Add(quantidade.ToString(), DataType.String, "normal");
            linha.Cells.Add(produtosId.custo.ToString("C"), DataType.String, "normal");
            linha.Cells.Add((produtosId.custo * quantidade).ToString("C"), DataType.String, "normal");
        }*/
        WorksheetRow total = sheet.Table.Rows.Add();
        WorksheetRow titulos2 = sheet.Table.Rows.Add();

        /*titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total de Produtos: " + totalQuantidades, DataType.String, "normal");
        titulos2.Cells.Add("", DataType.String, "normal");
        titulos2.Cells.Add("Total do Pedido: " + totalDoPedido.ToString("C"), DataType.String, "negrito");*/

        using (MemoryStream memoryStream = new MemoryStream())
        {
            var fornecedor = (from c in fornecedorDc.tbProdutoFornecedors where c.fornecedorId == usuario.idFornecedor select c).FirstOrDefault();
            string nomeArquivo = DateTime.Now.ToShortDateString().Replace("/", "-") + ".xls";


            book.Save(memoryStream);
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nomeArquivo);
            Response.BinaryWrite(memoryStream.ToArray());
            Response.Flush();
            Response.Close();
            Response.End();
        }
    }

    private void exportaExcelPedidoFornecedorPendentesTodos_v2()
    {
        HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
        usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
        int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
        var fornecedorDc = new dbCommerceDataContext();
        var usuario = (from c in fornecedorDc.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c).FirstOrDefault();

        var pedidosDc = new dbCommerceDataContext();

        SLDocument sl = new SLDocument();
        SLStyle style1 = sl.CreateStyle();
        style1.Font.Bold = true;
        sl.SetRowStyle(1, style1);
        sl.AddWorksheet("Pedido");

        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("Content-Disposition", "attachment; filename=todosProdutosPendentes.xlsx");

        var pedidosFornecedor = (from c in pedidosDc.tbPedidoFornecedorItems where c.tbPedidoFornecedor.idFornecedor == usuario.idFornecedor && c.entregue == false select c);

        sl.SetCellValue(2, 1, "Data Limite");
        sl.SetCellValue(2, 2, "Romaneio");
        sl.SetCellValue(2, 3, "Etiqueta");
        sl.SetCellValue(2, 4, "ID do Produto");
        sl.SetCellValue(2, 5, "ID da Empresa");
        sl.SetCellValue(2, 6, "Complemento");
        sl.SetCellValue(2, 7, "Nome");
        sl.SetCellValue(2, 8, "Quantidade");
        sl.SetCellValue(2, 9, "Valor");
        sl.SetCellValue(2, 10, "Total");

        if (usuario.idFornecedor == 72)
        {
            sl.SetCellValue(2, 11, "Peso Un");
            sl.SetCellValue(2, 12, "Peso Tot");
        }

        int linha = 3;
        foreach (var fornecedorItem in pedidosFornecedor)
        {

            sl.SetCellValue(linha, 1, fornecedorItem.tbPedidoFornecedor.dataLimite.ToShortDateString());
            sl.SetCellValue(linha, 2, fornecedorItem.tbPedidoFornecedor.idPedidoFornecedor.ToString());
            sl.SetCellValue(linha, 3, fornecedorItem.idPedidoFornecedorItem.ToString());
            sl.SetCellValue(linha, 4, fornecedorItem.idProduto.ToString());
            sl.SetCellValue(linha, 5, fornecedorItem.tbProduto.produtoIdDaEmpresa);
            sl.SetCellValue(linha, 6, fornecedorItem.tbProduto.complementoIdDaEmpresa);
            sl.SetCellValue(linha, 7, fornecedorItem.tbProduto.produtoNome);
            sl.SetCellValue(linha, 8, fornecedorItem.quantidade.ToString());
            sl.SetCellValue(linha, 9, fornecedorItem.custo.ToString("C"));
            sl.SetCellValue(linha, 10, (fornecedorItem.custo * fornecedorItem.quantidade).ToString("C"));

            if (fornecedorItem.tbPedidoFornecedor.idFornecedor == 72)
            {
                sl.SetCellValue(linha, 11, (fornecedorItem.tbProduto.produtoPeso).ToString());
                sl.SetCellValue(linha, 12, (fornecedorItem.tbProduto.produtoPeso * fornecedorItem.quantidade).ToString());
            }
            linha++;

        }

        //Exporta 
        sl.SaveAs(Response.OutputStream);
        Response.End();
    }

    private class fornecedorCusto
    {
        public decimal custo { get; set; }
        public int produtoId { get; set; }
        public string produtoNome { get; set; }
    }

    protected void imbInsert_OnClick(object sender, ImageClickEventArgs e)
    {
        fillGrid(true);
    }

    protected void btnExportar_OnCommand(object sender, CommandEventArgs e)
    {
        //exportaExcelPedidoFornecedor(Convert.ToInt32(e.CommandArgument));

        exportaExcelPedidoFornecedor_v2(Convert.ToInt32(e.CommandArgument));
    }

    protected void btnExportarPendentes_OnCommand(object sender, CommandEventArgs e)
    {
        //exportaExcelPedidoFornecedorPendentes(Convert.ToInt32(e.CommandArgument));

        exportaExcelPedidoFornecedorPendentes_v2(Convert.ToInt32(e.CommandArgument));
    }

    protected void btnExportarTodosPendentes_OnClick(object sender, EventArgs e)
    {
        //exportaExcelPedidoFornecedorPendentesTodos();

        exportaExcelPedidoFornecedorPendentesTodos_v2();
    }


    protected void grid_AutoFilterCellEditorCreate(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewEditorCreateEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
        if (e.Column.FieldName == "dataLimite")
        {
            DropDownEditProperties dde = new DropDownEditProperties();
            dde.EnableClientSideAPI = true;
            dateTemplate = new DateTemplate();
            dde.DropDownWindowTemplate = dateTemplate;
            e.EditorProperties = dde;
        }
    }
    protected void grid_AutoFilterCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        ASPxGridView grid = sender as ASPxGridView;
        if (e.Column.FieldName == "data")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'data'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
        if (e.Column.FieldName == "dataLimite")
        {
            ASPxDropDownEdit dde = e.Editor as ASPxDropDownEdit;
            dde.ClientSideEvents.CloseUp = String.Format("function (s, e) {{ ApplyFilter(s, {0}, {1}, 'dataLimite'); }}", dateTemplate.cIdFrom, dateTemplate.cIdTo);
            dde.ReadOnly = true;
        }
    }

    protected void grid_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Value == "|") return;
        if (e.Column.FieldName == "data")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["data"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("data") >= dateFrom) &
                             (new OperandProperty("data") <= dateTo);
            }
            else
            {
                if (Session["data"] != null)
                    e.Value = Session["data"].ToString();
            }
        }
        if (e.Column.FieldName == "dataLimite")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                Session["dataLimite"] = e.Value;
                String[] dates = e.Value.Split('|');
                DateTime dateFrom = Convert.ToDateTime(dates[0]),
                    dateTo = Convert.ToDateTime(dates[1]);
                e.Criteria = (new OperandProperty("dataLimite") >= dateFrom) &
                             (new OperandProperty("dataLimite") <= dateTo);
            }
            else
            {
                if (Session["dataLimite"] != null)
                    e.Value = Session["dataLimite"].ToString();
            }
        }
    }

    protected void btCarregaGrd_Click(object sender, EventArgs e)
    {
        fillGrid(true);
    }
}