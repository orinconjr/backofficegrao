﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="fornecedor_Default" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link rel="stylesheet" type="text/css" href="../admin/estilos/estilos.css">
</head>
<body>
    <form id="form1" runat="server">
    
    <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td height="1000" 
                style="background-image: url('../admin/images/bkLogin.jpg'); background-repeat: no-repeat; background-position: top" 
                valign="top">
                <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
                    <tr>
                        <td style="padding-top: 200px; padding-left: 294px">
                            <table cellpadding="0" cellspacing="0" style="width: 184px">
                                <tr>
                                    <td>
                                        <b class="textoCabecalho">Login:</b><br />
                                        <asp:TextBox ID="txtNome" runat="server" Width="184px" CssClass="campos"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span class="textoCabecalho" style="font-weight: bold">Senha:</span><br />
                                        <asp:TextBox ID="txtSenha" runat="server" TextMode="Password" Width="184px" 
                                            CssClass="campos"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" style="padding-top: 5px">
                                        <asp:ImageButton ID="imbEntrar" runat="server" 
                                            ImageUrl="~/admin/images/btEntrarLogin.jpg" onclick="imbEntrar_Click" />
    
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>

