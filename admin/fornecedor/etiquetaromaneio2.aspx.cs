﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class fornecedor_etiquetaromaneio2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["romaneio"] != null)
        {
            int romaneio = Convert.ToInt32(Request.QueryString["romaneio"]);
            HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
            usuarioFornecedorLogadoId = Request.Cookies["usuarioFornecedorLogadoId"];
            int idUsuarioFornecedor = Convert.ToInt32(usuarioFornecedorLogadoId.Value);
            var data = new dbCommerceDataContext();
            var usuario = (from c in data.tbFornecedorUsuarios where c.idFornecedorUsuario == idUsuarioFornecedor select c).FirstOrDefault();
            var detalheromaneio = (from c in data.tbPedidoFornecedors where c.idFornecedor == usuario.idFornecedor && c.idPedidoFornecedor == romaneio select c).FirstOrDefault();
            if (detalheromaneio != null)
            {
                var pedidoFornecedorWs = new PedidosFornecedorWs();
                var etiquetas = pedidoFornecedorWs.ListaDetalhesItemRomaneio(romaneio, 0, 0, "glmp152029", true);
                if (Request.QueryString["etiqueta"] != null)
                {
                    etiquetas = etiquetas.Where(x => x.idPedidoFornecedorItem == Convert.ToInt32(Request.QueryString["etiqueta"])).ToList();
                }
                lstEtiquetas.DataSource = etiquetas;
                lstEtiquetas.DataBind();
            }
        }
    }

    protected void lstEtiquetas_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        int pos = e.Item.DataItemIndex;
        //if (pos==0 || (pos + 1) % 8 == 0)
        if (pos == 0)
        {
            Literal litAbreDiv = (Literal)e.Item.FindControl("litAbreDiv");
            litAbreDiv.Text = "<div class='pagina'>";
        }
        PedidosFornecedorWs.DetalhesItemRomaneioWs dataItem = (PedidosFornecedorWs.DetalhesItemRomaneioWs)e.Item.DataItem;
        Image imgCodigoDeBarras = (Image)e.Item.FindControl("imgCodigoDeBarras");
        imgCodigoDeBarras.ImageUrl = string.Format("../BarCode.ashx?code={0}&ShowText={1}", dataItem.idPedidoFornecedorItem, 0);
        PlaceHolder qrCode1 = (PlaceHolder)e.Item.FindControl("qrCode1");
        PlaceHolder qrCode2 = (PlaceHolder)e.Item.FindControl("qrCode2");

        QRCodeGenerator qrGenerator = new QRCodeGenerator();
        var qrCodeData = qrGenerator.CreateQrCode(dataItem.idPedidoFornecedorItem.ToString(), QRCodeGenerator.ECCLevel.Q);
        QRCode qrCode = new QRCode(qrCodeData);
        System.Web.UI.WebControls.Image imgBarCode = new System.Web.UI.WebControls.Image();
        imgBarCode.Height = 60;
        imgBarCode.Width = 60;

        System.Web.UI.WebControls.Image imgBarCode2 = new System.Web.UI.WebControls.Image();
        imgBarCode2.Height = 110;
        imgBarCode2.Width = 110;



        using (System.Drawing.Bitmap bitMap = qrCode.GetGraphic(20))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] byteImage = ms.ToArray();
                imgBarCode.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
            }
            qrCode1.Controls.Add(imgBarCode);
        }

        using (System.Drawing.Bitmap bitMap = qrCode.GetGraphic(20))
        {
            using (MemoryStream ms = new MemoryStream())
            {
                bitMap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                byte[] byteImage = ms.ToArray();
                imgBarCode2.ImageUrl = "data:image/png;base64," + Convert.ToBase64String(byteImage);
            }
            qrCode2.Controls.Add(imgBarCode2);
        }

        if ((pos + 1) % 8 == 0)
        {
            Literal litFechaDiv = (Literal)e.Item.FindControl("litFechaDiv");
            litFechaDiv.Text = "</div><div class='pagina'><!-- Fecha Div -->";
        }
    }
}