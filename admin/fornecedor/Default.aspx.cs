﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using rakutenPedidos;

public partial class fornecedor_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void imbEntrar_Click(object sender, ImageClickEventArgs e)
    {
        var fornecedorDc = new dbCommerceDataContext();
        var usuarioFornecedor = (from c in fornecedorDc.tbFornecedorUsuarios where c.nome == txtNome.Text && c.senha == txtSenha.Text select c).FirstOrDefault();
        if (usuarioFornecedor != null)
        {
            HttpCookie usuarioFornecedorLogadoId = new HttpCookie("usuarioFornecedorLogadoId");
            usuarioFornecedorLogadoId.Value = usuarioFornecedor.idFornecedorUsuario.ToString();
            usuarioFornecedorLogadoId.Expires = DateTime.Now.AddHours(10);
            Response.Cookies.Add(usuarioFornecedorLogadoId);

            var acesso = new tbFornecedorUsuarioAcesso();
            acesso.idFornecedorUsuario = usuarioFornecedor.idFornecedorUsuario;
            acesso.idFornecedor = usuarioFornecedor.idFornecedor;
            acesso.dataHora = DateTime.Now;
            acesso.ip = Request.ServerVariables["REMOTE_HOST"];
            fornecedorDc.tbFornecedorUsuarioAcessos.InsertOnSubmit(acesso);
            fornecedorDc.SubmitChanges();

            Response.Write("<script>window.location=('fornecedor.aspx');</script>");
        }
        else
        {
            Response.Write("<script>alert('Dados inválidos, Tente novamente.');</script>");
        }
    }
}