﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;
using DevExpress.Web.Internal.InternalCheckBox;
public partial class usrComprasOrdemCondicoes : System.Web.UI.UserControl
{

    public const string cstNenhuma = "Nenhuma selecionada";

    public int IdComprasOrdem
    {
        get { return Convert.ToInt32(Request.QueryString["id"] == null ? int.Parse(hdfIdOrdem.Value) : Convert.ToInt32(Request.QueryString["id"])); }
        set { hdfIdOrdem.Value = value.ToString(); }
    }

    protected void Page_Load(object sender, EventArgs e) { }

    public void FillCondicoes(bool hideCond = false, bool showOnlySelCond = false)
    {

        var data = new dbCommerceDataContext();

        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == IdComprasOrdem select c).FirstOrDefault();

        var condicoes = (from c in data.tbComprasFornecedorCondicaoPagamentos
                         where c.idComprasFornecedor == ordem.idComprasFornecedor
                         select new
                         {
                             c.tbComprasCondicaoPagamento.nome,
                             c.acrescimo,
                             c.desconto,
                             c.idComprasFornecedorCondicaoPagamento,
                             c.tbComprasCondicaoPagamento.idComprasCondicaoPagamento,
                             c.tbComprasCondicaoPagamento.ignorarMesAtual
                         }).ToList();

        if (!hideCond)
        {
            ddlCondPag.DataSource = condicoes;
            ddlCondPag.DataBind();
            ddlCondPag.Items.Add(cstNenhuma);

            if (ordem.idComprasCondicaoPagamento == null) { ddlCondPag.Text = cstNenhuma; } else { ddlCondPag.SelectedIndex = 0; }

        }

        else { pnCondSel.Visible = false; }

        if (showOnlySelCond)
        {

            condicoes = condicoes.Where(x => x.idComprasCondicaoPagamento == ordem.idComprasCondicaoPagamento).ToList();

            if (condicoes.Count == 0)
            {
                pnShowSelCond.Visible = true;
                pnCondicoes.Visible = false;
            }

        }
        else { if (condicoes.Count == 0) { pnCondicoes.Visible = false; return; } }

        lstCondicoesPagamento.DataSource = condicoes;
        lstCondicoesPagamento.DataBind();

    }
    protected void lstCondicoesPagamento_OnItemDataBound(object sender, ListViewItemEventArgs e)
    {

        var hdfIdCondicao = (HiddenField)e.Item.FindControl("hdfIdCondicao");
        var litPrazos = (Literal)e.Item.FindControl("litPrazos");
        var litValorParcela = (Literal)e.Item.FindControl("litValorParcela");
        //var hdIgnorarMes = (HiddenField) e.Item.FindControl("hdIgnorarMes");

        int idCondicao = Convert.ToInt32(hdfIdCondicao.Value);

        var data = new dbCommerceDataContext();

        List<string> prazos = new List<string>();

        // Obtém a condição de pagamento do fornecedor (tbComprasFornecedorCondicaoPagamento)
        var condicao = (from c in data.tbComprasFornecedorCondicaoPagamentos
                        where c.idComprasFornecedorCondicaoPagamento == idCondicao
                        select c).First();

        // Obtém a lista com os dias dos prazos para as condições de pagamento do fornecedor (tbComprasCondicaoPagamentoDias)
        var condicaoPrazos = (from c in data.tbComprasCondicaoPagamentoDias
                              where c.idComprasCondicaoPagamento == condicao.tbComprasCondicaoPagamento.idComprasCondicaoPagamento
                              select c).ToList();

        // Obtém a linha da ordem atual ordem (tbComprasOrdem)
        var ordem = (from c in data.tbComprasOrdems
                     where c.idComprasOrdem == IdComprasOrdem
                     select c).First();


        if (condicaoPrazos.Count > 0)
        {

            var dataInicioPrazo = ordem.dataEnvioFinanceiro ?? DateTime.Now;

            if (condicao.tbComprasCondicaoPagamento.inicioPrazo == 2)
            {
                dataInicioPrazo = ordem.dataEntrega ?? DateTime.Now.AddDays(rnFuncoes.retornaPrazoDiasUteis(condicao.tbComprasFornecedor.prazo, 0));
            }

            if (condicao.tbComprasCondicaoPagamento.tipoPagamento == 1)
            {

                foreach (var condicaoPrazo in condicaoPrazos) { prazos.Add(dataInicioPrazo.AddDays(condicaoPrazo.dia).ToShortDateString()); }

            }

            else
            {

                var proximoFechamento = condicaoPrazos.OrderBy(x => x.dia).FirstOrDefault(x => x.dia > DateTime.Now.AddDays(1).Day);

                if ((condicao.tbComprasCondicaoPagamento.ignorarMesAtual ?? false) == true) proximoFechamento = condicaoPrazos.OrderBy(x => x.dia).Skip(1).FirstOrDefault(x => x.dia > DateTime.Now.AddDays(1).Day);

                if (proximoFechamento != null)
                {
                    var dia = new DateTime(dataInicioPrazo.Year, dataInicioPrazo.Month, proximoFechamento.dia);
                    prazos.Add(dia.ToShortDateString());
                }

                else
                {

                    var proximoFechamentoMes = condicaoPrazos.OrderBy(x => x.dia).FirstOrDefault();

                    int ultimoDiaMes = DateTime.DaysInMonth(dataInicioPrazo.Year, dataInicioPrazo.AddMonths(1).Month);
                    int diaProxFechamento = proximoFechamentoMes.dia;
                    if (diaProxFechamento > ultimoDiaMes)
                        diaProxFechamento = ultimoDiaMes;

                    int anoProxFechamento = dataInicioPrazo.Year;
                    if (dataInicioPrazo.AddMonths(1).Year > anoProxFechamento)
                        anoProxFechamento = dataInicioPrazo.AddMonths(1).Year;

                    // var dia = new DateTime(dataInicioPrazo.Year, dataInicioPrazo.AddMonths(1).Month, diaProxFechamento);
                    var dia = new DateTime(anoProxFechamento, dataInicioPrazo.AddMonths(1).Month, diaProxFechamento);
                    prazos.Add(dia.ToShortDateString());

                }

            }

            litPrazos.Text = String.Join(", ", prazos);
            var produtosPendentes = (from c in data.tbComprasOrdemProdutos
                                     where c.idComprasOrdem == IdComprasOrdem
                                     select new
                                     {
                                         c.idComprasOrdemProduto,
                                         c.idComprasSolicitacaoProduto,
                                         c.idComprasProduto,
                                         c.tbComprasProduto.produto,
                                         c.status,
                                         c.quantidade,
                                         c.preco,
                                         c.precoRecebido,
                                         c.quantidadeRecebida,
                                         c.comentario,
                                         unidade = (from d in data.tbComprasProdutoFornecedors
                                                    where
                                                    d.idComprasFornecedor == c.tbComprasOrdem.idComprasFornecedor &&
                                                    d.idComprasProduto == c.idComprasProduto
                                                    select d).First().tbComprasUnidadesDeMedida.unidadeDeMedida
                                     });
            var total = produtosPendentes.Any()
                ? produtosPendentes.Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade))
                : 0;

            var reprovado = produtosPendentes.Any(x => x.status == 2)
                ? produtosPendentes.Where(x => x.status == 2).Sum(x => (x.precoRecebido ?? x.preco) * (x.quantidadeRecebida ?? x.quantidade))
                : 0;

            decimal totalParcela = total - reprovado;
            if (condicao.acrescimo > 0)
            {
                totalParcela = totalParcela + ((totalParcela / 100) * condicao.acrescimo);
            }
            if (condicao.desconto > 0)
            {
                totalParcela = totalParcela - ((totalParcela / 100) * condicao.desconto);
            }
            if (condicao.tbComprasCondicaoPagamento.tipoPagamento == 1)
                totalParcela = totalParcela / condicaoPrazos.Count;
            litValorParcela.Text = totalParcela.ToString("C");
        }
    }

    protected void btnGravarCondicao_OnClick(object sender, EventArgs e)
    {

        var data = new dbCommerceDataContext();
        var ordem = (from c in data.tbComprasOrdems where c.idComprasOrdem == IdComprasOrdem select c).First();

        if (ddlCondPag.SelectedItem != null)
        {

            if (ddlCondPag.Text != cstNenhuma) { ordem.idComprasCondicaoPagamento = Convert.ToInt32(ddlCondPag.Value); } else { ordem.idComprasCondicaoPagamento = null; }
            data.SubmitChanges();
        }
        else
        {
            ordem.idComprasCondicaoPagamento = null;
            data.SubmitChanges();
        }

        Response.Redirect(Request.RawUrl);

    }

}