﻿<%@ Page Language="C#" Theme="Glass" AutoEventWireup="true" CodeFile="fornecedor.aspx.cs" Inherits="fornecedor" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1.Export, Version=11.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxGridView.Export" tagprefix="dxwgv" %>

<%@ Register assembly="DevExpress.Web.ASPxEditors.v11.1" namespace="DevExpress.Web.ASPxEditors" tagprefix="dxe" %>
<%@ Register assembly="DevExpress.Web.ASPxGridView.v11.1" namespace="DevExpress.Web.ASPxGridView" tagprefix="dxwgv" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>BarkCommerce - Sistema Administrativo</title>
    <link href="admin/estilos/estilos.css" rel="stylesheet" type="text/css" />
    <script language="JavaScript" type="text/javascript" src="admin/js/funcoes.js"></script>
</head>
<body>
    <form id="formulario" runat="server">
    <table align="center" cellpadding="0" cellspacing="0" class="tablePrincipal" 
        style="width: 100%">
        <tr>
            <td>
    <table cellpadding="0" cellspacing="0" style="width: 920px" align="center">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 920px">
                    <tr>
                        <td style="width: 210px; height: 97px">
                        </td>
                        <td style="width: 140px; height: 97px">
                        </td>
                        <td style="width: 189px; height: 97px">
                        </td>
                        <td style="width: 208px; height: 97px">
                        </td>
                        <td style="height: 97px">
                        </td>
                    </tr>
                    <tr valign="bottom">
                        <td style="width: 210px">
                            &nbsp;</td>
                        <td class="textoCabecalho" style="width: 140px">
                        </td>
                        <td class="textoCabecalho" style="width: 189px">
                            
                        </td>
                        <td class="textoCabecalho" style="width: 208px">
                                        </td>
                                        <td>
                                        </td>
                                        <td class="textoCabecalho">Seu IP:
                                            <asp:Label ID="lblIp" runat="server" Font-Bold="True" Text="200.258.110.123" 
                                EnableTheming="False"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
            </td>
        </tr>
        <tr>
            <td height="12"></td>
        </tr>
        
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td bgcolor="White" style="height: 300px" valign="top">
                
                <table cellpadding="0" cellspacing="0" style="width: 920px">
    <tr>
        <td class="tituloPaginas" valign="top">
            <asp:Label ID="lblAcao" runat="server">Produtos em Fabricação</asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            
    <table align="center" cellpadding="0" cellspacing="0" style="width: 834px">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" style="width: 834px">
                    <tr>
                        <td align="right">
                            &nbsp;</td>
                        <td height="38" 
                            style="background-image: url('images/bkExportar.jpg'); padding-bottom: 1px;" 
                            valign="bottom" width="231">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width: 97px">
                                        &nbsp;</td>
                                    <td style="width: 33px">
                                        <asp:ImageButton ID="btPdf" runat="server" ImageUrl="~/admin/images/btPdf.jpg" 
                                            style="margin-left: 5px" OnClick="btPdf_Click" />
                                    </td>
                                    <td style="width: 31px">
                                        <asp:ImageButton ID="btXsl" runat="server" ImageUrl="~/admin/images/btXls.jpg" 
                                            style="margin-left: 5px" OnClick="btXsl_Click" />
                                    </td>
                                    <td style="width: 36px">
                                        <asp:ImageButton ID="btRtf" runat="server" ImageUrl="~/admin/images/btRtf.jpg" 
                                            style="margin-left: 5px" OnClick="btRtf_Click" />
                                    </td>
                                    <td valign="bottom">
                                        <asp:ImageButton ID="btCsv" runat="server" ImageUrl="~/admin/images/btCsv.jpg" 
                                            style="margin-left: 5px" OnClick="btCsv_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <dxwgv:ASPxGridView ID="grd" runat="server" AutoGenerateColumns="False" 
                    KeyFieldName="itemPedidoId" Width="834px" 
                    Cursor="auto" OnHtmlRowCreated="gridClientes_HtmlRowCreated" >
                    <SettingsBehavior AutoFilterRowInputDelay="3200" ConfirmDelete="True" />
                    <Styles>
                        <Footer Font-Bold="True">
                        </Footer>
                    </Styles>
                    <SettingsText ConfirmDelete="Tem certeza que deseja excluir?" 
                        emptydatarow="Nenhum registro encontrado." />
                    <SettingsPager Position="TopAndBottom" PageSize="5000" 
                        ShowDisabledButtons="False" AlwaysShowPager="True">
                        <Summary AllPagesText="Páginas: {0} - {1} ({2} items)" 
                            Text="Página {0} de {1} ({2} registros encontrados)" />
                    </SettingsPager>
                    <settings showfilterrow="True" 
                        ShowHeaderFilterButton="True" ShowFooter="True" ShowGroupedColumns="True" ShowGroupPanel="True" />
                    <TotalSummary>
                        <dxwgv:ASPxSummaryItem DisplayFormat="c" FieldName="valorCobrado" 
                            ShowInColumn="Valor" ShowInGroupFooterColumn="Valor" SummaryType="Sum" />
                    </TotalSummary>
                    <SettingsEditing EditFormColumnCount="4" 
                        PopupEditFormHorizontalAlign="WindowCenter" PopupEditFormHorizontalOffset="-1" 
                        PopupEditFormModal="True" PopupEditFormVerticalAlign="WindowCenter" 
                        PopupEditFormWidth="700px" />
                    <Columns>
                        <dxwgv:GridViewDataTextColumn Caption="Id do Pedido" FieldName="pedidoId" VisibleIndex="0" Visible="False">
                            <EditFormSettings Visible="False" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataTextColumn Caption="Produto" FieldName="produtoNome" 
                            VisibleIndex="1" Settings-AutoFilterCondition="Contains">
                            <PropertiesTextEdit>
                                
<ValidationSettings>
                                    
<RequiredField ErrorText="Preencha o nome." IsRequired="True" />
                                
</ValidationSettings>
                            
</PropertiesTextEdit>
                            <Settings AutoFilterCondition="Contains" />
                            <EditFormSettings CaptionLocation="Top" />
                        </dxwgv:GridViewDataTextColumn>
                        <dxwgv:GridViewDataDateColumn Caption="Prazo" FieldName="prazoDeFabricacao" 
                            UnboundType="DateTime" VisibleIndex="3" >
                            <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy" EditFormat="Date">
                                
<CalendarProperties ClearButtonText="Apagar" TodayButtonText="Hoje">
                                </CalendarProperties>
                            
</PropertiesDateEdit>
                            <Settings GroupInterval="Date" />
                        </dxwgv:GridViewDataDateColumn>
                    </Columns>
                    <StylesEditors>
                        <Label Font-Bold="True">
                        </Label>
                    </StylesEditors>
                </dxwgv:ASPxGridView>
              <dxwgv:ASPxGridViewExporter ID="grdEx" runat="server" FileName="fornecedores" 
                    GridViewID="grd" Landscape="True" PreserveGroupRowStates="True">
                </dxwgv:ASPxGridViewExporter>

                <asp:ObjectDataSource ID="sqlPedidos" runat="server" SelectMethod="pedidoSelecionaProdutosFabricacaoAdmin" 
                    TypeName="rnPedidos">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlSituacao" runat="server" SelectMethod="situacaoSeleciona" 
                    TypeName="rnSituacao">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlPagamentos" runat="server" SelectMethod="condicaoDePagamentoSeleciona" 
                    TypeName="rnCondicoesDePagamento">
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="sqlTipoDeEntrega" runat="server" SelectMethod="tipoDeEntregaSeleciona" 
                    TypeName="rnTipoDeEntrega">
                </asp:ObjectDataSource>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
        </td>
    </tr>
</table>

            </td>
        </tr>
        <tr>
            <td style="height: 176px; background-image: url('images/rodape.jpg');">
                <table cellpadding="0" cellspacing="0" style="width: 920px">
                    <tr>
                        <td style="width: 705px; height: 63px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 705px">
                            &nbsp;</td>
                        <td>
                                            <asp:HyperLink ID="btEmail" runat="server" 
                                ImageUrl="images/email.gif" NavigateUrl="mailto:suporte@bark.com.br"></asp:HyperLink>
                                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
