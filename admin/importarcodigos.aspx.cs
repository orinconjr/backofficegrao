﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class importarcodigos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        string arquivoRetorno = "";
        if (fluArquivo.HasFile)
        {
            using (Stream fileStream = fluArquivo.PostedFile.InputStream)
            using (StreamReader sr = new StreamReader(fileStream))
            {
                string conteudo = null;
                while ((conteudo = sr.ReadLine()) != null)
                {
                    string codigo = conteudo.Split(';')[8].Replace("\"", "");
                    string pedidocompleto = conteudo.Split(';')[10].Replace("\"", "");
                    if (!string.IsNullOrEmpty(pedidocompleto))
                    {
                        string pedido = pedidocompleto.Substring(0, pedidocompleto.Length - 2);
                        int volume = Convert.ToInt32(pedidocompleto.Substring((pedidocompleto.Length - 2), 1));

                        var pedidoInterno = rnFuncoes.retornaIdInterno(Convert.ToInt32(pedido));
                        var data = new dbCommerceDataContext();
                        var pacote = (from c in data.tbPedidoPacotes
                            where c.idPedido == pedidoInterno && c.numeroVolume == volume && c.formaDeEnvio == "pac"
                            select c).FirstOrDefault();
                        if (pacote != null)
                        {
                            if (string.IsNullOrEmpty(pacote.rastreio))
                            {
                                pacote.rastreio = codigo;
                                data.SubmitChanges();

                                var pacotesGeral =
                                    (from c in data.tbPedidoPacotes
                                        where c.idPedido == pedidoInterno && pacote.rastreio == ""
                                        select c).Any();
                                if (!pacotesGeral)
                                {
                                    rnEmails.enviaCodigoDoTrakingCorreios("", pedidoInterno.ToString(), "");
                                }
                            }
                        }
                        else
                        {
                            Response.Write(pedidocompleto + ";" + codigo + "<br>");
                        }
                    }
                }
            }
        }
    }
}