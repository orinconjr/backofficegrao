﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class calculoJadlog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCalcular_OnClick(object sender, EventArgs e)
    {
        long cep = Convert.ToInt64(txtCep.Text.Replace("-", ""));
        int peso = Convert.ToInt32(txtPeso.Text.Replace("-", ""));
        decimal pesoEmKg = Convert.ToDecimal(peso) / 1000;

        decimal valorExpressa = 0;
        decimal valorPackage = 0;
        decimal valorRodo = 0;
        decimal valorCom = 0;


        var serviceJad = new serviceJadFrete.ValorFreteBeanService();
        var retornoExpressa = serviceJad.valorar(0, "L2F0M0E1", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
        var retornoPackage = serviceJad.valorar(3, "L2F0M0E1", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
        var retornoRodo = serviceJad.valorar(4, "L2F0M0E1", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
        var retornCom = serviceJad.valorar(9, "L2F0M0E1", "N", "100,00", "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "10924051000163");
        //var retornoServiceJad = serviceJad.valorar(modalidadeJad, "M2c0F1m4", "N", valorNota, "0,00", "14910000", cep.ToString().Replace("-", "").Replace(" ", ""), pesoEmKg.ToString("0.00"), "N", "D", "20907518000110");
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoExpressa);
        XmlNodeList parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
        foreach (XmlNode childrenNode in parentNode)
        {
            string retorno = childrenNode.ChildNodes[1].InnerText;
            valorExpressa = Convert.ToDecimal(retorno);
        }
        
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoPackage);
        parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
        foreach (XmlNode childrenNode in parentNode)
        {
            string retorno = childrenNode.ChildNodes[1].InnerText;
            valorPackage = Convert.ToDecimal(retorno);
        }
        
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornoRodo);
        parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
        foreach (XmlNode childrenNode in parentNode)
        {
            string retorno = childrenNode.ChildNodes[1].InnerText;
            valorRodo = Convert.ToDecimal(retorno);
        }
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(retornCom);
        parentNode = xmlDoc.GetElementsByTagName("Jadlog_Valor_Frete");
        foreach (XmlNode childrenNode in parentNode)
        {
            string retorno = childrenNode.ChildNodes[1].InnerText;
            valorCom = Convert.ToDecimal(retorno);
        }

        txtValores.Text = "Expressa: " + valorExpressa.ToString("C") + "<br>";
        txtValores.Text += "Package: " + valorPackage.ToString("C") + "<br>";
        txtValores.Text += "Rodo: " + valorRodo.ToString("C") + "<br>";
        txtValores.Text += "COM: " + valorCom.ToString("C") + "<br>";

    }
}