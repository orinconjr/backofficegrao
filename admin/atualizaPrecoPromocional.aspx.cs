﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class atualizaPrecoPromocional : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        var data = new dbCommerceDataContext();
        //var produtosCategoria = (from c in data.tbJuncaoProdutoCategorias where c.categoriaId == 638 select c.produtoId).Distinct().ToList();
        //foreach (var produtoCategoria in produtosCategoria)
        //{
        //    var produto = (from c in data.tbProdutos where c.produtoId == produtoCategoria select c).First();
        //    //decimal precoPromocional = produto.produtoPreco - ((produto.produtoPreco/100)*5);
        //    decimal precoPromocional = 0;
        //    produto.produtoPrecoPromocional = precoPromocional;
        //    data.SubmitChanges();
        //    rnBuscaCloudSearch.AtualizarProduto(produto.produtoId);
        //}
        //var produtosCategoria = new List<int>() { 22608, 22639, 22649, 22650, 22661, 22663, 22666, 22669, 22372, 18626, 18750, 18737, 22397, 18854, 18815, 22470, 22481, 22482, 22487, 22498, 22578, 22579, 22580, 29369, 29331, 25696, 25823, 25963, 28308, 28436, 28482, 29576, 23206, 23232, 23258, 23895, 24119, 24096, 24198, 24212, 24301, 24347, 24398, 24430, 24450, 24452, 24474, 24595, 24604, 24637, 24559, 24646, 24485, 24497, 24529, 22099, 22103, 22115, 22127, 22135, 22137, 22141, 22153, 22163, 22167, 22171, 22179, 22183, 22191, 22199, 22227, 22231, 22254 };
        //produtosCategoria.Add(25730);
        //produtosCategoria.Add(25731);
        //produtosCategoria.Add(25850);
        //produtosCategoria.Add(25851);
        //produtosCategoria.Add(25876);
        //produtosCategoria.Add(25877);
        //produtosCategoria.Add(25941);
        //produtosCategoria.Add(25942);
        //produtosCategoria.Add(25995);
        //produtosCategoria.Add(25996);
        //produtosCategoria.Add(26176);
        //produtosCategoria.Add(26177);


        /*
        var produtos = (from c in data.tbProdutos
                        join d in data.tbPrecoAntigos on c.produtoId equals d.produtoId into precos
                        where c.produtoAtivo.ToLower() == "true" && precos.Count() == 0
                        select c).ToList();
        foreach (var produto in produtos)
        {
            var produtoCheck = (from c in data.tbPrecoAntigos where c.produtoId == produto.produtoId select c).Any();
            if (!produtoCheck)
            {
                decimal precoPromocionalAtual = produto.produtoPrecoPromocional ?? 0;
                bool gerarPreco = false;
                decimal precoComDescontoBlack = produto.produtoPreco - ((produto.produtoPreco / 100) * 40);
                if (precoPromocionalAtual == 0) gerarPreco = true;
                else
                {
                    decimal descontoAtualAVista = precoPromocionalAtual - ((precoPromocionalAtual / 100) * 15);
                    if (precoComDescontoBlack < descontoAtualAVista) gerarPreco = true;
                }

                if (gerarPreco)
                {
                    Response.Write(produto.produtoId + "<br>");
                    decimal precoParaPromocao = (precoComDescontoBlack * 100) / 85;
                    var precoAntigo = new tbPrecoAntigo();
                    precoAntigo.produtoId = produto.produtoId;
                    precoAntigo.preco = produto.produtoPreco;
                    precoAntigo.precoPromocional = produto.produtoPrecoPromocional ?? 0;
                    data.tbPrecoAntigos.InsertOnSubmit(precoAntigo);
                    data.SubmitChanges();
                    produto.produtoPrecoPromocional = precoParaPromocao;
                    data.SubmitChanges();
                    rnProdutos.AtualizaValorParcelamento(produto.produtoId, (produto.produtoPrecoPromocional ?? 0), produto.produtoPreco);
                    rnBuscaCloudSearch.AtualizarProduto(produto.produtoId);
                }
            }
        }*/

        var produtosDesconto = (from c in data.tbPrecoAntigos select c).ToList();
        foreach (var precoAntigo in produtosDesconto)
        {
            var produto = (from c in data.tbProdutos where c.produtoId == precoAntigo.produtoId select c).FirstOrDefault();
            if (produto != null)
            {
                produto.produtoPreco = precoAntigo.preco;
                produto.produtoPrecoPromocional = precoAntigo.precoPromocional;
                data.SubmitChanges();
                data.tbPrecoAntigos.DeleteOnSubmit(precoAntigo);
                data.SubmitChanges();
                rnProdutos.AtualizaValorParcelamento(produto.produtoId, (produto.produtoPrecoPromocional ?? 0),
                    produto.produtoPreco);
                //rnBuscaCloudSearch.AtualizarProduto(produto.produtoId);
            }
        }
    }
}