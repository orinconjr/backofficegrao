﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Threading;
using System.Globalization;
using Amazon.S3;
using Amazon.S3.Model;
using System.Drawing.Imaging;
using System.Threading;
using System.Threading.Tasks;
using System.Transactions;

namespace atualizarFotos_Kraken
{
    public partial class Form1 : Form
    {
        private BackgroundWorker bwChecarQueue = new BackgroundWorker();
        //string caminhoFisico = "D:\\renato\\graodegente\\site\\";// teste
        string caminhoFisico = "C:\\inetpub\\wwwroot\\";// produção

        int qtdFotosCompactadas = 0;
        int qtdFotosCompactadas125 = 0;
        int qtdFotosCompactadasBanner = 0;
        string discoAzure = "DefaultEndpointsProtocol=https;AccountName=graodegentebr;AccountKey=3A1EAoKuO9fqEt9WeHWJIvG6xTlfuD7hjK951tz0mft6dakEydYWl2c1bPbKCpiAz7DVJmXqPjabIOBb2tyXXQ==";
        int faltaCompactar = 0;
        int faltaCompactar125 = 0;
        int faltaCompactarBanner = 0;

        public Form1()
        {
            InitializeComponent();
        }

        #region checarqueue
        private void timerQueue_Tick(object sender, EventArgs e)
        {
            timerQueue.Enabled = false;
            bwChecarQueue.WorkerSupportsCancellation = true;
            bwChecarQueue.DoWork += new DoWorkEventHandler(bwChecarQueue_DoWork);
            bwChecarQueue.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwChecarQueue_RunWorkerCompleted);
            try
            {
                //label5.Invoke((MethodInvoker)delegate
                //{
                //    label5.Text = "Tisparo do timer ultima checagem " + DateTime.Now.TimeOfDay;
                //});

                if (bwChecarQueue.IsBusy == false)
                {
                    label1.Text = "Consultando Queue";
                    bwChecarQueue.RunWorkerAsync();
                }
                else
                {
                    label1.Text = "Service em andamento aguarde...";
                }
            }
            catch (Exception ex)
            {
                label5.Invoke((MethodInvoker)delegate
                {
                    label5.Text = "erro: " + ex.Message;
                });
            }

            label5.Invoke((MethodInvoker)delegate
            {
                label5.Text = "Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
            });

            var data = new dbSiteDataContext();
            var queues360 = (from c in data.tbQueues where c.tipoQueue == 47 && c.andamento == false && c.concluido == false select c).ToList();
            foreach(var queue in queues360)
            {
                #region atualizaQueue
                queue.andamento = true;
                queue.dataInicio = DateTime.Now;
                data.SubmitChanges();
                #endregion

                System.Diagnostics.ProcessStartInfo start = new System.Diagnostics.ProcessStartInfo();
                start.FileName = @"C:\Users\Administrator\AppData\Local\Programs\Python\Python36-32\python";
                start.Arguments = string.Format("{0}", "c:\\generate360\\generate360.py -n \"C:\\Program Files\\Hugin\\binz\nona.exe\" " + queue.idRelacionado.ToString());
                start.UseShellExecute = false;
                start.RedirectStandardOutput = true;
                
                using (System.Diagnostics.Process process = System.Diagnostics.Process.Start(start))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        rnEmails.EnviaEmail("", "andre@graodegente.com.br", "", "", "", result, "Python");
                    }
                    process.WaitForExit();    // Wait up to five minutes.
                }

                #region finalizaQueue
                queue.concluido = true;
                queue.dataConclusao = DateTime.Now;
                data.SubmitChanges();
                #endregion
            }
        }


        private void bwChecarQueue_DoWork(object sender, DoWorkEventArgs e)
        {

            var data = new dbSiteDataContext();
            var agora = DateTime.Now.AddSeconds(-1);
            var periodo = new DateTime(2016, 5, 29);
            //var queues = (from c in data.tbQueues
            //              where c.andamento == false && c.concluido == false && c.agendamento <= agora && c.tipoQueue == 26
            //              select new
            //              {
            //                  c.tipoQueue,
            //                  c.idQueue
            //              }).ToList();
            var queues = new List<tbQueue>();
            using (new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
            {
                IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
            }))
            {
                using (var dataTrans = new dbSiteDataContext())
                {
                    queues = (from c in dataTrans.tbQueues
                              where c.andamento == false && c.concluido == false && c.tipoQueue == 26
                              select c).Take(50).ToList();

                }
            }

            label6.Invoke((MethodInvoker)delegate
                    {
                        label6.Text = "Em fila " + queues.Count();
                    });

            bool hasItens = false;
            int acompanhamento = 0;
            foreach (var queue in queues)
            {
                hasItens = true;

                if (queue.tipoQueue == 26) //Compactar imagem no Kraken
                {
                    ProcessarQueueNovo(queue.idQueue);
                    /*if (bwCompactarFotoKraken.IsBusy != true)
                    {
                        bwCompactarFotoKraken.RunWorkerAsync(queue.idQueue);
                    }*/
                }
            }

            if (!queues.Any())
            {
                //label1.Text = "Aguardando fotos";
            }

            if (hasItens)
            {
                timerQueue.Interval = 1000;
            }
            else
            {
                //timerQueue.Interval = 10000;
            }

        }


        private void ProcessarQueueNovo(long idQueue)
        {
            #region atualizaQueue
            var data = new dbSiteDataContext();
            var queue = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SubmitChanges();
            #endregion atualizaQueue

            try
            {

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "Compactando imagem produto: " + queue.idRelacionado;
                });

                int produtoFotoId = Convert.ToInt32(queue.idRelacionado);
                if (queue.mensagem == "1")
                {
                    GerarFotoNome(produtoFotoId, true);
                }
                else
                {
                    GerarFotoNome(produtoFotoId);
                }

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "ProdutoId: " + queue.idRelacionado + " compactada";
                });
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha finalização bwCompactarFotoKraken_DoWork - Kraken");
            }

            #region finalizaQueue
            var queueFinalizar = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            data.SubmitChanges();
            #endregion
        }

        private void GerarFotoNome(int produtoId)
        {
            GerarFotoNome(produtoId, false);
        }
        private void GerarFotoNome(int produtoId, bool refazer)
        {
            var data = new dbSiteDataContext();
            var fotos = (from c in data.tbProdutoFotos where c.produtoId == produtoId orderby (c.ordenacao == null ? 999 : c.ordenacao) select c).ToList();
            var produto = (from c in data.tbProdutos where c.produtoId == produtoId select c).First();
            string nomeLimpo = limpaString(produto.produtoNome.Trim().ToLower());
            foreach (var foto in fotos)
            {
                string caminhoFoto = "C:\\inetpub\\wwwroot\\fotos\\" + produtoId + "\\";
                if (foto.produtoFotoId.ToString() == foto.produtoFoto)
                {
                    if (File.Exists(caminhoFoto + foto.produtoFoto + ".jpg"))
                    {
                        /*var produtoFoto = new tbProdutoFoto();
                        produtoFoto.ordenacao = foto.ordenacao;
                        produtoFoto.produtoFoto = "";
                        produtoFoto.produtoFotoDescricao = foto.produtoFotoDescricao;
                        produtoFoto.produtoFotoDestaque = foto.produtoFotoDestaque;
                        produtoFoto.produtoId = foto.produtoId;
                        produtoFoto.dataDaCriacao = DateTime.Now;
                        data.tbProdutoFotos.InsertOnSubmit(produtoFoto);
                        data.SubmitChanges();*/

                        bool sucesso = true;
                        try
                        {
                            string produtoFoto = nomeLimpo + "-" + foto.produtoFotoId;
                            string fotoNomeProduto = nomeLimpo + "-" + foto.produtoFotoId + ".jpg";

                            File.Copy(caminhoFoto + foto.produtoFoto + ".jpg", caminhoFoto + "original_" + produtoFoto + ".jpg", true);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto Original para S3";
                            });
                            uploadImagemAmazonS3("original_" + fotoNomeProduto, produtoId);

                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto";
                            });
                            var tinify = new Tinify();
                            var fotoOtimizada = tinify.Shrink(caminhoFoto + foto.produtoFoto + ".jpg", caminhoFoto + produtoFoto + ".jpg");
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviado foto otimizada para S3";
                            });
                            uploadImagemAmazonS3(fotoNomeProduto, produtoId); label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto media";
                            });
                            gerarFotoWebP(caminhoFoto, fotoNomeProduto, produtoId);
                            Thread.Sleep(100);

                            //Cria a foto média
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto media";
                            });
                            tinify.Cover(fotoOtimizada, 250, 250, caminhoFoto + "media_" + fotoNomeProduto);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto media";
                            });
                            uploadImagemAmazonS3("media_" + fotoNomeProduto, produtoId);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto media";
                            });
                            gerarFotoWebP(caminhoFoto, "media_" + fotoNomeProduto, produtoId);
                            Thread.Sleep(100);

                            //Cria a foto pequena
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto pequena";
                            });
                            tinify.Cover(fotoOtimizada, 180, 180, caminhoFoto + "pequena_" + fotoNomeProduto);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto pequena";
                            });
                            uploadImagemAmazonS3("pequena_" + fotoNomeProduto, produtoId);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto media";
                            });
                            gerarFotoWebP(caminhoFoto, "pequena_" + fotoNomeProduto, produtoId);
                            Thread.Sleep(100);

                            //Cria a foto 500 mobile
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto 500";
                            });
                            tinify.Cover(fotoOtimizada, 500, 500, caminhoFoto + "500_" + fotoNomeProduto);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto 500";
                            });
                            uploadImagemAmazonS3("500_" + fotoNomeProduto, produtoId);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto 500";
                            });
                            gerarFotoWebP(caminhoFoto, "500_" + fotoNomeProduto, produtoId);
                            Thread.Sleep(100);

                            //Cria a foto 125 mobile
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto 125";
                            });
                            tinify.Cover(fotoOtimizada, 125, 125, caminhoFoto + "125_" + fotoNomeProduto);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto 125";
                            });
                            uploadImagemAmazonS3("125_" + fotoNomeProduto, produtoId);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto 125";
                            });
                            gerarFotoWebP(caminhoFoto, "125_" + fotoNomeProduto, produtoId);
                            Thread.Sleep(100);

                            //Cria a foto 70 mobile
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando foto 70";
                            });
                            tinify.Cover(fotoOtimizada, 70, 70, caminhoFoto + "70_" + fotoNomeProduto);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " enviando foto 70";
                            });
                            uploadImagemAmazonS3("70_" + fotoNomeProduto, produtoId);
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " webp foto 70";
                            });
                            gerarFotoWebP(caminhoFoto, "70_" + fotoNomeProduto, produtoId);
                            data.SubmitChanges();
                        }
                        catch (Exception ex)
                        {
                            sucesso = false;
                            var dataErro = new dbSiteDataContext();
                            var queueErro = new tbQueue()
                            {
                                agendamento = DateTime.Now,
                                andamento = false,
                                concluido = false,
                                tipoQueue = 25,
                                mensagem = "Foto " + ex.ToString()
                            };
                            dataErro.tbQueues.InsertOnSubmit(queueErro);
                            dataErro.SubmitChanges();
                            /*var dataAlt = new dbSiteDataContext();
                            var fotoDelete = (from c in dataAlt.tbProdutoFotos where c.produtoFotoId == produtoFoto.produtoFotoId select c).First();
                            dataAlt.tbProdutoFotos.DeleteOnSubmit(fotoDelete);
                            dataAlt.SubmitChanges();*/

                        }

                        if (sucesso)
                        {
                            if (produto.fotoDestaque == foto.produtoFoto)
                            {
                                produto.fotoDestaque = foto.produtoFoto;
                            }
                            foto.produtoFoto = nomeLimpo + "-" + foto.produtoFotoId;
                            data.SubmitChanges();

                            /*var dataAlt = new dbSiteDataContext();
                            var fotoDelete = (from c in dataAlt.tbProdutoFotos where c.produtoFotoId == foto.produtoFotoId select c).First();
                            dataAlt.tbProdutoFotos.DeleteOnSubmit(fotoDelete);
                            dataAlt.SubmitChanges();*/

                        }
                    }
                }
                else
                {
                    try
                    {

                        string fotoNomeProduto = foto.produtoFoto + ".jpg";

                        bool processarFoto = (!File.Exists(caminhoFoto + foto.produtoFoto + ".jpg"))
                            | (!File.Exists(caminhoFoto + "media_" + foto.produtoFoto + ".jpg"))
                            | (!File.Exists(caminhoFoto + "pequena_" + foto.produtoFoto + ".jpg"))
                            | (!File.Exists(caminhoFoto + "500_" + foto.produtoFoto + ".jpg"))
                            | (!File.Exists(caminhoFoto + "125_" + foto.produtoFoto + ".jpg"))
                            | (!File.Exists(caminhoFoto + "70_" + foto.produtoFoto + ".jpg")) | refazer;
                        if (processarFoto)
                        {
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " otimizando " + fotoNomeProduto;
                            });
                            bool possuiOriginal = true;
                            if (!File.Exists(caminhoFoto + "original_" + foto.produtoFoto + ".jpg"))
                            {
                                possuiOriginal = false;
                                File.Copy(caminhoFoto + foto.produtoFoto + ".jpg", caminhoFoto + "original_" + foto.produtoFoto + ".jpg", true);
                                uploadImagemAmazonS3("original_" + fotoNomeProduto, produtoId);
                            }

                            var tinify = new Tinify();
                            var fotoOtimizada = possuiOriginal ? tinify.Shrink(caminhoFoto + "original_" + foto.produtoFoto + ".jpg") : tinify.Shrink(caminhoFoto + foto.produtoFoto + ".jpg", caminhoFoto + foto.produtoFoto + ".jpg");
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " carregada ";
                            });
                            if (!File.Exists(caminhoFoto + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando original";
                                });
                                var fotoOtimizar = tinify.Shrink(caminhoFoto + "original_" + foto.produtoFoto + ".jpg", caminhoFoto + foto.produtoFoto + ".jpg");
                                uploadImagemAmazonS3(fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, fotoNomeProduto, produtoId, 99);
                            }

                            if (!File.Exists(caminhoFoto + "media_" + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando media";
                                });
                                //Cria a foto média

                                //ResizeImageFile(250, caminhoFoto + foto.produtoFoto + ".jpg", "media_" + caminhoFoto + foto.produtoFoto + ".jpg", ImageFormat.Jpeg);
                                //var fotoOtimizar = tinify.Shrink("media_" + caminhoFoto + foto.produtoFoto + ".jpg", "media_" + caminhoFoto + foto.produtoFoto + ".jpg");
                                tinify.Cover(fotoOtimizada, 250, 250, caminhoFoto + "media_" + fotoNomeProduto);
                                uploadImagemAmazonS3("media_" + fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, "media_" + fotoNomeProduto, produtoId);
                                Thread.Sleep(100);
                            }

                            if (!File.Exists(caminhoFoto + "pequena_" + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando pequena";
                                });
                                //Cria a foto pequena
                                tinify.Cover(fotoOtimizada, 180, 180, caminhoFoto + "pequena_" + fotoNomeProduto);
                                uploadImagemAmazonS3("pequena_" + fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, "pequena_" + fotoNomeProduto, produtoId);
                                Thread.Sleep(100);
                            }

                            if (!File.Exists(caminhoFoto + "500_" + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando 500";
                                });
                                //Cria a foto 500 mobile
                                tinify.Cover(fotoOtimizada, 500, 500, caminhoFoto + "500_" + fotoNomeProduto);
                                uploadImagemAmazonS3("500_" + fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, "500_" + fotoNomeProduto, produtoId);
                                Thread.Sleep(100);
                            }

                            if (!File.Exists(caminhoFoto + "125_" + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando 125";
                                });
                                //Cria a foto 125 mobile
                                tinify.Cover(fotoOtimizada, 125, 125, caminhoFoto + "125_" + fotoNomeProduto);
                                uploadImagemAmazonS3("125_" + fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, "125_" + fotoNomeProduto, produtoId);
                                Thread.Sleep(100);
                            }
                            if (!File.Exists(caminhoFoto + "70_" + foto.produtoFoto + ".jpg") | refazer)
                            {
                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = produtoId + " gerando 70";
                                });
                                //Cria a foto 70 mobile
                                tinify.Cover(fotoOtimizada, 70, 70, caminhoFoto + "70_" + fotoNomeProduto);
                                uploadImagemAmazonS3("70_" + fotoNomeProduto, produtoId);
                                gerarFotoWebP(caminhoFoto, "70_" + fotoNomeProduto, produtoId);
                                data.SubmitChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        label10.Invoke((MethodInvoker)delegate
                        {
                            label10.Text = ex.ToString();
                        });
                    }
                }
                //Thread.Sleep(500);
            }

            var colecoes = (from c in data.tbJuncaoProdutoColecaos where c.produtoId == produtoId && (c.tbColecao.imagem360 ?? "") != "" select c).ToList();
            if (colecoes.Count > 0)
            {
                var colecao = colecoes.First().tbColecao;
                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = produtoId + " gerando 360 " + colecao.colecaoId;
                });
                string caminhoColecao = "C:\\inetpub\\wwwroot\\colecoes\\" + colecao.colecaoId + "\\";
                string caminhoFoto = "C:\\inetpub\\wwwroot\\fotos\\" + produtoId + "\\";
                if (!string.IsNullOrEmpty(colecao.imagem360))
                {
                    bool processarFoto = true;
                    if (processarFoto)
                    {

                        var tinify = new Tinify();
                        File.Copy(caminhoColecao + "imagem360.jpg", caminhoFoto + "imagem360.jpg", true);
                        if (!File.Exists(caminhoColecao + "otimizada_imagem360.jpg"))
                        {
                            var fotoOtimizarColecao = tinify.Shrink(caminhoColecao + "imagem360.jpg", caminhoColecao + "otimizada_imagem360.jpg");
                        }
                        //if (!File.Exists(caminhoColecao + "m_imagem360.jpg"))
                        if (true == true)
                        {
                            label2.Invoke((MethodInvoker)delegate
                            {
                                label2.Text = produtoId + " gerando 360 M";
                            });
                            ResizeProportional(caminhoColecao + "imagem360.jpg", caminhoColecao + "med_imagem360.jpg", System.Drawing.Imaging.ImageFormat.Jpeg, 4000, 2000);
                            var fotoOtimizarColecao = tinify.Shrink(caminhoColecao + "med_imagem360.jpg", caminhoColecao + "m_imagem360.jpg");
                            //tinify.Scale(fotoOtimizada, 3000, null, caminhoColecao + "m_imagem360.jpg");
                        }
                        if (!File.Exists(caminhoColecao + "pequena_imagem360.jpg"))
                        {
                            var fotoOtimizada = tinify.Shrink(caminhoColecao + "otimizada_imagem360.jpg");
                            tinify.Cover(fotoOtimizada, 265, 60, caminhoColecao + "pequena_imagem360.jpg");
                        }
                        label2.Invoke((MethodInvoker)delegate
                        {
                            label2.Text = produtoId + " carregada ";
                        });

                        label2.Invoke((MethodInvoker)delegate
                        {
                            label2.Text = produtoId + " copiando 360";
                        });
                        File.Copy(caminhoColecao + "otimizada_imagem360.jpg", caminhoFoto + "otimizada_imagem360.jpg", true);
                        File.Copy(caminhoColecao + "pequena_imagem360.jpg", caminhoFoto + "pequena_imagem360.jpg", true);
                        File.Copy(caminhoColecao + "m_imagem360.jpg", caminhoFoto + "m_imagem360.jpg", true);
                        uploadImagemAmazonS3("imagem360.jpg", produtoId);
                        uploadImagemAmazonS3("otimizada_imagem360.jpg", produtoId);
                        //gerarFotoWebP(caminhoFoto, "otimizada_imagem360.jpg", produtoId, 90);


                        //Cria a foto pq
                        uploadImagemAmazonS3("pequena_imagem360.jpg", produtoId);
                        gerarFotoWebP(caminhoFoto, "pequena_imagem360.jpg", produtoId);

                        uploadImagemAmazonS3("m_imagem360.jpg", produtoId);
                        //gerarFotoWebP(caminhoFoto, "m_imagem360.jpg", produtoId);
                        Thread.Sleep(100);

                        var queue = new tbQueue()
                        {
                            agendamento = DateTime.Now,
                            andamento = false,
                            concluido = false,
                            idRelacionado = produto.produtoId,
                            mensagem = "",
                            tipoQueue = 15
                        };

                        data.tbQueues.InsertOnSubmit(queue);
                        data.SubmitChanges();

                    }
                }
            }

        }

        private void gerarFotoWebP(string caminhoFoto, string nomeArquivo, int produtoId)
        {
            gerarFotoWebP(caminhoFoto, nomeArquivo, produtoId, 70);
        }

        public static Size ResizeKeepAspect(Size src, int maxWidth, int maxHeight)
        {
            decimal rnd = Math.Min(maxWidth / (decimal)src.Width, maxHeight / (decimal)src.Height);
            return new Size((int)Math.Round(src.Width * rnd), (int)Math.Round(src.Height * rnd));
        }

        public static void ResizeProportional(string Location, string Target, ImageFormat Format, int maxWidth, int maxHeight)
        {
            Bitmap fullSizeImg = new Bitmap(Location);
            var finalSize = ResizeKeepAspect(fullSizeImg.Size, maxWidth, maxHeight);
            int x = 0;
            int y = 0;


            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            //First Resize the Existing Image 
            System.Drawing.Image thumbNailImg;
            //thumbNailImg = fullSizeImg.GetThumbnailImage(width, height, null, System.IntPtr.Zero);
            thumbNailImg = resizeImage(fullSizeImg, finalSize);
            //Clean up / Dispose... 
            fullSizeImg.Dispose();

            //Create a Crop Frame to apply to the Resized Image 
            Bitmap myBitmapCropped = new Bitmap(finalSize.Width, finalSize.Height);
            Graphics myGraphic = Graphics.FromImage(myBitmapCropped);

            //Apply the Crop to the Resized Image 
            myGraphic.DrawImage(thumbNailImg, new Rectangle(0, 0, finalSize.Width, finalSize.Height), x, y, finalSize.Width, finalSize.Height, GraphicsUnit.Pixel);
            //Clean up / Dispose... 
            myGraphic.Dispose();

            //Save the Croped and Resized image as a new square thumnail 
            myBitmapCropped.Save(Target, codecInfo, encoderParams);

            //Clean up / Dispose... 
            thumbNailImg.Dispose();
            myBitmapCropped.Dispose();
        }

        public static void ResizeImageFile(int Size, string Location, string Target, ImageFormat Format)
        {
            //string PhotoPath = Server.MapPath("~/http://www.jozan.com.br/images/");
            Bitmap fullSizeImg = new Bitmap(Location);
            int width = fullSizeImg.Width;
            int height = fullSizeImg.Height;
            int x = 0;
            int y = 0;

            //Determine dimensions of resized version of the image 
            if (width > height)
            {
                width = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)width / (decimal)height)), 0);

                height = Size;
                // moves cursor so that crop is more centered 
                x = Convert.ToInt32(Math.Ceiling(((decimal)(width - height) / 2M)));
            }
            else if (height > width)
            {
                height = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)height / (decimal)width)), 0);
                width = Size;
                // moves cursor so that crop is more centered 
                y = Convert.ToInt32(Math.Ceiling(((decimal)(height - width) / 2M)));
            }
            else
            {
                width = Size;
                height = Size;
            }

            //// required in case thumbnail creation fails?
            //System.Drawing.Image.GetThumbnailImageAbort dummyCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);

            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L);
            ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            //First Resize the Existing Image 
            System.Drawing.Image thumbNailImg;
            //thumbNailImg = fullSizeImg.GetThumbnailImage(width, height, null, System.IntPtr.Zero);
            thumbNailImg = resizeImage(fullSizeImg, new System.Drawing.Size(width, height));
            //Clean up / Dispose... 
            fullSizeImg.Dispose();

            //Create a Crop Frame to apply to the Resized Image 
            Bitmap myBitmapCropped = new Bitmap(Size, Size);
            Graphics myGraphic = Graphics.FromImage(myBitmapCropped);

            //Apply the Crop to the Resized Image 
            myGraphic.DrawImage(thumbNailImg, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
            //Clean up / Dispose... 
            myGraphic.Dispose();

            //Save the Croped and Resized image as a new square thumnail 
            myBitmapCropped.Save(Target, codecInfo, encoderParams);

            //Clean up / Dispose... 
            thumbNailImg.Dispose();
            myBitmapCropped.Dispose();
        }


        private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;

            int destWidth = (int)size.Width;
            int destHeight = (int)size.Height;

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (System.Drawing.Image)b;
        }
        public static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType == mimeType)
                {
                    return codecs[i];
                }
            }

            return null;
        }

        private void gerarFotoWebP(string caminhoFoto, string nomeArquivo, int produtoId, int resolucao)
        {
            bool webpCarregado = true;
            try
            {
                Imazen.WebP.Extern.LoadLibrary.LoadWebPOrFail();
            }
            catch (Exception ex)
            {
                webpCarregado = false;
            }

            if (webpCarregado)
            {
                try
                {
                    string nomeArquivoWebp = nomeArquivo.Split('.')[0] + ".webp";
                    System.Drawing.Bitmap mBitmap;
                    FileStream outStream = new FileStream(caminhoFoto + nomeArquivoWebp, FileMode.Create);
                    using (Stream BitmapStream = System.IO.File.Open(caminhoFoto + nomeArquivo, System.IO.FileMode.Open))
                    {
                        System.Drawing.Image img = System.Drawing.Image.FromStream(BitmapStream);

                        mBitmap = new System.Drawing.Bitmap(img);
                        var encoder = new Imazen.WebP.SimpleEncoder();
                        encoder.Encode(mBitmap, outStream, resolucao);
                        outStream.Close();
                        uploadImagemAmazonS3(nomeArquivoWebp, produtoId);
                    }
                }
                catch (Exception ex) { }
            }
        }


        private void uploadImagemAmazonS3(string foto, int produtoId)
        {
            string caminhoFoto = "C:\\inetpub\\wwwroot\\fotos\\";
            string s3ServiceUrl = "s3-sa-east-1.amazonaws.com";
            string bucketName = "cdn2.graodegente.com.br";
            string filePath = caminhoFoto + produtoId + "\\" + foto;
            string fileContentType = "image/jpg";
            Amazon.S3.Model.S3CannedACL fileCannedACL = Amazon.S3.Model.S3CannedACL.PublicReadWrite;

            string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
            string secretAccessKey = "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";

            try
            {
                AmazonS3Config config = new AmazonS3Config();
                config.ServiceURL = s3ServiceUrl;
                config.CommunicationProtocol = Protocol.HTTP;

                using (var s3Client = Amazon.AWSClientFactory.CreateAmazonS3Client(accessKey, secretAccessKey, config))
                {
                    PutObjectRequest request = new PutObjectRequest
                    {
                        BucketName = bucketName,
                        CannedACL = fileCannedACL,
                        Key = "fotos/" + produtoId + "/" + foto,
                        FilePath = filePath,
                        ContentType = fileContentType
                    };
                    request.AddHeader("expires", "Thu, 21 Mar 2042 08:16:32 GMT");
                    PutObjectResponse response = s3Client.PutObject(request);

                }

            }
            catch (AmazonS3Exception s3Exception)
            {

            }
        }

        public string limpaString(string texto)
        {
            string caracteresEspeciais = " ÄÅÁÂÀÃäáâàãÉÊËÈéêëèÍÎÏÌíîïìÖÓÔÒÕöóôòõÜÚÛüúûùÇç/`~!@#$%^&*()*+;:',<.>?|";
            string caracteresNormais = "-AAAAAAaaaaaEEEEeeeeIIIIiiiiOOOOOoooooUUUuuuuCc------------------------";

            for (int i = 0; i < caracteresEspeciais.Length; i++)
            {
                try
                {
                    texto = texto.Replace(caracteresEspeciais[i].ToString(), caracteresNormais[i].ToString());
                    texto = texto.Replace("\"", "");
                    texto = texto.Replace("'", "");
                }
                catch (Exception ex) { }
            }

            texto = RemoveAcentosComNormalizacao(texto).Replace("------", "-").Replace("-----", "-").Replace("----", "-").Replace("---", "-").Replace("--", "-");
            texto = texto.Trim('-');
            return texto;
        }


        public string RemoveAcentosComNormalizacao(string inputString)
        {
            if ((inputString == "") || (inputString == null))
                return "";

            string normalizedString = inputString.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < normalizedString.Length; i++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(normalizedString[i]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(normalizedString[i]);
                }
            }
            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }


        private void ProcessaQueueEnviaErro(long idQueue)
        {
            #region atualizaQueue
            var dataQueue = new dbSiteDataContext();
            var queue = (from c in dataQueue.tbQueues where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            dataQueue.SubmitChanges();
            #endregion

            rnEmails.EnviaEmail("", "andre@bark.com.br", "", "", "", queue.mensagem, "Erro na Fila");

            #region finalizaQueue
            var queueFinalizar = (from c in dataQueue.tbQueues where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            dataQueue.SubmitChanges();
            #endregion
        }

        private void bwChecarQueue_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (!timerQueue.Enabled)
                {
                    //timerQueue.Interval = 10000;
                    timerQueue.Enabled = true;
                }

                label1.Text = "Aguardando Checagem de Queue - " + Convert.ToInt32((timerQueue.Interval / 1000)) + "seg.";

                label5.Invoke((MethodInvoker)delegate
                {
                    label5.Text = "Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
                });

                if (Convert.ToInt32((timerQueue.Interval / 1000)) == 0)
                {
                    //timerQueue.Interval = 10000;
                    timerQueue.Enabled = true;

                    label5.Invoke((MethodInvoker)delegate
                    {
                        label5.Text = "*Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
                    });
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha finalização bwChecarQueue_RunWorkerCompleted - Kraken");
            }
            timerQueue.Interval = 1;
            timerQueue.Enabled = true;
        }
        #endregion

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Compactando Fotos";
                });


                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest();

                kr.Lossy = false;

                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }


                using (var data = new dbSiteDataContext())
                {

                    //List<int> idFotoJaCompactadas = new List<int>();

                    qtdFotosCompactadas = (from c in data.tbProdutoFotoKrakens select c.Id).Count();
                    //var fotosKraken = (from c in data.tbProdutoFotoKrakens select c.produtoFotoId).ToList().Distinct();

                    //foreach (var fotoKraken in fotosKraken)
                    //{
                    //    idFotoJaCompactadas.Add(fotoKraken.Value);
                    //}

                    var fotos = (from f in data.tbProdutoFotos join tbkraken in data.tbProdutoFotoKrakens on f.produtoFotoId equals tbkraken.produtoFotoId into fotosJaCompacatadas where fotosJaCompacatadas.Count() == 0 select f); // where !idFotoJaCompactadas.Contains(f.produtoFotoId) select f).ToList();

                    faltaCompactar = fotos.Count();

                    foreach (var foto in fotos)
                    {

                        label1.Invoke((MethodInvoker)delegate
                        {
                            label1.Text = "ProdutoId: " + foto.produtoId + " FotoId: " + foto.produtoFoto;
                        });

                        label2.Invoke((MethodInvoker)delegate
                        {
                            label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                        });



                        if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"))
                        {

                            #region Foto tamanho grande
                            // Pull the Stream of the image from the provider.
                            // This saves us from having to care about BlobStorage

                            System.IO.FileStream imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            // Can't trust the length of Stream. Converting to a MemoryStream
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                            {
                                imageData.CopyTo(ms);

                                kr.File = ms.ToArray();
                            }

                            kr.s3_store = new KrakenRequestS3Amazon();
                            kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                            kr.s3_store.S3Secret = "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";
                            kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                            kr.s3_store.S3Region = "sa-east-1";
                            kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/" + foto.produtoFoto + ".jpg";

                            var response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ""), ".jpg");

                            if (response.Success == false || response.Error != null)
                            {

                            }

                            using (var webClient = new System.Net.WebClient())
                            using (var stream = webClient.OpenRead(response.KrakedUrl))
                            {
                                //Check out the master to get a temp version.
                                //Image temp = _librariesManager.Lifecycle.CheckOut(image) as Image;

                                //Make the modifications to the temp version.


                                if (!System.IO.Directory.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                    System.IO.Directory.CreateDirectory(caminhoFisico + "fotos\\" + foto.produtoId + "\\");

                                webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                var controleFoto = new tbProdutoFotoKraken();
                                controleFoto.produtoId = foto.produtoId;
                                controleFoto.produtoFoto = foto.produtoFoto + ".jpg";
                                controleFoto.produtoFotoId = foto.produtoFotoId;
                                data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                data.SubmitChanges();

                                //#region Salva no Azure

                                //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(discoAzure);
                                //var blobClient = storageAccount.CreateCloudBlobClient();

                                //CloudBlobContainer container = blobClient.GetContainerReference("fotos");
                                ////container.CreateIfNotExists();

                                //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                                ////cria foto no Azure
                                //CloudBlockBlob blob = container.GetBlockBlobReference(foto.produtoId + "/" + foto.produtoFoto + ".jpg");
                                //blob.UploadFromStream(stream);
                                //blob.Properties.ContentType = "image/jpeg";

                                //#endregion Salva no Azure

                                qtdFotosCompactadas++;

                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                                });

                                //_librariesManager.Upload(temp, stream, Path.GetExtension(response.KrakedUrl));

                                //Checkin the temp and get the updated master version.
                                //After the check in the temp version is deleted.
                                //_librariesManager.Lifecycle.CheckIn(temp);

                                //_librariesManager.SaveChanges();

                                // Check to see if this image is already published.
                                // If it is, we need to publish the "Master" to update "Live"
                                //if (image.GetWorkflowState() == "Published")
                                //{
                                //    var bag = new Dictionary<string, string>();
                                //    bag.Add("ContentType", typeof(Image).FullName);
                                //    WorkflowManager.MessageWorkflow(image.Id, typeof(Image), albumProvider.Name, "Publish", false, bag);
                                //}
                            }

                            #endregion

                            imageData.Dispose();

                            #region foto tamanho medio

                            imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\media_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            if (imageData != null)
                            {


                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/media_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\media_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {
                                    if (!System.IO.Directory.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                        System.IO.Directory.CreateDirectory(caminhoFisico + "fotos\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = "media_" + foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    //#region Salva no Azure Foto Media

                                    //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(discoAzure);
                                    //var blobClient = storageAccount.CreateCloudBlobClient();

                                    //CloudBlobContainer container = blobClient.GetContainerReference("fotos");
                                    ////container.CreateIfNotExists();

                                    //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                                    ////cria foto no Azure
                                    //CloudBlockBlob blob = container.GetBlockBlobReference(foto.produtoId + "/media_" + foto.produtoFoto + ".jpg");
                                    //blob.UploadFromStream(stream);
                                    //blob.Properties.ContentType = "image/jpeg";

                                    //#endregion Salva no Azure Foto Media

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                                    });
                                }

                            }


                            #endregion

                            imageData.Dispose();

                            #region foto tamanho pequeno

                            imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            if (imageData != null)
                            {

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }


                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/pequena_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }


                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {
                                    if (!System.IO.Directory.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                        System.IO.Directory.CreateDirectory(caminhoFisico + "fotos\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = "pequena_" + foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    //#region Salva no Azure Foto Pequena

                                    //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(discoAzure);
                                    //var blobClient = storageAccount.CreateCloudBlobClient();

                                    //CloudBlobContainer container = blobClient.GetContainerReference("fotos");
                                    ////container.CreateIfNotExists();

                                    //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                                    ////cria foto no Azure
                                    //CloudBlockBlob blob = container.GetBlockBlobReference(foto.produtoId + "/pequena_" + foto.produtoFoto + ".jpg");
                                    //blob.UploadFromStream(stream);
                                    //blob.Properties.ContentType = "image/jpeg";

                                    //#endregion Salva no Azure Foto Pequena

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                                    });
                                }

                            }

                            #endregion

                            imageData.Dispose();

                            #region foto 500 mobile

                            imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            if (imageData != null)
                            {

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }


                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/500_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }


                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {
                                    if (!System.IO.Directory.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                        System.IO.Directory.CreateDirectory(caminhoFisico + "fotos\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = "500_" + foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    //#region Salva no Azure Foto Pequena

                                    //CloudStorageAccount storageAccount = CloudStorageAccount.Parse(discoAzure);
                                    //var blobClient = storageAccount.CreateCloudBlobClient();

                                    //CloudBlobContainer container = blobClient.GetContainerReference("fotos");
                                    ////container.CreateIfNotExists();

                                    //container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                                    ////cria foto no Azure
                                    //CloudBlockBlob blob = container.GetBlockBlobReference(foto.produtoId + "/pequena_" + foto.produtoFoto + ".jpg");
                                    //blob.UploadFromStream(stream);
                                    //blob.Properties.ContentType = "image/jpeg";

                                    //#endregion Salva no Azure Foto Pequena

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas + " Fotos Compactadas";
                                    });
                                }

                            }

                            #endregion foto 500 mobile

                        }


                    }
                }
            }
            catch (Exception ex)
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = ex.ToString();
                });
            }


        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            button1.Enabled = true;

            if (faltaCompactar == 0)
            {
                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Fim do Processo Fotos compactadas";
                });
            }
            else
            {

                label3.Invoke((MethodInvoker)delegate
                {
                    label3.Text = "Faltam " + faltaCompactar.ToString() + " fotos.";
                });

                Thread.Sleep(10000);

                backgroundWorker1.RunWorkerAsync();

                button1.Enabled = false;
            }
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Compactando Fotos 500";
                });


                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest();

                kr.Lossy = false;

                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }


                using (var data = new dbSiteDataContext())
                {

                    qtdFotosCompactadas = (from c in data.tbProdutoFotoKraken500s select c.Id).Count();

                    var fotos = (from f in data.tbProdutoFotos join tbkraken in data.tbProdutoFotoKraken500s on f.produtoFotoId equals tbkraken.produtoFotoId into fotosJaCompacatadas where !fotosJaCompacatadas.Any() select f); // where !idFotoJaCompactadas.Contains(f.produtoFotoId) select f).ToList();

                    faltaCompactar = fotos.Count();

                    foreach (var foto in fotos)
                    {

                        label1.Invoke((MethodInvoker)delegate
                        {
                            label1.Text = "ProdutoId: " + foto.produtoId + " FotoId: " + foto.produtoFoto;
                        });

                        label2.Invoke((MethodInvoker)delegate
                        {
                            label2.Text = qtdFotosCompactadas.ToString() + " Fotos 500 Compactadas";
                        });



                        if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"))
                        {

                            #region Formato 500

                            string Target;
                            string Location = caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg";
                            //Cria a foto 500 mobile
                            Target = caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto;
                            rnFotos.ResizeImageFile(500, Location, Target, System.Drawing.Imaging.ImageFormat.Jpeg);

                            #endregion Formato 500

                            #region Foto tamanho 500 mobile

                            if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"))
                            {

                                System.IO.FileStream imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon();
                                kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                                kr.s3_store.S3Secret = "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";
                                kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                                kr.s3_store.S3Region = "sa-east-1";
                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/500_" + foto.produtoFoto + ".jpg";

                                var response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    //if (!System.IO.Directory.Exists(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                    //System.IO.Directory.CreateDirectory(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken500();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    controleFoto.produtoFotoIdString = foto.produtoFotoId.ToString();
                                    data.tbProdutoFotoKraken500s.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos 500 Compactadas";
                                    });
                                }

                                imageData.Dispose();
                            }
                            #endregion Foto tamanho 500 mobile
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = ex.ToString();
                });
            }
        }

        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button2.Enabled = true;

            if (faltaCompactar == 0)
            {
                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Fim do Processo Fotos 500 compactadas";
                });
            }
            else
            {

                label3.Invoke((MethodInvoker)delegate
                {
                    label3.Text = "Faltam " + faltaCompactar.ToString() + " fotos 500.";
                });

                Thread.Sleep(10000);

                backgroundWorker2.RunWorkerAsync();

                button2.Enabled = false;
            }
        }

        private void backgroundWorker4_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                label7.Invoke((MethodInvoker)delegate
                {
                    label7.Text = "Compactando Fotos 125";
                });


                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest { Lossy = false };


                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }


                using (var data = new dbSiteDataContext())
                {

                    qtdFotosCompactadas125 = (from c in data.tbProdutoFotoKraken125s select c.Id).Count();

                    var fotos = (from f in data.tbProdutoFotos join tbkraken in data.tbProdutoFotoKraken125s on f.produtoFotoId equals tbkraken.produtoFotoId into fotosJaCompacatadas where !fotosJaCompacatadas.Any() select f).ToList().Take(10); // where !idFotoJaCompactadas.Contains(f.produtoFotoId) select f).ToList();

                    faltaCompactar125 = fotos.Count();

                    label7.Invoke((MethodInvoker)delegate
                    {
                        label7.Text = "Faltam " + faltaCompactar125 + " fotos 125.";
                    });
                    int numCompac = 0;
                    foreach (var foto in fotos)
                    {
                        numCompac++;
                        label7.Invoke((MethodInvoker)delegate
                        {
                            label7.Text = "ProdutoId: " + foto.produtoId + " FotoId: " + foto.produtoFoto;
                        });

                        label8.Invoke((MethodInvoker)delegate
                        {
                            label8.Text = qtdFotosCompactadas125.ToString() + " Fotos 125 Compactadas";
                        });



                        if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"))
                        {

                            #region Formato 125

                            string Target;
                            string Location = caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg";
                            //Cria a foto 500 mobile
                            Target = caminhoFisico + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ".jpg";
                            rnFotos.ResizeImageFile(125, Location, Target, System.Drawing.Imaging.ImageFormat.Jpeg);

                            #endregion Formato 125

                            label9.Invoke((MethodInvoker)delegate
                            {
                                label9.Text = "Compactando " + numCompac;
                            });

                            #region Foto tamanho 125 mobile

                            if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ".jpg"))
                            {

                                System.IO.FileStream imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = "AKIAIP7IE6WVNK2K6TIQ",
                                    S3Secret = "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",
                                    S3Bucket = "cdn2.graodegente.com.br",
                                    S3Region = "sa-east-1",
                                    S3Path = "fotos/" + foto.produtoId + "/125_" + foto.produtoFoto + ".jpg"
                                };

                                var response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken125();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    controleFoto.produtoFotoIdString = foto.produtoFotoId.ToString();
                                    data.tbProdutoFotoKraken125s.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    qtdFotosCompactadas125++;

                                    label8.Invoke((MethodInvoker)delegate
                                    {
                                        label8.Text = qtdFotosCompactadas125.ToString() + " Fotos 125 Compactadas";
                                    });
                                }
                            }
                            #endregion Foto tamanho 125 mobile
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                label7.Invoke((MethodInvoker)delegate
                {
                    label7.Text = ex.ToString();
                });
            }
        }

        private void backgroundWorker4_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button3.Enabled = true;

            if (faltaCompactar125 == 0)
            {
                label9.Invoke((MethodInvoker)delegate
                {
                    label9.Text = "Fim do Processo Fotos 125 compactadas";
                });
            }
            else
            {

                label7.Invoke((MethodInvoker)delegate
                {
                    label7.Text = "Faltam " + faltaCompactar125.ToString() + " fotos 125.";
                });

                //Thread.Sleep(10000);

                backgroundWorker4.RunWorkerAsync();

                button3.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();

            button1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            backgroundWorker2.RunWorkerAsync();

            button2.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            backgroundWorker4.RunWorkerAsync();

            button3.Enabled = false;
        }

        private void btnCompactarPorProdutoId_Click(object sender, EventArgs e)
        {
            backgroundWorker3.RunWorkerAsync();

            btnCompactarPorProdutoId.Enabled = false;
        }

        private void backgroundWorker3_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Compactando Fotos ProdutoId: " + txtProdutoId.Text;
                });

                int produtoId = Convert.ToInt32(txtProdutoId.Text);

                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest();

                kr.Lossy = false;

                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }


                using (var data = new dbSiteDataContext())
                {

                    var fotos = (from f in data.tbProdutoFotos where f.produtoId == produtoId select f);

                    faltaCompactar = fotos.Count();

                    foreach (var foto in fotos)
                    {

                        label1.Invoke((MethodInvoker)delegate
                        {
                            label1.Text = "ProdutoId: " + foto.produtoId + " FotoId: " + foto.produtoFoto;
                        });

                        label2.Invoke((MethodInvoker)delegate
                        {
                            label2.Text = qtdFotosCompactadas + " Fotos Compactadas";
                        });



                        if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"))
                        {

                            #region Foto tamanho grande
                            // Pull the Stream of the image from the provider.
                            // This saves us from having to care about BlobStorage

                            System.IO.FileStream imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            // Can't trust the length of Stream. Converting to a MemoryStream
                            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                            {
                                imageData.CopyTo(ms);

                                kr.File = ms.ToArray();
                            }

                            kr.s3_store = new KrakenRequestS3Amazon();
                            kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                            kr.s3_store.S3Secret = "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";
                            kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                            kr.s3_store.S3Region = "sa-east-1";
                            kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/" + foto.produtoFoto + ".jpg";

                            var response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ""), ".jpg");

                            if (response.Success == false || response.Error != null)
                            {

                            }

                            using (var webClient = new System.Net.WebClient())
                            using (var stream = webClient.OpenRead(response.KrakedUrl))
                            {
                                //Check out the master to get a temp version.
                                //Image temp = _librariesManager.Lifecycle.CheckOut(image) as Image;

                                //Make the modifications to the temp version.


                                if (!System.IO.Directory.Exists(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                    System.IO.Directory.CreateDirectory(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\");

                                webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                var controleFoto = new tbProdutoFotoKraken();
                                controleFoto.produtoId = foto.produtoId;
                                controleFoto.produtoFoto = foto.produtoFoto + ".jpg";
                                controleFoto.produtoFotoId = foto.produtoFotoId;
                                data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                data.SubmitChanges();

                                qtdFotosCompactadas++;

                                label2.Invoke((MethodInvoker)delegate
                                {
                                    label2.Text = qtdFotosCompactadas + " Fotos Compactadas";
                                });

                            }

                            #endregion

                            imageData.Dispose();

                            #region foto tamanho medio

                            imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\media_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            if (imageData != null)
                            {


                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/media_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\media_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {
                                    if (!System.IO.Directory.Exists(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                        System.IO.Directory.CreateDirectory(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = "media_" + foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();


                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                                    });
                                }

                            }


                            #endregion

                            imageData.Dispose();

                            #region foto tamanho pequeno

                            imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                            if (imageData != null)
                            {

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }


                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/pequena_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }


                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {
                                    if (!System.IO.Directory.Exists(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\"))//se não existir o diretório então cria
                                        System.IO.Directory.CreateDirectory(caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\");

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos-kraken\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = "pequena_" + foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    data.tbProdutoFotoKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos Compactadas";
                                    });
                                }

                            }

                            #endregion

                            imageData.Dispose();

                            #region foto 500 mobile

                            //verifica se é necessário criar foto em 500 para mobile
                            if (!System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"))
                            {
                                #region Formato 500

                                string Target;
                                string Location = caminhoFisico + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg";
                                //Cria a foto 500 mobile
                                Target = caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto;
                                rnFotos.ResizeImageFile(500, Location, Target, System.Drawing.Imaging.ImageFormat.Jpeg);

                                #endregion Formato 500
                            }

                            if (System.IO.File.Exists(caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"))
                            {

                                imageData = System.IO.File.Open((caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"), System.IO.FileMode.Open);// albumProvider.Download(image);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
                                {
                                    imageData.CopyTo(ms);

                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon();
                                kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                                kr.s3_store.S3Secret = "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";
                                kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                                kr.s3_store.S3Region = "sa-east-1";
                                kr.s3_store.S3Path = "fotos/" + foto.produtoId + "/500_" + foto.produtoFoto + ".jpg";

                                response = k.Upload(kr, (caminhoFisico + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbProdutoFotoKraken500();
                                    controleFoto.produtoId = foto.produtoId;
                                    controleFoto.produtoFoto = foto.produtoFoto + ".jpg";
                                    controleFoto.produtoFotoId = foto.produtoFotoId;
                                    controleFoto.produtoFotoIdString = foto.produtoFotoId.ToString();
                                    data.tbProdutoFotoKraken500s.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    qtdFotosCompactadas++;

                                    label2.Invoke((MethodInvoker)delegate
                                    {
                                        label2.Text = qtdFotosCompactadas.ToString() + " Fotos 500 Compactadas";
                                    });
                                }

                                imageData.Dispose();
                            }

                            #endregion foto 500 mobile

                            imageData.Dispose();

                        }


                    }
                }
            }
            catch (Exception ex)
            {

                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = ex.ToString();
                });
            }
        }

        private void backgroundWorker3_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnCompactarPorProdutoId.Enabled = true;

            if (faltaCompactar == 0)
            {
                label1.Invoke((MethodInvoker)delegate
                {
                    label1.Text = "Fim do Processo Fotos compactadas";
                });
            }
            else
            {

                label3.Invoke((MethodInvoker)delegate
                {
                    label3.Text = "Faltam " + faltaCompactar.ToString() + " fotos.";
                });

                Thread.Sleep(10000);

                backgroundWorker3.RunWorkerAsync();

                btnCompactarPorProdutoId.Enabled = false;
            }
        }


        #region Compactar Foto  - Kraken

        public void CompactarFotoKraken(int produtoFotoId)
        {

            try
            {
                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);
                KrakenRequest kr = new KrakenRequest { Lossy = false };
                string caminhoFotos = "C:\\inetpub\\wwwroot\\";

                //kr.s3_store.S3Key = "AKIAIP7IE6WVNK2K6TIQ";
                //kr.s3_store.S3Secret = "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";
                //kr.s3_store.S3Bucket = "cdn2.graodegente.com.br";
                //kr.s3_store.S3Region = "sa-east-1";

                string accessKey = "AKIAIP7IE6WVNK2K6TIQ";
                string secretAccessKey = "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";
                string bucketName = "cdn2.graodegente.com.br";
                string s3Region = "sa-east-1";

                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }
                DateTime dataValida = new DateTime(2016, 05, 25);
                using (var data = new dbSiteDataContext())
                {
                    var listaFotos = (from c in data.tbProdutoFotos where c.produtoFotoId == produtoFotoId select c);

                    foreach (var foto in listaFotos)
                    {
                        try
                        {

                            label3.Invoke((MethodInvoker)delegate
                            {
                                label3.Text = "Inicio Processo";
                            });

                            label1.Invoke((MethodInvoker)delegate
                            {
                                label1.Text = "Service em andamento aguarde...";
                            });

                            #region S3 foto Grande

                            if (File.Exists(caminhoFotos + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"))
                            {

                                File.Copy(caminhoFotos + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg", caminhoFotos + "fotos\\original_" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg", false);

                                FileStream imageData = File.Open((caminhoFotos + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ".jpg"), FileMode.Open);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = accessKey,
                                    S3Secret = secretAccessKey,
                                    S3Bucket = bucketName,
                                    S3Region = s3Region,
                                    S3Path = "fotos/" + foto.produtoId + "/" + foto.produtoFoto + ".jpg"
                                };

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactando imagem Grande";
                                });

                                var response = k.Upload(kr, (caminhoFotos + "fotos\\" + foto.produtoId + "\\" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFotos + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                                }


                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactada imagem Grande";
                                });
                            }

                            #endregion S3 foto Grande

                            label1.Invoke((MethodInvoker)delegate
                            {
                                label1.Text = "Service em andamento aguarde...";
                            });

                            #region S3 foto Media

                            if (File.Exists(caminhoFotos + "fotos\\" + foto.produtoId + "\\media_" + foto.produtoFoto + ".jpg"))
                            {

                                FileStream imageData =
                                    File.Open(
                                        (caminhoFotos + "fotos\\" + foto.produtoId +
                                         "\\media_" + foto.produtoFoto + ".jpg"), FileMode.Open);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = accessKey,
                                    S3Secret = secretAccessKey,
                                    S3Bucket = bucketName,
                                    S3Region = s3Region,
                                    S3Path = "fotos/" + foto.produtoId + "/media_" + foto.produtoFoto + ".jpg"
                                };

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactando imagem Media";
                                });

                                var response = k.Upload(kr,
                               (caminhoFotos + "fotos\\" + foto.produtoId +
                                "\\media_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl,
                                        caminhoFotos + "fotos\\" + foto.produtoId + "\\" +
                                        response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                                }

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactada imagem Media";
                                });

                            }

                            #endregion S3 foto Media

                            label1.Invoke((MethodInvoker)delegate
                            {
                                label1.Text = "Service em andamento aguarde...";
                            });

                            #region S3 foto Pequena

                            if (File.Exists(caminhoFotos + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ".jpg"))
                            {

                                FileStream imageData = File.Open((caminhoFotos + "fotos\\" + foto.produtoId + "\\pequena_" + foto.produtoFoto + ".jpg"), FileMode.Open);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = accessKey,
                                    S3Secret = secretAccessKey,
                                    S3Bucket = bucketName,
                                    S3Region = s3Region,
                                    S3Path = "fotos/" + foto.produtoId + "/pequena_" + foto.produtoFoto + ".jpg"
                                };

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactando imagem Pequena";
                                });

                                var response = k.Upload(kr,
                              (caminhoFotos + "fotos\\" + foto.produtoId +
                               "\\pequena_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl,
                                        caminhoFotos + "fotos\\" + foto.produtoId + "\\" +
                                        response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                                }

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactada imagem Pequena";
                                });

                            }

                            #endregion S3 foto Pequena

                            label1.Invoke((MethodInvoker)delegate
                            {
                                label1.Text = "Service em andamento aguarde...";
                            });

                            #region S3 foto 500 mobile

                            if (File.Exists(caminhoFotos + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"))
                            {

                                FileStream imageData = File.Open((caminhoFotos + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ".jpg"), FileMode.Open);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = accessKey,
                                    S3Secret = secretAccessKey,
                                    S3Bucket = bucketName,
                                    S3Region = s3Region,
                                    S3Path = "fotos/" + foto.produtoId + "/500_" + foto.produtoFoto + ".jpg"
                                };

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactando imagem 500";
                                });

                                var response = k.Upload(kr, (caminhoFotos + "fotos\\" + foto.produtoId + "\\500_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFotos + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                                }

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactada imagem 500";
                                });
                            }

                            #endregion S3 foto 500 mobile

                            label1.Invoke((MethodInvoker)delegate
                            {
                                label1.Text = "Service em andamento aguarde...";
                            });

                            #region S3 foto 125 mobile

                            if (File.Exists(caminhoFotos + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ".jpg"))
                            {

                                FileStream imageData = File.Open((caminhoFotos + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ".jpg"), FileMode.Open);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = accessKey,
                                    S3Secret = secretAccessKey,
                                    S3Bucket = bucketName,
                                    S3Region = s3Region,
                                    S3Path = "fotos/" + foto.produtoId + "/125_" + foto.produtoFoto + ".jpg"
                                };

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactando imagem 125";
                                });

                                var response = k.Upload(kr, (caminhoFotos + "fotos\\" + foto.produtoId + "\\125_" + foto.produtoFoto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFotos + "fotos\\" + foto.produtoId + "\\" + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));
                                }

                                label3.Invoke((MethodInvoker)delegate
                                {
                                    label3.Text = "Compactada imagem 125";
                                });
                            }

                            #endregion S3 foto 125 mobile
                        }
                        catch (Exception ex)
                        {
                            rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "fotos do produto: " + foto.produtoId + " - fotoId: " + produtoFotoId + "<br>erro message: " + ex.ToString() + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha compressão imagem - Kraken");
                        }

                        label3.Invoke((MethodInvoker)delegate
                        {
                            label3.Text = "Fim Processo";
                        });

                        label5.Invoke((MethodInvoker)delegate
                        {
                            label5.Text = "Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha CompactarFotoKraken() - Kraken");
            }

        }

        #endregion Compactar Foto  - Kraken



        private void ProcessarQueue(long idQueue)
        {
            #region atualizaQueue
            var data = new dbSiteDataContext();
            var queue = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SubmitChanges();
            #endregion atualizaQueue

            try
            {

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "Compactando imagem no kraken produtoFotoId: " + queue.idRelacionado;
                });

                int produtoFotoId = Convert.ToInt32(queue.idRelacionado);
                CompactarFotoKraken(produtoFotoId);

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "ProdutoFotoId: " + queue.idRelacionado + " compactada";
                });
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha finalização bwCompactarFotoKraken_DoWork - Kraken");
            }

            #region finalizaQueue
            var queueFinalizar = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            data.SubmitChanges();
            #endregion
        }



        private void bwCompactarFotoKraken_DoWork(object sender, DoWorkEventArgs e)
        {
            #region atualizaQueue
            var data = new dbSiteDataContext();
            int idQueue = Convert.ToInt32(e.Argument);
            var queue = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queue.andamento = true;
            queue.dataInicio = DateTime.Now;
            data.SubmitChanges();
            #endregion atualizaQueue

            try
            {

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "Compactando imagem no kraken produtoFotoId: " + queue.idRelacionado;
                });

                int produtoFotoId = Convert.ToInt32(queue.idRelacionado);
                CompactarFotoKraken(produtoFotoId);

                label2.Invoke((MethodInvoker)delegate
                {
                    label2.Text = "ProdutoFotoId: " + queue.idRelacionado + " compactada";
                });
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha finalização bwCompactarFotoKraken_DoWork - Kraken");
            }

            #region finalizaQueue
            var queueFinalizar = (from c in data.tbQueues where c.idQueue == idQueue select c).First();
            queueFinalizar.concluido = true;
            queueFinalizar.dataConclusao = DateTime.Now;
            data.SubmitChanges();
            #endregion
        }

        private void bwCompactarFotoKraken_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (!timerQueue.Enabled)
                {
                    //timerQueue.Interval = 10000;
                    timerQueue.Enabled = true;
                }

                label1.Text = "Aguardando Checagem de Queue: " + (timerQueue.Interval / 1000) + "seg.";

                label5.Invoke((MethodInvoker)delegate
                {
                    label5.Text = "Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
                });

                if ((timerQueue.Interval / 1000) == 0)
                {
                    //timerQueue.Interval = 10000;
                    timerQueue.Enabled = true;

                    label5.Invoke((MethodInvoker)delegate
                    {
                        label5.Text = "*Ultima checagem: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " timer: " + timerQueue.Enabled;
                    });
                }
            }
            catch (Exception ex)
            {
                rnEmails.EnviaEmail("", "renato@bark.com.br", "", "", "", "<br>erro message: " + ex + "<br>InnerException: " + ex.InnerException + "<br>erro message: " + ex.Message + "<br>stackTrace: " + ex.StackTrace + "<br>Data: " + ex.Data, "Falha finalização bwCompactarFotoKraken_RunWorkerCompleted - Kraken");
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            bwCompactarBannerMetade.RunWorkerAsync();

            button4.Enabled = false;
        }

        private void bwCompactarBannerMetade_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {

                label10.Invoke((MethodInvoker)delegate
                {
                    label10.Text = "Compactando Banner Metade";
                });


                KrakenConfig _kConfig = new KrakenConfig();
                string key = _kConfig.APIKey;
                string secret = _kConfig.APISecret;

                Kraken k = new Kraken(key, secret);

                KrakenRequest kr = new KrakenRequest { Lossy = false };


                if (_kConfig.UseCallbacks)
                {
                    kr.CallbackUrl = _kConfig.CallbackURL;
                }
                else
                {
                    kr.Wait = true;
                }


                using (var data = new dbSiteDataContext())
                {

                    qtdFotosCompactadasBanner = (from c in data.tbBannerKrakens select c.Id).Count();

                    var fotos = (from f in data.tbBanners join tbkraken in data.tbBannerKrakens on f.Id equals tbkraken.bannerId into fotosJaCompacatadas where !fotosJaCompacatadas.Any() select f).Take(10); // where !idFotoJaCompactadas.Contains(f.produtoFotoId) select f).ToList();

                    faltaCompactarBanner = fotos.Count();

                    foreach (var foto in fotos)
                    {

                        label12.Invoke((MethodInvoker)delegate
                        {
                            label12.Text = "BannerId: " + foto.Id + " FotoId: " + foto.foto;
                        });

                        label11.Invoke((MethodInvoker)delegate
                        {
                            label11.Text = qtdFotosCompactadasBanner.ToString() + " Fotos banner metade Compactadas";
                        });

                        if (File.Exists(caminhoFisico + (foto.local == 1 ? "banners\\home\\" : "banners\\") + foto.foto))
                        {

                            #region Formato Metade

                            int width = 0;
                            int heigth = 0;

                            System.Drawing.Image img = System.Drawing.Image.FromFile(caminhoFisico + (foto.local == 1 ? "banners\\home\\" : "banners\\") + foto.foto);

                            width = img.Width / 2;
                            heigth = img.Height / 2;
                            img.Dispose();

                            #endregion Formato Metade

                            #region Foto banner tamanho Metade

                            if (File.Exists(caminhoFisico + (foto.local == 1 ? "banners\\home\\" : "banners\\") + foto.foto))
                            {

                                FileStream imageData = File.Open((caminhoFisico + (foto.local == 1 ? "banners\\home\\" : "banners\\") + foto.foto), FileMode.Open);// albumProvider.Download(image);

                                // Can't trust the length of Stream. Converting to a MemoryStream
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    imageData.CopyTo(ms);
                                    kr.File = ms.ToArray();
                                }

                                kr.s3_store = new KrakenRequestS3Amazon
                                {
                                    S3Key = "AKIAIP7IE6WVNK2K6TIQ",
                                    S3Secret = "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",
                                    S3Bucket = "cdn2.graodegente.com.br",
                                    S3Region = "sa-east-1",
                                    S3Path = (foto.local == 1 ? "banners/home/m" : "banners/m") + foto.foto
                                };

                                kr.Resize = new KrakenResizeImage { Width = width, Height = heigth, Strategy = "auto" };

                                var response = k.Upload(kr, (caminhoFisico + (foto.local == 1 ? "banners\\home\\m" : "banners\\m") + foto.foto + ""), ".jpg");

                                if (response.Success == false || response.Error != null)
                                {

                                }

                                using (var webClient = new System.Net.WebClient())
                                using (var stream = webClient.OpenRead(response.KrakedUrl))
                                {

                                    imageData.Dispose();

                                    webClient.DownloadFile(response.KrakedUrl, caminhoFisico + (foto.local == 1 ? "banners\\home\\" : "banners\\") + response.KrakedUrl.Substring(response.KrakedUrl.LastIndexOf('/') + 1));

                                    var controleFoto = new tbBannerKraken { bannerId = foto.Id, bannerFoto = foto.foto };

                                    data.tbBannerKrakens.InsertOnSubmit(controleFoto);
                                    data.SubmitChanges();

                                    qtdFotosCompactadasBanner++;

                                    label10.Invoke((MethodInvoker)delegate
                                    {
                                        label10.Text = qtdFotosCompactadasBanner.ToString() + " Fotos banner metade Compactadas";
                                    });
                                }
                            }
                            #endregion Foto banner tamanho Metade
                        }
                        else
                        {
                            var controleFoto = new tbBannerKraken { bannerId = foto.Id, bannerFoto = foto.foto };

                            data.tbBannerKrakens.InsertOnSubmit(controleFoto);
                            data.SubmitChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                label12.Invoke((MethodInvoker)delegate
                {
                    label12.Text = ex.ToString();
                });
            }
        }

        private void bwCompactarBannerMetade_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            button4.Enabled = true;

            if (faltaCompactarBanner == 0)
            {
                label12.Invoke((MethodInvoker)delegate
                {
                    label12.Text = "Fim do Processo Fotos banner compactadas";
                });
            }
            else
            {

                label10.Invoke((MethodInvoker)delegate
                {
                    label10.Text = "Faltam " + faltaCompactar.ToString() + " fotos.";
                });

                //Thread.Sleep(10000);

                bwCompactarBannerMetade.RunWorkerAsync();

                button4.Enabled = false;
            }
        }
    }
}
