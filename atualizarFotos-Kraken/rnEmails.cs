﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Cryptography;
using System.Text;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using AmazonSES;
using Attachment = System.Net.Mail.Attachment;


public static class rnEmails
{
    public static bool EnviaEmail(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
    {
        string remetente = "";
        if (de != "")
        {
            remetente = de;
        }
        else
        {
            remetente = "atendimento@graodegente.com.br";
        }


        var destinatarios = new List<string>();
        if (para == "")
        {
            destinatarios.Add("atendimento@graodegente.com.br");
        }
        else
        {
            var emailsPara = para.Split(',');
            foreach (var emailPara in emailsPara)
            {
                destinatarios.Add(emailPara);
            }
        }


        var copias = new List<string>();
        if (cc != "")
        {
            var emailsCC = cc.Split(',');
            foreach (var emailCC in emailsCC)
            {
                copias.Add(emailCC);
            }
        }


        var copiasocultas = new List<string>();
        if (cco != "")
        {
            var emailsCCO = cco.Split(',');
            foreach (var emailCCO in emailsCCO)
            {
                copiasocultas.Add(emailCCO);
            }
        }

        string replyTo = responder == "" ? "atendimento@graodegente.com.br" : responder;

        //return false;
        var amazonMail = new AmazonSESWrapper("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/");
        var retorno = amazonMail.SendEmail(destinatarios, copias, copiasocultas, remetente, replyTo, assunto, mensagem);
        return !retorno.HasError;
    }

    //public static bool EnviaEmail(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
    //{
    //    var apiEmail = new bcApiEmails.IemailsClient();
    //    return apiEmail.EnviaEmail(de, para, cc, cco, responder, mensagem, assunto);
    //    ;
    //    string remetente = "";
    //    if (de != "")
    //    {
    //        remetente = de;
    //    }
    //    else
    //    {
    //        remetente = rnConfiguracoes.fromAddress;
    //    }


    //    var destinatarios = "";
    //    if (para == "")
    //    {
    //        destinatarios = rnConfiguracoes.fromAddress;
    //    }
    //    else
    //    {
    //        destinatarios = para;
    //    }

    //    //string replyTo = responder == "" ? ConfigurationManager.AppSettings["fromAddress"] : responder;

    //    var message = new PostmarkMessage();
    //    message.From = remetente;
    //    if (rnConfiguracoes.ambienteTeste)
    //    {
    //        message.To = rnConfiguracoes.emailDebug;
    //    }
    //    else
    //    {
    //        message.To = destinatarios;
    //        if (!string.IsNullOrEmpty(cc)) message.Cc = cc;
    //        if (!string.IsNullOrEmpty(cco)) message.Bcc = cco;
    //    }
    //    message.Subject = assunto;
    //    message.HtmlBody = mensagem;
    //    message.TrackOpens = true;

    //    PostmarkClient client = new PostmarkClient("42a1276f-e120-4038-8f71-7b4606ae6424");
    //    PostmarkResponse response = client.SendMessage(message);
    //    return true;

    //    //var amazonMail = new AmazonSESWrapper("AKIAIP7IE6WVNK2K6TIQ", "9UkGi7cYKpBGFG60a/J0SWmziuu2rYNWLnoqVWi/");
    //    //var retorno = amazonMail.SendEmail(destinatarios, copias, copiasocultas, remetente, replyTo, assunto, mensagem);
    //    //return retorno.HasError;
    //}

    public static bool EnviaEmailAutenticado(string de, string para, string cc, string cco, string responder, string mensagem, string assunto)
    {
        MailMessage mailMessage = new MailMessage();
        if (de != "")
        {
            mailMessage.From = new MailAddress("posvendas@graodegente.com.br", de);
        }
        else
        {
            mailMessage.From = new MailAddress("posvendas@graodegente.com.br", "Grão de Gente");
        }

        if (false)
        {
            mailMessage.To.Add("andre@bark.com.br");
        }
        else
        {

            if (para == "")
            {
                mailMessage.To.Add("posvendas@graodegente.com.br");
            }
            else
            {
                var emailsPara = para.Split(',');
                foreach (var emailPara in emailsPara)
                {
                    mailMessage.To.Add(new MailAddress(emailPara));
                }
            }

            if (cc != "")
            {
                var emailsCC = cc.Split(',');
                foreach (var emailCC in emailsCC)
                {
                    mailMessage.CC.Add(new MailAddress(emailCC));
                }
            }

            if (cco != "")
            {
                var emailsCCO = cco.Split(',');
                foreach (var emailCCO in emailsCCO)
                {
                    mailMessage.Bcc.Add(new MailAddress(emailCCO));
                }
            }

            if (responder != "")
            {
                try
                {
                    mailMessage.ReplyTo = new MailAddress(responder, de);
                }
                catch
                {
                }
            }
        }
        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient("smtp.gmail.com");
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential("posvendas@graodegente.com.br", "26003299");

        // Send Mail via SmtpClient
        //return true;
        smtpClient.Send(mailMessage);

        return true;
    }

    public static bool EnviaEmailComAnexo(string de, string para, string responder, string mensagem, string assunto, string anexo, string nomeAnexo, string contentTypeName)
    {

        MailMessage mailMessage = new MailMessage();
        mailMessage.From = new MailAddress(de);
        var emailsPara = para.Split(',');
        if (false)
        {
            mailMessage.To.Add("andre@bark.com.br");
        }
        else
        {
            foreach (var emailPara in emailsPara)
            {
                mailMessage.To.Add(new MailAddress(emailPara));
            }
        }
        mailMessage.ReplyTo = new MailAddress(responder);
        mailMessage.Subject = assunto;
        mailMessage.Body = mensagem;
        mailMessage.IsBodyHtml = true;
        mailMessage.Priority = MailPriority.Normal;

        var smtpClient = new SmtpClient("smtp.gmail.com");
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new System.Net.NetworkCredential("posvendas@graodegente.com.br", "26003299");

        using (MemoryStream memoryStream = new MemoryStream())
        {
            byte[] bom = { 0xEF, 0xBB, 0xBF };
            memoryStream.Write(bom, 0, bom.Length);

            byte[] contentAsBytes = Encoding.UTF8.GetBytes(anexo);
            memoryStream.Write(contentAsBytes, 0, contentAsBytes.Length);

            // Set the position to the beginning of the stream.
            memoryStream.Seek(0, SeekOrigin.Begin);

            // Create attachment
            ContentType contentType = new ContentType();
            contentType.MediaType = contentTypeName;
            contentType.Name = nomeAnexo;
            contentType.CharSet = "UTF-8";
            Attachment attachment = new Attachment(memoryStream, contentType);

            // Add the attachment
            mailMessage.Attachments.Add(attachment);

            //return true;
            // Send Mail via SmtpClient
            smtpClient.Send(mailMessage);
        }
        return true;
    }

    #region Envio de Emails com Anexos - Amazon

    public static Stream GenerateStreamFromString(string s)
    {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
    #endregion

    public static string carregarTemplateArquivo(string pathArquivo)
    {
        try
        {
            if (!File.Exists(pathArquivo)) throw new Exception(String.Format("Arquivo '{0}' não encontrado!", pathArquivo));

            return File.ReadAllText(pathArquivo, Encoding.GetEncoding("ISO-8859-1"));
        }
        catch (Exception ex)
        {
            return "";
        }
    }


    public static string Encrypt(string plainText)
    {
        string EncryptionKey = "GraoDeGe";
        byte[] clearBytes = Encoding.Unicode.GetBytes(plainText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                plainText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return plainText;
    }
}

