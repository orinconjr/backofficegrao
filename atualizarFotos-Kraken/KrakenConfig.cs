﻿using System;
using System.Configuration;
using System.Linq;


//[ObjectInfo(Title = "KrakenAPI", Description = "Configuration for Kraken.io")]
public class KrakenConfig
{
    //[ObjectInfo(Title = "API Key", Description = "API Key for Kraken.io")]
    //[ConfigurationProperty("APIKey", DefaultValue = "")]
    public string APIKey = "8871083b5e9bb4024d266584dd5be436";

    //[ObjectInfo(Title = "API Secret", Description = "API Secret for Kraken.io")]
    //[ConfigurationProperty("APISecret", DefaultValue = "")]
    public string APISecret = "f3a8880dc3962c10c5f28c03d86cdd02ce5c25ae";

    //[ObjectInfo(Title = "Use callbacks", Description = "Use callbacks instead of waiting for the image to be processed and returned in a single request. When checked, the task progress displayed refers to 'items sent for processing' instead of 'items processed'")]
    //[ConfigurationProperty("UseCallbacks", DefaultValue = false)]
    public bool UseCallbacks = false;

    //[ObjectInfo(Title = "Callback URL", Description = "URL where the Optimization controller can be accessed. This is only used if 'Use callbacks' is checked")]
    //[ConfigurationProperty("CallbackURL", DefaultValue = "http://www.yoursite.com/api/Optimization")]
    public string CallbackURL { get; set; }

    //[ObjectInfo(Title = "Lossy optimization", Description = "Use lossy optimization to save up to 90% of the initial file weight")]
    //[ConfigurationProperty("LossyOptimization", DefaultValue = false)]
    public bool LossyOptimization { get; set; }
}
