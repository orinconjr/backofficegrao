﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.Common;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;

/// <summary>
/// Summary description for rnFotos
/// </summary>
public class rnFotos
{

    #region gera thumbnail
    public static void ResizeImageFile(int Size, string Location, string Target, ImageFormat Format)
    {
        //string PhotoPath = Server.MapPath("~/http://www.jozan.com.br/images/");
        Bitmap fullSizeImg = new Bitmap(Location);
        int width = fullSizeImg.Width;
        int height = fullSizeImg.Height;
        int x = 0;
        int y = 0;

        //Determine dimensions of resized version of the image 
        if (width > height)
        {
            width = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)width / (decimal)height)), 0);

            height = Size;
            // moves cursor so that crop is more centered 
            x = Convert.ToInt32(Math.Ceiling(((decimal)(width - height) / 2M)));
        }
        else if (height > width)
        {
            height = (int)Decimal.Round(Convert.ToDecimal((decimal)Size * ((decimal)height / (decimal)width)), 0);
            width = Size;
            // moves cursor so that crop is more centered 
            y = Convert.ToInt32(Math.Ceiling(((decimal)(height - width) / 2M)));
        }
        else
        {
            width = Size;
            height = Size;
        }

        //// required in case thumbnail creation fails?
        //System.Drawing.Image.GetThumbnailImageAbort dummyCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);

        EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, 100L);
        ImageCodecInfo codecInfo = GetEncoderInfo("image/jpeg");
        EncoderParameters encoderParams = new EncoderParameters(1);
        encoderParams.Param[0] = qualityParam;

        //First Resize the Existing Image 
        System.Drawing.Image thumbNailImg;
        //thumbNailImg = fullSizeImg.GetThumbnailImage(width, height, null, System.IntPtr.Zero);
        thumbNailImg = resizeImage(fullSizeImg, new System.Drawing.Size(width, height));
        //Clean up / Dispose... 
        fullSizeImg.Dispose();

        //Create a Crop Frame to apply to the Resized Image 
        Bitmap myBitmapCropped = new Bitmap(Size, Size);
        Graphics myGraphic = Graphics.FromImage(myBitmapCropped);

        //Apply the Crop to the Resized Image 
        myGraphic.DrawImage(thumbNailImg, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
        //Clean up / Dispose... 
        myGraphic.Dispose();

        //Save the Croped and Resized image as a new square thumnail 
        myBitmapCropped.Save(Target, codecInfo, encoderParams);

        //Clean up / Dispose... 
        thumbNailImg.Dispose();
        myBitmapCropped.Dispose();
    }
    private static System.Drawing.Image resizeImage(System.Drawing.Image imgToResize, Size size)
    {
        int sourceWidth = imgToResize.Width;
        int sourceHeight = imgToResize.Height;

        float nPercent = 0;
        float nPercentW = 0;
        float nPercentH = 0;

        nPercentW = ((float)size.Width / (float)sourceWidth);
        nPercentH = ((float)size.Height / (float)sourceHeight);

        if (nPercentH < nPercentW)
            nPercent = nPercentH;
        else
            nPercent = nPercentW;

        int destWidth = (int)size.Width;
        int destHeight = (int)size.Height;

        Bitmap b = new Bitmap(destWidth, destHeight);
        Graphics g = Graphics.FromImage((System.Drawing.Image)b);
        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

        g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
        g.Dispose();

        return (System.Drawing.Image)b;
    }
    public static ImageCodecInfo GetEncoderInfo(string mimeType)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

        for (int i = 0; i < codecs.Length; i++)
        {
            if (codecs[i].MimeType == mimeType)
            {
                return codecs[i];
            }
        }

        return null;
    }
    #endregion


}
